function diff_robust = tbxdti_acid_dti_dki(dummy_DKIfiles, in_vols, diff_dirs, b_vals, RMatrix, brain_msk, dummy_Tfreiburg, Dthr, dummy_nlfit, dummy_dki_option)

% Created at 2020-10-12


%% defining tensor estimation
if(dummy_DKIfiles)
    % ---------------------------------------------------------------------
    % dummy_DKI
    % ---------------------------------------------------------------------
    
    dummy_DKI   = cfg_menu;
    dummy_DKI.tag     = 'dummy_DKI';
    dummy_DKI.name    = 'Estimate Kurtosis tensor';
    dummy_DKI.help    = {'If the DKI option is on, the output artguments will additionally include:'
        '- mean  (MK), perpendicular (Kper), parralel (Kpar) kurtosis, and axonal water fraction (AWF)'
        'The constrained ols Kurtosis fitting model has been used in Mohammadi et al., Front. Neurosci., 2015 and is based on the method presented in Tabesh et al., MRM, 2011.'
        'Please cite Mohammadi et al., Front. Neurosci., 2015 and Tabesh et al., MRM, 2011 when using the Kurtosis code.'};
    dummy_DKI.labels = {
        'NO'
        'YES'
        }';
   % dummy_DKI.values = {0 1};
      
    if(dummy_dki_option)
        dummy_DKI.labels = {
        'YES'
        }';
    else
        dummy_DKI.labels = {
        'NO'
        }';
    end
   
   


    % ---------------------------------------------------------------------
    % Dummy variable for writing the W maps in standard DKI 
    % ---------------------------------------------------------------------
%     dummy_write_W         = cfg_menu;
%     dummy_write_W.tag     = 'dummy_write_W';
%     dummy_write_W.name    = 'Write additional kurtosis variables';
%     dummy_write_W.help    = {'There is a difference between the kurtosis parameters denoted with K (=apparent kurtosis) and those denoted with W(=kurtosis tensor) (Check doi:10.1016/j.neuroimage.2016.08.022 for more details on W and K).'
%         'The default kurtosis output variables for DKI are the "K" parameters which refer to the parallel, perpendicular and mean kurtosis.'
%         'If you would also like to estimate the "W" parameters, choose "YES".'};
%     dummy_write_W.labels = {
%                    'NO'
%                    'YES'
%     }';
%     dummy_write_W.values = {0 1};
%     dummy_write_W.val    = {0};

    % ---------------------------------------------------------------------
    %
    % ---------------------------------------------------------------------

    % ---------------------------------------------------------------------
    % Dummy variable for writing all eigenvectors and eigenvalues and
    % kurtosis variables
    % ---------------------------------------------------------------------
    dummy_DT         = cfg_menu;
    dummy_DT.tag     = 'dummy_DT';
    dummy_DT.name    = 'Write additional parameters';
    dummy_DT.help    = {'There is a difference between the kurtosis parameters denoted with K (=apparent kurtosis) and those denoted with W (=kurtosis tensor) (Check doi:10.1016/j.neuroimage.2016.08.022 for more details on W and K).'
         'The default kurtosis output variables for DKI are the "K" parameters which refer to the parallel, perpendicular and mean kurtosis, while the default kurtosis output parameters for Axial Symmetric DKI are "W".'
        'If you would also like to estimate the "W" parameters in DKI or the "K" parameters in Axial Symmetric DKI, choose "YES". Note: There is no mean kurtosis in Axial Symmetric DKI.'
        'Also, choose YES to compute all three eigenvectors and all three eigenvalues, WARNING: currently this takes ~30mins for a dataset with dimensions 140x146x100. '
        'If you choose YES, e.g., the first eigenvector is (x1,y2,z1). If you choose NO, only the first eigenvector will be computed and written in the format (x,y,z).'};
    dummy_DT.labels = {
                   'YES'
                   'NO'
    }';
    dummy_DT.values = {0 1};
    dummy_DT.val    = {1};

    % ---------------------------------------------------------------------
    %
    % ---------------------------------------------------------------------

      
    
    
    
    
    
    if(dummy_dki_option)
        dummy_DKI.values = {1};
    else
        dummy_DKI.values = {0};
    end
    
    
    if(dummy_dki_option)
        dummy_DKI.val = {1};
    else
        dummy_DKI.val = {0};
    end
    
    % ---------------------------------------------------------------------
    % parallel_prog
    % ---------------------------------------------------------------------
    parallel_prog   = cfg_entry;
    parallel_prog.tag     = 'parallel_prog';
    parallel_prog.name    = 'Parallel programming';
    parallel_prog.help    = {'Note this option requires the parallel programming toolbox. Specify number of cores for parallel programming. No indicates that no parallel programming is used.'};
    parallel_prog.strtype = 'e';
    parallel_prog.num     = [1 1];
    parallel_prog.val     = {1};
    
end

% ---------------------------------------------------------------------
% dummy_BMSK
% ---------------------------------------------------------------------
dummy_BMSK   = cfg_menu;
dummy_BMSK.tag     = 'dummy_BMSK';
dummy_BMSK.name    = 'Write brain mask';
dummy_BMSK.help    = {'...'};
dummy_BMSK.labels = {
    'NO'
    'YES'
    }';
dummy_BMSK.values = {0 1};
dummy_BMSK.val    = {0};

% ---------------------------------------------------------------------
% dummy_robust
% ---------------------------------------------------------------------
dummy_robust   = cfg_menu;
dummy_robust.tag     = 'dummy_robust';
dummy_robust.name    = 'Fitting algorithm';
dummy_robust.help    = {'Choose tensor fitting method.'
    'Note that robust fitting need more time. Therefore we recommend using it together with a brain mask to reduce the number of voxels investigated and speed up the process (for an example batch see wiki page: https://bitbucket.org/siawoosh/acid-artefact-correction-in-diffusion-mri/wiki/TensorFitting%20Wiki).'
    'For high-resolution and/or HARDI data it is recommended to have more than 10 GB memory.'};

if(dummy_nlfit)
    dummy_robust.labels = {
        'Ordinary least squares'
        'Weighted least squares'
        'Robust tensor fitting'
        'nonlinear tensor fitting'
        }';
    dummy_robust.values = {0 1 2 4};
else
    dummy_robust.labels = {
        'Ordinary least squares'
        'Weighted least squares'
        'Robust tensor fitting'
        }';
    dummy_robust.values = {0 1 2};
end
dummy_robust.val    = {0};


% -----------robust fitting-------------------------------------------------
diff_robust         = cfg_exbranch;
diff_robust.tag     = 'diff_robust';


if(dummy_DKIfiles)
    diff_robust.val     = {in_vols diff_dirs b_vals RMatrix brain_msk dummy_robust dummy_BMSK parallel_prog dummy_DKI dummy_DT};
else
    diff_robust.val     = {in_vols diff_dirs b_vals RMatrix brain_msk dummy_robust};
end


diff_robust.help    = { 'Tensor estimation algorithm uses either ordinary least squares or robust tensor fitting.'
    'If the DKI option is of, the output artguments are:'
    '- various diffusion tensor indices: axial diffusivity (Axial_), radial diffusivity (Radial_), mean diffusitivty (MD_), axial - radial diffusivity (AD_RD_), fractional anisotropy (FA_)'
    '- other diffusion tensor estimates: root-mean square of tensor-fit error (RES_), estimate of b=0 image (b0_), mean of diffusion weigted images (meanDWI_), eigenvalues (EVAL_), eigenvectors (EVEC_)'
    'Changeable defaults: '
    '- smk: This option reduces holes in the brain mask by smoothing the mask or the tissue probability maps. If it is set to zero no smoothing is used.'
    '- cval: Confidence interval for robust fitting.'
    '- dthr: Threshold for minimal diffusivity'
    '- kmax: This variable determines the smoothing that is applied on the the residuals; smaller maxk results in more smoothing.'
    '- sigma0: Standard deviation of logarithm of the signal outside the brain. This measure is used if the noise cannot be estimated from outside the brain (see brain mask option).'
    '-dummyDT: Determines whether to write out all eigenvectors of the diffusion tensor, i.e. 1st, 2nd, and 3rd eigenvector and eigenvalues.The filename will be extended by the number of the eigenvector componenten and the eigenvector number, i.e. "filename-ij.img" with "i" being the eigenvector componenten and "j" being the eigenvector number.'
    'The robust fitting model has been used in Mohammadi et al., MRM, 2012 and is a modified version of the method presented in  Zwiers et al., Neuroimage 2010 and Mangin et al., Med Image Anal 2002.'
    'Please cite Mohammadi et al., MRM 2012 (doi: 10.1002/mrm.24467) and Zwiers et al., Neuroimage 2010 when using this code.'
    'If the DKI option is on, the output artguments will additionally include:'
    '- mean  (MK), perpendicular (Kper), parralel (Kpar) kurtosis, and axonal water fraction (AWF)'
    'The Kurtosis fitting model has been used in Mohammadi et al., Front. Neurosci., 2015 and is based on the method presented in  Tabesh et al., MRM, 2011.'
    'Please cite Mohammadi et al., Front. Neurosci., 2015 and Tabesh et al., MRM, 2011 when using the Kurtosis code.'
    };
diff_robust.prog = @local_diff_fit;

if(dummy_dki_option)
    diff_robust.vout = @vout_dki_fit;
else
    diff_robust.vout = @vout_dti_fit;
end

%diff_robust.vout = @vout_diff_fit;



%----Diff Fit-------------------
function out = local_diff_fit(job)

dummy_DTIsm = acid_get_defaults('diffusion.dummy_DTIsm');

dummy_weights = acid_get_defaults('diffusion.dummy_weights');

dummy_plot = acid_get_defaults('diffusion.dummy_plot');



thr_cond  = acid_get_defaults('diffusion.thr_cond');
thr_DTvar = acid_get_defaults('diffusion.thr_DTvar');
% dummy_DT  = acid_get_defaults('diffusion.dummyDT');
kmax      = acid_get_defaults('diffusion.kmax');
Cval      = acid_get_defaults('diffusion.cval');
sigma0    = acid_get_defaults('diffusion.sigma0');
Dthr      = acid_get_defaults('diffusion.dthr');
smk       = acid_get_defaults('diffusion.smk');
dummy_Tfreiburg = 0;
Niter     = acid_get_defaults('diffusion.Niter');



if(isfield(job,'kernelvox'))
    if(isfield(job,'dummy_DKI'))
        acid_dki_main(char(job.in_vols),char(job.brain_msk),job.diff_dirs,job.b_vals,job.dummy_robust,dummy_DTIsm,Cval,kmax,job.dummy_DT,job.dummy_Tfreiburg, thr_DTvar, sigma0, job.dummy_BMSK,job.kernelvox, Niter,job.dummy_DKI,thr_cond,job.Dthr,job.RMatrix,job.parallel_prog); % thr_cond
    else
        Vweights = acid_dti_main(char(job.in_vols),char(job.brain_msk),job.diff_dirs,job.b_vals,job.dummy_robust,dummy_DTIsm,Cval,kmax,job.dummy_DT,job.dummy_Tfreiburg, thr_DTvar, sigma0, job.dummy_BMSK,job.kernelvox, Niter,0,'','',job.RMatrix,smk);
    end
else
    if(isfield(job,'dummy_DKI'))
        if(job.dummy_DKI==1)
            acid_dki_main(char(job.in_vols),char(job.brain_msk),job.diff_dirs,job.b_vals,job.dummy_robust,dummy_DTIsm,Cval,kmax,job.dummy_DT,dummy_Tfreiburg, thr_DTvar, sigma0, job.dummy_BMSK,'',Niter,job.dummy_DKI,thr_cond,Dthr,job.RMatrix,job.parallel_prog);
            Vweights = [];
        else
            Vweights =  acid_dti_main(char(job.in_vols),char(job.brain_msk),job.diff_dirs,job.b_vals,job.dummy_robust,dummy_DTIsm,Cval,kmax,job.dummy_DT,dummy_Tfreiburg, thr_DTvar, sigma0, dummy_weights,dummy_plot,'',Niter,0,'','',job.RMatrix,smk);
            
        end
    else
        Vweights =       acid_dti_main(char(job.in_vols),char(job.brain_msk),job.diff_dirs,job.b_vals,job.dummy_robust,dummy_DTIsm,Cval,kmax,job.dummy_DT,dummy_Tfreiburg, thr_DTvar, sigma0, dummy_weights,dummy_plot,'',Niter,0,'','',job.RMatrix,smk);
    end
end
if(job.dummy_robust == 0)
    midfix = 'ols_';
elseif job.dummy_robust == 1
    midfix = 'wols_';
elseif job.dummy_robust == 2
    midfix = 'robust_';
elseif job.dummy_robust == 3
    midfix = 'adaptive_';
end

out.files       = job.in_vols(:);
out.FAfiles     = my_spm_file(job.in_vols{1},'prefix',['FA_' midfix],'format','.nii');
out.MDfiles     = my_spm_file(job.in_vols{1},'prefix',['MD_' midfix],'format','.nii');
out.MKfiles     = my_spm_file(job.in_vols{1},'prefix',['MK_' midfix],'format','.nii');
out.ADfiles     = my_spm_file(job.in_vols{1},'prefix',['Dpara_' midfix],'format','.nii');
out.RDfiles     = my_spm_file(job.in_vols{1},'prefix',['Dperp_' midfix],'format','.nii');
out.RESfiles    = my_spm_file(job.in_vols{1},'prefix',['RES_' midfix],'format','.nii');
% out.HARDIfiles  = my_spm_file(job.in_vols{1},'prefix',midfix,'ending','_HARDI','format','.mat');
out.DWIfiles    = my_spm_file(job.in_vols{1},'prefix','meanDWI_','format','.nii');
out.b0files     = my_spm_file(job.in_vols{1},'prefix',['b0_' midfix],'format','.nii');
out.Kperpfiles  = my_spm_file(job.in_vols{1},'prefix',['Kperp_' midfix],'format','.nii');
out.Kparafiles  = my_spm_file(job.in_vols{1},'prefix',['Kpara_' midfix],'format','.nii');

if job.dummy_DT == 0
    out.Wperpfiles     = my_spm_file(job.in_vols{1},'prefix',['Wperp_' midfix],'format','.nii');
    out.Wparafiles     = my_spm_file(job.in_vols{1},'prefix',['Wpara_' midfix],'format','.nii');
    out.Wmeanfiles     = my_spm_file(job.in_vols{1},'prefix',['Wmean_' midfix],'format','.nii');
end
if logical(~isempty(Vweights))
    out.wfiles  = {Vweights(1).fname};
    out.wfiles  = acid_my_select(out.wfiles(:),true);
    out.wfiles  = out.wfiles(:);
else
    out.wfiles     = [];
end

out.bval        = job.b_vals;
out.bvec        = job.diff_dirs;

%---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);
                if(nargin>=5)
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end
        else
            varout  = varargin{1};
        end
    end
end

%------------------------
function dep = vout_dti_fit(job)

dep(1)            = cfg_dep;
dep(1).sname      = 'Input images';
dep(1).src_output = substruct('.','files');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(2)            = cfg_dep;
dep(2).sname      = 'FA map';
dep(2).src_output = substruct('.','FAfiles');
dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(3)            = cfg_dep;
dep(3).sname      = 'MD map';
dep(3).src_output = substruct('.','MDfiles');
dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(4)            = cfg_dep;
dep(4).sname      = 'Dparallel map';
dep(4).src_output = substruct('.','ADfiles');
dep(4).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(5)            = cfg_dep;
dep(5).sname      = 'Dperp map';
dep(5).src_output = substruct('.','RDfiles');
dep(5).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(6)            = cfg_dep;
dep(6).sname      = 'b0 map';
dep(6).src_output = substruct('.','b0files');
dep(6).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(7)            = cfg_dep;
dep(7).sname      = 'RES map';
dep(7).src_output = substruct('.','RESfiles');
dep(7).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(8)            = cfg_dep;
dep(8).sname      = 'DWI map';
dep(8).src_output = substruct('.','DWIfiles');
dep(8).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(9)            = cfg_dep;
dep(9).sname      = 'Weight images';
dep(9).src_output = substruct('.','wfiles');
dep(9).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
% dep(10)            = cfg_dep;
% dep(10).sname      = 'HARDI-mat file from Freiburg Fibertools';
% dep(10).src_output = substruct('.','HARDIfiles');
% dep(10).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
dep(10)            = cfg_dep;
dep(10).sname      = 'b-values';
dep(10).src_output = substruct('.','bval');
dep(10).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
dep(11)            = cfg_dep;
dep(11).sname      = 'b-vectors';
dep(11).src_output = substruct('.','bvec');
dep(11).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});

function dep = vout_dki_fit(job)

dep(1)            = cfg_dep;
dep(1).sname      = 'Input images';
dep(1).src_output = substruct('.','files');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(2)            = cfg_dep;
dep(2).sname      = 'FA map';
dep(2).src_output = substruct('.','FAfiles');
dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(3)            = cfg_dep;
dep(3).sname      = 'MD map';
dep(3).src_output = substruct('.','MDfiles');
dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(4)            = cfg_dep;
dep(4).sname      = 'Dpara map';
dep(4).src_output = substruct('.','ADfiles');
dep(4).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(5)            = cfg_dep;
dep(5).sname      = 'Dperp map';
dep(5).src_output = substruct('.','RDfiles');
dep(5).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(6)            = cfg_dep;
dep(6).sname      = 'b0 map';
dep(6).src_output = substruct('.','b0files');
dep(6).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(7)            = cfg_dep;
dep(7).sname      = 'RES map';
dep(7).src_output = substruct('.','RESfiles');
dep(7).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(8)            = cfg_dep;
dep(8).sname      = 'Kparallel map';
dep(8).src_output = substruct('.','Kparfiles');
dep(8).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(9)            = cfg_dep;
dep(9).sname      = 'Kperp map';
dep(9).src_output = substruct('.','Kperpfiles');
dep(9).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(10)            = cfg_dep;
dep(10).sname      = 'MK map';
dep(10).src_output = substruct('.','MKfiles');
dep(10).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(11)            = cfg_dep;
dep(11).sname      = 'b-values';
dep(11).src_output = substruct('.','bval');
dep(11).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
dep(12)            = cfg_dep;
dep(12).sname      = 'b-vectors';
dep(12).src_output = substruct('.','bvec');
dep(12).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});

if job.dummy_DT ==0
    dep(13)            = cfg_dep;
    dep(13).sname      = 'Wmean map';
    dep(13).src_output = substruct('.','Wmeanfiles');
    dep(13).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(14)            = cfg_dep;
    dep(14).sname      = 'Wparallel map';
    dep(14).src_output = substruct('.','Wparafiles');
    dep(14).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(15)            = cfg_dep;
    dep(15).sname      = 'Wperp map';
    dep(15).src_output = substruct('.','Wperpfiles');
    dep(15).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end


% dep(12)            = cfg_dep;
% dep(12).sname      = 'AWF map';
% dep(12).src_output = substruct('.','AWFfiles');
% dep(12).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
% dep(13)            = cfg_dep;
% dep(13).sname      = 'lRadial map';
% dep(13).src_output = substruct('.','lRDfiles');
% dep(13).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
