function [resDT0,resDT0e] = acid_dti_compute_error_core(DT,MSKidx,V,VG,bvals,B,dm,p,dummy)
% ========================================================================
% This function calculates the residuals from the tensor.
% Two options are available for residual estimation:
% dummy == 0 		estimating it from the signal, 
%                   i.e. RES = rms(ln(Smeas)-ln(SModel)). This option is 
%                   more sensitive to instrumental effects, because 
%                   directly depends on the measured signal.
% dummy == 1        estimating it from the ADCs, 
%                   i.e. RES = rms(ADCmeas-ADCmodel) with 
%                   ADCmeas(bvalue) = 1/b x (ln(Smeas(bvalue))-ln(Smeas(min(bvalue)))). 
%                   This option is more sensitive to perturbations that 
%                   will have a direct influence on your model estimates, 
%                   because the baseline signal (ln(Smeas(min(bvalue)))) 
%                   is substracted taken from the measured signal.
%
% Input:
%   DT          - elements of the diffusion tensor for each voxel, 2d array [num_of_voxels x 7]
%   MSK         - 2D mask to exclude voxels with logS < 0, 2D array [num_of_voxels x 1]
%   V           - headers of the dMRI volumes
%   VG          - header of the reference volume
%   bvals       - b values of the dMRI volumes
%   B           - B matrix (design matrix for dMRI experiment)
%   dm          - matrix size
%   index_dw    - indices of the dw volumes
%   p           - slice index
%   dummy       - see above
% 
% S.Mohammadi 22.12.2013
% ========================================================================

% extracting a single slice from DT
% -> DT:    2D, num_of_voxels x 7
% -> DT_sl: 2D, num_of_voxels(slice) x 7
DT_sl = zeros([dm(1:2) size(DT,2)]);
for i = 1:size(DT,2)
    AMSKvol         = zeros(dm);
    AMSKvol(MSKidx) = DT(:,i);
    DT_sl(:,:,i) = AMSKvol(:,:,p);
end
DT_sl = reshape(DT_sl,[],size(DT_sl,3));

% calculating the logarithm of all volumes (including b0 volumes) for a specific slice.
%       V - struct array containing info on the selected images
%       bmin - minimum b value
%       bvalues - bval vector
%       p - index of slice
%       ADC - log(S) for all voxels and volumes in given slice; 2D, num_of_voxels(slice) x num_of_volumes
%       LS0 - log(S) for all voxels and b0 volumes in given slice; 2D, num_of_voxels(slice) x num_of_volumes
[ADC,lS0] = acid_dti_get_logS_2D(V,VG,bvals,min(bvals),p);

% initialization
inx = 1;
resDT0 = zeros(size(ADC));
resDT0e = zeros(size(ADC));

% calculating resdiuals of ln(Signal), i.e. ADC = ln(Signal)
if dummy == 0 

    % estimate modelled ln(signal)=ln(Smodel) 
    %   log(Smeas) = alpha*D + el;
    %   log(Smeas) = log(Smodel) + el; 
    %   log(Smodel) = alpha*D;
    % log(Smodel): ADCmodel; alpha: DM; D: Asym0p
    % ADCmodel: log(Smeas) for all voxels and volumes in given slice; 2D, num_of_voxels(slice) x num_of_volumes(dw)
    ADCmodel = (B*DT_sl')';
    
    % compute residual error (e)
    % el = ln(Smean)-ln(Smodel) -> resDT0;  2D, num_of_voxels(slice) x num_of_volumes(dw)
    % e = Smean - Smodel        -> resDT0e; 2D, num_of_voxels(slice) x num_of_volumes(dw)    
    for kj = 1:numel(V)
        resDT0(:,kj) = ADC(:,kj) - ADCmodel(:,kj);
        resDT0e(:,kj)= exp(ADC(:,kj)) - exp(ADCmodel(:,kj));
    end

% calculating resdiuals of ADC
elseif dummy == 1 
    
    % calculating lS0-lSWi from data
    ADC = bsxfun(@minus,lS0,ADC);
    % calculating ADC from lS0-lSWi
    ADC = bsxfun(@rdivide,ADC,bvals);

    % calculating lS0-lSWi from model
    lS0model = mean(B(bvals==min(bvals),:)*DT_sl',1);
    lS0model = permute(lS0model,[2 1]);
    lSmodel  = (B*DT_sl');
    lSmodel  = permute(lSmodel,[2 1]);
    ADCmodel = bsxfun(@minus,lS0model,lSmodel);
    
    % calculating ADC from lS0-lSWi
    ADCmodel = bsxfun(@rdivide,ADCmodel,bvals);
    for kj = 1:numel(V)
        resDT0(:,inx)  = ADC(:,kj) - ADCmodel(:,kj);
        resDT0e(:,inx) = exp(ADC(:,kj)) - exp(ADCmodel(:,kj));
        inx = inx+1;
    end
end

end