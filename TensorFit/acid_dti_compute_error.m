function acid_dti_compute_error(DT,oMSKidx,wMSKidx,V,VG,bvals,B,b,idx,dummy_write_error,dummy_plot_error,dummy_shellwise)

% ========================================================================
% The script computes and writes out the model-fit error, defined as the
% difference between the measured and fitted signal. Model-fit error
% quantifies the quality of fitting, and high model-fit error might
% indicate an artifactual data point.
%
% IMPORTANT: In the current implementation, the errors derived from the ols
% fit are written out (even if robust fitting was used).
%
% Input:
%   DT          - diffusion tensor for each voxel (within mask), 2D array [num_of_voxels x 7]
%   wAMSK       - mask in which model-fit error is plotted
%   MSKidx      - mask storing the linear indices of voxels to be included, 2d array [num_of_voxels_within_mask x 1]
%   V           - headers of the dMRI dataset
%   VG          - header of the reference volume
%   bvals       - b values of the dMRI dataset, 2D array [1 x num_of_volumes]
%   B           - B matrix (design matrix for dMRI experiment)
%   b           - b values of each shell
%   index       - binary array indicating the volume indices for each shell
%   midfix      - midfix for output file name
%   dummy_plot_error - option for plotting model-fit error
%   dummy_sepshells  - option for generating rms error maps for each shell separately
% ========================================================================

% get image dimensions
dm = VG.dim;

% get indices of dw volumes
idx_dw = [];
for k = 1:numel(idx)
    idx_dw = cat(2,idx_dw,idx{k});
end

% initialization
resErrorlog4d = zeros([dm numel(V)]);
resError4d = zeros([dm numel(V)]);
RMSresErrorlog3d = zeros(dm);
RMSresError3d  = zeros(dm);
    
for p = 1:dm(3)
        
    % calculate model-fit error
    % Note that model-fit error can be defined in two different ways: 
    % 1) abs(S_meas - S_fit)
    % 2) abs(log(S_meas)-log(S_fit))
    %  resErrorlog   - model-fit error of the logarithmic signal in a given slice for all volumes , abs(log(S_meas) - log(S_fit)), 2d array [num_of_voxels_within_slice x num_of_dw_vols]
    %  resError      - model-fit error of the signal in a given slice for all volumes, abs(S_meas - S_fit), 2d array [num_of_voxels_within_mask x num_of_dw_vols]
    [resErrorlog,resError] = acid_dti_compute_error_core(DT,oMSKidx,V,VG,bvals,B,dm,p,0);
        
    % store model-fit error in a 4D file
    resErrorlog4d(:,:,p,:) = reshape(resErrorlog,[dm(1:2) numel(V)]);
    resError4d(:,:,p,:)    = reshape(resError,[dm(1:2) numel(V)]);
        
    % rms model-fit error
    % residual of the exponential of ln(S)
    RMSresErrorlog = sqrt(mean(min(resErrorlog(:,idx_dw).*resErrorlog(:,idx_dw),1e10),2));
    RMSresError    = sqrt(mean(min(resError(:,idx_dw).*resError(:,idx_dw),1e10),2));
        
    % store rms model-fit error in a 3D file
    RMSresErrorlog3d(:,:,p) = reshape(RMSresErrorlog,dm(1:2));
    RMSresError3d(:,:,p)    = reshape(RMSresError,dm(1:2));
        
    % do the above for each shell separately
    if dummy_shellwise
        for k = 2:numel(idx)
            RMSresErrorlog_{k-1} = sqrt(mean(min(resErrorlog(:,idx{k}).*resErrorlog(:,idx{k}),1e10),2));
            RMSresError_{k-1}    = sqrt(mean(min(resError(:,idx{k}).*resError(:,idx{k}),1e10),2));
            RMSresErrorlog3d_{k-1}(:,:,p) = reshape(RMSresErrorlog_{k-1},dm(1:2));
            RMSresError3d_{k-1}(:,:,p)    = reshape(RMSresError_{k-1},dm(1:2));
        end
    end
end

% normalised max-norm of tensor fit error
% mresDT0 = max(abs(resDT0),[],2)./sqrt(mean(resDT0.^2,2));

% create diagnostic plots on model-fit error
if dummy_plot_error
    acid_dti_plot_error(resErrorlog4d,wMSKidx,VG);
end

% write out 4D model-fit error
if dummy_write_error
    prefix = 'error_log_';
    VG.dim = [VG.dim, size(resError,2)];
    VG.n   = [size(resError,2), 1];
    dm = VG.dim;
    oMSKidx4d = oMSKidx;
    for i = 2:size(resError,2)
        oMSKidx4d = cat(1,oMSKidx4d,oMSKidx + (i-1)*numel(resError4d(:,:,:,1)));
    end

    if(exist('ending','var'))
        acid_write_data(resErrorlog4d(oMSKidx4d),VG,prefix,dm,oMSKidx4d,ending); 
    else
        acid_write_data(resErrorlog4d(oMSKidx4d),VG,prefix,dm,oMSKidx4d);
    end
end

% write out rms model-fit error
prefix = 'rms_error_log_dw_';
VG.dim = VG.dim(1:3); VG.n = [1,1];
acid_write_data(RMSresErrorlog3d(oMSKidx),VG,prefix,dm(1:3),oMSKidx);
if dummy_shellwise
    for k = 2:numel(idx)
        switch numel(num2str(b(k)))
        case 1
            prefix = ['rms_error_log_b000' num2str(b(k)) '_'];
        case 2
            prefix = ['rms_error_log_b00' num2str(b(k)) '_'];
        case 3
            prefix = ['rms_error_log_b0' num2str(b(k)) '_'];
        case 4
            prefix = ['rms_error_log_b' num2str(b(k)) '_'];
        end
        VG.dim = VG.dim(1:3); VG.n = [1,1];
        acid_write_data(RMSresErrorlog3d_{k-1}(oMSKidx),VG,prefix,dm(1:3),oMSKidx);
    end
end    
end