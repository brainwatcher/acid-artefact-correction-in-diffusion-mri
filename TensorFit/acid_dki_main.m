function acid_dki_main(P,PMSK,bvecs,bvals,dummy_robust,dummy_DTIsm,C,maxk,dummy_DT,dummy_TFreiburg,thr_DTvar,sigma0,dummy_BMSK,kernelmm,Niter,dummy_DKI,thr_cond,Dthr,RM,parallel_prog)
% S.Mohammadi 11/10/2012
% Tensor estimation algorithm uses either ordinary least squares or robust
% fitting. This robust fitting model has been used in Mohammadi et al., 
% MRM, 2012 and is based on the method presented in 
% Zwiers et al., Neuroimage 2010 and
% Mangin et al., Med Image Anal 2002
% 
% Please cite Mohammadi et al., MRM 2012 (doi: 10.1002/mrm.24467) when using this algorithm.

% Inputs:
% P          - File names of DTI images (i.e. low and high b-value
%                 images). The order of the images must resample the order
%                 of the b-vector and b-values (see below).
% PMSK          - Region of interest, in which the tensor estimates are
%                 calculated. Could also be a brain mask. Is optional.  
% DD            - b-vectors, i.e. vector of diffusion gradients; 
%                 it should be a matrix of size 3 x number of diffusion 
%                 directions
% bvals       - b-value for each image, i.e. b-value for each diffusion 
%                 gradients; it should be a matrix of size 1 x number of diffusion 
%                 directions
% dummy_robust  - 0, 1, or 2, determines whether ordinar or weighted least 
%                 square or robust fitting should be used
% dummy_DTIsm   - 0 or 1, determines whether model estimate for DTI dataset 
%                 should be written out  
% C             - constant of usually 0.3, defines confidence intervall in 
%                 robust fitting algorithm (smaller C means less outliers 
%                 will be rejected, see Zwiers et al., 2010)
% maxk          - determines the smoothing that is applied on the the
%                 residuals; smaller maxk results in more smoothing 
% dummy_DT      - determines whether to write out all diffusion directions,
%                 i.e. 1st, 2nd, and 3rd eigenvector and eigenvalues
% dummy_TFreiburg - writes out the Freiburg Format to do tractography
% RM            - Matrix to reorient b-values


      
%% start timer
tic

%     bvals = bvals';
%     bvals = round(bvals/100)*100;
%     bvals = bvals';


%%
% check whether Freiburg-Tools are set
if (dummy_TFreiburg == 1)
    dummy_DT = 0;
end

% bf_new
[p,~,~] = fileparts(P(1,:));
diary([p filesep 'logfile_fit.txt'])

% predefine Vweights
Vweights = [];

% bf_new
if (~isempty(bvals) && ~isempty(bvecs)) 
    % load headers of input images
    V = spm_vol(P);

    % header of the first input image
    VG = V(1);
    
    % misc
    interp = -4;      % interpolation kernel
    dm = V(1).dim;    % image matrix size
    
    % check bval dimension
    if size(bvals,1) > 2
        error('Please transpose b values!')
    elseif size(bvals,2) ~= size(V,1)
        error('The number of source images and entries of the corresponding b-value vector has to be the same!');
    end

    % check b-vector dimension
    if size(bvecs,1) ~= 3
        error('Please transpose b vector!')
    elseif size(bvals,2) ~= size(bvecs,2)
        error('The length of b-values and b-vectors has to be the same!')
    end 
end
    %{
    bf_changed
    % check b-vector dimension
    if(size(DD,1)~=3)
        error('Please transpose b vector!')
    end

    
    bf_changed
    % check b-value dimension
    if(size(bvals,1)>2)
        error('Please transpose b values!')
    end


    % check whether b-values and b-vectors have same length
    if(size(bvals,2)~=size(DD,2))
        error('The length of b-values and b-vectors have to be the same!')
    end

    % check whether images have the same length as b-vector/bvals
    if(size(P,1)~=size(DD,2))
        error('The number of images have to be the same as the number of b-values/vectors!')
    end
    %}



    % check whether images have the same dimension as Mask
if(~isempty(PMSK))
    V1 = spm_vol(PMSK);
    V2 = spm_vol(P);
    if(~isequal(V1.dim,V2(1).dim))
        error('Dimension of Mask and DTIs must agree!')
    end
    clear V1 V2;
end
% reorient diffusion directions
bvecs_reoriented = RM * bvecs;


%bf_changed
% indices of the volumes of the different shells
b = sort(unique(bvals));
for k = 1:numel(b)
    idx{k} = find(bvals == b(k));
end

idx_b0 = idx{1}; % indices of the b0 shells
idx_dw = find(bvals ~= b(1)); % indices of the non-b0 shells



% normalise b-values between 0 and 1
% bvals     = bsxfun(@rdivide,bvals,max(bvals,[],2));
% bvals     = bvals*1e-3;
% no change on b-values, because it will influence the exponential decay




%% interpolate all images to the first
%get V and Aall0up

%bf_changed
%res_hold = -4; % interpolation kernel

%bf_changed
%{
%V       = spm_vol(P);
VG     = V(1);
bS0MSK = find(min(bvals(1,:))==bvals);
dm      = V(1).dim;
Atmp    = zeros([dm numel(bS0MSK)]);
kk      = 1;
for j=bS0MSK
    for p=1:dm(3)
        M = V(j).mat*spm_matrix([0 0 p]);
        Atmp(:,:,p,kk) = spm_slice_vol(V(j),V(bS0MSK(1)).mat\M,dm(1:2),res_hold);
    end
    kk=kk+1;
end
if(numel(size(Atmp))==4)
    S0     = mean(Atmp,4);
elseif(numel(size(Atmp))==3)
    S0     = Atmp;
else
    error('The dimensionality of your images is wrong.');
end
clear Atmp bS0MSK;
%}

%% calculate average S0
% match the header of all b0 volumes with the first b0 volume
S0tmp = zeros([dm numel(idx_b0)]);
kk = 1;
for j = idx_b0
    for p = 1:dm(3)
        M = V(j).mat*spm_matrix([0 0 p]);
        S0tmp(:,:,p,kk) = spm_slice_vol(V(j),V(idx_b0(1)).mat\M,dm(1:2),interp);
    end
    kk = kk+1;
end
% calculate average S0
if(numel(size(S0tmp))==4)
    S0 = mean(S0tmp,4);
elseif(numel(size(S0tmp))==3)
    S0 = S0tmp;
else
    error('The dimensionality of your images is wrong.');
end
clear S0tmp;

% clear voxels with negative log(Aall0)

if(~isempty(PMSK))
    VMSK    = spm_vol(PMSK);
    AMSKtmp = zeros([VG.dim size(VMSK,1)]);
    for inx = 1:size(VMSK,1)
        AMSKtmp(:,:,:,inx)        = acid_read_vols(VMSK(inx),VG,1);
    end
    if(numel(size(AMSKtmp))==3)
        MSK     = find(AMSKtmp>0 & S0>0);
        MSKseg  = '';
    elseif(size(AMSKtmp,4)==2)
        MSKGM     = find(AMSKtmp(:,:,:,1)>0 & S0>0);
        MSKWM     = find(AMSKtmp(:,:,:,2)>0 & S0>0);
        MSKseg    = struct('GM',MSKGM,'WM',MSKWM);
        MSK       = find((mean(AMSKtmp,4)>0 & S0>0));
    elseif(size(AMSKtmp,4)==3)
        MSKGM     = find(AMSKtmp(:,:,:,1)>0 & S0>0);
        MSKWM     = find(AMSKtmp(:,:,:,2)>0 & S0>0);
        MSKseg    = struct('GM',MSKGM,'WM',MSKWM);
        MSK       = find(AMSKtmp(:,:,:,3)>0 & S0>0);
    end
    AMSK = zeros(size(S0));
    AMSK(MSK) = 1;
else
    AMSK    = zeros(size(S0));
    tmp     = ones(size(S0));
    MSK1    = find(tmp>0 & S0>0);
    MSK     = MSK1;
    AMSK(MSK) = 1;
end

%% ---- olsq

% ordinary least squares
% ordinary least squares


if(dummy_DKI)
    % separate b0s form others
    minb    = min(bvals(1,:));
    MSK_b   = find(bvals(1,:)>minb);
    
    b0      = min(bvals(1,:));
    bMSK0   = find(bvals(1,:)==b0);
    b1      = min(bvals(1,bvals(1,:)>b0));
    bMSK1   = find(bvals(1,:)==b1);
    b2      = min(bvals(1,bvals(1,:)>b1));
    if(isempty(b2)),  error('Not enough b-values! Change to tensor estimation.');end
    bMSK2   = find(bvals(1,:)==b2);
    bMSK    = find(bvals(1,:)>b0); 
    [bC,bia,bic] = unique(bvals(1,bvals(1,:)>=b2));  
       
        % if DKI, check for 2 shells
    if dummy_DKI
        if isempty(b(2))
            error('Not enough b-values! Change to tensor estimation.');
        else
            idx_dw2 = find(bvals ~= b(2)); % indices of the non-b0 shells
        end
        if isempty(parallel_prog)
            parallel_prog = 0;
        end
    end
    
    if(numel(bC))
%         for iterb = 1:numel(bC)
        bMSK2   = find(bvals(1,:)>=b2);
        binfo   = struct('b0',b0,'bMSK0',bMSK0,'b1',b1,'bMSK1',bMSK1,'b2',bC,'bMSK2',bMSK2,'bMSK',bMSK,'bpos',bia,'binx',bic); 
        [DM,dummy_DKI,lSW,Asym,lS0]=make_Design_DWSignal_Tensor_nl_parallel(dummy_DKI,bvals(1,:),bvecs_reoriented,binfo,V,AMSK,S0,VG,thr_cond,0,parallel_prog);
%  end
    else
        binfo   = struct('b0',b0,'bMSK0',bMSK0,'b1',b1,'bMSK1',bMSK1,'b2',b2,'bMSK2',bMSK2,'bMSK',bMSK); 
        [DM,IDM0,dummy_DKI,lSW,Asym,lS0]=make_Design_DWSignal_Tensor(dummy_DKI,bvals(1,:),bvecs_reoriented,binfo,V,MSK,S0,VG,thr_cond);
    end
    % !!!hacked!!!
    Asym_wlsq   = Asym.Asym_olsq;
    dummy_robust=0; % fitting algorithm is set by default: olsq method 
end
if(dummy_DKI==0)
    % separate b0s form others
    minb    = min(bvals(1,:));
    MSK_b   = find(bvals(1,:)>minb);
    
    b0      = min(bvals(1,:));
    if(size(bvals,1)==1)
        [IDM0,DM0] = acid_dti_get_bmatrix(bvecs_reoriented,bvals(1,:));    
    elseif(size(bvals,1)==2)
        [IDM0,DM0] = acid_dti_get_bmatrix_varTE_varb(bvecs_reoriented,bvals);    
    end
    % calculate log of DTIs
    [lSWvol,lS0]    = acid_dti_get_logS_test(MSK,V,b0,bvals,dummy_DKI);   

    Asym_olsq = zeros(numel(MSK),size(DM0,2));
    for ki=1:size(DM0,2)
        Asym_olsq(:,ki)  = IDM0(ki,:)*lSWvol';
    end

    midfix  = 'meanDWI_';
    my_write_data(mean(exp(lSWvol(:,MSK_b)),2),VG,midfix,S0,MSK);
end

if(~exist('sigmaest'))
    sigmaest= sigma0;
end


if(dummy_robust ~=0)
    warning('Kurtosis estimation works only with ols option');
    dummy_robust = 0;
end

% acid_dti_compute_error(Asym_olsq,MSK,MSK,V,VS0,bvalues0,DM0,bC,bMSK,1,1,1);

if(dummy_robust==0)
    %% ---- write olsq
       % write original data
    midfix = 'ols_';
    if(dummy_DKI)
         % estimate tensor
        Asymtmp = struct('Asym_olsq',Asym.Asym_olsq','Asym_X4',Asym.Asym_X4');
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asym.Asym_olsq',MSK,dummy_DT);
        
         % lSW to lSi-lS0
        lSW         = bsxfun(@minus,lS0,lSW);
        lSW         = permute(lSW,[2 1]);
         % lSi-lS0 to ADC
        lSW         = bsxfun(@rdivide,lSW,bvals(bvals>binfo.b0));
        lSW         = permute(lSW,[2 1]);
        % write_DTIdataset_Kurtosis(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSW,DM,Asymtmp,MSK,bvals,dummy_DT,Dthr./binfo.b2(end));
        write_DTIdataset_Kurtosis_Leipzig062016(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSW,DM,Asymtmp,MSK,bvals,dummy_DT,Dthr./binfo.b2(end),bvecs_reoriented,bvals)

    else
        % estimate tensor
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asym_olsq,MSK,dummy_DT);
        acid_dti_compute_DTImaps(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSWvol,DM0,squeeze(Asym_olsq),MSK,bvals(1,:),dummy_DT,Dthr);
    end

    if(dummy_TFreiburg), write_freiburgFT(VG,EVEC10,midfix,bvecs_reoriented,bvals,P,RM); end
    
   %% write DTIs
    if(dummy_DTIsm==1)
        midfix5 = 'olsq_';
        if(dummy_DKI==1), DM0 = DM'; Asym_olsq = cat(1,Asym.Asym_olsq,Asym.Asym_X4); end
        for i=1:size(DM0,1)
            inx = bMSK(i);
            lSW_oslq  = (DM0(i,:)*Asym_olsq)+lS0;
            if(inx<10)
                ending = ['-00' num2str(inx)];        
            elseif(inx>=10 && inx<100)
                ending = ['-0' num2str(inx)];
            elseif(inx>=100)
                ending = ['-' num2str(inx)];
            end
            write_data(exp(lSW_oslq),VG,midfix5,S0,MSK,ending);        
        end
        for i=bMSK0
            lSW_oslq  = lS0;
            if(i<10)
                ending = ['-00' num2str(i)];        
            elseif(i>=10 && i<100)
                ending = ['-0' num2str(i)];
            elseif(i>=100)
                ending = ['-' num2str(i)];
            end
            write_data(exp(lSW_oslq),VG,midfix5,S0,MSK,ending); 
        end
%         midfix2  = ['meanDWI_' midfix];
%         write_data(mean(exp(lSW_oslq(:,MSK_b)),2),VG,midfix2,S0,MSK);
    end

%% ---- end write olsq
elseif(dummy_robust == 1)
    clear lSW03d lSW_olsq ending;
    %% ---- write weighted least squares
     % !!!hacked!!!     
    midfix  = 'wls_';
    b0 = min(bvals(1,:));
    [Asym_wlsq,sigma,AMSK_brain] = wLSQ_tensorfit(DM0,lSWvol,Asym_olsq,AMSK,bvals(1,:),sigma0,b0);

    % estimate tensor
     if(dummy_DKI)
        error('The weighted least square has not been calcualted!!')
     else
        % write data
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asym_wlsq,MSK,dummy_DT);
        acid_dti_compute_DTImaps(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSWvol,DM0,squeeze(Asym_wlsq),MSK,bvals(1,:),dummy_DT,Dthr);
     end
    if(dummy_TFreiburg),  write_freiburgFT(VG,EVEC10,midfix,bvecs_reoriented,bvals,P,RM); end
    
    % write DTIs
    if(dummy_DTIsm==1)
        midfix5 = 'wls_';
        for i=1:size(DM0,1)
            lSW_wlsq  = DM0(i,:)*Asym_wlsq';
            if(i<10)
                ending = ['-0' num2str(i)];        
            else
                ending = ['-' num2str(i)];
            end
            write_data(exp(lSW_wlsq(:,i)),VG,midfix5,S0,MSK,ending);        
        end
        midfix2  = ['wlsq_' midfix];
        write_data(mean(exp(lSW_wlsq(:,MSK_b)),2),VG,midfix2,S0,MSK);
    end
   %% ---- end write weighted least squares    
elseif(dummy_robust == 2)
    clear lSW03d lSW_olsq ending;

    %% ---- robust fitting
    b0 = min(bvals(1,:));
    midfix = 'robust_';
    if(dummy_DKI)
        [Asym_robust2,sigmaplanes] = robust_estimate_Zwiers_slicew_DKI(DM,lSW,Asym,AMSK,thr_DTvar,bvals(1,:),C,maxk,b0,sigma0,V,dummy_DKI); 
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asym_robust2.Asym_robust,MSK,dummy_DT);
        Asym_robust2.Asym_X4_ad(isnan(mean(Asym_robust2.AsymX4_robust,2)),:)=0;
        write_DTIdataset_ISMRM2013(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSW,DM,Asym_robust2,MSK,'',dummy_DT,Dthr);
    else
        [Asym_robust2,sigmaplanes] = robust_estimate_Zwiers2010_slicew_prod(DM0,lSWvol,Asym_olsq,AMSK,thr_DTvar,bvals(1,:),C,maxk,b0,sigma0,V); 
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asym_robust2,MSK,dummy_DT);
        acid_dti_compute_DTImaps(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSWvol,DM0,squeeze(Asym_robust2),MSK,bvals(1,:),dummy_DT,Dthr);
    end
    
    if(dummy_TFreiburg),  write_freiburgFT(VG,EVEC10,midfix,bvecs_reoriented,bvals,P,RM); end
    
    % write DTIs
    if(dummy_DTIsm==1)
        lSW_robust = (DM0*Asym_robust2')';
        for i=1:size(DM0,1)
            if(i<10)
                ending = ['-0' num2str(i)];        
            else
                ending = ['-' num2str(i)];
            end
            write_data(exp(lSW_robust(:,i)),VG,midfix,S0,MSK,ending);     
        end
        midfix2  = ['meanDWI_' midfix];
        write_data(mean(exp(lSW_robust(:,MSK_b)),2),VG,midfix2,S0,MSK);
    end
elseif(dummy_robust == 3)
    clear lSW03d lSW_olsq ending;
    %% ---- adaptive smoothing fitting    
    midfix = 'adaptive_';
    if(dummy_DKI)
        [Asymad,Aw0out,Aw0X4out] = adaptive_smoothing_v01(VG,Asym,S0,lSW,bvals(1,:),DM,kernelmm,sigmaest,C,Niter,MSK,dummy_DKI,b0,thr_cond,binfo,V,bvecs_reoriented,MSKseg);
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asymad.Asym_ad(MSK,:),MSK,dummy_DT);
        Asymad.Asym_X4_ad(isnan(mean(Asymad.Asym_X4_ad,2)),:)=0;
        Asymadtmp = struct('Asym_ad',Asymad.Asym_ad(MSK,:), 'Asym_X4_ad',Asymad.Asym_X4_ad(MSK,:));
        write_DTIdataset_ISMRM2013(midfix,VG,S0,FA0,EVAL0,EVEC10,lSW,DM,Asymadtmp,MSK,'',dummy_DT,Dthr);
    else
%         [Asymad] = adaptive_smoothing(VG,Asym_wlsq,S0,lSWvol,bvals(1,:),DM0,kernelmm,sigmaest,C,Niter,MSK,dummy_DKI);
        [Asymad,Aw0out,Aw0X4out] = adaptive_smoothing_v01(VG,Asym_olsq,S0,lSWvol,bvals(1,:),DM0,kernelmm,sigmaest,C,Niter,MSK,dummy_DKI,b0,thr_cond,'',V,bvecs_reoriented,MSKseg);
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asymad,MSK,dummy_DT);
        acid_dti_compute_DTImaps(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSWvol,DM0,squeeze(Asymad),MSK,bvals(1,:),dummy_DT,Dthr);
    end 
%     Asym_ad = Asym_wlsq'; % zeros(size(Asym_tmp)); 
%     Asym_ad(MSK,:) = Asymad.Asym_ad;
    if(dummy_TFreiburg),  write_freiburgFT(VG,EVEC10,midfix,bvecs_reoriented,bvals,P,RM); end
    
    % write DTIs
    if(dummy_DTIsm==1)
        lSW_ad = (DM0*Asymad')';
        for i=1:size(DM0,1)
            if(i<10)
                ending = ['-0' num2str(i)];        
            else
                ending = ['-' num2str(i)];
            end
            write_data(exp(lSW_ad(:,i)),VG,midfix,S0,MSK,ending);     
        end
        midfix2  = ['meanDWI_' mdfix];
        write_data(mean(exp(lSW_ad(:,MSK_b)),2),VG,midfix2,S0,MSK);
    end
elseif(dummy_robust == 4)
     midfix = 'nonlinear_';
    if(dummy_DKI)
            error('Nonlinear fitting is not available, yet, for the kurtosis estimation.')
    else
        [Asymnl]=make_nltensor_FAIR(DM0,Asym_olsq,lSWvol);
        [FA0,EVEC10,EVAL0,tmp0] = acid_dti_manage_eigenvalues(Asymnl,MSK,dummy_DT);
        acid_dti_compute_DTImaps(midfix,VG,S0,lS0,FA0,EVAL0,EVEC10,lSWvol,DM0,squeeze(Asym_olsq),MSK,bvals(1,:),dummy_DT,Dthr);
    end
    if(dummy_TFreiburg),  write_freiburgFT(VG,EVEC10,midfix,bvecs_reoriented,bvals,P,RM); end
    
    % write DTIs
    if(dummy_DTIsm==1)
        midfix5 = 'olsq_';
        for i=1:size(DM0,1)
            lSW_oslq  = DM0(i,:)*Asym_olsq';
            if(i<10)
                ending = ['-0' num2str(i)];        
            else
                ending = ['-' num2str(i)];
            end
            write_data(exp(Asym_olsq),VG,midfix5,S0,MSK,ending);        
        end
        midfix2  = ['meanDWI_' midfix];
        write_data(mean(exp(Asym_olsq(:,MSK_b)),2),VG,midfix2,S0,MSK);
    end
    
end

% end timer
T=toc/60;
disp(['The total of ' midfix(1:end-1) ' estimate was: ' num2str(T) ' min']);
%% End Main Program

%- write data -------------------
function write_data(yVol,V,prefix,A,MSK,ending)
vol1        = zeros(size(A));
vol1(MSK)   = yVol;
[pth,fname,ext] = fileparts(V.fname);
if(~exist('ending'))
    V.fname=[pth filesep prefix fname ext];
else
    V.fname=[pth filesep prefix fname ending ext];
end
V=rmfield(V,'pinfo');
spm_write_vol(V, vol1);
clear tmp1;
