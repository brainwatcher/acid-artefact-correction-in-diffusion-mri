function [exponent] = total_exponent(bvalues,DiffVecORIG,parameters)


Dl=parameters(1,:); 
Dt=parameters(2,:);
S0=parameters(3,:);
theta=parameters(4,:);
phi=parameters(5,:);
Wpara=parameters(6,:);
Wperp=parameters(7,:);
Wmean=parameters(8,:);




% 
% % S0=par(1);
% theta=par(1);
% phi=par(2);
% % Dl=par(3);
% % Dt=par(4);
% x_1 = par(3); % assuming x_1 = Dl - Dt
% Wl=par(4);
% Wt=par(5);

% Wbar=par(8);

Dt = ( 3 * Dbar - x_1 ) / 3;
Dl = ( 3 * Dbar + 2 * x_1 ) / 3;

[nb]=size(b,3);
u = [sin(theta)*cos(phi),sin(theta)*sin(phi),cos(theta)]';

Trb=zeros(1,nb);
ubu=zeros(1,nb);
ubbu=zeros(1,nb);
Trbb=zeros(1,nb);
for i=1:nb
    Trb(i)=trace(b(:,:,i));
    ubu(i)=u'*b(:,:,i)*u;
    Trbb(i)=sum(sum(b(:,:,i).^2));
    ubbu(i)=u'*b(:,:,i)*b(:,:,i)*u;
end

Wbb=1/2*(10*Wt+5*Wl-15*Wbar)*ubu.^2+1/2*(5*Wbar-Wl-4*Wt)*(ubu.*Trb+2*ubbu)+Wt/3*(Trb.^2+2*Trbb);
Db=Trb*Dt+(Dl-Dt)*ubu;



% Dbar=(2*Dt+Dl)/3;

S=S0*exp(-Db+ 1/6*Dbar.^2.*Wbb)';


end