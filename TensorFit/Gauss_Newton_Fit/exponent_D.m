function [exp_D] = exponent_D(bvalues,DiffVecORIG,parameters)
% Dpara=parameters(1,:); 
% Dperp=parameters(2,:);
% S0=parameters(3,:);
% theta=parameters(4,:);
% phi=parameters(5,:);
% Wpara=parameters(6,:);
% Wperp=parameters(7,:);
% Wmean=parameters(8,:);

% syms gx gy gz Wperp Wpara Wmean Dperp Dpara



for i=1:numel(bvalues)
    
     b_matrices= bvalues(i).* [DiffVecORIG(1,i).* DiffVecORIG(1,i),DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(1,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(3,i),DiffVecORIG(2,i).*DiffVecORIG(3,i),DiffVecORIG(3,i).*DiffVecORIG(3,i)];
     
    tr_bmatrices = trace(b_matrices);

    for j=1: size(parameters,2)
        u=[ sin(parameters(4,j)).*cos(parameters(5,j)), sin(parameters(4,j)).*sin(parameters(5,j)),cos(parameters(4,j))]';
        exp_D(j,i)=( tr_bmatrices*parameters(2,j) + (parameters(1,j)-parameters(2,j))* (u'*b_matrices*u) );
    end
end