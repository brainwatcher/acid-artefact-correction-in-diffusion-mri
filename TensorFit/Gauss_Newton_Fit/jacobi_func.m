function [jacobi_i] = jacobi_func(dummy_model,parameters,design_kurtosis,pred,S0,DM0)

    if(dummy_model == 2)  

        jacobi_i=jacobian_matrix_DKI_non_lin(parameters,design_kurtosis,pred,S0); 

    end


    if(dummy_model == 1)

      jacobi_i=jacobian_matrix_DTI_non_lin(parameters,DM0,pred,S0); 

    end


end

