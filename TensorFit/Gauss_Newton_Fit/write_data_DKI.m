function  write_data_DKI(bvalues,V,AMSK,DiffVecORIG,Asym_olsq_rice,MSK,VS0,AS0,dummy_DT,Dthr,bvalues0,dummy_model,dummy_RBC)

  [minb,MSK_b,b0,b1,b2,bMSK0,bMSK1,bC,bia,bic,binfo,lSW,lS0,X4] = hack_to_make_QL_writing_work(bvalues,V,AMSK,DiffVecORIG);


            Asymtmp = struct('Asym_olsq',Asym_olsq_rice(:,1:6),'Asym_X4',Asym_olsq_rice(:,8:22));
           [FA0_rice,EVEC10_rice,EVAL0_rice,tmp0_rice] = estimate_tensornew(Asym_olsq_rice,MSK,dummy_DT); 
           
%            [FA0,EVEC,EVAL0] = acid_dti_compute_eigenvalues(Asym_olsq_rice,MSK,dummy_DT);
           
            FA0_rice(FA0_rice<0)=0;
            FA0_rice(FA0_rice>1000)=1000;          

            S0=exp(Asym_olsq_rice(:,7)); %estimated S0

             midfix = ''; 
             pre = def_pre(dummy_model,dummy_RBC); % "pre" is the prefix of the names of the maps being written in the next line of code
             write_DTIdataset_Kurtosis_Leipzig062016_GN(pre,midfix,VS0,AS0,lS0,S0,FA0_rice,EVAL0_rice,EVEC10_rice,lSW,X4,Asymtmp,MSK,bvalues,dummy_DT,Dthr./binfo.b2(end),DiffVecORIG,bvalues0);
             



end

