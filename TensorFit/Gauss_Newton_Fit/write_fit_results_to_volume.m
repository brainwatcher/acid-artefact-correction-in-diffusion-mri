function [Asym_olsq_rice,Asym_olsq0,Asym_olsq00,mDWI_GN] = write_fit_results_to_volume(Asym_olsq_rice,Asym_olsq0,Asym_olsq00,sz,MSK_b,AMSK,dummy_model,volume_of_fit_results,volume_of_lS)

%          if(dummy_mat_ols == 1)  
%              
%             for i=1:size(Asym_olsq0,4)                                      %size(Asym_olsq0,4) is the number of columns in the DM0 matrix: (Asym_olsq0,4)=size(DM0,2)
%                 tmp2                    = Asym_olsq0(:,:,:,i);              %tmp2 is being identified with the i th component of the 4th dimension of Asym_olsq0 (at this point it would be 0 s only)
%                 tmp2(MSKvol>0)          = tmp(i,:);                         %tmp(i;:) contains the i th component of the diffusion tensor, MSKvol is greater 0 only for a certain p (see line 215) (p defines the image slice)
%                 Asym_olsq0(:,:,:,i)     = tmp2;                             %Asym_olsq0 now is being succesively filled with the diffusion tensor s values (indexed by i), this is being done i=1 to 7 times for each tensor value and
%             end                                                             %repeated for every slice.
%             
%             tmp         = zeros(sz);
%             tmp(MSKvol>0)           = mean(exp(lS(:,MSK_b)),2);
%             mDWI(:,:,p) = tmp(:,:,p);
%             tmpS0(MSKvol>0) = lS0slice;  
%             
%             
%             Asym_olsq   = zeros(numel(MSK),size(DM0,2));
%             
%             for i=1:size(Asym_olsq0,4)
%                 tmp  = Asym_olsq0(:,:,:,i);
%                 Asym_olsq(:,i) = tmp(AMSK>0);
%             end
%             
%          end  
         
        
%              
%             if(dummy_model == 1)
%                 n_parameters = 7;
%             elseif(dummy_model == 2)
%                 n_parameters =22;
%             elseif(or(dummy_model ==4 ,dummy_model == 3))
%                 n_parameters = 8;
%             end
%             
%            
%             
%             
%             for i=1:n_parameters 
%                 tmp4                    = Asym_olsq00(:,:,p,i);
%                 tmp4(AMSKslice>0)       = tmp3(i,:);
%                 Asym_olsq00(:,:,p,i)    = tmp4;
%             end
%                        
%             tmp3      =      zeros(sz);
%             tmp3(MSKvol>0)           = mean(exp(lS(:,MSK_b)),2);
%             mDWI_GN(:,:,p) = tmp3(:,:,p);
%             tmpS0_rice(MSKvol>0) = lS0slice;
%      
%               
%             for i=1:n_parameters
%               tmp3  = Asym_olsq00(:,:,:,i);
%               Asym_olsq_rice(:,i) = tmp3(AMSK>0);
%             end
%             
          
         if(dummy_model == 1 || dummy_model == 6)
                n_parameters = 7;
            elseif(dummy_model == 2)
                n_parameters =22;
            elseif(or(dummy_model ==4 ,dummy_model == 3))
                n_parameters = 8;
            elseif(dummy_model == 5)
                n_parameters = 2;

         end


 for inx_vol = 1:numel(volume_of_fit_results)
    if ~isempty(volume_of_fit_results{inx_vol})
              
        MSKvol          = zeros(sz);
        MSKvol(:,:,inx_vol)   = ones(sz(1:2));
        AMSKslice       = AMSK(:,:,inx_vol);
        
        tmp3 = volume_of_fit_results{inx_vol};
        lS   = volume_of_lS{inx_vol};
            
        

            for i=1:n_parameters 
                tmp4                    = Asym_olsq00(:,:,inx_vol,i);
                tmp4(AMSKslice>0)       = tmp3(i,:);
                Asym_olsq00(:,:,inx_vol,i)    = tmp4;
            end
            
            tmp3      =      zeros(sz);
            tmp3(MSKvol>0)           = mean(exp(lS(:,MSK_b)),2);
            mDWI_GN(:,:,inx_vol) = tmp3(:,:,inx_vol);
%             tmpS0_rice(MSKvol>0) = lS0slice;
     
              
            for i=1:n_parameters
              tmp3  = Asym_olsq00(:,:,:,i);
              Asym_olsq_rice(:,i) = tmp3(AMSK>0);
            end    
            
            
    elseif isempty(volume_of_fit_results{inx_vol})
        
            AMSKslice       = AMSK(:,:,inx_vol);
            mDWI_GN(:,:,inx_vol) = NaN(size(AMSKslice));
        
    end
       
            
end   











end

