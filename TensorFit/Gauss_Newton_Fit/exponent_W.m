function [exp_W] = exponent_W(bvalues,DiffVecORIG,parameters)
% Dpara=parameters(1,:); 
% Dperp=parameters(2,:);
% S0=parameters(3,:);
% theta=parameters(4,:);
% phi=parameters(5,:);
% Wpara=parameters(6,:);
% Wperp=parameters(7,:);
% Wmean=parameters(8,:);



for i=1:numel(bvalues)
    
     b_matrices= bvalues(i).* [DiffVecORIG(1,i).* DiffVecORIG(1,i),DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(1,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(2,i),DiffVecORIG(2,i).*DiffVecORIG(3,i);DiffVecORIG(1,i).*DiffVecORIG(3,i),DiffVecORIG(2,i).*DiffVecORIG(3,i),DiffVecORIG(3,i).*DiffVecORIG(3,i)]; 
     Trbb=sum(sum(b_matrices.^2));
     trace_tensor_product=Trbb;

    
      tr_bmatrices = trace(b_matrices);
     for j=1: size(parameters,2)
        u=[ sin(parameters(4,j)).*cos(parameters(5,j)), sin(parameters(4,j)).*sin(parameters(5,j)),cos(parameters(4,j))]';
         traceterm_1 = ((parameters(1,j)+ 2 * parameters(2,j))/3); %for the trace term in the DKI signal equation
         traceterm = traceterm_1.^2;

        exp_W(j,i)=(1/6)*traceterm*((1/2)*(10.*parameters(7,j)+5*parameters(6,j)-15*parameters(8,j))*(u'*b_matrices*u).^2+(1/2)*(5*parameters(8,j)-parameters(6,j)-4*parameters(7,j))*(u'*b_matrices*u*tr_bmatrices+2*u'*(b_matrices*b_matrices)*u)+(parameters(7,j)/3)*(tr_bmatrices^2+2*trace_tensor_product));
    end
end



