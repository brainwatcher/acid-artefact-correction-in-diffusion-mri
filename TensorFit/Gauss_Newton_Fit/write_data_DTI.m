function  write_data_DTI(Asym_olsq_rice,MSK,dummy_DT,VS0,AS0,lS0,V,DM0,bvalues,Dthr,dummy_model,dummy_RBC)

            [FA0,EVEC10,EVAL0,tmp0] = estimate_tensornew(Asym_olsq_rice,MSK,0);% here the Fractional Anisotropy and Mean Diffusivity are being determined using the C-routine "DTI2EVEW"   
%           [FA0,EVEC10,EVAL0,tmp0] =   acid_dti_manage_eigenvalues(Asym_olsq_rice,MSK,0);

            FA0(FA0<0)=0;
            FA0(FA0>1000)=1000;

            S0=exp(Asym_olsq_rice(:,7)); % estimated S0

            midfix = '';
            pre = def_pre(dummy_model,dummy_RBC);
            write_DTIdataset_prod_GN(pre,midfix,VS0,AS0,lS0,S0,FA0,EVAL0,EVEC10,V,DM0,squeeze(Asym_olsq_rice),MSK,bvalues(1,:),dummy_DT,Dthr);   


end

