function [pre] = def_pre_DTI(dummy_model,dummy_RBC)

    if(dummy_model == 2)
            
         if(dummy_RBC ==1)   
             pre  = 'DKI_non_linear_rice_';
         elseif(dummy_RBC == 0)
             pre = 'DKI_non_linear_no_rice_';
         end
       end
      
       
       
    if(dummy_model == 1)
            
         if(dummy_RBC == 1)   
             pre  = 'DTI_non_linear_rice_';
         elseif(dummy_RBC == 0)
             pre = 'DTI_non_linear_no_rice_';
         end
     

        
    end

    if(dummy_model == 3)
        if(dummy_RBC == 1)
            pre = 'Axial_symmetric_DKI_rice_';
        elseif(dummy_RBC == 0)
             pre = 'Axial_symmetric_DKI_no_rice_';
        end
    end




end

