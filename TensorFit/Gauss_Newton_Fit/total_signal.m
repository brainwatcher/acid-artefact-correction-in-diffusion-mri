function [pred] = total_signal(bvalues,DiffVecORIG,parameters)


Dl=parameters(1,:)'; 
Dt=parameters(2,:)';
S0=parameters(3,:)';
theta=parameters(4,:)';
phi=parameters(5,:)';
Wl=parameters(6,:)';
Wt=parameters(7,:)';
Wbar=parameters(8,:)';


   bfunc = @(bvec,bval) bvec*bvec'.*bval;
   for inx=1:numel(bvalues)

    b(:,:,inx) = bfunc(DiffVecORIG(:,inx),bvalues(inx));
   end

% 
% % S0=par(1);
% theta=par(1);
% phi=par(2);
% % Dl=par(3);
% % Dt=par(4);
% x_1 = par(3); % assuming x_1 = Dl - Dt
% Wl=par(4);
% Wt=par(5);

% Wbar=par(8);
% 
% Dt = ( 3 * Dbar - x_1 ) / 3;
% Dl = ( 3 * Dbar + 2 * x_1 ) / 3;

[nb]=size(bvalues,2);
for j=1:size(parameters,2)
u(:,j) = [sin(theta(j))*cos(phi(j)),sin(theta(j))*sin(phi(j)),cos(theta(j))]';
end

Trb=zeros(1,nb);
ubu=zeros(1,nb);
ubbu=zeros(1,nb);
Trbb=zeros(1,nb);
for i=1:nb
    for j=1:size(parameters,2)
    Trb(j,i)=trace(b(:,:,i));
    ubu(j,i)=u(:,j)'*b(:,:,i)*u(:,j);
    Trbb(j,i)=sum(sum(b(:,:,i).^2));
    ubbu(j,i)=u(:,j)'*b(:,:,i)*b(:,:,i)*u(:,j);
    end
end

Wbb=1/2.*(10.*Wt+5.*Wl-15.*Wbar).*ubu.^2+1/2.*(5.*Wbar-Wl-4.*Wt).*(ubu.*Trb+2.*ubbu)+Wt./3.*(Trb.^2+2.*Trbb);
Db=Trb.*Dt+(Dl-Dt).*ubu;



 Dbar=(2.*Dt+Dl)./3;

pred=(S0.*exp(-Db+ 1/6.*Dbar.^2.*Wbb))';


end