function diff_GN = tbxdti_acid_gauss_newton_DTI(fit_option)




%% Gauss Newton Algorihm Input
% ---------------------------------------------------------------------
% in_vols diffusion weighted images
% ---------------------------------------------------------------------
in_vols_GN         = cfg_files;
in_vols_GN.tag     = 'in_vols_GN';
in_vols_GN.name    = 'Diffusion weighted images';
in_vols_GN.help    = {'Select the measured diffusion weighted magnitude images'
                                };
in_vols_GN.filter = 'image';
in_vols_GN.ufilter = '.*';
in_vols_GN.num     = [0 Inf];

% ---------------------------------------------------------------------
% diffusion directions
% ---------------------------------------------------------------------
diff_dirs_GN         = cfg_entry;
diff_dirs_GN.tag     = 'diff_dirs_GN';
diff_dirs_GN.name    = 'Diffusion directions (bvec)';
diff_dirs_GN.help    = {'Provide a 3 x N  - matrix with b-vectors, b-vectors should appear in the same order as the low- and high-diffusion weighted images were entered. The b-vectors are dimensionless.' 
                             'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'
                             };
diff_dirs_GN.strtype = 'e';
diff_dirs_GN.num     = [3 Inf];
diff_dirs_GN.val     = {[1 0 0; 0 1 0; 0 0 1; 0 1/sqrt(2) 1/sqrt(2)]'};
% ---------------------------------------------------------------------
% Dummy variable for writing all eigenvectors and eigenvalues and kurtosis
% variables
% ---------------------------------------------------------------------
dummy_DT         = cfg_menu;
dummy_DT.tag     = 'dummy_DT';
dummy_DT.name    = 'Write additional parameters';
dummy_DT.help    = {'There is a difference between the kurtosis parameters denoted with K (=apparent kurtosis) and those denoted with W(=kurtosis tensor) (Check doi:10.1016/j.neuroimage.2016.08.022 for more details on W and K).'
     'The default kurtosis output variables for DKI are the "K" parameters which refer to the parallel, perpendicular and mean kurtosis, while the default kurtosis output parameters for Axial Symmetric DKI are "W".'
    'If you would also like to estimate the "W" parameters in DKI or the "K" parameters in Axial Symmetric DKI, choose "YES". Note: There is no mean kurtosis in Axial Symmetric DKI.'
    'Also, choose YES to compute all three eigenvectors and all three eigenvalues, WARNING: currently this takes ~30mins for a dataset with dimensions 140x146x100. '
    'If you choose YES, e.g., the first eigenvector is (x1,y2,z1). If you choose NO, only the first eigenvector will be computed and written in the format (x,y,z).'};
dummy_DT.labels = {
               'YES'
               'NO'
}';
dummy_DT.values = {0 1};
dummy_DT.val    = {1};

% ---------------------------------------------------------------------
% b values
% ---------------------------------------------------------------------
b_vals_GN         = cfg_entry;
b_vals_GN.tag     = 'b_vals_GN';
b_vals_GN.name    = 'b-values (bval)';
b_vals_GN.help    = {'IMPORTANT: Please provide b-values in s/mm^2. Provide a 1 x N  - matrix with b-values, b-values should appear in the same order as the low- and high-diffusion weighted images were entered.' 
                     'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'
                            };
b_vals_GN.strtype = 'e';
b_vals_GN.num     = [1 Inf];
b_vals_GN.val     = {[0 1000 1250 2000]};

% ---------------------------------------------------------------------
% b value labels
% ---------------------------------------------------------------------
b_val_labels_GN         = cfg_entry;
b_val_labels_GN.tag     = 'b_val_labels_GN';
b_val_labels_GN.name    = 'b-value labels';
b_val_labels_GN.help    = {'Only relevant for "power-law" fitting: please provide a list of bvalue labels used for dMRI acquisition, sorted in ascending order, e.g., [0,1,2.5] if data were acquired for b=0, b=1 and b=2.5 s/mm^2 shells.' 
                            };
b_val_labels_GN.strtype = 'e';
b_val_labels_GN.num     = [1 Inf];
b_val_labels_GN.val     = {[0 1000 1250 2000]};

% ---------------------------------------------------------------------
% L for Rician Bias Correction (RBC)
% ---------------------------------------------------------------------
in_L_RBC   = cfg_entry;
in_L_RBC.tag     = 'in_L_RBC';
in_L_RBC.name    = 'Number of coils L used in your measurement';
in_L_RBC.help    = {'Number of coils used in your measurement, the noise estimate (sigma) must have been done with the same number of coils L.'};
in_L_RBC.strtype = 'e';
in_L_RBC.num     = [1 1];
in_L_RBC.val    = {1};

% ---------------------------------------------------------------------
% Fit Method
% ---------------------------------------------------------------------
dummy_algorithm_GN   = cfg_menu;
dummy_algorithm_GN.tag     = 'dummy_algorithm_GN';
dummy_algorithm_GN.name    = 'Choose the Fitting Method';
dummy_algorithm_GN.help    = {'DTI estimates 7 Parameters (6 tensor parameters + non diffusion weighted signal)'
                              };
dummy_algorithm_GN.labels = {
               'Diffusion Tensor Imaging (DTI)'
  
}';
dummy_algorithm_GN.values = {1};
dummy_algorithm_GN.val    = {fit_option};


% ---------------------------------------------------------------------
% Dummy Rician Bias Correction (RBC)
% ---------------------------------------------------------------------
dummy_RBC_GN   = cfg_menu;
dummy_RBC_GN.tag     = 'dummy_RBC_GN';
dummy_RBC_GN.name    = 'Rician bias correction';
dummy_RBC_GN.help    = {'This option allows you to activate/deactivate Rician bias correction on model parameter estimation.'};
dummy_RBC_GN.labels = {
               'NO'
               'YES'
}';
dummy_RBC_GN.values = {0 1};
dummy_RBC_GN.val    = {0};




% ---------------------------------------------------------------------
% sigma for Rician Bias Correction (RBC)
% ---------------------------------------------------------------------
in_sigma_RBC   = cfg_entry;
in_sigma_RBC.tag     = 'in_sigma_RBC';
in_sigma_RBC.name    = 'Standard deviation for Rician bias correction';
in_sigma_RBC.help    = {'Standard deviation used for the Rician bias correction, it is assumed that every coil is contaminated with the same size Gaussian noise.'};
in_sigma_RBC.strtype = 'e';
in_sigma_RBC.num     = [1 1];
in_sigma_RBC.val    = {10};
% ---------------------------------------------------------------------
% Mask
% ---------------------------------------------------------------------
in_msk_GN         = cfg_files;
in_msk_GN.tag     = 'in_msk_GN';
in_msk_GN.name    = 'Region of interest image (or done for none)';
in_msk_GN.help    = {'Select a binary mask for parameter estimation in a particular region of interest (ROI).'};
in_msk_GN.filter  = 'image';
in_msk_GN.val     = {{''}};
in_msk_GN.ufilter = '.*';
in_msk_GN.num     = [0 1];


% ---------------------------------------------------------------------
% Noise-Map
% ---------------------------------------------------------------------
in_noise_map_GN         = cfg_files;
in_noise_map_GN.tag     = 'in_noise_map_GN';
in_noise_map_GN.name    = 'Noise Map';
in_noise_map_GN.help    = {'Select a map containing a noise estimate (sigma) for every image voxel.'};
in_noise_map_GN.filter  = 'image';
in_noise_map_GN.ufilter = '.*';
in_noise_map_GN.val     = {{''}};
in_noise_map_GN.num     = [0 1];
% ---------------------------------------------------------------------
% Beta-Map
% ---------------------------------------------------------------------
in_beta_map_GN         = cfg_files;
in_beta_map_GN.tag     = 'in_beta_map_GN';
in_beta_map_GN.name    = 'Beta Map obtained via power-law fitting';
in_beta_map_GN.help    = {'Select a map containing beta estimates (obtained via power-law fitting) for every image voxel.'};
in_beta_map_GN.filter  = 'image';
in_beta_map_GN.ufilter = '.*';
in_beta_map_GN.num     = [0 1];
in_beta_map_GN.val     = {''};
 

% ---------------------------------------------------------------------
% Reorientation Matrix for b-vectors
% ---------------------------------------------------------------------
RMatrix_GN         = cfg_entry;
RMatrix_GN.tag     = 'RMatrix_GN';
RMatrix_GN.name    = 'Reorientation Matrix';
RMatrix_GN.help    = {
                      'If the vendor uses another coordinate system than the coordinate system, in which your b-vectors were defined, you need to reorient them.'
                      'Provide a 3 x 3  - matrix to reorient b-vectors.'
};
RMatrix_GN.strtype = 'e';
RMatrix_GN.num     = [3 3];
RMatrix_GN.val     = {[1 0 0; 0 1 0; 0 0 1]};
% ---------------------------------------------------------------------
% Number of workers for multicore computation
% ---------------------------------------------------------------------
in_npool   = cfg_entry;
in_npool.tag     = 'in_npool';
in_npool.name    = 'Parallel programming';
in_npool.help    = {'Choose the number of workers (CPU cores) for multicore execution of the algorithm.'};
in_npool.strtype = 'e';
in_npool.num     = [1 1];
in_npool.val    = {1};

%{
% ---------------------------------------------------------------------
% Number iterations for GN fit
% ---------------------------------------------------------------------
in_niteration   = cfg_entry;
in_niteration.tag     = 'in_niteration';
in_niteration.name    = 'Number of fit iterations';
in_niteration.help    = {'Choose the number of fit iterations.'};
in_niteration.strtype = 'e';
in_niteration.num     = [1 1];
in_niteration.val    = {75};
%}
               
               
diff_GN         = cfg_exbranch;
diff_GN.tag     = 'diff_GN';
diff_GN.name    = 'Gauss Newton algorithm based diffusion model parameter estimation';
diff_GN.val     = {in_vols_GN diff_dirs_GN b_vals_GN dummy_algorithm_GN dummy_RBC_GN in_sigma_RBC in_msk_GN RMatrix_GN in_L_RBC in_noise_map_GN in_npool dummy_DT};
diff_GN.help    = {'Gauss Newton algorithm based diffusion model parameter estimation. The Gauss Newton based parameter estimation algorithm uses the non-linear DTI signal model to estimate the 7 model parameters.'
                   'Also, Rician bias corrected parameter estimation can be done which, technically, is referred to as "quasi likelihood" estimation.'};
diff_GN.prog = @local_diff_GN;



%-------------- Gauss Newton ---------------------------------------------

function out = local_diff_GN(job)

in_niteration = acid_get_defaults('diffusion.in_niteration');

job.b_val_labels_GN = [];
job.in_beta_map_GN  = [];
job.dummy_algorithm_GN = 1;
job.dummy_write_K_or_W =0;
DTI_olsq_robust_GN(char(job.in_vols_GN),job.diff_dirs_GN,job.b_vals_GN,job.dummy_algorithm_GN, job.dummy_RBC_GN, job.in_sigma_RBC, char(job.in_msk_GN) ,job.RMatrix_GN, job.in_L_RBC, char(job.in_noise_map_GN), job.in_npool, in_niteration, job.b_val_labels_GN, char(job.in_beta_map_GN),job.dummy_DT);
out=3;
