function [design_kurtosis_axial_symmetry]  = design_matrix_kurtosis_axial_symmetry(V,bvalue,AMSK)


% design matrix diffusion tensor
X2(1,:)  = V(1,:).^2;
X2(2,:)  = V(2,:).^2 + V(3,:).^2;
X2(3,:)  = 1;
X2(4,:)  = 2.*V(1,:).*V(2,:) + 2.* V(3,:).*V(1,:); 
% design matrix kurtosis tensor
% X4(1,:)  = V(1,:).^4 - 1.5 .* V(3,:).^2 .* V(1,:).^2 - 1.5 .* V(1,:).^2 .* V(2,:).^2; 
% X4(2,:)  = V(3,:).^4 + V(2,:) .^4 + 2 .* V(3,:).^2 .* V(2,:).^2 - 4 .* V(3,:).^2 .* V(1,:).^2 - 4 .* V(1,:).^2 .* V(2,:).^2 ; 
% X4(3,:)  = 7.5 .* V(3,:).^2 .* V(1,:).^2 + 7.5 .* V(1,:).^2 .* V(2,:).^2; 



szb = size(bvalue);
MSK = find(AMSK>0); 
if(szb(1)==1)
%   normalization of bvalues
%    nbvalue = bval/max(bval);
    nbvalue = bvalue;
    mb      = min(nbvalue);    
    bmax    = max(nbvalue);
    bMSK    = find(nbvalue>mb);
    XX2     = bsxfun(@times,X2, nbvalue);
    XX2(3,:) = -1; % this entry is corresponding to the ln(S0) part in the parameter vector
%     XX4     = bsxfun(@times,X4, (nbvalue.^2)/6);
    XX      = cat(1,-XX2);%,XX4);
    %XX      = XX(:,bMSK);
else
    error('This version cannot handle b-values other than of the form: 1xN')
end
design_kurtosis_axial_symmetry = XX';