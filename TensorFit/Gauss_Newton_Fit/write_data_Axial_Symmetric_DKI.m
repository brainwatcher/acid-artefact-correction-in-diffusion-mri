function   write_data_Axial_Symmetric_DKI(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK,dummy_DT)
     
            % Dpara=parameters(1,:); 
            % Dperp=parameters(2,:);
            % S0=parameters(3,:);
            % theta=parameters(4,:);
            % phi=parameters(5,:);
            % Wpara=parameters(6,:);
            % Wperp=parameters(7,:);
            % Wmean=parameters(8,:);

              ending = '';   
              
        first_ev = Asym_olsq_rice(:,1);                                     %the first eigenvalue in the axial symmetric case is the first estimated parameter (D_parallel)
        second_ev = Asym_olsq_rice(:,2);                                    %the second eigenvalue in the axial symmetric case is the second estimated parameter (D_perp), the third eigenvalue is equal to the second
      
        
        MD = (first_ev+2.* second_ev) ./3;
        FA = (3/2)^0.5 .* sqrt( ( (first_ev-MD) .^ 2 + 2 .* (second_ev-MD) .^ 2 ) ./ (first_ev.^2 + 2 .* second_ev .^ 2 ) );
%         FA2 = Compute_FA_Axial_Symmetry(first_ev,second_ev,Asym_olsq_rice)';
%         FA =((0.5)^0.5.* sqrt((first_ev-second_ev).^2+(second_ev-first_ev).^2))./ sqrt(first_ev+2.*second_ev);
        FA(FA>1)=1;
        FA(FA<0)=0;
        
        
        Kpara = Asym_olsq_rice(:,6) .* (MD.^2) ./ first_ev.^2;
        Kperp = Asym_olsq_rice(:,7) .* (MD.^2) ./ second_ev.^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%        
        %%%%% These are the components of the axis of symmetry, first
        %%%%% inspection looks good but haven't checked it thoroughly, yet.
        theta_estimated = Asym_olsq_rice(:,4);
        phi_estimated   = Asym_olsq_rice(:,5);

            axis_of_symmetry_x = sin(theta_estimated) .* cos(phi_estimated); 
            axis_of_symmetry_y = sin(theta_estimated).* sin(phi_estimated);
            axis_of_symmetry_z = cos(theta_estimated);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
        
        
        
        
        if(dummy_RBC == 0)
            midfix = '';
            
            prefix = ['NLLS_AxDKI_RBC_OFF_MD_' midfix];
            my_write_data(MD,VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_FA_' midfix];
            my_write_data(FA,VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_Wparallel_' midfix];
            my_write_data(Asym_olsq_rice(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_Wperp_' midfix];
            my_write_data(Asym_olsq_rice(:,7),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_Wmean_' midfix];
            my_write_data(Asym_olsq_rice(:,8),VS0,prefix,AS0,MSK,ending);
            
             if (dummy_DT == 0)
                prefix = ['NLLS_AxDKI_RBC_OFF_Kparallel_' midfix];
                my_write_data(Kpara,VS0,prefix,AS0,MSK,ending);
                prefix = ['NLLS_AxDKI_RBC_OFF_Kperp_' midfix];
                my_write_data(Kperp,VS0,prefix,AS0,MSK,ending);
             end
             
            prefix = ['NLLS_AxDKI_RBC_OFF_b0_' midfix];
            my_write_data(exp(Asym_olsq_rice(:,3)),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_Axis_Of_Symmetry_x_' midfix];
            my_write_data(axis_of_symmetry_x,VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_AxDKI_RBC_OFF_Axis_Of_Symmetry_y_' midfix];
            my_write_data(axis_of_symmetry_y,VS0,prefix,AS0,MSK,ending); 
            prefix = ['NLLS_AxDKI_RBC_OFF_Axis_Of_Symmetry_z_' midfix];
            my_write_data(axis_of_symmetry_z,VS0,prefix,AS0,MSK,ending);    
            
            prefix = ['NLLS_AxDKI_RBC_OFF_Dparallel_' midfix];
            my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_OFF_Dperp_' midfix];
            my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending);
            
        elseif(dummy_RBC == 1)
            
            midfix = '';
            
            prefix = ['NLLS_AxDKI_RBC_ON_MD_' midfix];
            my_write_data(MD,VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_FA_' midfix];
            my_write_data(FA,VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_Wparallel_' midfix];
            my_write_data(Asym_olsq_rice(:,6),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_Wperp_' midfix];
            my_write_data(Asym_olsq_rice(:,7),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_Wmean_' midfix];
            my_write_data(Asym_olsq_rice(:,8),VS0,prefix,AS0,MSK,ending);
            
               if (dummy_DT == 0)
                    prefix = ['NLLS_AxDKI_RBC_ON_Kparallel_' midfix];
                    my_write_data(Kpara,VS0,prefix,AS0,MSK,ending);
                    prefix = ['NLLS_AxDKI_RBC_ON_Kperp_' midfix];
                    my_write_data(Kperp,VS0,prefix,AS0,MSK,ending);
               end
               
            prefix = ['NLLS_AxDKI_RBC_ON_b0_' midfix];
            my_write_data(exp(Asym_olsq_rice(:,3)),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_Axis_Of_Symmetry_x_' midfix];
            my_write_data(axis_of_symmetry_x,VS0,prefix,AS0,MSK,ending);   
            prefix = ['NLLS_AxDKI_RBC_ON_Axis_Of_Symmetry_y_' midfix];
            my_write_data(axis_of_symmetry_y,VS0,prefix,AS0,MSK,ending); 
            prefix = ['NLLS_AxDKI_RBC_ON_Axis_Of_Symmetry_z_' midfix];
            my_write_data(axis_of_symmetry_z,VS0,prefix,AS0,MSK,ending); 
            
            
            prefix = ['NLLS_AxDKI_RBC_ON_Dparallel_' midfix];
            my_write_data(Asym_olsq_rice(:,1),VS0,prefix,AS0,MSK,ending);
            prefix = ['NLLS_AxDKI_RBC_ON_Dperp_' midfix];
            my_write_data(Asym_olsq_rice(:,2),VS0,prefix,AS0,MSK,ending); 
              
              
        end

