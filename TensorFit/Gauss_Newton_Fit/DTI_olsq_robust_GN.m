function Vweights = DTI_olsq_robust_GN(PInd,DD,bvalues0,dummy_model,dummy_RBC,sigma,PMSK,RM, number_of_coils_L, noise_map, npool, maxIter, bvalue_labels, constraints_map,dummy_DT)

% Gauss Newton Fit and Rician Bias Correction 

% PInd          - File names of DTI images (i.e. low and high b-value
%                 images). The order of the images must resample the order
%                 of the b-vector and b-values (see below).
% PMSK          - Region of interest, in which the tensor estimates are
%                 calculated. Could also be a brain mask. Is optional.  
% DD            - b-vectors, i.e. vector of diffusion gradients; 
%                 it should be a matrix of size 3 x number of diffusion 
%                 dections
% bvalues0       - b-value for each image, i.e. b-value for each diffusion 
%                 gradients; it should be a matrix of size 1 x number of diffusion 
%                 directions
% RM            - Matrix to reorient b-values
% dummy_model   - Chooses the model to fit the data, 1:DTI,2:DKI,
%                                                    3:Axial symmetric DKI
% dummy_RBC     - Activates/Deactivates Rician bias corrected model
%                 parameter estimation (1: ON, 0:OFF)
% dummy_model   - Chooses signal model fitted to the data
%                1:'Diffusion Tensor Imaging (DTI)'
%                2:'Diffusion Kurtosis Imaging (DKI)'
%                3:'Axial Symmetric DKI'
%                4:'Biophysical Model'
%                5:'Power-law fitting to obtain beta'
%                6:'Constrained biophysical model using beta'



%% start timer
% tic

use_noise_map = 0;


%     bvals = bvalues0';
%     bvals = round(bvals/100)*100;
%     bvalues0 = bvals';


% predefine Vweights
Vweights = [];

% check b-vector dimension
if(size(DD,1)~=3)
    error('Please transpose b vector!')
end

% check b-value dimension
if(size(bvalues0,1)>2)
    error('Please transpose b values!')
end

% check whether b-values and b-vectors have same length
if(size(bvalues0,2)~=size(DD,2))
    error('The length of b-values and b-vectors have to be the same!')
end

% Prepare if 4D-Volumes (one image == 4D)
if size(PInd,1) == 1
    struct4D   = nifti(PInd);
    dim4D = struct4D.dat.dim;
    n   = dim4D(4);
    if n == 1 
        error('A single 3D source image was selected. Choose a single 4D volume or select all 3D volumes manually!');
    end
    PInd = strcat(repmat(PInd(1:end-2), n, 1), ',', num2str([1:n]'));
end
    
% check whether images have the same length as b-vector/bvalues0
if(size(PInd,1)~=size(DD,2))
    error('The number of images have to be the same as the number of b-values/vectors!')
end

% check whether images have the same dimension as Mask
if(~isempty(PMSK))
    V1 = spm_vol(PMSK);
    V2 = spm_vol(PInd);
    if(~isequal(V1.dim,V2(1).dim))
        error('Dimension of Mask and DTIs must agree!')
    end
end
% reorient diffusion directions
DiffVecORIG = RM * DD;

% dummy_DT = 1; % Write all three eigenvectors (dummy_DT =0, first eigenvector = (x1,y1,z1)) or only first (dummy_DT=1)

% normalise b-values between 0 and 1
% bvalues     = bsxfun(@rdivide,bvalues0,max(bvalues0,[],2));
% bvalues     = bvalues0*1e-3;
% no change on b-values, because it will influence the exponential decay
bvalues     = bvalues0;

if(max(bvalues)<= 200)
    fprintf(2,'Important: Please provide b-values in s/mm^2 (typically in the order of 1000).\n');
    error('Fit aborted because b-values were not given in s/mm^2 which might cause numerical problems.'); 
end

bvalues = bvalues / 1000;

%% interpolate all images to the first
%get V and Aall0up
res_hold = -4; % interpolation kernel
V       = spm_vol(PInd);
VS0     = V(1);
bAS0MSK = find(min(bvalues(1,:))==bvalues);
dm      = V(1).dim;
Atmp    = zeros([dm numel(bAS0MSK)]);
kk      = 1;
for j=bAS0MSK
    for p=1:dm(3)
        M = V(j).mat*spm_matrix([0 0 p]);
        Atmp(:,:,p,kk) = spm_slice_vol(V(j),V(bAS0MSK(1)).mat\M,dm(1:2),res_hold);
    end
    kk=kk+1;
end
if(numel(size(Atmp))==4)
    AS0     = mean(Atmp,4);
elseif(numel(size(Atmp))==3)
    AS0     = Atmp;
else
    error('The dimensionality of your images is wrong.');
end
clear Atmp bAS0MSK;


%%
% check whether Freiburg-Tools are set


% clear voxels with negative log(Aall0)

if(~isempty(PMSK))
    if(size(PMSK,1)>1)
        vx  = sqrt(sum(VS0.mat(1:3,1:3).^2));
        if(~exist('smk','var'))
            smk = 5;
        end
        if(~exist('perc','var'))
            perc = 0.7;
        end

        dummy_options.smk   = round(vx.*smk);
        dummy_options.perc  = perc;
        MSK = ACID_make_Bmask(PMSK,[],[],dummy_options);
    else
        AMSKtmp = acid_read_vols(spm_vol(PMSK),VS0,res_hold);
        MSK     = find(AMSKtmp>0);
    end


    AMSK            = zeros(size(AS0));
    AMSK(MSK)       = 1;
    AMSK(AS0<=0)    = 0;
    MSK             = find(AMSK>0);
else
    AMSK    = zeros(size(AS0));
    tmp     = ones(size(AS0));
    MSK1    = find(tmp>0 & AS0>0);
    MSK     = MSK1;
    AMSK(MSK) = 1;
end


%%
    % separate b0s form others
    minb    = min(bvalues(1,:));
    MSK_b   = find(bvalues(1,:)>minb);
    
    b0      = min(bvalues(1,:));
  
    
        [DM0,design] = DTI_Design_Matrix_GN(DiffVecORIG,bvalues(1,:)); % DM0 is the DTI-design-matrix of the measurement. It is built using the b-vectors (gx,gy,gz) and the b-values for each
                                                                            % individual measurement. DM0 is a #measurements x 7 Matrix containing 1's in the last column which represent the
                                                                            % logarithm of the signal acquired for b=0 (=lS0). (see for example Koay et al. doi: 10.1016/j.jmr.2006.06.020) 

    
    sz          = VS0.dim;
    Asym_olsq0  = zeros([sz size(DM0,2)]);
    mDWI        = zeros(sz);
    tmpS0       = zeros(sz);
%    tmp2        = zeros(sz);

    [design_kurtosis] = design_matrix_kurtosis(DiffVecORIG,bvalues(1,:),AMSK);  

    [design_kurtosis_axial_symmetry] = design_matrix_kurtosis_axial_symmetry(DiffVecORIG,bvalues(1,:),AMSK);

    Asym_olsq0     = zeros([sz size(DM0,2)]);
    Asym_olsq00    = zeros([sz size(design_kurtosis,2)]);
    
    
    nParameters         = number_of_fit_parameters(dummy_model);   % model dependent number of fit parameters                      
    Asym_olsq_rice      = zeros(numel(MSK),nParameters);
    
    if (dummy_model ==3)
       Asym_olsq00 = zeros([sz 8]); 
    end
    
    
    if ~isempty(noise_map)
        use_noise_map = 1;
        noise_map = spm_vol(noise_map);
        sigma_map = acid_read_vols(noise_map,noise_map(1),1);
    else
        sigma_map = zeros(sz(1:3)); 
    end
   
    if ~isempty(constraints_map)
        constraints_map = spm_vol(constraints_map);
        beta_map = acid_read_vols(constraints_map,constraints_map(1),1);
    else
        beta_map = zeros(sz(1:3)); 
    end
    
    
%%    


if (exist('parpool','file'))&&(npool>1)
    if isempty(gcp('nocreate'))
        parpool('local',npool); 
        M = npool;   
    else 
        poolobj = gcp('nocreate');
        M = min(poolobj.NumWorkers,npool);
    end
else
    M = 0;
end
   


volume_of_lS = cell(1,sz(3));  %preallocate
volume_of_fit_results = cell(1,sz(3));  %preallocate

 parfor (p = 1:sz(3), M)
     
        disp(['slice:' num2str(p)]);
        
        MSKvol          = zeros(sz);
        MSKvol(:,:,p)   = ones(sz(1:2));
        
        AMSKslice       = AMSK(:,:,p);
        sigma_map_slice = sigma_map(:,:,p);
        beta_map_slice  = beta_map(:,:,p);
        
     if(~isempty(find(MSKvol>0)) && ~isempty(find(AMSKslice>0)))

        [lS,lS0slice]    = acid_dti_get_logS_2D(V,minb,bvalues,p,res_hold);
        volume_of_lS{p} = lS;

        
  %% Gauss Newton Fit Implementation      

%   
  %dummy_RBC: 1= yes, 0= No
        datascale           = 100;     
        measured_signal     = exp(lS)/datascale; 

        parameters = 0.0001*ones(nParameters,size(measured_signal,1));       % initialization of model parameters
 
         if(dummy_model == 3 )                                   
             parameters(3,:) = exp(lS0slice')/datascale;
         elseif(dummy_model == 4 || dummy_model == 6 )
             parameters(1,:) = exp(lS0slice')/datascale;             % 1 Arbitrarily chosen
         elseif(dummy_model == 5 )
             %%%Do nothing
         else
             parameters(7,:) = exp(lS0slice')/datascale;
         end
         
         
         parameters_MSK = parameters(:,AMSKslice>0);
        
         
         fctn = @(parameters_MSK,credit) ACIDObjectiveFunction(parameters_MSK,measured_signal(AMSKslice>0,:),sz,DM0,credit,datascale,design_kurtosis,design_kurtosis_axial_symmetry,sigma,dummy_model,dummy_RBC,DiffVecORIG,bvalues,bvalue_labels,number_of_coils_L,use_noise_map,sigma_map_slice(AMSKslice>0), beta_map_slice(AMSKslice>0)');

         
         
         [m0big,lower,upper] = def_starting_guess_and_bounds(dummy_model,nParameters,measured_signal,lS0slice,datascale,AMSKslice,DiffVecORIG,bvalues0,sz); %define the starting guess and the lower/upper bounds for the algorithm

         
          tolJ = 1e-5;     
          tolG = 1e-4;
          tolU = 1e-4;
 
    
  
         estimated_parameters = ACIDGaussNewton(fctn,m0big(:,AMSKslice>0),'verbose',0,'tolJ',tolJ,'tolG',tolG,'tolU',tolU,'lower',lower,'upper',upper,'maxIter',maxIter,'maxStep',10); % estimated parameters within ACIDGaussNewton = uc
               
         
  
         
         
         if(or(dummy_model ==1, dummy_model ==2))
           
             estimated_parameters(7,:) = log(estimated_parameters(7,:)*datascale);      %The data writing algorithms expects the log of the measured signal ln(S0), so for the non linear case where S0 is estimated the found values need to be onverted.
   
         end
         
         if(dummy_model == 3)
               estimated_parameters(3,:) = log(estimated_parameters(3,:)*datascale);
         end
         
         estimated_parameters = estimated_parameters(:);

          
         tmp3      = [];

         tmp3      = reshape(estimated_parameters,nParameters,[]);
         volume_of_fit_results{p} = tmp3;
%          res.coeff = mi1;       
             
%         clear tmp1  tmp3 tmp4;

    
      end
 end
 
 if (M > 0)
     poolobj = gcp('nocreate');
     delete(poolobj);
 end

 
 %% Write Results to volume
  [Asym_olsq_rice,~,~,mDWI_GN] = write_fit_results_to_volume(Asym_olsq_rice,Asym_olsq0,Asym_olsq00,sz,MSK_b,AMSK,dummy_model,volume_of_fit_results,volume_of_lS);

    if(dummy_model == 2)
         Asym_olsq_rice(:,1:6) = Asym_olsq_rice(:,1:6) /1000;
    elseif(dummy_model == 1)
         Asym_olsq_rice(:,1:6) = Asym_olsq_rice(:,1:6) /1000;
    elseif(dummy_model == 3)
         Asym_olsq_rice(:,1:2) = Asym_olsq_rice(:,1:2) /1000;
    elseif(dummy_model == 4)
        %%% Nothing yet
    elseif(dummy_model == 5)
        %Nothing yet
    end
   
  
  
  
        midfix = def_pre(dummy_model,dummy_RBC); % this is the midfix name term for the meanDWI map
    
        mean_DWI_fix = [midfix 'meanDWI_'];
        my_write_vol_nii(mDWI_GN,VS0,mean_DWI_fix);
    
    

 Dthr = 1e-7;  % diffusivity threshold
 
if(dummy_model == 2)
     write_data_DKI(bvalues,V,AMSK,DiffVecORIG,Asym_olsq_rice,MSK,VS0,AS0,dummy_DT,Dthr,bvalues0,dummy_model,dummy_RBC)
elseif(dummy_model == 1)
     lS0 = tmpS0(AMSK>0);
     write_data_DTI(Asym_olsq_rice,MSK,dummy_DT,VS0,AS0,lS0,V,DM0,bvalues,Dthr,dummy_model,dummy_RBC)
elseif(dummy_model == 3)
    write_data_Axial_Symmetric_DKI(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK,dummy_DT)
elseif(dummy_model == 4)
    write_data_biophysical_model(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK)
elseif(dummy_model == 5)
    write_data_power_law(Asym_olsq_rice,dummy_RBC,VS0,AS0,MSK)
end




