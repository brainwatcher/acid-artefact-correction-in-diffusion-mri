function dout=kummer_numerical_derivative(k,kummer_derivative)


xvec = linspace(0,100,10^5);

tmp = abs(k'-xvec);
[kinx,inx]=min(tmp,[],2);

dout = kummer_derivative(inx);

end