function [minb,MSK_b,b0,b1,b2,bMSK0,bMSK1,bC,bia,bic,binfo,lSW,lS0,X4] = hack_to_make_QL_writing_work(bvalues,V,AMSK,DiffVecORIG)

 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%           
   %The following operations were collected/"hacked" from the DKI-FIT that already
   %existed in the ACID-Toolbox. They are required for
   %"write_DTIdataset_Kurtosis_Leipzig062016_QL" to work
   %Start of Hack
        % separate b0s form others
        
        
        
        
    minb    = min(bvalues(1,:));
    MSK_b   = find(bvalues(1,:)>minb);
    
    b0      = min(bvalues(1,:));
    bMSK0   = find(bvalues(1,:)==b0);
    b1      = min(bvalues(1,bvalues(1,:)>b0));
    bMSK1   = find(bvalues(1,:)==b1);
    b2      = min(bvalues(1,bvalues(1,:)>b1));
    if(isempty(b2)),  error('Not enough b-values! Change to tensor estimation.');end
    bMSK2   = find(bvalues(1,:)==b2);
    bMSK    = find(bvalues(1,:)>b0); 
    [bC,bia,bic] = unique(bvalues(1,bvalues(1,:)>=b2));    
   
    if(numel(bC))
%         for iterb = 1:numel(bC)
        bMSK2   = find(bvalues(1,:)>=b2);
        binfo   = struct('b0',b0,'bMSK0',bMSK0,'b1',b1,'bMSK1',bMSK1,'b2',bC,'bMSK2',bMSK2,'bMSK',bMSK,'bpos',bia,'binx',bic); 
        [lSW,lS0] = callogDW_vstruct_vol(find(AMSK>0),V,binfo.b0,bvalues);      
    end
            
              
        lSW         = bsxfun(@minus,lS0,lSW);
        lSW         = permute(lSW,[2 1]);
        
        lSW         = bsxfun(@rdivide,lSW,bvalues(bvalues>binfo.b0));
        lSW         = permute(lSW,[2 1]);
        
        X4 = creat_DesigneM_4Kurtosis_new(DiffVecORIG,bvalues);  
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%            
   %End of Hack         






end

