function [alpha,bp_2,bp_3,bp_4,bp_5]=find_bp_parameters_WMTI(per_D_image, para_D_image, per_W_image, para_W_image, mean_W_image,dummy_um,dummy_convert_K,branch)


% bp_1 = Kappa = inf. per definition
% bp_2 = De,perpendicular
% bp_3 = Da
% bp_4 = De,parallel
% bp_5 = f

% per_D = 1.048 ;
% para_D = 1.557 ;
% mean_W = 0.330 ;
% per_W = 0.708 ;
% para_W = 0.396;

% per_D = 0.195 ;
% para_D = 1.503 ;
% mean_W = 0.926 ;
% per_W = 0.291 ;
% para_W = 1.456;

    PP=[];
    

    Vol_1=spm_vol(per_D_image); 
    VG = Vol_1(1);
    per_D=acid_read_vols(Vol_1,VG,1);

    Vol_2=spm_vol(para_D_image); 
    para_D=acid_read_vols(Vol_2,VG,1);


    Vol_3=spm_vol(per_W_image); 
    per_W=acid_read_vols(Vol_3,VG,1);

    Vol_4=spm_vol(para_W_image); 
    para_W=acid_read_vols(Vol_4,VG,1);

    Vol_5=spm_vol(mean_W_image); 
    mean_W=acid_read_vols(Vol_5,VG,1);


%     branch = 1;

    MSK = find(abs(nansum(cat(4,per_D, para_D, mean_W, per_W, para_W),4))>1e-6);

    per_D   = per_D(MSK);
    para_D  = para_D(MSK);


    if(dummy_um == 1)

        per_D = per_D*1e3;
        para_D = para_D*1e3;

    end 

    D0=1/3.*(2.*per_D+para_D);
    mean_W  = mean_W(MSK);
    per_W   = per_W(MSK);
    para_W  = para_W(MSK);



    if(dummy_convert_K == 1)

        per_W = per_W.*( (per_D.^2)./ (D0.^2) );
        para_W = para_W.*( (para_D.^2)./ (D0.^2) );

    end




bp_5 = 1./ (1 + 3 .*  ( (per_D.^2) ./ (per_W  .* D0.^2 ))) ;

bp_2 = per_D ./ (1 - bp_5) ;

if (branch == 1)
    
bp_4 = para_D - (2/3) .*  ( bp_5 ./ (1-bp_5 )) .* ( per_D + sqrt( ( 15.* (1 - bp_5 ) ./ (4.* bp_5)) .* D0.^2 .* mean_W - 5 .* per_D .^ 2) ) ; 

bp_3 = para_D - (2/3) .*  ( per_D - sqrt( ( 15.* (1 - bp_5 ) ./ (4.* bp_5)) .* D0.^2 .* mean_W - 5 .* per_D .^ 2) ) ; 

elseif (branch == 2) 
    
bp_4 = para_D - (2/3) .*  ( bp_5 ./ (1-bp_5 )) .* ( per_D - sqrt( ( 15.* (1 - bp_5 ) ./ (4.* bp_5)) .* D0.^2 .* mean_W - 5 .* per_D .^ 2) ) ; 

bp_3 = para_D - (2/3) .*  ( per_D + sqrt( ( 15.* (1 - bp_5 ) ./ (4.* bp_5)) .* D0.^2 .* mean_W - 5 .* per_D .^ 2) ) ; 

end


     
    Atmp = zeros(VG.dim);
    Atmp(MSK) = bp_2; 
    prefix = ['WMTI_De_perp_Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = bp_3;
    prefix = ['WMTI_Da_Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = bp_4;
    prefix = ['WMTI_De_par_Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = bp_5;
    prefix = ['WMTI_f_Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    




