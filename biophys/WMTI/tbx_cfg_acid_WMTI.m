function acid_WMTI = tbx_cfg_acid_WMTI

%% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%% define variable: Radial
in_Radial         = cfg_files;
in_Radial.tag     = 'in_Radial';
in_Radial.name    = 'Perpendicular Diffusivity image';
in_Radial.help    = {'Select the Perpendicular Diffusivity image. The ACID output filename for this map starts with "Radial.." (not to be confused with the filename "IRadial..") for the ordinary least squares method. If you have chosen the axial symmetric fit, the output filename starts with "Axial_symmetric_D_perp...".'};
in_Radial.filter = 'image';
in_Radial.ufilter = '.*';
in_Radial.num     = [0 1];

%% define variable: Axial
in_Axial         = cfg_files;
in_Axial.tag     = 'in_Axial';
in_Axial.name    = 'Parallel Diffusivitiy image';
in_Axial.help    = {'Select the Parallel Diffusivity image. The ACID output filename for this map starts with "Axial.." for the ordinary least squares method. If you have chosen the axial symmetric fit, the output filename starts with "Axial_symmetric_D_para...".'};
in_Axial.filter = 'image';
in_Axial.ufilter = '.*';
in_Axial.num     = [0 1];

%% define variable: Wpara
in_Wpara         = cfg_files;
in_Wpara.tag     = 'in_Wpara';
in_Wpara.name    = 'Parallel Kurtosis image';
in_Wpara.help    = {'Select the Parallel Kurtosis image. The ACID output filename for this map starts with "Kparallel.." for the ordinary least squares method. The ACID ordinary least squares fit estimates the apparent kurtosis which need to be converted (chose dummy_WK=1 in this case). If you have chosen the axial symmetric fit, the output filename starts with "Axial_symmetric_W_para...", these are the kurtosis tensor variables that need not be converted (chose dummy_WK=0 in this case).'};
in_Wpara.filter = 'image';
in_Wpara.ufilter = '.*';
in_Wpara.num     = [0 1];


%% define variable: Werp
in_Wperp         = cfg_files;
in_Wperp.tag     = 'in_Wperp';
in_Wperp.name    = 'Perpedicular Kurtosis image';
in_Wperp.help    = {'Select the Perpendicular Kurtosis image. The ACID output filename for this map starts with "Kperp.." for the ordinary least squares method. These are the apparent kurtosis parameters which need to be converted (chose dummy_WK=1 in this case). If you have chosen the axial symmetric fit, the output filename starts with "Axial_symmetric_W_perp...", these are the kurtosis tensor variables that need not be converted (chose dummy_WK=0 in this case).'};
in_Wperp.filter = 'image';
in_Wperp.ufilter = '.*';
in_Wperp.num     = [0 1];
%% define variable: Wmean
in_Wmean         = cfg_files;
in_Wmean.tag     = 'in_Wmean';
in_Wmean.name    = 'Mean Kurtosis image';
in_Wmean.help    = {'Select the Mean Kurtosis image. The ACID output filename for this map starts with "MK.." for the ordinary least squares method. The ACID ordinary least squares fit estimates the apparent kurtosis which need to be converted (chose dummy_WK=1 in this case). If you have chosen the axial symmetric fit, the output filename starts with "Axial_symmetric_W_mean...", these are the kurtosis tensor variables that need not be converted (chose dummy_WK=0 in this case).'};
in_Wmean.filter = 'image';
in_Wmean.ufilter = '.*';
in_Wmean.num     = [0 1];

%% define variable: white matter mask
% in_msk         = cfg_files;
% in_msk.tag     = 'in_msk';
% in_msk.name    = 'White matter mask';
% in_msk.help    = {'Select a binary white matter mask.'};
% in_msk.filter = 'image';
% in_msk.ufilter = '.*';
% in_msk.num     = [0 1];


% ---------------------------------------------------------------------
% dummy variable for changing diffusivity values from mm�/s to um�/ms 
% ---------------------------------------------------------------------
dummy_um         = cfg_entry;
dummy_um.tag     = 'in_dummy_um';
dummy_um.name    = 'dummy_um';
dummy_um.help    = {'Here, the units of the parallel and perpendicular diffusivities can be change from mm�/s to um�/ms, please chose 1 if you want to convert them to um�/ms.'};
dummy_um.strtype = 'e';
dummy_um.num     = [0 1];
dummy_um.val     = {0};
% ---------------------------------------------------------------------
% Conversion of K to W
% ---------------------------------------------------------------------
dummy_WK         = cfg_entry;
dummy_WK.tag     = 'in_dummy_WK';
dummy_WK.name    = 'dummy_WK';
dummy_WK.help    = {'Dummy variable to change from K (=apparent kurtosis) to W(=kurtosis tensor) variables. If your kurtosis values are W chose 0, if they are K chose 1. (Please check doi:10.1016/j.neuroimage.2016.08.022 for more details).'};
dummy_WK.strtype = 'e';
dummy_WK.num     = [0 1];
dummy_WK.val     = {0};
% ---------------------------------------------------------------------
% Branch
% ---------------------------------------------------------------------
dummy_branch         = cfg_entry;
dummy_branch.tag     = 'in_dummy_branch';
dummy_branch.name    = 'dummy_branch';
dummy_branch.help    = {'Chose the branch (1 or 2).'};
dummy_branch.strtype = 'e';
dummy_branch.num     = [0 1];
dummy_branch.val     = {1};

%% noddi dti
acid_WMTI         = cfg_exbranch;
acid_WMTI.tag     = 'acid_WMTI';
acid_WMTI.name    = 'WMTI';
acid_WMTI.val     = {in_Radial in_Axial in_Wperp in_Wpara in_Wmean dummy_um dummy_WK dummy_branch};
acid_WMTI.help    = {
                    'This Function uses the WMTI approximation (Kappa=axon dispersion=infinity) to compute the Biophysical Parameters f (axonal water fraction), Da (intra-axonal diffusivity), De_perp (perpendicular extra-axonal diffusivity) and De_par (parallel extra-axonal diffusivity).'
                    'The output parameter map names will start with "WMTI", then comes the map�s name (e.g. "Kappa") and finally the name of the perpendicular diffusivity map you have put into the algorithm.'
                    'The estimation is based upon the work of Jespersen et al. (2018) (https://doi.org/10.1016/j.neuroimage.2017.08.039).'
};
acid_WMTI.prog = @local_acid_WMTI;
% acid_biophysical_params.vout = @vout_acid_biophysical_params;

%% dependencies
function out = local_acid_WMTI(job)
find_bp_parameters_WMTI(char(job.in_Radial),char(job.in_Axial),char(job.in_Wperp),char(job.in_Wpara),char(job.in_Wmean), job.in_dummy_um, job.in_dummy_WK, job.in_dummy_branch);
out=3;


% function dep = vout_acid_biophysical_params(job)
% dep(1)            = cfg_dep;
% dep(1).sname      = 'Kappa image';
% dep(1).src_output = substruct('.','kappa');
% dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
% dep(2).sname      = 'icvf image';
% dep(2).src_output = substruct('.','icvf');
% dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

