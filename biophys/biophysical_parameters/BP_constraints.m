
function [c,ceq] = BP_constraints(x,c1,c2,c3,c4,branch,array_p2,xvec_p2)


%x is the unknown parameter, which is to be numerically determined


% c(1)=((intra_para_D-extra_para_D)./extra_per_D)-(4+sqrt(40/3));  % conditions from jespersen et al 2018
% c(2)=(4-sqrt(40/3))-((intra_para_D-extra_para_D)./extra_per_D);  % conditions from jespersen et al 2018

%  p4in = (1/(32.*((x(1)).^2))*(105+(12.*(x(1))).*(5+(x(1)))+(5.*sqrt((x(1))).*(2.*(x(1))-21))./(dawson_behnam(sqrt((x(1)))))));
 p2in = p2_numeric(x(1),array_p2,xvec_p2);%(0.25.*((3./(sqrt((x(1))).*dawson_behnam(sqrt((x(1))))))-2-3./(x(1))));

 M1 = c1 ;
 M2 = c2./p2in;
 M3 = c3 ;
 M4 = c4./p2in;
 
a = ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) )^2- ( (7/3) + 2*M2./ ( (1/3).*(M1-M2) )) .* ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ) + M4 ./ ((1/9).* (M1-M2)^2); % "c" needed another name because "c" is reserved for the constraints
the_c_in_the_BP_formulas =   ( ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ) - 5 - M2 ./ ( (1/3) .* (M1- M2) ) ) ^2;
f1 = ( (-40 / 3) + a + the_c_in_the_BP_formulas - ( -4.*a.* the_c_in_the_BP_formulas +( (40/3)-a- the_c_in_the_BP_formulas )^2)^0.5) ./ (2.*a);
f2 = ( (-40 / 3) + a + the_c_in_the_BP_formulas + ( -4.*a.* the_c_in_the_BP_formulas +( (40/3)-a- the_c_in_the_BP_formulas )^2)^0.5) ./ (2.*a);

if branch == 1
 %this is for f=f2
 f=f2;
else
%this is for f=f1  
f=f1;
end

D_bar =  ( (1/3) * (M1-M2) ) ;
per_De = D_bar ./ (1-f);
d_a = ( ( ( M3./( (1/9).*(M1-M2)^2 ) - M4./ ( (1/9).*(M1-M2)^2 ) ).* (1-f)-5- ( M2 ./ ( (1/3).*(M1-M2))) ) ./ (-f) ) ;
D_a = d_a .* D_bar ;
para_De = ( ( ( M2 ./ ( (1/3).*(M1-M2))) - f.* d_a ) ./ (1-f) ) .* D_bar + per_De;





eps_sia=1e-6;

% c(1) = ((D_a - para_De)./per_De) - 4 - (40/3)^0.5 + eps_sia;  % branch dependent constraints found in jespersen et. al. "diffusion time dependence of microstructural parameters in fixed spinal cord"
% c(2) = 4 - (40/3)^0.5 - ((D_a - para_De)./per_De) + eps_sia;  % branch dependent constraints found in jespersen et. al. "diffusion time dependence of microstructural parameters in fixed spinal cord"
% c(5) = f-1;
% c(6) = -f;
c(1) = 4.*a.* the_c_in_the_BP_formulas -( (40/3) -a - the_c_in_the_BP_formulas ).^2; % so that the objective function (more precise : f ) does not become complex
% 




if branch ==1 % these conditions are to make sure 0 =< f =< 1 
    c(2) = (40/3) - a ;
    c(3) = -(1/3) * (3*a-4* sqrt(30) * sqrt(a) +40 )+the_c_in_the_BP_formulas ;
elseif branch == 2
    c(2) = (40/3) - a ;
    c(3) = -(1/3) * (3*a-4* sqrt(30) * sqrt(a) +40 )+the_c_in_the_BP_formulas ;
    c(4) = -the_c_in_the_BP_formulas;
end

    
% if branch==1 % nu=1:
%  c(3)=para_De + eps_sia - D_a ;
%  c(4)=eps_sia - para_De ; 
% else
%  % branch nu=-1:
%  c(3)=D_a + eps_sia - para_De;
%  c(4)=eps_sia-D_a;
% end



ceq=[];



end
