  
function [Dc,para,dD,H,Hstar] = BP_GN_ObjectiveFunction(parameters,data,c1,c2,c3,c4,c5,branch,credit,array_p2,array_p4,array_dawson,xvec_p2,xvec_p4,xvec_dawson)
%
% Input:
%
% parameters - this is an array which contains in its columns the model's
% parameters which are to be found (here, for example, it would be the 6 diffusion
% tensor's parameters and b=0 weighted signal strength for DTI)
%
% measured_signal - this is a "number of active voxels" x "number of
% acquired diffusion weighted images" array containting the
% measured_signals
%
% credit - specifies indices of the voxels that are being worked on actively
%
% biasParams=[sigma,L] these are the parameters being used for the Rician
% noise correction, sigma is the noise level (= standard deviation) L is
% the number of receiver coil systems (2 coils in each system) used in the signal acquisition 


if  isempty(credit) 
    credit = 1:size(data,1); 
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


 
ndata      = size(data,2);                                       %this is the number of data points for every voxel

parameters = parameters(:,credit); 

nvoxel     = size(parameters,2);                                 %this is the number of active voxels                                                                            %and "parameters" has the form: "number-of-parameters"x"number-of-voxles"

 doDerivative = (nargout > 2);
 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                                                                            % Here the measured data are being modeled, "pred" is a prediciton based
                                                                            % upon the model's parameters. The model's parameters are iteratively being
                                                                            % changed in order to make the model fit the measured signals better and
                                                                            % better. "pred" must have the same size and structure as "data"(=that which is being modeled).

pred = Objective_Kappa(parameters,c1(credit,:),c2(credit,:),c3(credit,:),c4(credit,:),c5(credit,:),branch,array_p2,array_p4,array_dawson,xvec_p2,xvec_p4,xvec_dawson);  

pred = pred';  
data = data';
data = data(:,credit);

res  = -1*(data-pred);                                                      %these are the residuals, the -1 here is important for the search direction (=dD)
Dcs  = sum(res.^2,1);                                                       %first sum of residuals
Dc   = 0.5*sum(Dcs);                                                        %this is the least squares sum which is to be minimized



para = struct('omega',[],'m',[],'nvoxel',nvoxel,'Dc',Dc,'Dcs',Dcs,'Rc',0.0,'res',res); %hier könnte ich die schwarzen Namen ändern, wenn ich sie in dieser Routine änderen möchte. Die rosa Namen müssen gleich bleiben, weil diese sich auf die Struktur beziehen, mit der ProjGaussNewton arbeitet)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if not(doDerivative)    
     
    dD = [];
    H  = []; 

else
    

        jacobi_i      =   BP_Kappa_Jacobian(parameters,c1,c2,c3,c4,c5); %21.04.2020: Bis hier hin gekommen;% here the Jacobians for each individual voxel are being computed for the different cases (DTI/DKI, non-linear/linear)
        jacobi_matrix = blkdiag(jacobi_i{1,:});                         %here the Jacobian for the whol image slice (each individual voxel) is being constructed by putting 

        dD                      = jacobi_matrix'*(res(:));
        para.dDs                = reshape(dD,size(parameters,1),[]); 


    
     Hstar = jacobi_matrix'*jacobi_matrix;                                  %This is the Hessian-Matrix which is used for finding the search direction (= in what direction in the parameter space should the algorithm vary the parameters

     H  = Hstar + 1e-5*speye(size(parameters,2)*size(parameters,1));        %Axial Symmetric DKI-Model
    
 end





