function [bp_1,bp_2,bp_3,bp_4,bp_5]=find_bp_parameters_memo_GN_Algorithm(per_D,para_D,mean_W,per_W,para_W,dummy_um,dummy_convert_K,sz,AMSK)
% bp_1 = Kappa
% bp_2 = De,perpendicular
% bp_3 = Da
% bp_4 = De,parallel
% bp_5 = f




for p = 1:sz(3) 
    
 disp(['slice:' num2str(p)]);
 
  MSKvol          = zeros(sz);                                        
  MSKvol(:,:,p)   = ones(sz(1:2));                                   
%   AMSKslice       = AMSK(:,:,p);

  MSK_temporary_only_for_getting_to_work_the_implementation = find(abs(nansum(cat(4,per_D(:,:,p), para_D(:,:,p), mean_W(:,:,p), per_W(:,:,p), para_W(:,:,p)),4))>1e-6);
 
  if ~isempty(MSK_temporary_only_for_getting_to_work_the_implementation)
      
  nParameters = 1;
  data = zeros(sz(1)*sz(2),1);
  parameters = 0.001*ones(nParameters,size(data,1));  
  parameters_MSK=parameters(:,MSK_temporary_only_for_getting_to_work_the_implementation);

  
  
% define branch
branch=1;

    if(dummy_um == 0)

        per_D = per_D*1e3;
        para_D = para_D*1e3;

    end  
    
% MSK = find(abs(nansum(cat(4,per_D, para_D, mean_W, per_W, para_W),4))>1e-6);
per_D = per_D(:,:,p);
para_D = para_D(:,:,p);
mean_W = mean_W(:,:,p);
per_W = per_W(:,:,p);
para_W = para_W(:,:,p);

per_D   = per_D(MSK_temporary_only_for_getting_to_work_the_implementation);
para_D  = para_D(MSK_temporary_only_for_getting_to_work_the_implementation);
D0=1/3.*(2.*per_D+para_D);

mean_W  = mean_W(MSK_temporary_only_for_getting_to_work_the_implementation);

per_W   = per_W(MSK_temporary_only_for_getting_to_work_the_implementation);
para_W  = para_W(MSK_temporary_only_for_getting_to_work_the_implementation);

    if(dummy_convert_K == 1)

        per_W = per_W.*( (per_D.^2)./ (D0.^2) );
        para_W = para_W.*( (para_D.^2)./ (D0.^2) );

    end


D2 = 2/3.*(para_D- per_D);
W0 = mean_W;
W2 = 1/7.*(3.*para_W+5.*mean_W-8.*per_W);
W4 = 4/7.*(para_W-3*mean_W+2*per_W);
 
 
% disp(['D,parallel =' num2str(para_D)]);
% disp(['D,perpendicular =' num2str(per_D)]);
% disp(['W,parallel =' num2str(para_K)]);
% disp(['W,perpendicular =' num2str(per_K)]);
% disp(['Wmean =' num2str(mean_W)]);





c1 = 3.*D0;
c2 = (3/2).*D2;
c3 = D2.^2+5.*D0.^2.*(1+W0./3);
c4 = 1/2.*D2.*(D2+7.*D0)+(7/12*W2.*D0.^2);
c5 = 9/4*D2.^2+35/24*W4.*D0.^2.; 

sz = numel(D0);  
x0      = zeros(sz,1);

%  Vol_1=(spm_vol(spm_select(1,'image','Select Kappa Map for the initial guess'))); 
 VG = Vol_1(1);
 kappa_initial_guess = acid_read_vols(Vol_1,VG,1);
 kappa_initial_guess = kappa_initial_guess(MSK_temporary_only_for_getting_to_work_the_implementation);
 
 m0big = 0.001*ones(nParameters,size(data,1)); 
 m0big(MSK_temporary_only_for_getting_to_work_the_implementation) = kappa_initial_guess;
 tolerance = 1e-5;
 maxIter   = 75; 
 
 lower=-Inf*ones(size(m0big(MSK_temporary_only_for_getting_to_work_the_implementation))); %lower is the lower bound for the parameters to be found by the algorithm
 upper=Inf*ones(size(m0big(MSK_temporary_only_for_getting_to_work_the_implementation)));  %upper is the upper bound for the parameters to be found by the algorithm
       
 
 fctn = @(parameters_MSK,credit) BP_GN_ObjectiveFunction(parameters_MSK,data(MSK_temporary_only_for_getting_to_work_the_implementation,:),c1,c2,c3,c4,c5,branch,credit);

 mi1 = BP_GaussNewton(fctn,m0big(:,MSK_temporary_only_for_getting_to_work_the_implementation),'verbose',1,'tolJ',tolerance,'tolG',tolerance,'tolU',tolerance,'lower',lower,'upper',upper,'maxIter',maxIter); % in diesen Gauss-Newton Algorithmus tut man die ObjectiveFunction und die Startwerte rein und bekommt die geschätzten Parameter (=uc) heraus


    end
end





X = zeros(5,sz);


for inx=1:length(cX)
    X(1,inx)    = cX{inx}; 

 p4in(inx) = p4_numeric(X(1,inx));%(1/(32.*((X(1,inx)).^2))*(105+(12.*(X(1,inx))).*(5+(X(1,inx)))+(5.*sqrt((X(1,inx))).*(2.*(X(1,inx))-21))./(dawson(sqrt((X(1,inx)))))));
 p2in(inx) = p2_numeric(X(1,inx));%(0.25.*((3./(sqrt((X(1,inx))).*dawson(sqrt((X(1,inx))))))-2-3./(X(1,inx))));

 M1(inx) = c1(inx) ;
 M2(inx) = c2(inx)./p2in(inx);
 M3(inx)= c3(inx) ;
 M4(inx)= c4(inx)./p2in(inx);
 
a(inx) = ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) )^2- ( (7/3) + 2*M2(inx)./ ( (1/3).*(M1(inx)-M2(inx)) )) .* ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) + M4(inx) ./ ((1/9).* (M1(inx)-M2(inx))^2);
c(inx)=   ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) - 5 - M2(inx) ./ ( (1/3) .* (M1(inx)- M2(inx)) ) ) .^2;
f1(inx) = ( (-40 ./ 3) + a(inx) +c(inx)- ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));
f2(inx) = ( (-40 ./ 3) + a(inx) +c(inx)+ ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));

if branch == 1
 f(inx)=f2(inx);
else
 f(inx)=f1(inx);
end

D_bar(inx) =  ( (1/3) * (M1(inx)-M2(inx)) ) ;


X(2,inx) = D_bar(inx) ./ (1-f(inx)); % per_De
X(3,inx) = ( ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ).* (1-f(inx))-5- ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) ) ./ (-f(inx)) ) .* D_bar(inx) ; % D_a
X(4,inx) = ( ( ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) - f(inx).* ( X(3,inx)./D_bar(inx) ) ) ./ (1-f(inx)) ) .* D_bar(inx) + X(2,inx); % para_De
X(5,inx) = f(inx);

end
 

bp_1 = X(1,:);
bp_2 = X(2,:);
bp_3 = X(3,:);
bp_4 = X(4,:);
bp_5 = X(5,:);


% disp(['f =' num2str(X(5,1))]);
% disp(['Da =' num2str(X(3,1))]);
% disp(['De,parallel =' num2str(X(4,1))]);
% disp(['De,perpendicular =' num2str(X(2,1))]);
% disp(['kappa =' num2str(X(1,1))]);

end



 


