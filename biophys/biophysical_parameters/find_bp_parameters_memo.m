function [bp_1,bp_2,bp_3,bp_4,bp_5,MSK_good_kappa_vals]=find_bp_parameters_memo(per_D,para_D,mean_W,per_W,para_W,dummy_um,npool,dummy_convert_K,MSK,branch,array_p2,array_p4,xvec_p2,xvec_p4)
% bp_1 = Kappa
% bp_2 = De,perpendicular
% bp_3 = Da
% bp_4 = De,parallel
% bp_5 = f






    if(dummy_um == 0)

        per_D = per_D*1e3;
        para_D = para_D*1e3;

    end
    
    if isempty(MSK)
        MSK = find(abs(nansum(cat(4,per_D, para_D, mean_W, per_W, para_W),4))>1e-6);
    end
    
    per_D   = per_D(MSK);
    para_D  = para_D(MSK);
    D0=1/3.*(2.*per_D+para_D);

    mean_W  = mean_W(MSK);

    per_W   = per_W(MSK);
    para_W  = para_W(MSK);

    if(dummy_convert_K == 1)

        per_W = per_W.*( (per_D.^2)./ (D0.^2) );
        para_W = para_W.*( (para_D.^2)./ (D0.^2) );

    end


D2 = 2/3.*(para_D- per_D);
W0 = mean_W;
W2 = 1/7.*(3.*para_W+5.*mean_W-8.*per_W);
W4 = 4/7.*(para_W-3*mean_W+2*per_W);
 






c1 = 3.*D0;
c2 = (3/2).*D2;
c3 = D2.^2+5.*D0.^2.*(1+W0./3);
c4 = 1/2.*D2.*(D2+7.*D0)+(7/12*W2.*D0.^2);
c5 = 9/4*D2.^2+35/24*W4.*D0.^2.; 





sz = numel(D0);  



%%%%%%%%%%%%%% Activate to explore objective function


%  plot_objective_function_to_explore(per_D,para_D,mean_W,para_W,per_W,branch)

%%%%%%%%%%%%%%%%%%%%%




%% start parallel computing
if (exist('parpool','file'))&&(npool>1)
    if isempty(gcp('nocreate'))
        parpool('local',npool); 
        M = npool;   
    else 
        poolobj = gcp('nocreate');
        M = min(poolobj.NumWorkers,npool);
    end
else
    M = 0;
end


parfor (inx = 1:numel(c1), M)
    if mod(inx,250) == 0
     disp(inx)
    end
  
   exactness = 1000;

   [cX{inx}] = find_minimum_BP(xvec_p2,xvec_p4,array_p2,array_p4,c1(inx),c2(inx),c3(inx),c4(inx),c5(inx),0,50,exactness,branch); 
       
end

if (M > 0)
     poolobj = gcp('nocreate');
     delete(poolobj);
end


X = zeros(5,sz);
    
    for o=1:numel(cX) % this has to be done because if the objective function is sampled (dummy_Fit_Method == 3), sometimes there are multiple kappa values
        
        if isempty(cX{o})
            cX{o} = NaN;
        else
            tmp=cX{o};

            tmp=tmp(1);
            cX{o}=tmp;
        end
    end

for inx=1:length(cX)
    
    if or( isempty(cX{inx}) , (isnan(cX{inx})))
        X(1,inx) = NaN;
        X(2,inx) = NaN; % per_De
        X(3,inx) = NaN ; % D_a
        X(4,inx) = NaN; % para_De
        X(5,inx) = NaN;
        
    else
                X(1,inx)    = cX{inx}; 

         p4in(inx) = p4_numeric(X(1,inx),array_p4,xvec_p4,0,70,100000);%(1/(32.*((X(1,inx)).^2))*(105+(12.*(X(1,inx))).*(5+(X(1,inx)))+(5.*sqrt((X(1,inx))).*(2.*(X(1,inx))-21))./(dawson(sqrt((X(1,inx)))))));
         p2in(inx) = p2_numeric(X(1,inx),array_p2,xvec_p2,0,70,100000);%(0.25.*((3./(sqrt((X(1,inx))).*dawson(sqrt((X(1,inx))))))-2-3./(X(1,inx))));

         M1(inx) = c1(inx) ;
         M2(inx) = c2(inx)./p2in(inx);
         M3(inx)= c3(inx) ;
         M4(inx)= c4(inx)./p2in(inx);

        a(inx) = ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) )^2- ( (7/3) + 2*M2(inx)./ ( (1/3).*(M1(inx)-M2(inx)) )) .* ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) + M4(inx) ./ ((1/9).* (M1(inx)-M2(inx))^2);
        c(inx)=   ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ) - 5 - M2(inx) ./ ( (1/3) .* (M1(inx)- M2(inx)) ) ) .^2;
        f1(inx) = ( (-40 ./ 3) + a(inx) +c(inx)- ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));
        f2(inx) = ( (-40 ./ 3) + a(inx) +c(inx)+ ( -4.*a(inx).*c(inx)+( (40/3)-a(inx)-c(inx)).^2).^0.5) ./ (2.*a(inx));

        if branch == 1
         f(inx)=f2(inx);
        else
         f(inx)=f1(inx);
        end

        D_bar(inx) =  ( (1/3) * (M1(inx)-M2(inx)) ) ;


        X(2,inx) = D_bar(inx) ./ (1-f(inx)); % per_De
        X(3,inx) = ( ( ( M3(inx)./( (1/9).*(M1(inx)-M2(inx))^2 ) - M4(inx)./ ( (1/9).*(M1(inx)-M2(inx))^2 ) ).* (1-f(inx))-5- ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) ) ./ (-f(inx)) ) .* D_bar(inx) ; % D_a
        X(4,inx) = ( ( ( M2(inx) ./ ( (1/3).*(M1(inx)-M2(inx)))) - f(inx).* ( X(3,inx)./D_bar(inx) ) ) ./ (1-f(inx)) ) .* D_bar(inx) + X(2,inx); % para_De
        X(5,inx) = f(inx);
    end
end
 


bp_1 = X(1,:);
bp_2 = X(2,:);
bp_3 = X(3,:);
bp_4 = X(4,:);
bp_5 = X(5,:);


MSK_tmp = find(imag(bp_1) == 0);
MSK_good_kappa_vals = zeros(size(bp_1));
MSK_good_kappa_vals(MSK_tmp) = 1;
MSK_good_kappa_vals = logical(MSK_good_kappa_vals);



end



 


