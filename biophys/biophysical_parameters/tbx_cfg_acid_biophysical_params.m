function acid_biophysical_params = tbx_cfg_acid_biophysical_params

%% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%% define variable: Radial
in_Radial         = cfg_files;
in_Radial.tag     = 'in_Radial';
in_Radial.name    = 'Perpendicular Diffusivity image';
in_Radial.help    = {'Select the Perpendicular Diffusivity image either obtained with standard or axial symmetric DKI. The ACID output filename for this map is "Dperp".'};
in_Radial.filter = 'image';
in_Radial.ufilter = '.*';
in_Radial.num     = [0 1];

%% define variable: Axial
in_Axial         = cfg_files;
in_Axial.tag     = 'in_Axial';
in_Axial.name    = 'Parallel Diffusivitiy image';
in_Axial.help    = {'Select the Parallel Diffusivity image either obtained with standard or axial symmetric DKI. The ACID output filename for this map is "Dparallel".'};
in_Axial.filter = 'image';
in_Axial.ufilter = '.*';
in_Axial.num     = [0 1];

%% define variable: Wpara
in_Wpara         = cfg_files;
in_Wpara.tag     = 'in_Wpara';
in_Wpara.name    = 'Parallel Kurtosis image';
in_Wpara.help    = {'Select the Parallel Kurtosis image either obtained with standard or axial symmetric DKI. Use either "Kparallel" or "Wparallel", if you use "Kparallel", you have to change from K to W with the menu option below in this GUI.'};
in_Wpara.filter = 'image';
in_Wpara.ufilter = '.*';
in_Wpara.num     = [0 1];


%% define variable: Werp
in_Wperp         = cfg_files;
in_Wperp.tag     = 'in_Wperp';
in_Wperp.name    = 'Perpedicular Kurtosis image';
in_Wperp.help    = {'Select the Perpendicular Kurtosis image either obtained with standard or axial symmetric DKI. Use either "Kperp" or "Wperp", if you use "Kperp", you have to change from K to W with the menu option below in this GUI.'};
in_Wperp.filter = 'image';
in_Wperp.ufilter = '.*';
in_Wperp.num     = [0 1];
%% define variable: Wmean
in_Wmean         = cfg_files;
in_Wmean.tag     = 'in_Wmean';
in_Wmean.name    = 'Wmean (mean kurtosis) Image';
in_Wmean.help    = {'Select the Mean Kurtosis image either obtained with standard or axial symmetric DKI. It is highly recommended to use the "Wmean" map. "MK" and "Wmean" are similar but not identical and cannot be transformed.'};
in_Wmean.filter = 'image';
in_Wmean.ufilter = '.*';
in_Wmean.num     = [0 1];



% define variable: white matter mask
in_msk         = cfg_files;
in_msk.tag     = 'in_msk';
in_msk.name    = 'Region of interest image (or done for none)';
in_msk.help    = {'Select a binary mask for computation of the biophysical parameters in a particular region of interest (ROI).'};
in_msk.filter = 'image';
in_msk.ufilter = '.*';
in_msk.num     = [0 1];



% ---------------------------------------------------------------------
% dummy variable for changing diffusivity values from mm�/s to um�/ms 
% ---------------------------------------------------------------------
dummy_um         = cfg_entry;
dummy_um.tag     = 'in_dummy_um';
dummy_um.name    = 'Change diffusivity units from mm^2/s to um^2/ms';
dummy_um.help    = {'The parallel and perpendicular diffusivities have to be in um^2/ms, if they are chose the value 1, if they are in mm^2/s chose the value 0.'};
dummy_um.strtype = 'e';
dummy_um.num     = [0 1];
dummy_um.val     = {1};
% ---------------------------------------------------------------------
% number of processors for parallel computing
% ---------------------------------------------------------------------
npool         = cfg_entry;
npool.tag     = 'in_npool';
npool.name    = 'Parallel programming';
npool.help    = {'Choose the number of workers (CPU cores) you would like to use for parameter estimation.'};
npool.strtype = 'e';
npool.num     = [0 1];
npool.val     = {1};
% ---------------------------------------------------------------------
% Conversion of K to W
% ---------------------------------------------------------------------
dummy_WK         = cfg_entry;
dummy_WK.tag     = 'in_dummy_WK';
dummy_WK.name    = 'Change the kurtosis variables from K to W';
dummy_WK.help    = {'Dummy variable to change "Kparallel" and "Kperp" (="apparent kurtosis" variables) to "Wparallel" and "Wperp" (="kurtosis tensor" variables). Chose 0, if your kurtosis variables are W already, chose 1 if they are K. (Check doi:10.1016/j.neuroimage.2016.08.022 for more details on W and K).'};
dummy_WK.strtype = 'e';
dummy_WK.num     = [0 1];
dummy_WK.val     = {0};
% ---------------------------------------------------------------------
% Branch
% ---------------------------------------------------------------------
dummy_branch         = cfg_entry;
dummy_branch.tag     = 'in_dummy_branch';
dummy_branch.name    = 'Chose the biophysical branch you would like to estimate';
dummy_branch.help    = {'Chose the branch, 1 = "+ branch" or 2 ="- branch".'};
dummy_branch.strtype = 'e';
dummy_branch.num     = [0 1];
dummy_branch.val     = {1};


%% noddi dti
acid_biophysical_params         = cfg_exbranch;
acid_biophysical_params.tag     = 'acid_biophysical_parameters';
acid_biophysical_params.name    = 'Experimental stage: estimate biophysical parameters based on axial symmetric DKI parameters';
acid_biophysical_params.val     = {in_Radial in_Axial in_Wperp in_Wpara in_Wmean dummy_um npool dummy_WK in_msk dummy_branch};
acid_biophysical_params.help    = {
                    'This function computes the biophysical parameters Kappa (axon dispersion), f (axonal water fraction), Da (intra-axonal diffusivity), De_perp (perpendicular extra-axonal diffusivity) and De_par (parallel extra-axonal diffusivity) based on the axial symmetric DKI parameters.'
                    'The axial symmetric DKI parameters can be obtained by both the axial symmetric DKI fit, as well as the standard DKI fit where they are calculated as aggregate, for example, the perpendicular diffusivity can be calculated as (lambda2+lambda3)/2, where lambda are the diffusion tensor´s eigenvalues.'
                    'The output parameter map names will start with "BP", then comes the map´s name (e.g. "Kappa") and finally the name of the perpendicular diffusivity map you have put into the algorithm.'
                    'The estimation is based upon the work of Jespersen et al. (2018)(doi: http://dx.doi.org/10.1016/j.neuroimage.2017.08.039) and Novikov et al. (2018) (https://doi.org/10.1016/j.neuroimage.2018.03.006).'
};
acid_biophysical_params.prog = @local_acid_biophysical_params;
% acid_biophysical_params.vout = @vout_acid_biophysical_params;

%% dependencies
function out = local_acid_biophysical_params(job)
Estimate_BP_ACID(char(job.in_Radial),char(job.in_Axial),char(job.in_Wperp),char(job.in_Wpara),char(job.in_Wmean), job.in_dummy_um , job.in_npool, job.in_dummy_WK, char(job.in_msk), job.in_dummy_branch );
out=3;


% function dep = vout_acid_biophysical_params(job)
% dep(1)            = cfg_dep;
% dep(1).sname      = 'Kappa image';
% dep(1).src_output = substruct('.','kappa');
% dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
% dep(2).sname      = 'icvf image';
% dep(2).src_output = substruct('.','icvf');
% dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

