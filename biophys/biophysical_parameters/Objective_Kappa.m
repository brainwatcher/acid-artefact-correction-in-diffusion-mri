function [pred] = Objective_Kappa(parameters,c1,c2,c3,c4,c5,branch,array_p2,array_p4,array_dawson,xvec_p2,xvec_p4,xvec_dawson)

%% kappa=parameters(1,:)


 p4in =zeros(size(parameters(1,:)));
 p2in =zeros(size(parameters(1,:)));
 
 for inx=1:size(parameters(1,:),2)
     p4in(inx) = p4_numeric(parameters(1,inx),array_p4,xvec_p4) ; % (1/(32.*((parameters(1,:)).^2))*(105+(12.*(parameters(1,:))).*(5+(parameters(1,:)))+(5.*sqrt((parameters(1,:))).*(2.*(parameters(1,:))-21))./(dawson_behnam(sqrt((parameters(1,:)))))));
     p2in(inx) = p2_numeric(parameters(1,inx),array_p2,xvec_p2) ; %(0.25.*((3./(sqrt((parameters(1,:))).*dawson_behnam(sqrt((parameters(1,:))))))-2-3./(parameters(1,:))));
 end
 % Diese for loop braucht ca. 30 Sekunden mit dem Laptop und einem Kern f�r
 % einen Wert, aber manchmal ist es super schnell....
 
 M1 = c1 ;
 M2 = c2./p2in';
 M3 = c3 ;
 M4 = c4./p2in';
 
 
a = ( M3./( (1/9).*(M1-M2).^2 ) - M4./ ( (1/9).*(M1-M2).^2 ) ).^2- ( (7/3) + 2*M2./ ( (1/3).*(M1-M2) )) .* ( M3./( (1/9).*(M1-M2).^2 ) - M4./ ( (1/9).*(M1-M2).^2 ) ) + M4 ./ ((1/9).* (M1-M2).^2);
c =   ( ( M3./( (1/9).*(M1-M2).^2 ) - M4./ ( (1/9).*(M1-M2).^2 ) ) - 5 - M2 ./ ( (1/3) .* (M1- M2) ) ) .^2;
f1 = ( (-40 / 3) + a +c - ( -4.*a.*c+( (40/3)-a-c ).^2).^0.5) ./ (2.*a);
f2 = ( (-40 / 3) + a +c + ( -4.*a.*c+( (40/3)-a-c ).^2).^0.5) ./ (2.*a);

if branch == 1
 %this is for f=f2
 f=f2;
else
%this is for f=f1  
f=f1;
end

D_bar =  ( (1/3) * (M1-M2) ) ;
per_De = D_bar ./ (1-f);
d_a = ( ( ( M3./( (1/9).*(M1-M2).^2 ) - M4./ ( (1/9).*(M1-M2).^2 ) ).* (1-f)-5- ( M2 ./ ( (1/3).*(M1-M2))) ) ./ (-f) ) ;
D_a = d_a .* D_bar ;
para_De = ( ( ( M2 ./ ( (1/3).*(M1-M2))) - f.* d_a ) ./ (1-f) ) .* D_bar + per_De;



pred =  (p4in'.*(f.*D_a.^2+(1-f).*(para_De-per_De).^2))-c5  ; % This is to be minimized to 0


