function out = Estimate_BP_ACID(per_D_image, para_D_image, per_W_image, para_W_image, mean_W_image,dummy_um,npool,dummy_convert_K,MSK, branch)  



load('xvec_p2.mat');
load('xvec_p4.mat');
load('array_p2.mat');
load('array_p4.mat');
load('array_dawson.mat');
load('xvec_dawson.mat');




    

    Vol_1=spm_vol(per_D_image); 
    VG = Vol_1(1);
    per_D=acid_read_vols(Vol_1,VG,1);

    Vol_2=spm_vol(para_D_image); 
    para_D=acid_read_vols(Vol_2,VG,1);


    Vol_3=spm_vol(per_W_image); 
    per_W=acid_read_vols(Vol_3,VG,1);

    Vol_4=spm_vol(para_W_image); 
    para_W=acid_read_vols(Vol_4,VG,1);

    Vol_5=spm_vol(mean_W_image); 
    mean_W=acid_read_vols(Vol_5,VG,1);
 
    

if ~isempty(MSK)
    MSK_Vol = spm_vol(MSK);
    MSK = acid_read_vols(MSK_Vol,VG,1);
    MSK = logical(MSK);
end
   


    if exist ('MSK', 'var')
     [kappa,De_perp,Da,De_par,f,MSK_good_kappa_vals]=find_bp_parameters_memo(per_D,para_D,mean_W,per_W,para_W,dummy_um,npool,dummy_convert_K,MSK,branch,array_p2,array_p4,xvec_p2,xvec_p4);
    else
     [kappa,De_perp,Da,De_par,f,MSK_good_kappa_vals]=find_bp_parameters_memo(per_D,para_D,mean_W,per_W,para_W,dummy_um,npool,dummy_convert_K,[],branch,array_p2,array_p4,xvec_p2,xvec_p4);   
    end

    if isempty(MSK)
      MSK = find(abs(nansum( cat(4,per_D, para_D, mean_W, per_W, para_W),4))>1e-9);
    end

 
       
    method = 'Sample_Objective_';
        
    
    Atmp = zeros(VG.dim);
    Atmp(MSK) = De_perp; 
    prefix = ['BP_De_perp_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = Da;
    prefix = ['BP_Da_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = kappa;
    prefix = ['BP_Kappa_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = De_par;
    prefix = ['BP_De_par_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = f;
    prefix = ['BP_f_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);
    
    Atmp(MSK) = MSK_good_kappa_vals;
    prefix = ['BP_MSK_good_kappa_vals_' method 'Branch_' num2str(branch) '_'];
    my_write_vol_nii(Atmp,VG,prefix);










