% Assumes ijkl is vector of unique tensor element positions.
%
% luke.edwards@ucl.ac.uk

function Wtensor=Wvec2Wtensor(Wvec,ijkl)

Wtensor=zeros(3,3,3,3);

for idx=1:4
    Wtensor(sub2ind([3,3,3,3],ijkl(:,1),ijkl(:,2),ijkl(:,3),ijkl(:,4)))=Wvec;
    ijkl=circshift(ijkl,1,2);
end

end
