function D=DtauDmd(MD,FA)

d=fixedParams();

tau=MDFA2tau(MD,FA,0);
tau(tau<0)=nan;

D=(tau-1/3).*(1./MD+1./(d-MD).*sign(d-MD));

end