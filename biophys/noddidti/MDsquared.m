function MD2=MDsquared(E1,E2,E3)

MD2=(E1.^2+E2.^2+E3.^2)/5+2*(E1.*E2 + E1.*E3 + E2.*E3)./15;
