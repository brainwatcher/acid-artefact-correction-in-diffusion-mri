% Definitions of in vivo fixed parameters used by NODDI.
%
% luke.edwards@ucl.ac.uk & S.Mohammadi
% 25/05/2020

function [d,diso]=fixedParams(dummy_invivo)

    if ~exist('dummy_invivo','var')
        dummy_invivo = 1;
    end

    if dummy_invivo
        d       = 1.7e-3;
        diso    = 3.0e-3; % CSF compartment
    else % ex vivo parameters as provided in the noddi toolbox
        d    = 0.6e-3;
        diso = 2.0e-3;
    end
end
