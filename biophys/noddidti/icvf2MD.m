% Forward relationship between ficvf and fiso output from NODDI and MD 
% output from a diffusion tensor fit using a relationship discovered via 
% cumulant expansion of the NODDI signal model.
%
% luke.edwards@ucl.ac.uk

function MD=icvf2MD(ficvf,fiso)

[d,diso]=fixedParams();

%[a,b,c]=noddiMDcoeffs(d,diso,iso);
%MD=a.*icvf.^2+b.*icvf+c;

MD=(1/3).*((-1).*d.*(3+2.*((-2)+ficvf).*ficvf).*((-1)+fiso)+3.*diso.*fiso);

end



