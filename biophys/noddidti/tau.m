% Relationship between the two dispersion parameters kappa and tau.
%
% luke.edwards@ucl.ac.uk

function t=tau(kappa)

low_kappa=(abs(kappa)<1e-12); % bound determined through trial and error

t(~low_kappa)=0.5*(-1./kappa(~low_kappa)...
    +1./(sqrt(kappa(~low_kappa)).*dawson(sqrt(kappa(~low_kappa)))));

t(low_kappa)=1/3; % analytical limit

end
