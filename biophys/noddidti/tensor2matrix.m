% Converts tensor generated from uniqueTensorElements2Tensor.m into a 
% matrix.
%
% luke.edwards@ucl.ac.uk

function Wmat=tensor2matrix(W)

Wmat=reshape(W,[9,9]);

end