close all

mu=rand(3,1);
mu=mu/norm(mu);
kappa=-5;
icvf=0.5;

npoints=50;
[x,y,z]=sphere(npoints);

dotProduct=mu(1)*x+mu(2)*y+mu(3)*z;

W=icvf*exp(kappa*dotProduct.^2).*(sqrt(kappa)./erfi(sqrt(kappa))./2./pi./sqrt(pi));

figure
axis([-1 1 -1 1 -1 1 0 1])
surf(x,y,z,W)
set(gca,'visible','off');
title('ODF')
