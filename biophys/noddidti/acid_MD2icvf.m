% Inversion of the relationship between ficvf and fiso output from NODDI
% and MD output from a diffusion tensor fit using a relationship discovered
% via cumulant expansion of the NODDI signal model.
% Returns -1 for any unphysical values

function ficvf=MD2icvf(MD)

d=fixedParams();

% Equation (2) of NODDI-DTI paper
ficvf=1-sqrt(0.5*(3.*MD./d-1));

% Check icvf reasonable
erricvf=(real(ficvf)<0)|(real(ficvf)>1)|(abs(imag(ficvf))>1e-12)|isnan(ficvf);
ficvf(erricvf)=-1;

% Remove any small imaginary components
ficvf=real(ficvf);

end
