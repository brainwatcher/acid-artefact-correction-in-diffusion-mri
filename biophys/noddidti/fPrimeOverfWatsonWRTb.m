function fDiv=fPrimeOverfWatsonWRTb(nu,kappa,gd,fibredir,b)

% set smalldel and delta and G
GAMMA = 2.675987E8;
protocol.smalldel = 1;
protocol.delta = (b + protocol.smalldel/3)/protocol.smalldel;
protocol.G = 1/GAMMA;

protocol.grad_dirs=repmat(gd.',length(b),1);
protocol.pulseseq='PGSE';

x(1)=nu;
x(2)=1.7e-3;
x(3)=kappa;
x(4)=0;
x(5)=0;
x(6)=1;
[E,J]=SynthMeasWatsonSHStickTortIsoV_B0(x, protocol, fibredir);

fDiv=(x(2)./b).*J(:,2).'./E.';

end