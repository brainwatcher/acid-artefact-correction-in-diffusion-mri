%function covD2K()

clear, spm12

Dfiles={'Tensor_olsq_maskedData0000-01.nii',...
    'Tensor_olsq_maskedData0000-02.nii',...
    'Tensor_olsq_maskedData0000-03.nii',...
    'Tensor_olsq_maskedData0000-04.nii',...
    'Tensor_olsq_maskedData0000-05.nii',...
    'Tensor_olsq_maskedData0000-06.nii'};
Kfiles={'Tensor_olsq_maskedData0000-07.nii',...
    'Tensor_olsq_maskedData0000-08.nii',...
    'Tensor_olsq_maskedData0000-09.nii',...
    'Tensor_olsq_maskedData0000-10.nii',...
    'Tensor_olsq_maskedData0000-11.nii',...,...
    'Tensor_olsq_maskedData0000-12.nii',...
    'Tensor_olsq_maskedData0000-13.nii',...
    'Tensor_olsq_maskedData0000-14.nii',...
    'Tensor_olsq_maskedData0000-15.nii',...,...
    'Tensor_olsq_maskedData0000-16.nii',...
    'Tensor_olsq_maskedData0000-17.nii',...
    'Tensor_olsq_maskedData0000-18.nii',...
    'Tensor_olsq_maskedData0000-19.nii',...
    'Tensor_olsq_maskedData0000-20.nii',...
    'Tensor_olsq_maskedData0000-21.nii'};

VMD=spm_vol('MD_olsq_maskedData0000.nii');
VMK=spm_vol('MK_olsq_maskedData0000.nii');

flags.dtype=32;

VMD2=VMD;
VMD2.fname='MD2.nii';
eigfiles={'EVAL_olsq_maskedData0000-x.nii','EVAL_olsq_maskedData0000-y.nii','EVAL_olsq_maskedData0000-z.nii'};
D2string='';
for catIdx1=1:3
    for catIdx2=1:3
        D2string=[D2string,sprintf('i%i.*i%i.*(1+2*%i)/15+',catIdx1,catIdx2,catIdx1==catIdx2)];
    end
end
D2string=[D2string '0'];
VMD2=spm_imcalc(eigfiles,VMD2,D2string,flags);

[ij,Dnperms]=defineDVector();
[ijkl,Wnperms]=defineKurtosisVector();

imcalcWString='';
for catIdx=1:15
    imcalcWString=[imcalcWString,sprintf('i%1$i*Wnperms(%1$i)*prod(nvec(ijkl(%1$i,:)))+',catIdx)]; %#ok<AGROW>
end
imcalcWString=[imcalcWString,'0'];

imcalcDString='';
for catIdx=1:6
    imcalcDString=[imcalcDString,sprintf('i%1$i*Dnperms(%1$i)*prod(nvec(ij(%1$i,:)))+',catIdx)]; %#ok<AGROW>
end
imcalcDString=[imcalcDString,'0'];

VDtemp=VMD;
VDtemp.fname='Dtemp.nii';
VWtemp=VMK;
VWtemp.fname='Wtemp.nii';

orientations=load('/home/ledwards/toolboxes/spinach_1.7.2996/kernel/grids/lebedev_ab_rank_35.mat');
theta=orientations.betas;
phi=orientations.alphas;
weights=orientations.weights;

numOrientations=length(theta);

VWMD2=VMD;
VWMD2.fname='WMD2.nii';
spm_write_vol(VWMD2,zeros(VWMD2.dim));

for orientation=1:numOrientations
    nvec=[sin(theta(orientation)).*sin(phi(orientation));
        sin(theta(orientation)).*cos(phi(orientation));
        cos(theta(orientation))];
    
    VDtemp=spm_imcalc(Dfiles,VDtemp,imcalcDString,flags,nvec,Dnperms,ij);
    VWtemp=spm_imcalc(Kfiles,VWtemp,imcalcWString,flags,nvec,Wnperms,ijkl);
    
    weight=weights(orientation);
    VWMD2=spm_imcalc([VWMD2,VWtemp,VMD],VWMD2,'i1+(i2.*i3.^2)*weight',flags,weight);
    
end

VD2MK=VMD;
VD2MK.fname='D2MK.nii';
spm_write_vol(VD2MK,zeros(VD2MK.dim));
VD2MK=spm_imcalc([VMK,VMD2],VD2MK,'i1.*i2',flags);

Vcov=VMD;
Vcov.fname='covD2K.nii';
spm_write_vol(Vcov,zeros(Vcov.dim));
Vcov=spm_imcalc([VWMD2,VD2MK],Vcov,'i1-i2',flags);

cov=spm_read_vols(Vcov);
mask=logical(spm_read_vols(spm_vol('../NODDImask.nii')));
cov=cov(mask);
figure,hist(cov,1e2)

D2MK=spm_read_vols(VD2MK);
mask=logical(spm_read_vols(spm_vol('../NODDImask.nii')));
D2MK=D2MK(mask);
figure,hist(D2MK,1e2)
