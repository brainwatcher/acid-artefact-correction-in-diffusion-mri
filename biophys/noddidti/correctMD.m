% Heuristic correction of MD for the effects of the mean kurtosis.
% 
% E1, E2, and E3 are the eigenvalues of the diffusion tensor calculated
% using data with a b-value of b. MK is an estimate of the mean kurtosis; 
% if left unspecified, MK is assumed to be unity.
%
% luke.edwards@ucl.ac.uk

function MD=correctMD(b,E1,E2,E3,MK)

MD0=(E1+E2+E3)/3;

MD2=MDsquared(E1,E2,E3);

if ~exist('MK','var')
    MK=1;
end

correction=b/6.*MD2.*MK;

MD=MD0+correction;

end
