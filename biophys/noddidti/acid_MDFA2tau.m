% Maps DTI MD and FA values to tau, a measure of dispersion,
% using a relationship discovered via cumulant expansion of
% the NODDI signal model.

function tau=MDFA2tau(MD,FA)

d=acid_fixedParams();

% Equation (3) of NODDI-DTI paper
tau=1/3*(1+4*FA.*(MD./abs(d-MD))./sqrt(3-2*FA.^2));

% Check tau reasonable
errtau=(real(tau)<1/3)|(real(tau)>1)|(abs(imag(tau))>1e-10)|isnan(tau);
tau(errtau)=-1;

% Remove any small imaginary components
tau=real(tau);

end
