% Heuristic correction of MD for the effects of the mean kurtosis.
% 
% E1, E2, and E3 are the eigenvalues of the diffusion tensor calculated
% using data with a b-value of b. MK is an estimate of the mean kurtosis; 
% if left unspecified, MK is assumed to be unity.

function MD=correctMD(b,E1,E2,E3,MK)

MD0=(E1+E2+E3)/3;

% Equation (B.11) of NODDI-DTI paper
MD2=(E1.^2+E2.^2+E3.^2)/5+2*(E1.*E2 + E1.*E3 + E2.*E3)./15;

if ~exist('MK','var')
    MK=1;
end

% Equation (B.12) in NODDI-DTI paper (reduces to Equation (5) when MK=1)
MD=MD0+b/6.*MD2.*MK;

end
