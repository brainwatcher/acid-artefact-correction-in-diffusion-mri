function fa=FA(e1,e2,e3)

md=(e1+e2+e3)/3;

fa=sqrt(3/2)*sqrt((e1-md).^2+(e2-md).^2+(e3-md).^2)./sqrt(e1.^2+e2.^2+e3.^2);

end
