% Expression of MD of approximate NODDI diffusion tensor as coefficients of
% a quadratic polynomial in icvf, i.e.
%   MD = a*icvf.^2 + b*icvf + c.
%
% luke.edwards@ucl.ac.uk

function [a,b,c]=noddiMDcoeffs(d,diso,iso)

a=-2*d/3.*(iso-1);
b=+4*(d/3).*(iso-1);
c=diso.*iso-d.*(iso-1);

end
