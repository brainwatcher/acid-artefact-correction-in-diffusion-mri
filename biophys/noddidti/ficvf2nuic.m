% Converts ficvf output by NODDI to nuic, the volume fraction of the 
% intracellular compartment.
%
% luke.edwards@ucl.ac.uk

function Vo=ficvf2nuic(file_prefix)

FNicvf=[file_prefix, '_output_ficvf.nii'];
FNiso=[file_prefix, '_output_fiso.nii'];

Vnuic=spm_vol(FNicvf);
Vnuic.fname=[file_prefix,'_output_nuic.nii'];

Vo=spm_imcalc({FNiso,FNicvf},Vnuic,'(1-i1).*i2');

end

