function bound=FA_upper_bound(MD)
d=fixedParams();
bound=min(1,sqrt(3).*(d-MD)./sqrt(4.*MD.^2+2*(d-MD).^2));
end
