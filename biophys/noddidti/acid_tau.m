% Relationship between the two dispersion parameters kappa and tau.

function t=tau(kappa)

low_kappa=(abs(kappa)<1e-12); % bound determined through trial and error

t(~low_kappa)=0.5*(-1./kappa(~low_kappa)...
    +1./(sqrt(kappa(~low_kappa)).*dawson(sqrt(kappa(~low_kappa)))));

t(low_kappa)=1/3; % analytical limit

end

% Computes Dawson's function.

function a=dawson(x)

a=exp(-x.^2).*erfi(x)*sqrt(pi)/2;

end

