% Converts ficvf output by NODDI to the absolute volume fraction of the 
% intracellular compartment.
%
% luke.edwards@ucl.ac.uk

function realIVCF=noddiICVF2realICVF(ficvf,fiso)

realIVCF=(1-fiso).*ficvf;

end
