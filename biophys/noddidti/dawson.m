% Computes Dawson's function.
%
% luke.edwards@ucl.ac.uk

function a=dawson(x)

a=exp(-x.^2).*erfi(x)*sqrt(pi)/2;

end
