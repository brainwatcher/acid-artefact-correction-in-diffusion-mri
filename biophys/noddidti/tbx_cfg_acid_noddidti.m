function acid_noddidti = tbx_cfg_acid_noddidti


%% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%% define variable: FA 
in_FA         = cfg_files;
in_FA.tag     = 'in_FA';
in_FA.name    = 'FA image';
in_FA.help    = {'Select FA image.'};
in_FA.filter = 'image';
in_FA.ufilter = '.*';
in_FA.num     = [0 1];

%% define variable: MD
in_MD         = cfg_files;
in_MD.tag     = 'in_MD';
in_MD.name    = 'MD image';
in_MD.help    = {'Select MD image. Note that a bias in MD will also bias the icvf value. Details can be found in Edwards et al., 2017.'};
in_MD.filter = 'image';
in_MD.ufilter = '.*';
in_MD.num     = [0 1];

%% define variable: EVEC
in_eval         = cfg_files;
in_eval.tag     = 'in_eval';
in_eval.name    = 'Eigenvalues (3 images)';
in_eval.help    = {'The eigenvalues are used for the heuristic bias correction of the MD map (details see Edwards et al., 2017).'};
in_eval.filter = 'image';
in_eval.ufilter = '.*';
in_eval.num     = [0 3];

%% define variable: white matter mask
in_msk         = cfg_files;
in_msk.tag     = 'in_msk';
in_msk.name    = 'White matter mask';
in_msk.help    = {'Select a binary white matter mask.'};
in_msk.filter = 'image';
in_msk.ufilter = '.*';
in_msk.num     = [0 1];

% ---------------------------------------------------------------------
% dummy_invivo
% ---------------------------------------------------------------------
dummy_invivo   = cfg_menu;
dummy_invivo.tag     = 'dummy_invivo';
dummy_invivo.name    = 'In vivo or ex vivo';
dummy_invivo.help    = {'The fixed diffusivities from NODDI are used also in NODDI-DTI. This flag defines whether the in vivo or ex vivo diffusivities are used (in vivo: diso = 3e-3mm^2/s, d=1.7mm^2/s; ex vivo: 2e-3mm^2/s, d=0.6mm^2/s). '};
dummy_invivo.labels = {
               'In vivo'
               'Ex vivo'
}';
dummy_invivo.values = {1 0};
dummy_invivo.val    = {1};

% ---------------------------------------------------------------------
% b-value directions
% ---------------------------------------------------------------------
b_vals         = cfg_entry;
b_vals.tag     = 'b_vals';
b_vals.name    = 'b-value (done for none)';
b_vals.help    = {'Provide a b-value for the heuristic kurtosis bias correction of the MD map. If none b-value is provided, no correction is performed (assuming minimal kurtosis bias, e.g., when performing the whole DKI fit).'};
b_vals.strtype = 'e';
b_vals.num     = [0 1];
b_vals.val     = {1000};


%% noddi dti
acid_noddidti         = cfg_exbranch;
acid_noddidti.tag     = 'acid_noddidti';
acid_noddidti.name    = 'NODDI-DTI';
acid_noddidti.val     = {in_FA in_MD in_eval in_msk b_vals dummy_invivo};
acid_noddidti.help    = {
                    'This function extracts biophysical tissue properties from DTI data, using the NODDI-DTI model.'
                    'Note that NODDI-DTI is defined only in white matter, and should be applied only to healthy white matter.' 
                    'Reference:'
                    ''
};
acid_noddidti.prog = @local_acid_noddidti;
acid_noddidti.vout = @vout_acid_noddidti;

%% dependencies
function out = local_acid_noddidti(job)
[Vkappa,Vicvf]=acid_noddidti(job.b_vals,char(job.in_FA),char(job.in_MD),char(job.in_eval),char(job.in_msk),job.dummy_invivo);

out.kappa   = {Vkappa.fname};
out.icvf    = {Vicvf.fname};

function dep = vout_acid_noddidti(job)
dep(1)            = cfg_dep;
dep(1).sname      = 'Kappa image';
dep(1).src_output = substruct('.','kappa');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(2).sname      = 'icvf image';
dep(2).src_output = substruct('.','icvf');
dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

