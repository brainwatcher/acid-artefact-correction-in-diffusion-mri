function D=DtauDfa(MD,FA)

tau=MDFA2tau(MD,FA,0);
tau(tau<0)=nan;

D=(tau-1/3).*(1./FA+6*FA./sqrt(3-2*FA.^2));

end