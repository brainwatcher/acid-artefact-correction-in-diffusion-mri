% Converts a kurtosis tensor into the kurtosis in a given direction.
%
% luke.edwards@ucl.ac.uk

function K=W2K(D,W,n)

Wn=kron(n',n')*W*kron(n,n);

meanDiffusion=trace(D)/3;

scaledDn=(n'*D*n)/meanDiffusion;

K=Wn/scaledDn^2;

end