function [ VKAPPA, Vicvf] = acid_noddidti(bvalue,PFA,PMD,PE13,PMSK,dummy_invivo)
% This is a wrapper for the NODDI-DTI tools written by Luke Edwards, as
% described in Edwards et al., 2017. The NODDI-DTI wrapper estimates a kappa
% and icvf map from DTI data.
%
% Inputs:
% bvalue         - bvalue at which the DTI dataset was acquired
% PFA            - filename of FA image
% PMD            - filename of MD image
% PE13           - filenames of eigenvalue images
% PMSK           - filename of mask image
% dummy_invivo   - whether in vivo or ex vivo diffusivities are used(false or true) 
% Output: 
% VKAPPA         - structure file corresponding to kappa image
% Vicvf          - structure file corresponding to icvf image
%
% L. Edwards and S. Mohammadi (20/12/2017)
%  S.Mohammadi added the option to have diffusivities for ex vivo.
% 25/05/2020
    
    % resampling method (sinc of 7th order)
    res = -7;
    % set flag
    if(~isempty(bvalue) && bvalue>0)
        correctionFlag=true;
    else
        correctionFlag=false;
    end
    fiso = 0; % no isotropic compartment

    
    % read in files
    VG = spm_vol(PFA); % take the FA image as spatial reference
    if(~isempty(PMSK))
         AMSK = acid_read_vols(spm_vol(PMSK),VG,res);
    else
        AMSK = ones(VG.dim);
    end
        
    FA = AMSK.*acid_read_vols(spm_vol(PFA),VG,res);
    MD = AMSK.*acid_read_vols(spm_vol(PMD),VG,res);
    VEVEC = spm_vol(PE13);
    E1 = AMSK.*acid_read_vols(VEVEC(1),VG,res);
    E2 = AMSK.*acid_read_vols(VEVEC(2),VG,res);
    E3 = AMSK.*acid_read_vols(VEVEC(3),VG,res);
    
    % find mask
    MSK = find(AMSK>0);

    Atau    = MDFA2tau(MD,FA,zeros(size(FA)),dummy_invivo);
    taulook     = 1/3:0.01:1;
    kappaLook   = tau2kappa(taulook);
    ytau        = Atau(MSK);
    ykappa      = zeros(numel(ytau),1);
    Aficvf = MD2icvf(MD,fiso,correctionFlag,bvalue,E1,E2,E3,dummy_invivo);
    for ii=1:numel(ytau)
       [~,xval]         = min(abs(ytau(ii)-taulook));
       ykappa(ii)   = kappaLook(xval);
    end

    Akappa = zeros(size(Atau));
    Akappa(MSK) = ykappa;

    VKAPPA  = my_write_vol_nii(Akappa,VG,'KAPPA_',[],VG.descrip);        
    Vicvf   = my_write_vol_nii(Aficvf,VG,'ICVF_',[],VG.descrip);
    

end

