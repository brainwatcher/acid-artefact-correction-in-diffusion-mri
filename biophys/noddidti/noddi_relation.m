clear, close all

vivo='in';
switch vivo
    case 'in'
        irfrac=[]; % not needed in vivo
    case 'ex'
        irfrac=0; % ex vivo completely restricted compartment
end
[d,diso,irfrac]=fixedParams(irfrac);
viso=0.1; % CSF compartment

tau=@(k) 1./(2*sqrt(k).*dawson(sqrt(k))) -1./(2*k);

etapar=@(k) tau(k);
etaperp=@(k)(1-tau(k))/2;

epar =@(k,v) (1-irfrac).*(viso.*diso+(1-viso).*(v.*d.*etapar(k) +(1-v).*(v.*d.*etapar(k)+(1-v).*d)));
eperp =@(k,v) (1-irfrac).*(viso.*diso+(1-viso).*(v.*d.*etaperp(k) +(1-v).*(v.*d.*etaperp(k)+(1-v).*d)));

% MD=@(v) (epar(1,v)+2*eperp(1,v))/3;
[a,b,c]=noddiMDcoeffs(d,diso,viso,irfrac);
c=@(MD) c-MD;

MD=@(v)a.*v.^2+b.*v+c(0);
linearMD=@(v,v0)(2.*a.*v0+b).*(v-v0)+MD(v0);

% Independent of d if viso=0
FA=@(k,v) sqrt(3/2)*sqrt((epar(k,v)-MD(v)).^2+2*(eperp(k,v)-MD(v)).^2)./sqrt(epar(k,v).^2+2*eperp(k,v).^2);

OD=@(k) 2/pi*atan(1./k);

kappa=linspace(0.01,50,100);
icvf=linspace(0,1,100);

% From Mathematica, imported using ToMatlab.m
FAderiv=@(k,v)(-1).*2.^(1/2).*((-1)+irfrac).^2.*(d.*(3+2.*((-2)+v).*v).*((-1)+ ...
    viso)+(-3).*diso.*viso).^2.*k.^(-1).*dawson(k.^(1/2)).^(-1).*(( ...
    -3).*k.^(1/2)+(3+2.*k).*dawson(k.^(1/2))).^(-1).*(d.^2.*((-2)+v) ...
    .^2.*v.^2.*((-1)+irfrac).^2.*((-1)+viso).^2.*k.^(-2).*dawson(k.^( ...
    1/2)).^(-2).*((-3).*k.^(1/2)+(3+2.*k).*dawson(k.^(1/2))).^2).^( ...
    1/2).*((-2).*k+2.*k.^(1/2).*((-1)+2.*k).*dawson(k.^(1/2))+4.* ...
    dawson(k.^(1/2)).^2).*(((-1)+irfrac).^2.*k.^(-2).*dawson(k.^(1/2)) ...
    .^(-2).*(3.*d.^2.*((-2)+v).^2.*v.^2.*((-1)+viso).^2.*k+(-2).* ...
    d.^2.*((-2)+v).^2.*v.^2.*((-1)+viso).^2.*k.^(1/2).*(3+2.*k).* ...
    dawson(k.^(1/2))+((-16).*d.*diso.*(3+2.*((-2)+v).*v).*((-1)+viso) ...
    .*viso.*k.^2+24.*diso.^2.*viso.^2.*k.^2+d.^2.*((-1)+viso).^2.*(3.* ...
    ((-2)+v).^2.*v.^2+4.*((-2)+v).^2.*v.^2.*k+4.*(6+((-2)+v).*v.*(8+ ...
    3.*((-2)+v).*v)).*k.^2)).*dawson(k.^(1/2)).^2)).^(-3/2);

FAapprox=@(k,v,k0) FA(k0,v)+(k-k0).*FAderiv(k0,v);

v0=0.2;
mdplot=figure; plot(icvf,[MD(icvf);linearMD(icvf,v0)]),xlabel('icvf'),ylabel('MD')

figure,plot(icvf,FA(0.1,icvf)),xlabel('icvf'),ylabel('FA')

icvf0=0.8;
k0=1;
maxLinearKappa=(1.1-FA(k0,icvf0))./FAderiv(k0,icvf0)+k0;
figure,plot(FA(kappa,icvf0),kappa),xlabel('FA'),ylabel('kappa')
hold on,plot(FAapprox(kappa(kappa<=maxLinearKappa),icvf0,k0),kappa(kappa<=maxLinearKappa),'r')

% Invert relation between MD and icvf
fitmd=1;
if fitmd
    md=MD(icvf);
    icvfFromMD=@(MD) (-b-sqrt(b^2-4.*a.*c(MD)))/2/a;
    figure(mdplot),hold on,plot(icvfFromMD(md),md,'r'),hold off
end

% Invert relation between FA (and MD) and kappa
fitFA=0;
if fitFA
    FAloc=FA(kappa,icvf0);
    kmin=0*FAloc;
    for idx=1:length(FAloc)
        FAobj=@(k) (FAloc(idx)-FA(k,icvf0));
        
        try
            kmin(idx)=fzero(FAobj,[0.001,300]);
        catch
            kmin(idx)=-1; % error
        end
    end
    figure,plot(FAloc,kmin),xlabel('FA'),ylabel('kappa')
end
