function fa=FAsymm(e1,e2,e3)

Es=cat(3,e1,e2,e3);

Es=sort(Es,3);

e1=Es(:,:,3);
e2=Es(:,:,2);
e3=Es(:,:,1);

e3Neg=e3<0;

% Assume noise causes repulsion of identical eigenvalues, so mean is true
% eigenvalue
e2(~e3Neg)=(e2(~e3Neg)+e3(~e3Neg))/2;
e3(~e3Neg)=e2(~e3Neg);

% Assume negative eigenvalues anomalous, but diffusion tensor still
% symmetric
e2(e3Neg)=e2(e3Neg);
e3(e3Neg)=e2(e3Neg);

fa=FA(e1,e2,e3);

end