MD=spm_read_vols(spm_vol('MD_olsq_maskedData0000.nii'));
FA=spm_read_vols(spm_vol('FA_olsq_maskedData0000.nii'));
MK=spm_read_vols(spm_vol('MK_olsq_maskedData0000.nii'));

x=spm_read_vols(spm_vol('EVEC1_olsq_maskedData0000-x.nii'));
y=spm_read_vols(spm_vol('EVEC1_olsq_maskedData0000-y.nii'));
z=spm_read_vols(spm_vol('EVEC1_olsq_maskedData0000-z.nii'));
mask=logical(spm_read_vols(spm_vol('../NODDImask.nii')));

mask=mask&(MD>0)&(MD<1.5e-3)&(FA>0)&(FA<1);

histogram2(z(mask),FA(mask),'DisplayStyle','tile')
