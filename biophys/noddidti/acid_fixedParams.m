% Definitions of in vivo fixed parameters used by NODDI.
% see Zhang, et al., Neuroimage (2012).

function [d,diso]=fixedParams()

d=1.7e-3; % intra- and extraaxonal diffusivity
diso=3.0e-3; % CSF compartment diffusivity

end
