% Ordering of the diffusion tensor elements in Siawoosh's ACID toolbox.

function [ij,nperms]=defineDVector()

% define D tensor elements
ij=[...
    1,1; % 1
    2,2; % 2
    3,3; % 3
    1,2; % 4
    1,3; % 5
    2,3; % 6
    ];

nperms=[1;1;1;2;2;2];

end
