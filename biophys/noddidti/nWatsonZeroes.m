function [N,N2]=nWatsonZeroes(nu,kappa,gd,fibredir,maxReB,maxImB)

bc=sqrt(maxReB^2+maxImB^2);

fDiv=@(theta) fPrimeOverfWatsonWRTb(nu,kappa,gd,fibredir,bc*exp(1i*theta));
jacobian=@(theta) 1i*bc*exp(1i*theta);

N=integral(@(theta) jacobian(theta).*fDiv(theta),0,2*pi)/2/pi/1i;

fDiv2=@(x) fPrimeOverfWatsonWRTb(nu,kappa,gd,fibredir,x);
N2 = (integral(fDiv2,bc*(1+1i),bc*(1+1i),'Waypoints',bc*[-1+1i,-1-1i,1-1i]))/2/pi/1i;