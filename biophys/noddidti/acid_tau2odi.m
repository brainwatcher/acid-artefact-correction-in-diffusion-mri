function odi=tau2odi(tauForODI)

odi=0*tauForODI;

% Avoid for loop for voxels that would fail a priori
failTau=(tauForODI<(1/3))|(tauForODI>1);
odi(failTau)=-1;

otherTau=tauForODI(~failTau);
otherODI=0*otherTau;

parfor n=1:length(otherTau)
    try
        otherODI(n)=fzero(@(odi) tau(odi2kappa(odi))-otherTau(n),[eps,1]);
    catch
        otherODI(n)=-2;
    end
end

odi(~failTau)=otherODI;

odi=reshape(odi,size(tauForODI));

end
