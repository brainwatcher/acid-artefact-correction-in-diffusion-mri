% Ordering of the kurtosis tensor elements in Siawoosh's ACID toolbox.
% The resulting ijkl can be fed into 
% diffusionTensors2kurtosisTensorElements.m in order to generate the
% tensor elements.

function [ijkl,nperms]=defineKurtosisVector()

% define Kurtosis
ijkl=[...
    1,1,1,1; % 1
    2,2,2,2; % 2
    3,3,3,3; % 3
    1,1,1,2; % 4
    1,1,1,3; % 5
    2,2,2,1; % 6
    2,2,2,3; % 7
    3,3,3,1; % 8
    3,3,3,2; % 9
    1,1,2,2; % 10
    1,1,3,3; % 11
    2,2,3,3; % 12
    1,1,2,3; % 13
    2,2,1,3; % 14
    3,3,1,2; % 15
    ];

nperms=[1;1;1;4;4;4;4;4;4;6;6;6;12;12;12];

end
