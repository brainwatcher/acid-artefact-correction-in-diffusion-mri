
clear

nSamples=1e3;
N=zeros(1,nSamples);
N2=zeros(1,nSamples);

maxReB=1.3e3; %min = 1.2409e3 for kappa = inf, nu = 0.1384.
maxImB=pi;
warning('off','SH:zwarn')
for n=1:nSamples
    gd=rand(3,1);
    gd=gd/norm(gd);
    fibredir=[0;0;1]; % no loss of generality
    kappa=100*rand(1);
    nu=rand(1);
    [N(n),N2(n)]=nWatsonZeroes(nu,kappa,gd,fibredir,maxReB,maxImB);
end

hist(abs(N),1e1)