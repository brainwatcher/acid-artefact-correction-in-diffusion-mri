function [V4D, Ni] = ACID_write_4D(n4D,V,VG,prefix)
% This function writes 4D volumes. It requires that you upfront do some
% definitions - see acid_eb_riciancorr.
% S.Mohammadi 10/10/2019

    [~,fname,~] = spm_fileparts(V(1).fname);
    wV       = VG(1);
    V        = spm_vol(V(1).fname);


    dt        = [spm_type('int16'),spm_platform('bigend')];
    dm        = [VG(1).dim n4D];
    Ni        = nifti;
    Ni.mat    = VG(1).mat;
    Ni.mat0   = VG(1).mat;
    V4D       = spm_file_merge(wV,[prefix fname],spm_type('UINT8'));

    Ni.dat    = file_array(V4D.fname,dm,dt, 0,1,0);
    Ni.descrip = V.descrip;
    create(Ni);
    
end