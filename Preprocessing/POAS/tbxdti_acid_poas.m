function poas_choice = tbxdti_acid_poas(in_vols, brain_msk, diff_dirs, dummy_3dor4d)



%% b values
poas_bvals_file         = cfg_files;
poas_bvals_file.tag     = 'poas_bvals_file';
poas_bvals_file.name    = 'File (*.mat|txt|bval)';
poas_bvals_file.help    = {'Select a file specifying the b-values (bval) of the N dMRI volumes.'
                       'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
poas_bvals_file.filter  = 'any';
poas_bvals_file.ufilter = 'mat|txt|bval';
poas_bvals_file.num     = [0 1];
poas_bvals_file.val     = {{''}};

poas_bvals_exp         = cfg_entry;
poas_bvals_exp.tag     = 'poas_bvals_exp';
poas_bvals_exp.name    = 'Expression';
poas_bvals_exp.help    = {'Specify the b-values (bval) of the N dMRI volumes.'
                             'Note: The expression should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
poas_bvals_exp.strtype = 'e';
poas_bvals_exp.num     = [Inf Inf];
poas_bvals_exp.val     = {''};

poas_bvals_type        = cfg_choice;
poas_bvals_type.tag    = 'poas_bvals_type';
poas_bvals_type.name   = 'b-values (recommended)';
poas_bvals_type.help   = {''};
poas_bvals_type.values = {poas_bvals_file, poas_bvals_exp};
%poas_bvals_type.val    = {poas_bvals_file};




%% b vectors
poas_bvectors_file         = cfg_files;
poas_bvectors_file.tag     = 'poas_bvectors_file';
poas_bvectors_file.name    = 'File (*.mat|txt|bvec)';
poas_bvectors_file.help    = {'Select a file specifying the b-vectors (bvec) of the N dMRI volumes.'
                       'Note: The file should contain a 3 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
poas_bvectors_file.filter  = 'any';
poas_bvectors_file.ufilter = 'mat|txt|bvec';
poas_bvectors_file.num     = [0 1];
poas_bvectors_file.val     = {{''}};

poas_bvectors_exp         = cfg_entry;
poas_bvectors_exp.tag     = 'poas_bvectors_exp';
poas_bvectors_exp.name    = 'Expression';
poas_bvectors_exp.help    = {'Specify the b-vectors (bvec) of the N dMRI volumes.'
                             'Note: The expression should contain a 3 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
poas_bvectors_exp.strtype = 'e';
poas_bvectors_exp.num     = [Inf Inf];
poas_bvectors_exp.val     = {''};

poas_bvectors_type        = cfg_choice;
poas_bvectors_type.tag    = 'poas_bvectors_type';
poas_bvectors_type.name   = 'b-vectors (recommended)';
poas_bvectors_type.help   = {''};
poas_bvectors_type.values = {poas_bvectors_file, poas_bvectors_exp};
%poas_bvectors_type.val    = {poas_bvectors_file};





% ---------------------------------------------------------------------
%% freeze
% ---------------------------------------------------------------------

kstar         = cfg_entry;
kstar.tag     = 'kstar';
kstar.name    = 'k star';
kstar.help    = {'Number of iterations. Typically 10 to 20. Defines maximum size of neighborhood for POAS.'};
kstar.strtype = 'e';
kstar.num     = [1 1];
kstar.val    = {1e1};

kappa         = cfg_entry;
kappa.tag     = 'kappa';
kappa.name    = 'kappa';
kappa.help    = {'Angular resolution. Change with care!'};
kappa.strtype = 'e';
kappa.num     = [1 1];
kappa.val    = {8e-1};

lambda         = cfg_entry;
lambda.tag     = 'lambda';
lambda.name    = 'lambda';
lambda.help    = {'Adpatation parameter'};
lambda.strtype = 'e';
lambda.num     = [1 1];
lambda.val    = {10};

sigma         = cfg_entry;
sigma.tag     = 'sigma';
sigma.name    = 'sigma';
sigma.help    = {'Needs to be provided by background estimator or similar. SD parameter of noise distribution.'};
sigma.strtype = 'e';
sigma.num     = [1 1];
sigma.val    = {3e1};

ncoils         = cfg_entry;
ncoils.tag     = 'ncoils';
ncoils.name    = 'ncoils';
ncoils.help    = {'Number of receiver coils. Effective number can be less!'};
ncoils.strtype = 'e';
ncoils.num     = [1 1];
ncoils.val    = {1e0};

% prefix
poas_prefix         = cfg_entry;
poas_prefix.tag     = 'poas_prefix';
poas_prefix.name    = 'Prefix for output';
poas_prefix.help    = {'Prefix for the output filename.'};
poas_prefix.strtype = 's';
poas_prefix.val     = {'poas'};

%% begin defining fair_epi susceptibility correction
% ---------------------------------------------------------------------
% poas
% ---------------------------------------------------------------------
poas         = cfg_exbranch;
poas.tag     = 'poas';
poas.name    = 'POAS';
poas.val     = {in_vols brain_msk poas_bvectors_type poas_bvals_type kstar sigma lambda kappa ncoils dummy_3dor4d poas_prefix};
% poas.val     = {in_vols brain_msk diff_dirs b_vals kstar kappa lambda sigma ncoils dummy_3dor4d};
poas.help    = {
    'Changeable defaults: '
    '- kstar: Number of iterations. Typically 10 to 20. Defines maximum size of neighborhood for POAS.'
    '- kappa: Angular resolution. Change with care!'
    '- lambda: Adpatation parameter'
    '- sigma: Needs to be provided by background estimator or similar. SD parameter of noise distribution.'
    '- ncoils: Number of receiver coils. Effective number can be less!'
};
poas.prog = @local_poas;
poas.vout = @vout_poas;

%% begin    POAS correction
% ---------------------------------------------------------------------
% in_vols
% ---------------------------------------------------------------------
in_vols_mask         = cfg_files;
in_vols_mask.tag     = 'in_vols_mask';
in_vols_mask.name    = 'Mask image';
in_vols_mask.help    = {'Select one binary image that defines the regions within which the noise will be calculated.'};
in_vols_mask.filter = 'image';
in_vols_mask.ufilter = '.*';
in_vols_mask.num     = [0 1];

% ---------------------------------------------------------------------
% sigma estimation
% ---------------------------------------------------------------------
sigma_est         = cfg_exbranch;
sigma_est.tag     = 'sigma_est';
sigma_est.name    = 'Sigma estimation';
sigma_est.val     = {in_vols in_vols_mask ncoils};
sigma_est.help    = {
    'Select the images you want to calculate sigma from from as well as the noise mask.'
    'Changeable default: '
    '- ncoils: Number of receiver coils. Effective number can be less!'
};
sigma_est.prog = @local_sigma_est;
sigma_est.vout = @vout_sigma_est;
%% begin    sigma estimation 


%% ---------------------------------------------------------------------
% POAS choice
% ---------------------------------------------------------------------
poas_choice         = cfg_choice;
poas_choice.tag     = 'poas_choice';
poas_choice.name    = 'Choose POAS options';

poas_choice.help    = {
                 'Sigma estimation'
                 'POAS'
}';
poas_choice.values  = {sigma_est poas};

    

%----poas-------------------
function out = local_poas(job)


% read in bvals
if isfield(job.poas_bvals_type,'poas_bvals_file')
    job.poas_bvals = job.poas_bvals_type.poas_bvals_file;
    job.poas_bvals = cell2mat(job.poas_bvals);
    [~,~,e] = fileparts(job.poas_bvals);
    if ~isempty(job.poas_bvals_type.poas_bvals_file{1})
        switch lower(e)
        case '.mat'
            job.poas_bvals = cell2mat(struct2cell(load(job.poas_bvals)));
        case '.nii'
            error('Unexpected file extension!')
        otherwise
            job.poas_bvals = dlmread(job.poas_bvals);
        end
    else
        job.poas_bvals = '';
    end
elseif isfield(job.poas_bvals_type,'poas_bvals_exp')
    if ~isempty(job.poas_bvals_type.poas_bvals_exp)
        job.poas_bvals = job.poas_bvals_type.poas_bvals_exp;
    else
        job.poas_bvals = '';
    end
end
 

% read in bvecs
if isfield(job.poas_bvectors_type,'poas_bvectors_file')
    job.poas_bvectors = job.poas_bvectors_type.poas_bvectors_file;
    job.poas_bvectors = cell2mat(job.poas_bvectors);
    [~,~,e] = fileparts(job.poas_bvectors);
    if ~isempty(job.poas_bvectors_type.poas_bvectors_file{1})
        switch lower(e)
        case '.mat'
            job.poas_bvectors = cell2mat(struct2cell(load(job.poas_bvectors)));
        case '.nii'
            error('Unexpected file extension!')
        otherwise
            job.poas_bvectors = dlmread(job.poas_bvectors);
        end
    else
        job.poas_bvectors = '';
    end
elseif isfield(job.poas_bvectors_type,'poas_bvectors_exp')
    if ~isempty(job.poas_bvectors_type.poas_bvectors_exp)
        job.poas_bvectors = job.poas_bvectors_type.poas_bvectors_exp;
    else
        job.poas_bvectors = '';
    end
end







[dummy_4D, V4D, bval, bvec] = POAS(job.poas_bvectors, job.poas_bvals, job.kstar, job.kappa, job.lambda, job.sigma, job.ncoils, char(job.in_vols), char(job.brain_msk),job.dummy_3dor4d, job.poas_prefix);

% out.rothers_up = my_spm_file(char(job.others_up(1)),'prefix','r','format','.nii');
if(dummy_4D==false)
    out.rsource = my_spm_file(char(job.in_vols(job.b_vals>min(job.b_vals))),'prefix','poas_','format','.nii');
    tmp = my_spm_file(char(job.in_vols(job.b_vals>min(job.b_vals))),'prefix','poas_','format','.nii');
    out.rsource(2:size(tmp,2)+1) = tmp;
    out.rsource(1) = my_spm_file(char(job.in_vols(1)),'prefix','poas_b0_','format','.nii');
elseif(dummy_4D==true)
    out.rsource = {V4D(1).fname};
end
out.bval = bval;
out.bvec = bvec;
out.rsource = acid_my_select(out.rsource(:),dummy_4D);
out.rsource = out.rsource(:);    

%------------------------
function dep = vout_poas(job)
dep(1)            = cfg_dep;
dep(1).sname      = 'POAS smoothed data';
dep(1).src_output = substruct('.','rsource');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
dep(2)            = cfg_dep;
dep(2).sname      = 'b-values';
dep(2).src_output = substruct('.','bval');
dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
dep(3)            = cfg_dep;
dep(3).sname      = 'b-vectors';
dep(3).src_output = substruct('.','bvec');
dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});

    
    %----sigma_est-------------------
function out = local_sigma_est(job)


msig = Sigma_estmate(char(job.in_vols), char(job.in_vols_mask), job.ncoils);
out.sigma_estimates =  msig;
% out.sigma_estimates = acid_my_select(out.sigma_estimates(:));
% out.sigma_estimates = out.sigma_estimates(:);

% to be finished-----------------------
function dep = vout_sigma_est(job)
dep(1)            = cfg_dep;
dep(1).sname      = 'Sigma estimates';
dep(1).src_output = substruct('.','sigma_estimates');
dep(1).tgt_spec   = cfg_findspec({{'strtype','e'}});

%---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);                
                if(nargin>=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end    
        else
            varout  = varargin{1};            
        end
    end
end

    