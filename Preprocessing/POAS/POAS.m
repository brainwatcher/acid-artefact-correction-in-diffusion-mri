function [dummy_4D, V4D, bval, bvec] = POAS( DiffVec, bvalues, kstar, kappa0, lambda, sigma, ncoils, PInd, PMSK, dummy_4D, prefix)

    % Implementation of msPOAS 
    % Becker et al. (2014) NeuroImage 95, 90?105
    % http://dx.doi.org/10.1016/j.neuroimage.2014.03.053
    
    % Original implementation in R-package dti
    % J. Polzehl August 2013

    
    % Test existence of required parameters
    if(~exist('prefix', 'var'))
        % diffusion gradient vectors
        prefix = 's'; 
    end    
    
    
    
    % Test existence of required parameters
    if(~exist('DiffVec', 'var'))
        % diffusion gradient vectors
        DiffVec = spm_input('Select diffusion gradient vectors (for diffusion weighted and non-weighted images)'); 
    end
    if(~exist('bvalues', 'var'))
        % b-values
        bvalues = spm_input('Select b-values (for diffusion weighted and non-weighted images)'); 
    end
    if(~exist('kstar', 'var'))
        % number of iterations for POAS
        kstar   = spm_input('Select number of iterations'); % 1 x number of iterations
    end
    if(~exist('PInd', 'var'))
        % diffusion weighted and non-diffusion weighted image volumes
        PInd = char(cfg_getfile(size(DiffVec, 2), 'IMAGE', 'Select images', '', '', '.*')); 
    end

    
    
    time1 = tic;
    disp('Start initializations');

    % defaults
    % TODO: check, whether these go to the defaults file
    level      = 10;   % JP: threshold for S0 images to define head mask, needs to be adjusted
    tolgrad    = 0.01; % tolerance for normalizing gradient directions
    interp_def = -7;   % SM: interpolation kernel for reslicing the data

    % Look up table for Gauss approximation of non-central chi
    % columns NCP (1), ExpV (2), SD (3) and Var (4)
    load 'sofmchitable.mat' sofmchitable;
    if (ncoils > 32)
        warning('No lookup table for ncoils larger then 32! set ncoils to 1');
        ncoils = 1;
    end
    sofmchitable = sofmchitable(:, ncoils, :);
    
    % Determine indices of diffusion and non-diffusion weighted images
    % JP: slightly different b-values should be identified, 
    %     currently this needs to be done manually when 
    %     specifying the b-values
    % TODO: find a better way
    bvals = bvalues';
    bvals = round(bvals/100)*100;
    bvals = bvals';
    
    b0     = min(bvals(1, :));
    MSK_b0 = find(bvals(1, :) == b0);
    MSK_b  = find(bvals(1, :) > b0);
    
    
        % Prepare if 4D-Volumes (one image == 4D)
if size(PInd,1) == 1
    struct4D   = nifti(PInd);
    dim4D = struct4D.dat.dim;
    n   = dim4D(4);
    if n == 1
        error('A single 3D source image was selected. Choose a single 4D volume or select all 3D volumes manually!');
    end
    PInd = strcat(repmat(PInd(1:end-2), n, 1), ',', num2str([1:n]'));
end
    
    
    
    
    
    
    % Initialize standardized images
    % SM: This is to check whether the data are 3D or 4D 
    if(~isempty(PInd))
        for i = 1:size(PInd, 1)
            Vtmp = spm_vol(PInd(i, :));
            if(size(Vtmp, 1) > 1)
                V(i) = Vtmp(1);
            else
                V(i) = Vtmp;
            end
        end
        if(size(PInd,1) >= 2)
            if(strcmp(V(1).fname, V(2).fname))
                dummy_4D = true;
            end
        end
    else
        V = spm_vol(PInd);
    end
    

    
    
    
    % Sanity check whether for each volume there is a b-value and a
    % diffusion gradient vector
    if (size(bvals, 2) ~= size(DiffVec, 2))
        error('Number of b-values and diffusion gradient vectors do not match!');
    end
    if (size(bvals, 2) ~= size(V, 2))
        error('Number of b-values and volumes do not match!');
    end
   
    % Spatial dimension of the volumes
    ddim = V(MSK_b0(1)).dim;
    % Number of non-diffusion weighted images
    ns0 = length(MSK_b0);
    
    % A0 is an 4D-array for the non-diffusion weigthed images
    % SM: Note that the b0 images are resampled to the first b0 image,
    %     ensuring that previous mat-files are encountered before averaging 
    %     all b0 images.
    A0 = zeros([ddim ns0]);
    kk = 1;
    for i = MSK_b0
        for p = 1:ddim(3)
            M = spm_matrix([0 0 -p 0 0 0 1 1 1]);
            M1 = inv(M * inv(V(MSK_b0(1)).mat) * V(i).mat); % TODO: resolve inv()
            A0(:, :, p, kk)  = spm_slice_vol(V(i), M1, ddim(1:2), interp_def);
        end
        kk = kk + 1;
    end    
    A0  = single(A0); 

    % Determine mean non-diffusion weigthed image 
    % scaled with the noise parameter sigma
    s0factor = sqrt(ns0); % JP: needed for rescaling of estimated s0
    s0       = sum(A0./sigma, 4) / s0factor; % JP: this keeps the variance the same as for A0./sigma
    ws0      = 1/ns0;
    clear A0; % no longer needed

    % si is an 4D-array for the diffusion weigthed images
    % SM: All DW images are resliced with respect to the first image
    si = zeros([ddim length(MSK_b)]);
    kk = 1;
    for i = MSK_b
        for p = 1:ddim(3)
            M = spm_matrix([0 0 -p 0 0 0 1 1 1]);
            M1 = inv(M * inv(V(MSK_b0(1)).mat) * V(i).mat);  % TODO: resolve inv()
            si(:, :, p, kk) = spm_slice_vol(V(i), M1, ddim(1:2), interp_def);
        end
        kk = kk + 1;
    end
    si = single(si);
    % scaled with the noise parameter sigma
    si = si ./ sigma;

    % Normalize gradients
    % TODO: check
    normgrad = sqrt(sum(DiffVec .* DiffVec, 1)); %JP: need norm not norm^2
    if(~isempty(find(normgrad ~= 1)))
        if(~isempty(find((normgrad-1) > tolgrad))) % SM: if norm of gradients is within the tolerance message won't be visible 
            warning('Diffusion gradients have been manually normalised!'); 
        end
        MSKgrad = find(normgrad > 0);
        DiffVec(:, MSKgrad) = bsxfun(@rdivide, DiffVec(:, MSKgrad), normgrad(MSKgrad));
    end
    clear normgrad;
    
    % Find all diffusion gradient vectors 
    grad = DiffVec(:, MSK_b);
    n3g = getnext3g(DiffVec, bvals);
    %    nshell = n3g{1}+1;
    % JP n3g is a list (cell structure) with 5 components 
    %    n3g{1} = nbv;  number of DW shells (without b0)
    %    n3g{2} = bv;   bvals
    %    n3g{3} = ubv;  unique bvals
    %    n3g{4} = ind;  index array of corners for triangles containg the
    %                   gradient indices (0 - (ngrad-1)
    %    n3g{5} = w;    weights for spherical interpolation

    % Read brain mask
    % SM: interpolation to the first b0 image - using the spm-mat files
    % TODO: Improve handling of mask if not given! Change default?
    if(exist('PMSK', 'var') && ~isempty(PMSK))
        VMSK = spm_vol(PMSK);
        if(size(VMSK, 1) > 1)
            warning('Currently, only the first mask image is used!');
        end
        mask = acid_read_vols(VMSK(1), V(MSK_b0(1)), interp_def);
        mask = mask > 0;
    else
        mask = [];
    end
    if(isempty(mask))
        mask = s0 > (level/sigma*s0factor);% JP: *s0factor to scale back to range of s0 
    end
    dmask = zeros(size(mask), 'single');
    dmask(mask) = 1;
    
    % Initializations
    ni0  = ones(size(s0), 'single');
    ni   = ones(size(si), 'single');
    vxg  = sqrt(sum(V(1).mat(1:3, 1:3).^2)); % [yvoxel/xvoxel zvoxel/xvoxel]
    vext = [vxg(2)/vxg(1) vxg(3)/vxg(1)];
    gradstats = getkappasmsh3(grad, n3g{1}, n3g{2}, n3g{3});
    [hseqi, nw] = gethseqfullse3msh(kstar, gradstats, kappa0, vext);
    nind = int16(nw * 1.25);
    zth  = ones(size(si), 'single');
    zth0 = ones(size(s0), 'single');
    if(lambda < 1d10) 
        kinit = 0;
    else
        disp('Performing non-adaptive smoothing for last step only');
        kinit = kstar;
    end

    etime1 = toc(time1);
    disp(['End initializations: Elapsed time ' num2str(etime1) ' seconds']);
    disp(['Starting first of ' num2str(kstar) ' iterations']); 

    % Perform POAS iteration 
    spm_progress_bar('Init', kstar, 'Running msPOAS', 'iteration');
    for k = kinit:kstar
        time2 = tic;
        hakt = hseqi(:,k+1);
        [msth, msni, msth0, msni0] = interpolatesphere0(zth, zth0, ni, ni0, n3g, mask);
        clear zth zth0; % save memory, will be recomputed before next read
        [parind, parw, parnind] = lkfullse3msh(hakt, kappa0./hakt, gradstats, vext, nind);
        hakt0 = mean(hakt);
        [parind0, parw0, parnind0] = lkfulls0(hakt0, vext, nind); % mex
        parind0 = parind0(:, 1:parnind0);
        parw0 = parw0(1:parnind0);        
        vmsth = linterpol(msth, sofmchitable, 2, 4)/2;
        vmsth = reshape(vmsth,size(msth));
        vmsth0 = linterpol(msth0, sofmchitable, 2, 4)/2;
        vmsth0 = reshape(vmsth0, size(msth0));
        [nni, zth, nni0, zth0] = adsmse3ms(si, s0, msth, msni, msth0, msni0, vmsth, vmsth0, dmask, lambda, ws0, parind, parw, parnind, parind0, parw0, parnind0);
        clear msth msni msth0 msni0 vmsth vmsth0; % save memory, will be recomputed before next read
        ni = max(ni, nni);
        ni0 = max(ni0, nni0);
        clear nni nni0; % save memory, will be recomputed before next read

        etime2 = toc(time2);
        disp(['Iteration ' num2str(k) ' completed in ' num2str(etime2) ' seconds']);        

        spm_progress_bar('Set', k);
    end 
    
    etime1 = toc(time1);
    disp(['End of iterations: Elapsed time ' num2str(etime1) ' seconds']);      
    spm_progress_bar('Clear');

    disp('Now collecting and writing results');
    % rescale th0 image with sigma/s0factor to get correct scale
    zth0 = zth0./s0factor.*sigma ; 

    % Write smoothed data
    prefix1 = strcat(prefix , '_');
    % Rescale results to original range
    zth = zth.*sigma;
    if(~dummy_4D)
        for i = 1:numel(MSK_b)
            my_write_vol_nii(zth(:, :, :, i), V(MSK_b(i)), prefix1);
        end
        prefix2 = strcat(prefix , 'b0_');
        % b=0 image
        my_write_vol_nii(zth0, V(MSK_b0(1)), prefix2);
        V4D = [];
    else
        zth = cat(4,zth0,zth);
        % TODO: do we need a special POAS function here?
        V4D = poas_write_image_4d(zth, V, V(MSK_b0(1)), prefix1);
    end
    
    % SM write new b-values
    bval = [bvalues(1, MSK_b0(1)) bvalues(1, find(bvals(1, :) > b0))];
    bvec = cat(2, DiffVec(:, MSK_b0(1)), DiffVec(:, find(bvals(1, :) > b0)));
    [p, f, ~] = spm_fileparts(V(MSK_b0(1)).fname);
    dlmwrite([p filesep 'diffparams_poas_' f '.bval'], bval);
    dlmwrite([p filesep 'diffparams_poas_' f '.bvec'], bvec);
    save([p filesep 'diffparams_poas_' f '.mat'], 'bval', 'bvec');

    etime1 = toc(time1);
    disp(['Finished POAS: Total elapsed time ' num2str(etime1) ' seconds']);      
end
