function powrice = tbx_cfg_powrice

%% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%% define variable: volumes
in_vols         = cfg_files;
in_vols.tag     = 'in_vols';
in_vols.name    = 'images';
in_vols.help    = {'Select DWI images'};
in_vols.filter = 'image';
in_vols.ufilter = '.*';
in_vols.num     = [0 Inf];

%--------------------------------------------------------------------------
% define entry: noise estimate
%--------------------------------------------------------------------------
noise_est         = cfg_entry;
noise_est.tag     = 'noise_est';
noise_est.name    = 'Noise Estimate';
noise_est.help    = {'Type in a scalar for the noise estimate.'};
noise_est.strtype = 'e';
noise_est.num     = [0 1];
noise_est.val     = {[1]};
%--------------------------------------------------------------------------
% define entry: Number of channels
%--------------------------------------------------------------------------
encha         = cfg_entry;
encha.tag     = 'encha';
encha.name    = 'Effective number of channels.';
encha.help    = {'Effective number of channels. I do not have a receipe for calculating the right value here. But it will definintely scale with parallel imaging factor, i.e. for PAT=2 the effective number of channels should be >= 2.'};
encha.strtype = 'e';
encha.num     = [1 1];
encha.val    = {1e0};

%--------------------------------------------------------------------------
% prefix Filename Prefix
%--------------------------------------------------------------------------
prefix         = cfg_entry;
prefix.tag     = 'prefix';
prefix.name    = 'Filename Prefix';
prefix.help    = {'Specify the string to be prepended to the filenames of weighted combined file(s). Default prefix is ''co''.'};
prefix.strtype = 's';
prefix.num     = [1 Inf];
prefix.val     = {'pow'};
% ---------------------------------------------------------------------
% dummy whether 3d or 4d output
% ---------------------------------------------------------------------
dummy_3dor4d          = cfg_menu;
dummy_3dor4d.tag     = 'dummy_3dor4d';
dummy_3dor4d.name    = 'Output format';
dummy_3dor4d.help    = {''
'Specify whether data should be in 3d or 4d.'
''};
dummy_3dor4d.labels = {'3d output','4d output'};
dummy_3dor4d.values = {0 1};
dummy_3dor4d.val    = {0};


%% call local resample function
powrice         = cfg_exbranch;
powrice.tag     = 'powrice';
powrice.name    = 'Rician correction on raw DWI';
powrice.val     = {in_vols noise_est encha prefix dummy_3dor4d};
powrice.help    = {
                    'Rician bias correction on raw data using the M2 method described in Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531).'
};
powrice.prog = @local_ricianbias;
powrice.vout = @vout_ricianbias;

%% dependencies
function out = local_ricianbias(job)
[V4D,dummy_3dor4d]= acid_eb_riciancorr_raw(job.prefix, job.encha, job.noise_est, char(job.in_vols), job.dummy_3dor4d);
if(dummy_3dor4d==false)
    out.ricefile = my_spm_file(char(job.ricefile),'prefix',prefix,'format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V4D)))
    out.ricefile = {V4D(1).fname};
elseif(dummy_3dor4d==true && logical(isempty(V4D)))
    out.ricefile = [];
end


function dep = vout_ricianbias(job)
dep(1)            = cfg_dep;
dep(1).sname      = 'Rician bias corrected';
dep(1).src_output = substruct('.','ricefile');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});


%---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);                
                if(nargin>=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end    
        else
            varout  = varargin{1};            
        end
    end
end