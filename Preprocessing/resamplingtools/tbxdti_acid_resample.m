function resample2dim = tbxdti_acid_resample(sources)

    % ---------------------------------------------------------------------
    % b-value directions
    % ---------------------------------------------------------------------
    Voxdim         = cfg_entry;
    Voxdim.tag     = 'Voxdim';
    Voxdim.name    = 'Voxel size in Millimeters';
    Voxdim.help    = {'Provide a 1 x 3  - vector with the desired voxel size.'};
    Voxdim.strtype = 'e';
    Voxdim.num     = [1 3];
    Voxdim.val     = {[1 1 1]};
    %% Begin Resample tool
    % ---------------------------------------------------------------------
    % Make brain mask
    % ---------------------------------------------------------------------
    resample2dim         = cfg_exbranch;
    resample2dim.tag     = 'resample2dim';
    resample2dim.name    = 'Resample to dimension';
    resample2dim.val     = {sources Voxdim};
    resample2dim.help    = {
                        'This function resamples the DTI dataset to a resolution of choice.'
                        'Interpolation to a higher spatial resolution might be advantageous for improved delination of small structures in the brain.' 
                        'For spinal cord DTI (Mohammadi et al., Neuroimage, 2013), we showed that interpolation to higher in-plane spatial resolution increased the effective resolution of the tensor estimates and thus improved deliniation of the butterfly-shaped gray matter structure in the spinal cord.'
                        'Changeable defaults: - interpol_reslice: Interpolation order as defined in spm_reslice. A splin-interpolation is used by default. '}';
    resample2dim.prog = @local_resampletool;
    resample2dim.vout = @vout_resampletool;


%----interpolation-------------------
function out = local_resampletool(job)

interpol_reslice = acid_get_defaults('resample.interpol_reslice');

resize_img_rotate(char(job.sources), job.Voxdim, nan(2,3), false, interpol_reslice);
out.rfiles = my_spm_file(job.sources(:),'prefix','i');
% out.rfiles   = spm_file(job.sources(:), 'prefix','i');

%------------------------
function dep = vout_resampletool(job)
dep(1)            = cfg_dep;
dep(1).sname      = 'Resized images';
dep(1).src_output = substruct('.','rfiles');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

%---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);                
                if(nargin>=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end    
        else
            varout  = varargin{1};            
        end
    end
end


