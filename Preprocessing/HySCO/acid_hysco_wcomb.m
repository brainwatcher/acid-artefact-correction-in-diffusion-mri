% acid_hysco_wcomb
% Input:
% 
%  PVG1         - filename of reference volume
%  POI1         - matrix of filenames for additional blip-up volumes
%  POI2         - matrix of filenames for additional blip-down volumes
%  Pw1          - filenames of weights for blip-up volumes (optional)
%  Pw2          - filenames of weights for blip-down volumes (optional)
%  VB           - filename of inhomogeneity estimate produced by HySCO
%  pe_direction - phase-encoding direction, 1 for x_1, 2 for x_2, 3 for x_3
%                 (data dimensions will be flipped accordingly)
%  dummy_3dor4d - 3D or 4D dataset
%  dummy_write  - write data (arithmetic mean, weighted combination, or both)
%  dummy_ACID   - the fieldmap is based on HySCO or FSL topup
%
% Please cite one of the following works when using this software
%
%
% =======================================================================================
% Based on HySCO_main to allow for combination of multiple HySCO
% fieldmaps as well as their weighted combination.  
% S. Mohammadi 2.10.2019

function [dummy_3dor4d,V14Dwa,V14Dam] = acid_hysco_wcomb(PVG1,POI1,POI2,Pw1,Pw2,PB,pe_direction,dummy_3dor4d,dummy_write,dummy_ACID)
VG1  = spm_vol(PVG1);
VOI1 = spm_vol(POI1);
VOI2 = spm_vol(POI2);
% default reslice parameter
if(~exist('res','var'))
    res = -7;
end
if(~exist('kt','var'))
    if dummy_ACID==1
        kt = 10;
    else
        kt = 5;%20;
    end
end
if(~exist('k0','var'))
    k0 = 1;
end
V14Dwa = [];
V14Dam = [];
% extract data resolution and domain info 
% 
% Note that domain is assumed to be rectangular and aligned to the coordinate system,
% i.e. omega = [omega(1),omega(2)] x [omega(3),omega(4)] x [omega(5),omega(6)]
if numel(VOI1)>=1,
    m     = VG1.dim;
    Vmat  = sqrt(sum(VG1.mat(1:3,1:3).^2)); % modified by SM to make sure that voxel size is kept
elseif numel(VOI2)>=1,
    m     = VG1.dim;
    Vmat  = sqrt(sum(VG1.mat(1:3,1:3).^2)); % modified by SM to make sure that voxel size is kept
else
    error('No input volumes supplied!');
end
    
omega = zeros(1,6);
omega(2:2:end) = Vmat(1:3).*m; % modified by SM to make sure that voxel size is kept

% sort images if 4d
if(~isempty(POI1))
    for i=1:size(POI1,1)
        Vtmp = spm_vol(POI1(i,:));
        if(size(Vtmp,1)>1)
            VOI1(i) = Vtmp(1);
        else
            VOI1(i) = Vtmp;
        end
    end
    if(size(POI1,1)>=2)
        Vtmp1=spm_vol(POI1(1,:));
        Vtmp2=spm_vol(POI1(2,:));
        if(strcmp(Vtmp1.fname,Vtmp2.fname))
            dummy_3dor4d = true;
        end
    end
else
    VOI1 = spm_vol(POI1);
end

if(~isempty(POI2))
    for i=1:size(POI2,1)
        Vtmp = spm_vol(POI2(i,:));
        if(size(Vtmp,1)>1)
            dummy_3dor4d = true;
            VOI2(i) = Vtmp(1);
        else
            VOI2(i) = Vtmp;
        end
    end
    if(size(POI2,1)>=2)
        Vtmp1=spm_vol(POI2(1,:));
        Vtmp2=spm_vol(POI2(2,:));
        if(strcmp(Vtmp1.fname,Vtmp2.fname))
            dummy_3dor4d = true;
        end
    end
else
    VOI2 = spm_vol(POI2);
end

% sort images if 4d
if(~isempty(Pw1))
    for i=1:size(Pw1,1)
        Vtmp = spm_vol(Pw1(i,:));
        if(size(Vtmp,1)>1)
            Vw1(i) = Vtmp(1);
        else
            Vw1(i) = Vtmp;
        end
    end
    if(size(Pw1,1)>=2)
        Vtmp1=spm_vol(Pw1(1,:));
        Vtmp2=spm_vol(Pw1(2,:));
        if(strcmp(Vtmp1.fname,Vtmp2.fname))
            dummy_3dor4d = true;
        end
    end
else
    Vw1 = spm_vol(Pw1);
end

if(~isempty(Pw2))
    for i=1:size(Pw2,1)
        Vtmp = spm_vol(Pw2(i,:));
        if(size(Vtmp,1)>1)
            Vw2(i) = Vtmp(1);
        else
            Vw2(i) = Vtmp;
        end
    end
    if(size(Pw2,1)>=2)
        Vtmp1=spm_vol(Pw2(1,:));
        Vtmp2=spm_vol(Pw2(2,:));
        if(strcmp(Vtmp1.fname,Vtmp2.fname))
            dummy_3dor4d = true;
        end
    end
else
    Vw2 = spm_vol(Pw2);
end

if ~isempty(PB)
    % find out if inhomogeneity is nodal or staggered
    Bc = spm_read_vols(spm_vol(PB(1,:))); 
    if dummy_ACID
        isNodal = numel(Bc)==prod(m+1);
        mstg    = m; mstg(pe_direction) = mstg(pe_direction)+1;
        isStg   = numel(Bc)==prod(mstg);
        if not(isNodal) && not(isStg),
            error('resolution of inhomogeneity not compatible with data');
        end

        % permute data dimensions such that phase encoding is along second index
        if isNodal
            switch pe_direction
                case 1
                    read_data   = @(str) permute(sm_read_vols(str,VG1,res),[2 1 3]);
                    write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[2 1 3]),V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[2 1 3]),V,VG,prefix,Ni,Nvol,sz);
                    omega=omega([3 4 1 2 5 6]);  m= m([2 1 3]);
                    vecperm = [2 1 3];
                case 2
                    read_data   = @(str) sm_read_vols(str,VG1,res);
                    write_data  = @(A,V,prefix) spm_write_image(A,V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(A,V,VG,prefix,Ni,Nvol,sz);
                    vecperm = [1 2 3];
                case 3
                    read_data   = @(str) permute(sm_read_vols(str,VG1,res),[1 3 2]);
                    write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[1 3 2]),V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[1 3 2]),V,VG,prefix,Ni,Nvol,sz);
                    omega=omega([1 2 5 6 3 4]);  m=m([1 3 2]);
                    vecperm = [1 3 2];
            end
        elseif isStg
            switch pe_direction
                case 1
                    read_data   = @(str) sm_read_vols(str,VG1(1),res);
                    write_data  = @(A,V,prefix) spm_write_image(A,V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(A,V,VG,prefix,Ni,Nvol,sz);
                    vecperm = [1 2 3];
                case 2
                    read_data   = @(str) permute(sm_read_vols(str,VG1(1),res),[2 1 3]);
                    write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[2 1 3]),V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[2 1 3]),V,VG,prefix,Ni,Nvol,sz);
                    omega=omega([3 4 1 2 5 6]);  m= m([2 1 3]);
                    vecperm = [2 1 3];
                case 3
                    read_data   = @(str) permute(sm_read_vols(str,VG1(1),res),[3 1 2]);
                    write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[3 1 2]),V,prefix,VG1);
                    write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[3 1 2]),V,VG,prefix,Ni,Nvol,sz);
                    omega=omega([ 5 6 1:4]);  m=m([3 1 2]);
                    vecperm = [3 1 2];
            end
        end
        % save inhomogeneity
        Bc = zeros(size(permute(spm_read_vols(spm_vol(PB(1,:))),vecperm)));
        for inx=1:size(PB,1)
            Bctmp = permute(spm_read_vols(spm_vol(PB(inx,:))),vecperm);
            Bc = Bctmp+Bc;
        end

        % compute transformations and intensity modulations
        if isNodal,
            y1    = getTrafoEPI(Bc,[0;1;0],omega,m,'matrixFree',1);
            y2    = getTrafoEPI(Bc,[0;-1;0],omega,m,'matrixFree',1);
            y1    = nodal2center(y1,m);
            y2    = nodal2center(y2,m);
            pB    = getPartialB(Bc,omega,m,'cc','matrixFree',1);
        elseif isStg
            xc    = reshape(getCellCenteredGrid(omega,m),[],3);
            Bc    = reshape(Bc,m+[1,0,0]);                  % 1-staggered
            Bcc   = .5*(Bc(1:end-1,:,:) + Bc(2:end,:,:));   % cell-centered
            y1    = xc; y1(:,1) = y1(:,1) + Bcc(:);
            y2    = xc; y2(:,1) = y2(:,1) - Bcc(:);
            h     = (omega(2:2:end)-omega(1:2:end))./m;
            pB    = (Bc(2:end,:,:) - Bc(1:end-1,:,:))/h(1); % partial derivative
        end
    elseif dummy_ACID==0 % FSL fieldmap is assumed
        AB=acid_read_vols(spm_vol(PB),VG1,1);
         switch pe_direction
            case 1
                read_data   = @(str) permute(sm_read_vols(str,VG1,res),[2 1 3]);
                write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[2 1 3]),V,prefix,VG1);
                write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[2 1 3]),V,VG,prefix,Ni,Nvol,sz);
                omega=omega([3 4 1 2 5 6]);  m= m([2 1 3]);
                vecperm = [2 1 3];
            case 2
                read_data   = @(str) sm_read_vols(str,VG1,res);
                write_data  = @(A,V,prefix) spm_write_image(A,V,prefix,VG1);
                write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(A,V,VG,prefix,Ni,Nvol,sz);
                vecperm = [1 2 3];
            case 3
                read_data   = @(str) permute(sm_read_vols(str,VG1,res),[1 3 2]);
                write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[1 3 2]),V,prefix,VG1);
                write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[1 3 2]),V,VG,prefix,Ni,Nvol,sz);
                omega=omega([1 2 5 6 3 4]);  m=m([1 3 2]);
                vecperm = [1 3 2];
        end
        h       = (omega(2:2:end)-omega(1:2:end))./m;
        
        Bc              = permute(AB,vecperm);
        % Bc              = zscore(Bc);
        pB              =  permute(zeros(m),vecperm); 
        pB(2:end,:,:)   = (Bc(2:end,:,:) - Bc(1:end-1,:,:))/h(1); % partial derivative
    end
    Jac1  = 1 + pB; Jac1(Jac1<0)=0;
    Jac2  = 1 - pB; Jac2(Jac2<0)=0;
    write_data(Jac1,VG1,'Jac1_');
    write_data(Jac2,VG1,'Jac2_');
    
    % save corrected data. prefix is motivated by fieldmap toolbox
    prefix = ['uH' num2str(size(PB,1))];

    if (dummy_write)==1 || (dummy_write)==3
        % write weighted-average of blipup and down image volumes
        for vol=1:numel(VOI1),
            fprintf('Estimate weighted combination for volume %d\n',vol);
            I1 = read_data(VOI1(vol));
            I2 = read_data(VOI2(vol));
            if dummy_ACID
                f1 = 2-acid_fermi(Jac1,k0,kt); % we take 2- to down-weigh regions that were squeezed
                f2 = 2-acid_fermi(Jac2,k0,kt); % we take 2- to down-weigh regions that were squeezed
            else
                f1 = acid_fermi(Jac1,k0,kt); % we take acid_fermi to down-weigh regions that were squeezed
                f2 = acid_fermi(Jac2,k0,kt); % we take acid_fermi to down-weigh regions that were squeezed    
            end            
            if  ~logical(isempty(Vw1)) && ~logical(isempty(Vw1))
                Aw1 = read_data(Vw1(vol));
                Aw1(Aw1==0)=1;
                Aw2 = read_data(Vw2(vol));
                Aw2(Aw2==0)=1;
            else
                Aw1 = 1;
                Aw2 = 1;
            end
            Iwavg = (I1.*f1.*Aw1 + I2.*f2.*Aw2)./(f1.*Aw1+f2.*Aw2);
            prefixwa = [prefix 'wa'];
            fprintf('Write weighted average of blip-up and down volume %d\n',vol);
            if(dummy_3dor4d==1)
                if(vol==1)
                   [V14Dwa,Ni2] = write_data1(Iwavg,VOI1(vol),VG1,prefixwa,[],vol,numel(VOI1));
                else

                   [~,Ni2] = write_data1(Iwavg,VOI1(vol),VG1,prefixwa,Ni2,vol,[]);
                end
            else
                write_data(Iwavg,VOI1(vol),prefixwa);
            end
        end
    end 
end

if (dummy_write)==2 || (dummy_write)==3
    
    if isempty(PB)
        switch pe_direction
            case 1
                read_data   = @(str) permute(sm_read_vols(str,VG1,res),[2 1 3]);
                write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[2 1 3]),V,prefix,VG1);
            case 2
                read_data   = @(str) sm_read_vols(str,VG1,res);
                write_data  = @(A,V,prefix) spm_write_image(A,V,prefix,VG1);
            case 3
                read_data   = @(str) permute(sm_read_vols(str,VG1,res),[1 3 2]);
                write_data  = @(A,V,prefix) spm_write_image(ipermute(A,[1 3 2]),V,prefix,VG1);
                write_data1 = @(A,V,VG,prefix,Ni,Nvol,sz) spm_write_image_4d(ipermute(A,[1 3 2]),V,VG,prefix,Ni,Nvol,sz);
        end
        prefix = '';
    end
    
    % write arithmetic-average of blipup and down image volumes
    for vol=1:numel(VOI1),
        fprintf('Estimate weighted combination for volume %d\n',vol);
        I1 = read_data(VOI1(vol));
        I2 = read_data(VOI2(vol));
        
        Iwavg = (I1 + I2)*0.5;
        prefixwm = [prefix 'am'];
        if(dummy_3dor4d==1)
            if(vol==1)
               [V14Dam,Ni2] = write_data1(Iwavg,VOI1(vol),VG1,prefixwm,[],vol,numel(VOI1));
            else

               [~,Ni2] = write_data1(Iwavg,VOI1(vol),VG1,prefixwm,Ni2,vol,[]);
            end
        else
            write_data(Iwavg,VOI1(vol),prefixwm);
        end
    end
end 

% ========================================================================
% function spm_write_image(I,V,prefix)
%
% writes image volume
% 
% Input:
%   I      - image data
%   V      - structure containing image volume information
%   prefix - default 'u'
%
% ========================================================================
% ========================================================================
function Atmp = sm_read_vols(strS,strT,res)
V       = strS;
VG      = strT;
Atmp    = acid_read_vols(V,VG,res);

% ========================================================================
% function spm_write_image(I,V,prefix,VG,Nvol)
%
% writes image volume
% 
% Input:
%   I      - image data
%   V      - structure containing image volume information
%   prefix - default 'u'
%   VG     - target image
%   Nvol   - volume number
%
% ========================================================================
function V = spm_write_image(I,V,prefix,VG,Nvol)
[pth,fname,ext] = fileparts(V.fname);
if(exist('Nvol','var'))
    V.fname =   fullfile(pth, sprintf('%s%s_%03d%s',prefix,fname,Nvol,ext));
else
    V.fname = [pth filesep prefix fname ext];
end
if(~exist('VG','var'))
    VG = V;
end
if(isempty(VG))
    VG = V;
end
dt        = [spm_type('float32'),spm_platform('bigend')];
dm        = VG.dim;
Ni        = nifti;
Ni.mat    = VG.mat;
Ni.mat0   = VG.mat;
% Ni.descrip= sprintf('Averaged %s images, robust fitted', nam1{ii});
if(exist('Nvol','var'))
    Ni.dat =   file_array(fullfile(pth,sprintf('%s%s-%03d',prefix,fname,Nvol,ext)),dm,dt, 0,1,0);
else
    Ni.dat    = file_array(fullfile(pth,[prefix fname '.nii']),dm,dt, 0,1,0);
end
create(Ni);
spm_progress_bar('Init',dm(3),Ni.descrip,'planes completed');

for p=1:size(I,3)
    Ni.dat(:,:,p) = I(:,:,p);
    spm_progress_bar('Set',p);
end
spm_progress_bar('Clear');

function [V4D,Ni] = spm_write_image_4d(I,V,VG,prefix,Ni,Nvol,sz)

if(Nvol==1)
    % define nifti for EC-corrected volumes
    [pth,fname,ext] = spm_fileparts(V(1).fname);
    wV       = VG(1);
    V        = spm_vol(V(1).fname);


    dt        = [spm_type('int16'),spm_platform('bigend')];
    dm        = [VG(1).dim sz];
    Ni        = nifti;
    Ni.mat    = VG(1).mat;
    Ni.mat0   = VG(1).mat;

    V4D       = spm_file_merge(wV,fullfile(pth,[prefix fname]),spm_type('UINT8'));

    Ni.dat    = file_array(V4D.fname,dm,dt, 0,1,0);
    Ni.descrip = ['4d array of HYSCO corrected images'];
    create(Ni);
    spm_progress_bar('Init',dm(4),Ni.descrip,'volumeses completed');
else 
    V4D = [];
end

% select the image to write
Ni.dat(:,:,:,Nvol) = I;
spm_progress_bar('Set',Nvol);

disp(['Image #: ' num2str(Nvol) ' undistorted'])
if(Nvol == size(Ni.dat,4))
    spm_progress_bar('Clear');
end

%{
    (c) Lars Ruthotto and Jan Modersitzki 2013

    This file is part of HySCO (Version 1.0, 2013/03/28)
                           -  Hyperelastic Susceptibility Artefact Correction for DTI

    
    HySCO is free but copyright software, distributed under the terms of the 
    GNU General Public Licence as published by the Free Software Foundation 
    (Version 3, 29 June 2007) http://www.gnu.org/licenses/gpl.html

 
    This code is provided "as is", without any warranty of any kind, either
    expressed or implied, including but not limited to, any implied warranty
    of merchantibility or fitness for any purpose. In no event will any party
    who distributed the code be liable for damages or for any claim(s) by
    any other party, including but not limited to, any lost profits, lost
    monies, lost data or data rendered inaccurate, losses sustained by
    third parties, or any other special, incidental or consequential damages
    arising out of the use or inability to use the program, even if the
    possibility of such damages has been advised against. The entire risk
    as to the quality, the performace, and the fitness of the program for any
    particular purpose lies with the party using the code.

    This code is especially not intended for any clinical or diagnostic use. 
  
%}
