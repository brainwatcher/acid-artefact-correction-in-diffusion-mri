function fair_epi_choice = tbxdti_acid_hysco(dummy_3dor4d, dummy_hysco_apply_multi)


% ---------------------------------------------------------------------
% source up
% ---------------------------------------------------------------------
source_up         = cfg_files;
source_up.tag     = 'source_up';
source_up.name    = 'Reference blip-up image';
source_up.help    = {
''
'Select one image volume acquired with blip-up. The field inhomogeneity is estimated by minimizing the sum-of-squared difference between this image and the blip-down image chosen below and regularization.'
''};
source_up.filter = 'image';
source_up.ufilter = '.*';
source_up.num     = [0 1];

% ---------------------------------------------------------------------
% source dw
% ---------------------------------------------------------------------
source_dw         = cfg_files;
source_dw.tag     = 'source_dw';
source_dw.name    = 'Reference blip-down image';
source_dw.help    = {''
'Select one image volume acquired with blip-down. The field inhomogeneity is estimated by minimizing the sum-of-squared difference between this image and the blip-up image chosen above and regularization.'
''};
source_dw.filter = 'image';
source_dw.ufilter = '.*';
source_dw.num     = [0 1];

% ---------------------------------------------------------------------
% others up
% ---------------------------------------------------------------------
others_up         = cfg_files;
others_up.tag     = 'others_up';
others_up.name    = 'Other blip-up images';
others_up.help    = {''
'(optional) Choose other image volumes acquired with blip-up that need to be corrected. The data is corrected by applying the transformation estimated by the reference bip-up/down data. If an equal number of blip-up and blip-down data is provided, you may also want to disable ''Apply to other images''.'
''};
others_up.filter = 'image';
others_up.ufilter = '.*';
others_up.num     = [0 Inf];
others_up.val    = {{''}};

% ---------------------------------------------------------------------
% others dw
% ---------------------------------------------------------------------
others_dw         = cfg_files;
others_dw.tag     = 'others_dw';
others_dw.name    = 'Other blip-down images';
others_dw.help    = {''
'(optional) Choose other image volumes acquired with blip-down that need to be corrected. The data is corrected by applying the transformation estimated by the reference bip-up/down data. If an equal number of blip-up and blip-down data is provided, you may also  want to disable ''Apply to other images''.'
''};
others_dw.filter = 'image';
others_dw.ufilter = '.*';
others_dw.num     = [0 Inf];
others_dw.val    = {{''}};

% ---------------------------------------------------------------------
% perm_dim
% ---------------------------------------------------------------------
perm_dim         = cfg_menu;
perm_dim.tag     = 'perm_dim';
perm_dim.name    = 'Dimension of phase-encoding';
perm_dim.help    = {''
'Specify the phase-encoding or distortion direction of your data.'
''};
perm_dim.labels = {'x','y','z'};
perm_dim.values = {1 2 3};
perm_dim.val    = {2};

% ---------------------------------------------------------------------
% dummy_fast
% ---------------------------------------------------------------------
dummy_fast         = cfg_menu;
dummy_fast.tag     = 'dummy_fast';
dummy_fast.name    = 'Maximal data resolution';
dummy_fast.help    = {''
'Choose the finest discretization level for field inhomogeneity estimation. If set to ''full'' a multi-level strategy with three discretization levels is used, where the resolution on the finest level equals the data resolution. To save computation time, choose ''half''. The multi-level scheme will be stopped after the second level (i.e. half of data resolution) and the inhomogeneity estimate will be interpolated to the data resolution.'
''};
dummy_fast.labels = {
               'half'
               'full'
};
dummy_fast.values = {0 1};
dummy_fast.val    = {1};

% ---------------------------------------------------------------------
% dummy_ecc
% ---------------------------------------------------------------------
dummy_ecc         = cfg_menu;
dummy_ecc.tag     = 'dummy_ecc';
dummy_ecc.name    = 'Apply to other images';
dummy_ecc.help    = {''
'Apply the field inhomogeneity estimated from the reference images to the ''other image volumes'' (see 3 and 4). If set to ''no'' and if the same number of diffusion-weighted images is provided for blip-up and blip-down the susceptibility correction is done for each image separately (This might be useful to correct for the distortions induced by nonlinear eddy current fields). To this end, the field-inhomogeneity estimated from the non-diffusion weighted images is used as a starting guess for minimization of the distance between the respective diffusion-weighted image pairs. Optimization is only carried out on the finest discretization level to save computation time.'
''};
dummy_ecc.labels = {
               'no'
               'yes'
};
dummy_ecc.values = {1 0};
dummy_ecc.val    = {0};
%{
% ---------------------------------------------------------------------
% theta
% ---------------------------------------------------------------------
theta         = cfg_entry;
theta.tag     = 'theta';
theta.name    = 'Smoothing of spline-interpolation.';
theta.help    = {'Choose parameter theta that balances between the data fit and the smoothness of the cubic B-spline approximation of the image data. For theta equal to zero a standard cubic B-spline interpolation is used. For positive theta data is only approximated, but the image representation is smoother. Thus, theta can be used to adjust for noise level of the data. Note that this scheme is only used for the optimization and in particular that the corrected image data is obtained by resampling using a standard tri-linear interpolation.'
'' 
'For details see Section 3.4 in:'
'Modersitzki, J. FAIR: Flexible Algorithms for Image Registration. Society for Industrial and Applied Mathematics (SIAM); 2009.'
''
};
theta.strtype = 'e';
theta.num     = [1 1];
theta.val    = {1e-1};

% ---------------------------------------------------------------------
% restrictdim
% ---------------------------------------------------------------------
restrictdim         = cfg_entry;
restrictdim.tag     = 'restrictdim';
restrictdim.name    = 'Choose dimensions for multilevel coarsening.';
restrictdim.help    = {'Choose data dimensions that are coarsened in multilevel optimization. '
    ''
    'Background: To accelerate computations and improve robustness against local minima, we use a multilevel'
    'strategy for optimization. One part of this is to represent the image data by coarsened versions using averaging.'
    'This setting affects which dimensions are coarsened.'
    ''
    'Default: [1,1,1] coarsens data along all coordinate directions'
    ''
    'Example: choosing [1,1,0] avoids coarsening along the third direction (commonly, the slice selection direction).'
    'This choice is motivated by the fact that EPI distortions are typically restricted within slices.'
    ''
    'Note: This flag does not affect the final resolution of the estimated inhomogeneity, which will be'
    'related to the number of voxels in the original image data.'
    ''
    };
restrictdim.strtype = 'w';
restrictdim.num     = [3 1];
restrictdim.val    = {[1 1 1]};

% ---------------------------------------------------------------------
% alpha
% ---------------------------------------------------------------------
alpha         = cfg_entry;
alpha.tag     = 'alpha';
alpha.name    = 'Weight for "diffusion" regularizer.';
alpha.help    = {                  
                    ''
                    'Choose regularization parameter alpha that weights the ''diffusion'' regularizer. For larger values of alpha, the computed solution will in general be smoother, but the image distance between the corrected blip-up and blip-down image will be larger.'
                    ''};
alpha.strtype = 'e';
alpha.num     = [1 1];
alpha.val    = {50};

% ---------------------------------------------------------------------
% beta
% ---------------------------------------------------------------------
beta         = cfg_entry;
beta.tag     = 'beta';
beta.name    = 'Weight for "Jacobian" regularizer.';
beta.help    = {                  
                  ''
                  'Choose regularization parameter beta that weights the ''Jacobian'' regularizer that guarantees the invertibility of the transformations. By design the value of this regularization functional grows to infinity when the Jacobian determinant of either of the geometrical transformations approaches zero. Thus, for larger/smaller values of beta, the range of the Jacobian determinants, which translates to the maximum compression/expansion of volume, becomes smaller/larger. However, for any beta that is greater than zero both transformations will be invertible.'
                  ''};
beta.strtype = 'e';
beta.num     = [1 1];
beta.val    = {10};
%% begin defining fair_epi susceptibility correction
%}
% ---------------------------------------------------------------------
% fair_epi susceptibility correction
% ---------------------------------------------------------------------
fair_epi         = cfg_exbranch;
fair_epi.tag     = 'hysco';
fair_epi.name    = 'HySCO 1.0';
fair_epi.val     = {source_up source_dw others_up others_dw perm_dim dummy_fast dummy_ecc dummy_3dor4d};
%fair_epi.val     = {source_up source_dw others_up others_dw perm_dim dummy_fast dummy_ecc theta alpha beta dummy_3dor4d restrictdim};
fair_epi.help    = {
                    'Hyperelastic susceptibility artifact correction of diffusion weighted images'
                    ''
                    'HySCO enables correction of susceptibility artifacts in diffusion weighted images using two images acquired with reversed phase encoding gradients.'
                    ''
                    'A nonlinear regularization functional, which is inspired by hyperelasticity, ensures smoothness of the field inhomogeneity and invertibility of the geometrical transformations. It consists of two components for which regularization parameters have to be chosen: A "diffusion" part that enforces smoothness of the field and a "Jacobian" part that guarantees invertibility of the estimated transformations.'
                    'Changeable defaults: '
                    '- alpha: Choose regularization parameter alpha that weights the ''diffusion'' regularizer. For larger values of alpha, the computed solution will in general be smoother, but the image distance between the corrected blip-up and blip-down image will be larger.'
                    '- beta: Choose regularization parameter beta that weights the ''Jacobian'' regularizer that guarantees the invertibility of the transformations. By design the value of this regularization functional grows to infinity when the Jacobian determinant of either of the geometrical transformations approaches zero. Thus, for larger/smaller values of beta, the range of the Jacobian determinants, which translates to the maximum compression/expansion of volume, becomes smaller/larger. However, for any beta that is greater than zero both transformations will be invertible.'
                    '- theta: Choose parameter theta that balances between the data fit and the smoothness of the cubic B-spline approximation of the image data. For theta equal to zero a standard cubic B-spline interpolation is used. For positive theta data is only approximated, but the image representation is smoother. Thus, theta can be used to adjust for noise level of the data. Note that this scheme is only used for the optimization and in particular that the corrected image data is obtained by resampling using a standard tri-linear interpolation.'
                                'For details see Section 3.4 in:'
                                'Modersitzki, J. FAIR: Flexible Algorithms for Image Registration. Society for Industrial and Applied Mathematics (SIAM); 2009.'
                    '- restrictdim: Choose data dimensions that are coarsened in multilevel optimization.'

                                    'Background: To accelerate computations and improve robustness against local minima, we use a multilevel'
                                    'strategy for optimization. One part of this is to represent the image data by coarsened versions using averaging.'
                                    'This setting affects which dimensions are coarsened.'

                                    'Default: [1,1,1] coarsens data along all coordinate directions'

                                    'Example: choosing [1,1,0] avoids coarsening along the third direction (commonly, the slice selection direction).'
                                    'This choice is motivated by the fact that EPI distortions are typically restricted within slices.'

                                    'Note: This flag does not affect the final resolution of the estimated inhomogeneity, which will be'
                                    'related to the number of voxels in the original image data.'
                    'For more information and references see:'
                    ''
                    'Ruthotto, L, Kugel, H, Olesch, J, Fischer, B, Modersitzki, J, Burger, M, and Wolters, C H. '
                    'Diffeomorphic Susceptibility Artefact Correction of Diffusion-Weighted Magnetic Resonance Images. '
                    'Physics in Medicine and Biology, 57(18), 5715-5731; 2012.'
                    ''
                    'Ruthotto, L, Mohammadi, S, Heck, C, Modersitzki, J, and Weiskopf, N.'
                    'HySCO - Hyperelastic Susceptibility Artifact Correction of DTI in SPM.'
                    'Presented at the Bildverarbeitung fuer die Medizin 2013.'
};
fair_epi.prog = @local_fairepi_main;
fair_epi.vout = @vout_fairepi_main;
  % ---------------------------------------------------------------------
% source up
% ---------------------------------------------------------------------
source_up         = cfg_files;
source_up.tag     = 'source_up';
source_up.name    = 'Blip-up image';
source_up.help    = {
''
'Select one image volume acquired with blip-up. The reference field inhomogeneity is estimated from this image.'
''};
source_up.filter = 'image';
source_up.ufilter = '.*';
source_up.num     = [0 1];

% ---------------------------------------------------------------------
% source dw
% ---------------------------------------------------------------------
source_dw         = cfg_files;
source_dw.tag     = 'source_dw';
source_dw.name    = 'Blip-down image';
source_dw.help    = {''
'Select one image volume acquired with blip-down. The reference field inhomogeneity is estimated from this image.'
''};
source_dw.filter = 'image';
source_dw.ufilter = '.*';
source_dw.num     = [0 1];
% ---------------------------------------------------------------------
% source up
% ---------------------------------------------------------------------
bfield         = cfg_files;
bfield.tag     = 'bfield';
bfield.name    = 'Inhomogeneity field (or done for none)';
bfield.help    = {
''
'Select one image volume that contains the field inhomoneities.'
''};
bfield.filter = 'image';
bfield.ufilter = '.*';
bfield.num     = [0 Inf];

%% begin defining hysco2 susceptibility correction
% ---------------------------------------------------------------------
% fair_epi susceptibility correction
% ---------------------------------------------------------------------
hysco2         = cfg_exbranch;
hysco2.tag     = 'hysco2';
hysco2.name    = 'HySCO';
hysco2.val     = {source_up source_dw others_up others_dw perm_dim dummy_fast dummy_ecc dummy_3dor4d};
hysco2.help    = {
                    'Hyperelastic susceptibility artifact correction of diffusion weighted images'
                    ''
                    'HySCO enables correction of susceptibility artifacts in diffusion weighted images using two images acquired with reversed phase encoding gradients.'
                    ''
                    'A nonlinear regularization functional, which is inspired by hyperelasticity, ensures smoothness of the field inhomogeneity and invertibility of the geometrical transformations. It consists of two components for which regularization parameters have to be chosen: A "diffusion" part that enforces smoothness of the field and a "Jacobian" part that guarantees invertibility of the estimated transformations.'
                    ''
                    'This code is faster and more accurate than HySCO 1.0 due to the inexact Gauss-Newton optimization proposed in Macdonald and Ruthotto 2016'
                    ''
                    'For more information and references see:'
                    ''
                    'MacDonald, J, Ruthotto, L, '
                    'Efficient Numerical Optimization For Susceptibility Artifact Correction Of EPI-MRI'
                    'arXiv, 2016'
                    ''
                    'Ruthotto, L, Kugel, H, Olesch, J, Fischer, B, Modersitzki, J, Burger, M, and Wolters, C H. '
                    'Diffeomorphic Susceptibility Artefact Correction of Diffusion-Weighted Magnetic Resonance Images. '
                    'Physics in Medicine and Biology, 57(18), 5715-5731; 2012.'
                    ''
                    'Ruthotto, L, Mohammadi, S, Heck, C, Modersitzki, J, and Weiskopf, N.'
                    'HySCO - Hyperelastic Susceptibility Artifact Correction of DTI in SPM.'
                    'Presented at the Bildverarbeitung fuer die Medizin 2013.'
};
hysco2.prog = @local_hysco2_main;
hysco2.vout = @vout_hysco2_main;



%% begin defining of hysco_write 
% ---------------------------------------------------------------------
% define space defining image 
% ---------------------------------------------------------------------

source_up_write = source_up;
source_up_write.help     = {
    'Space defining image. Select the image volume acquired with blip-up that was used as reference image to estimate fieldmap.'};

% ---------------------------------------------------------------------
% hysco_write apply susceptibility correction
% ---------------------------------------------------------------------
hysco_apply         = cfg_exbranch;
hysco_apply.tag     = 'hysco_write';
hysco_apply.name    = 'Write HySCO corrected images';
hysco_apply.val     = {source_up_write others_up others_dw bfield perm_dim dummy_3dor4d};
hysco_apply.help    = {
                    'This option applies the field-inhomogeneities from the Hyperelastic susceptibility artifact correction of diffusion weighted images to undistort images of choice.'					
};
hysco_apply.prog = @local_hysco_write;
hysco_apply.vout = @vout_hysco_write;

% ---------------------------------------------------------------------
% define weight images
% ---------------------------------------------------------------------

weights_up = others_up;
weights_up.tag     = 'weights_up';
weights_up.name    = 'Weightes for blip-up image';
weights_up.val    = {{''}};

weights_up.help     = {
    'Weights for weighted combination.'};


weights_dw = others_dw;
weights_dw.tag     = 'weights_dw';
weights_dw.name    = 'Weightes for blip-dw image';
weights_dw.val    = {{''}};

weights_dw.help     = {
    'Weights for weighted combination.'};

% ---------------------------------------------------------------------
% multihysco_write apply susceptibility correction
% ---------------------------------------------------------------------
multihysco_wcomb_write          = cfg_menu;
multihysco_wcomb_write.tag     = 'multihysco_wcomb_write';
multihysco_wcomb_write.name    = 'Write combined images';
multihysco_wcomb_write.help    = {''
'Specify what is written out.'
''};
multihysco_wcomb_write.labels = {'Write nothing','Write weighted mean image', 'Write arithmetic mean image', 'Write arithmetic and weighted mean image'};
multihysco_wcomb_write.values = {0 1 2 3};
multihysco_wcomb_write.val    = {1};

% ---------------------------------------------------------------------
% hysco_write apply susceptibility correction
% ---------------------------------------------------------------------
hysco_apply_multi         = cfg_exbranch;
hysco_apply_multi.tag     = 'hysco_apply_multi';
hysco_apply_multi.name    = 'Write corrected images use multiple HySCO fieldmaps';
hysco_apply_multi.val     = {source_up_write others_up others_dw bfield perm_dim dummy_3dor4d };
hysco_apply_multi.help    = {
                    'This module writes the Jacobian estimated from the HySCO map, as well as unwarped blip-up and down image.'
                    'If more than one HySCO map is provided, the HySCO maps will be added up.'
};
hysco_apply_multi.prog = @local_hysco_write_multi;
hysco_apply_multi.vout = @vout_hysco_write_multi;

% ---------------------------------------------------------------------
% Unwarped blip-up images
% ---------------------------------------------------------------------
others_u2_up         = cfg_files;
others_u2_up.tag     = 'others_u2_up';
others_u2_up.name    = 'Unwarped blip-up images';
others_u2_up.help    = {''
'Choose unwarped image volumes acquired with blip-up.'
};
others_u2_up.filter = 'image';
others_u2_up.ufilter = '.*';
others_u2_up.num     = [0 Inf];
others_u2_up.val    = {{''}};
 % ---------------------------------------------------------------------
% Unwarped blip-dw images
% ---------------------------------------------------------------------
others_u2_dw         = cfg_files;
others_u2_dw.tag     = 'others_u2_dw';
others_u2_dw.name    = 'Unwarped blip-dw images';
others_u2_dw.help    = {''
'Choose unwarped image volumes acquired with blip-down.'
};
others_u2_dw.filter = 'image';
others_u2_dw.ufilter = '.*';
others_u2_dw.num     = [0 Inf];
others_u2_dw.val    = {{''}};

% ---------------------------------------------------------------------
% Is the fieldmap from FSL or ACID?
% ---------------------------------------------------------------------
dummy_ACID          = cfg_menu;
dummy_ACID.tag     = 'dummy_ACID';
dummy_ACID.name    = 'ACID or FSL fieldmap';
dummy_ACID.help    = {''
'Specify the toolbox from which the fieldmap was generated. By default an ACID-fieldmap is assumed (the name starts with "HySCO").'
''};
dummy_ACID.labels = {'ACID','FSL'};
dummy_ACID.values = {1 0};
dummy_ACID.val    = {1};
% ---------------------------------------------------------------------
% hysco_write comb susceptibility correction
% ---------------------------------------------------------------------
hysco_wcomb_multi         = cfg_exbranch;
hysco_wcomb_multi.tag     = 'hysco_wcomb_multi';
hysco_wcomb_multi.name    = 'Write combined images using multiple HySCO fieldmaps';
hysco_wcomb_multi.val     = {source_up_write others_u2_up others_u2_dw weights_up weights_dw bfield perm_dim dummy_3dor4d multihysco_wcomb_write dummy_ACID};
hysco_wcomb_multi.help    = {
                    'This option applies the field-inhomogeneities from the Hyperelastic susceptibility artifact correction of diffusion weighted images to undistort images of choice.'
                    'The output is either the arithmetic mean or the weighted combination.'
};
hysco_wcomb_multi.prog = @local_hysco_wcomb_multi;
hysco_wcomb_multi.vout = @vout_hysco_wcomb_multi;
%% ---------------------------------------------------------------------
% HYSCO choice
% ---------------------------------------------------------------------
fair_epi_choice         = cfg_choice;
fair_epi_choice.tag     = 'hysco_choice';
fair_epi_choice.name    = 'Choose HySCO options';

fair_epi_choice.help    = {
                 'Estimate field-inhomogeneities from blip-up and -down images and apply it to undistort the images.'
                 'Apply field-inhomogneities to undistorted other image(s).'
}';
if dummy_hysco_apply_multi
    fair_epi_choice.values  = {hysco2 hysco_apply hysco_apply_multi hysco_wcomb_multi};
else
    fair_epi_choice.values  = {hysco2 hysco_apply};
end

% if dummy_hysco_apply_multi
%     fair_epi_choice.values  = {fair_epi hysco2 hysco_apply hysco_apply_multi hysco_wcomb_multi};
% else
%     fair_epi_choice.values  = {fair_epi hysco2 hysco_apply};
% end


% ----fair_epi-------------------
function out = local_fairepi_main(job)

alpha       = acid_get_defaults('hysco.alpha');
beta        = acid_get_defaults('hysco.beta');
theta       = acid_get_defaults('hysco.theta');
restrictdim = acid_get_defaults('hysco.restrictdim');


[dummy_3dor4d,V14D,V24D] = HySCO_main(char(job.source_up), char(job.source_dw), char(job.others_up), char(job.others_dw),job.perm_dim,job.dummy_fast,job.dummy_ecc,theta,alpha,beta,job.dummy_3dor4d,restrictdim);

% out.rothers_up = my_spm_file(char(job.others_up(1)),'prefix','r','format','.nii');
if(dummy_3dor4d==false)
    out.rothers_up = my_spm_file(char(job.others_up),'prefix','u','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V14D)))
    out.rothers_up = {V14D(1).fname};
end
out.rothers_up = acid_my_select(out.rothers_up(:),dummy_3dor4d);
out.rothers_up = out.rothers_up(:);
if(dummy_3dor4d==false)
    out.rothers_dw = my_spm_file(char(job.others_dw),'prefix','u','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V24D)))
    out.rothers_dw = {V24D(1).fname};
end
out.rothers_dw = acid_my_select(out.rothers_dw(:),dummy_3dor4d);
out.rothers_dw = out.rothers_dw(:);
out.fieldmap   = my_spm_file(char(job.source_dw(1)),'prefix','HySCO_','format','.nii');
out.fieldmap   = acid_my_select(out.fieldmap(:));
out.fieldmap   = out.fieldmap(:);
%------------------------
function dep = vout_fairepi_main(job)
kk=1;
dep(1)            = cfg_dep;
dep(1).sname      = 'Unwarped Blip up images';
dep(1).src_output = substruct('.','rothers_up');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk+1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Unwarped Blip down images';
dep(kk).src_output = substruct('.','rothers_dw');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated fieldmap';
dep(kk).src_output = substruct('.','fieldmap');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

% ----fair_epi-------------------
function out = local_hysco2_main(job)

alpha       = acid_get_defaults('hysco.alpha');
beta        = acid_get_defaults('hysco.beta');
restrictdim = acid_get_defaults('hysco.restrictdim');

[dummy_3dor4d,V14D,V24D] = HySCO2_main(char(job.source_up), char(job.source_dw), char(job.others_up), char(job.others_dw),job.perm_dim,job.dummy_fast,job.dummy_ecc,alpha,beta,job.dummy_3dor4d,restrictdim);

out.rothers_up = {''};
% out.rothers_up = my_spm_file(char(job.others_up(1)),'prefix','r','format','.nii');
if(dummy_3dor4d==false)
    out.rothers_up = my_spm_file(char(job.others_up),'prefix','u2','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V14D)))
    out.rothers_up = {V14D(1).fname};
end
if(logical(strcmp('rothers_up',fieldnames(out))) && logical(~isempty(out.rothers_up)))
    out.rothers_up = acid_my_select(out.rothers_up(:),dummy_3dor4d);
    out.rothers_up = out.rothers_up(:);
end

out.rothers_dw = {''};
if(dummy_3dor4d==false)
    out.rothers_dw = my_spm_file(char(job.others_dw),'prefix','u2','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V24D)))
    out.rothers_dw = {V24D(1).fname};
end
if(logical(sum(strcmp('rothers_dw',fieldnames(out)))>0) && logical(~isempty(out.rothers_up)))
    out.rothers_dw = acid_my_select(out.rothers_dw(:),dummy_3dor4d);
    out.rothers_dw = out.rothers_dw(:);
else
    out.rothers_dw = [];
end
out.fieldmap   = my_spm_file(char(job.source_dw(1)),'prefix','HySCOv2_','format','.nii');
out.fieldmap   = acid_my_select(out.fieldmap(:));
out.fieldmap   = out.fieldmap(:);
%------------------------
function dep = vout_hysco2_main(job)
kk=1;
dep(1)            = cfg_dep;
dep(1).sname      = 'Unwarped Blip up images';
dep(1).src_output = substruct('.','rothers_up');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk+1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Unwarped Blip down images';
dep(kk).src_output = substruct('.','rothers_dw');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated fieldmap';
dep(kk).src_output = substruct('.','fieldmap');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

%----hysco_write-------------------
function out = local_hysco_write(job)
[dummy_3dor4d,V14D,V24D] = HySCO_write(char(job.source_up), char(job.others_up), char(job.others_dw), char(job.bfield),job.perm_dim,job.dummy_3dor4d);
if(dummy_3dor4d==false)
    out.rothers_up = my_spm_file(char(job.others_up),'prefix','u','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V14D)))
    out.rothers_up = {V14D(1).fname};
elseif(dummy_3dor4d==true && logical(isempty(V14D)))
    out.rothers_up = [];
end
out.rothers_up = acid_my_select(out.rothers_up(:),dummy_3dor4d);
out.rothers_up = out.rothers_up(:);
if(dummy_3dor4d==false)
    out.rothers_dw = my_spm_file(char(job.others_dw),'prefix','u','format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V24D)))
    out.rothers_dw = {V24D(1).fname};
elseif(dummy_3dor4d==true && logical(isempty(V24D)))
    out.rothers_dw = [];
end
out.fieldmap   = job.bfield(:);

%------------------------
function dep = vout_hysco_write(job)
kk=1;
dep(1)            = cfg_dep;
dep(1).sname      = 'Unwarped Blip up images';
dep(1).src_output = substruct('.','rothers_up');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk+1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Unwarped Blip down images';
dep(kk).src_output = substruct('.','rothers_dw');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated fieldmap';
dep(kk).src_output = substruct('.','fieldmap');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});


%----hysco_write_multi-------------------
function out = local_hysco_write_multi(job)
[dummy_3dor4d,V14D,V24D,VJacup,VJacdw] = HySCO_write_multiB(char(job.source_up), char(job.others_up), char(job.others_dw), char(job.bfield),job.perm_dim,job.dummy_3dor4d);

out.fieldmap   = job.bfield(:);
if logical(~isempty(VJacup))
    out.rothers_jacup = {VJacup(1).fname};
else
    out.rothers_jacup = [];
end
if logical(~isempty(VJacdw))
    out.rothers_jacdw = {VJacdw(1).fname};
else
    out.rothers_jacdw = [];
end
prefix = ['uH' num2str(numel(out.fieldmap))];

if(dummy_3dor4d==false)
    out.rothers_up = my_spm_file(char(job.others_up),'prefix',prefix,'format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V14D)))
    out.rothers_up = {V14D(1).fname};
elseif(dummy_3dor4d==true && logical(isempty(V14D)))
    out.rothers_up = [];
end
out.rothers_up = acid_my_select(out.rothers_up(:),dummy_3dor4d);
out.rothers_up = out.rothers_up(:);

if(dummy_3dor4d==false)
    out.rothers_dw = my_spm_file(char(job.others_dw),'prefix',prefix,'format','.nii');
elseif(dummy_3dor4d==true && logical(~isempty(V24D)))
    out.rothers_dw = {V24D(1).fname};
elseif(dummy_3dor4d==true && logical(isempty(V24D)))
    out.rothers_dw = [];
end
out.rothers_dw = acid_my_select(out.rothers_dw(:),dummy_3dor4d);
out.rothers_dw = out.rothers_dw(:);

%------------------------
function dep = vout_hysco_write_multi(job)
kk=1;
dep(1)            = cfg_dep;
dep(1).sname      = 'Unwarped Blip up images';
dep(1).src_output = substruct('.','rothers_up');
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk+1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Unwarped Blip down images';
dep(kk).src_output = substruct('.','rothers_dw');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk+1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated fieldmap';
dep(kk).src_output = substruct('.','fieldmap');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated Jacoby up';
dep(kk).src_output = substruct('.','rothers_jacup');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated Jacoby dw';
dep(kk).src_output = substruct('.','rothers_jacdw');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});

%----hysco_write_multi-------------------
function out = local_hysco_wcomb_multi(job)
[dummy_3dor4d,V14Dwa,V14Dam] = acid_hysco_wcomb(char(job.source_up), char(job.others_u2_up), char(job.others_u2_dw), char(job.weights_up),char(job.weights_dw), char(job.bfield),job.perm_dim,job.dummy_3dor4d,job.multihysco_wcomb_write,job.dummy_ACID);

out.fieldmap   = job.bfield(:);

prefix = ['uH' num2str(numel(out.fieldmap))];

if ~logical(isempty(V14Dwa))
    if(dummy_3dor4d==false)
        out.rothers_wa = my_spm_file(char(job.others_dw),'prefix',[prefix 'wa'],'format','.nii');
    elseif(dummy_3dor4d==true)
        out.rothers_wa = {V14Dwa(1).fname};
    end
    out.rothers_wa = acid_my_select(out.rothers_wa(:),dummy_3dor4d);
    out.rothers_wa = out.rothers_wa(:);
else
    out.rothers_wa = [];
end

if ~logical(isempty(V14Dam))
    if(dummy_3dor4d==false)
        out.rothers_am = my_spm_file(char(job.others_dw),'prefix',[prefix 'am'],'format','.nii');
    elseif(dummy_3dor4d==true)
        out.rothers_am = {V14Dam(1).fname};
    end
    out.rothers_am = acid_my_select(out.rothers_am(:),dummy_3dor4d);
    out.rothers_am = out.rothers_am(:);
else 
    out.rothers_am = [];
end



%------------------------
function dep = vout_hysco_wcomb_multi(job)
kk=1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Weighted mean of blip-up and down images';
dep(kk).src_output = substruct('.','rothers_wa');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Arithmetic mean of blip-up and down images';
dep(kk).src_output = substruct('.','rothers_am');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
kk = kk + 1;
dep(kk)            = cfg_dep;
dep(kk).sname      = 'Estimated fieldmap';
dep(kk).src_output = substruct('.','fieldmap');
dep(kk).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});


%---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);                
                if(nargin>=5)            
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end    
        else
            varout  = varargin{1};            
        end
    end
end


