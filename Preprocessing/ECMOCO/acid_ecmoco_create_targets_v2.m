function [VG,bC] = acid_ecmoco_create_targets_v2(P,bval)

% =========================================================
% Calculation of mean from each shell.
%
% Input:
%   P     - filename of dMRI dataset
%   bval  - b values
%   freeze - degrees of freedom of registration
%   Pmask - filename of the mask 
%   Pbias - filename of bias field image
%   dummy_init - dummy for initialization mode
%
% Output:
%   VG  - header of the target images
%   bC  - 
%
% S. Mohammadi 13.05.2015
% =========================================================
      
% get header of dMRI dataset
V = spm_vol(P);

% get dimension
dm = V(1).dim;

% get unique b values
bval = round(bval/100)*100;
[bC,~,~] = unique(bval);

% initialization
idx  = cell(numel(bC),1);
idx1 = zeros(numel(bC,1));
Vol1 = zeros([dm(1:3) numel(bC)]);
PO   = cell(numel(bC),1);

for k = 1:numel(bC)
    
    % indices of the first volumes of each shell    
    idx{k,1}  = find(bval==bC(k));
    idx1(k,1) = idx{k,1}(1);
    
    % load in first volumes of each shell
    for zpos = 1:V(1).dim(3)        
        Vol1(:,:,zpos,k) = acid_read_vols(V(idx1(k)),V(1),1,zpos);
    end
    
    % smooth first volumes
    [p,~,~] = fileparts(P(idx1(k),:));
    PO{k,1} = [p filesep 'target_b' int2str(bC(k)) '_tmp.nii'];    
    spm_smooth(P(idx1(k),:),PO{k,1},[2 2 0],0);
    
end

% correct headers of target images
VG1 = spm_vol(PO);
VG = cell(numel(bC),1);
for k = 1:numel(bC)
    tmp = spm_read_vols(VG1{k,1});
    tmp = tmp(:,:,:,end);
    VG{k,1} = VG1{k,1}(end,1);
    VG{k,1}.n = [1,1];
    VG{k,1}.private.dat.dim = VG{k,1}.dim;
    VG{k,1}.fname = [VG1{k,1}(end).fname(1:end-8) '.nii'];
    VG{k,1}.pinfo = [1;0;352];
    spm_write_vol(VG{k,1},tmp);
    delete(VG1{k,1}(end).fname);
end

clear VG1;

fprintf('\nTarget generation finished.\n');
    
end
    