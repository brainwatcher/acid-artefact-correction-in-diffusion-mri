function ecmoco2 = acid_tbx_cfg_ecmoco_v2

% source images
ecmoco2_sources         = cfg_files;
ecmoco2_sources.tag     = 'ecmoco2_sources';
ecmoco2_sources.name    = 'Source Images';
ecmoco2_sources.help    = {'Select source images. These images will be registered to the sources image.'};
ecmoco2_sources.filter  = 'image';
ecmoco2_sources.ufilter = '.*';
ecmoco2_sources.num     = [0 Inf];

% registration type
ecmoco2_dummy_type         = cfg_menu;
ecmoco2_dummy_type.tag     = 'ecmoco2_dummy_type';
ecmoco2_dummy_type.name    = 'Registration type';
ecmoco2_dummy_type.help    = {'Specify type of registration: volume-wise (3D) or slice-wise (2D).'
                      'We recommend to use volume-wise registration for the brain, and slice-wise for the spinal cord.',};
ecmoco2_dummy_type.labels = {'Volume-wise','Slice-wise'};
ecmoco2_dummy_type.values = {0 1};
ecmoco2_dummy_type.val    = {0};

% dof1
ecmoco2_dof1_file         = cfg_files;
ecmoco2_dof1_file.tag     = 'ecmoco2_dof1_file';
ecmoco2_dof1_file.name    = 'File (*.mat|txt)';
ecmoco2_dof1_file.help    = {'Select a file containing the degtrees of freedom (volume-wise).'
                             'Note: The file should contain a 1 x 12 vector.'};
ecmoco2_dof1_file.filter  = 'any';
ecmoco2_dof1_file.ufilter = 'mat|txt';
ecmoco2_dof1_file.num     = [1 1];
ecmoco2_dof1_file.val     = {{''}};

ecmoco2_dof1_exp         = cfg_menu;
ecmoco2_dof1_exp.tag     = 'ecmoco2_dof1_exp';
ecmoco2_dof1_exp.name    = 'Choose predefined option';
ecmoco2_dof1_exp.help    = {'Recommended parameters:' 
                           'a) Correcting only for subject motion: [1 1 1 1 1 1 0 0 0 0 0 0];'
                           'b) Correcting only for subject motion and whole-brain eddy currents: [1 1 1 1 1 1 0 1 0 1 1 0];'
                           'c) Correcting distortions in a spherical phantom: [1 1 1 0 0 0 1 0 1 1 0].'};
ecmoco2_dof1_exp.labels = {'Correcting only for subject motion','Correcting only for subject motion and whole-brain eddy currents', 'Correcting distortions in a spherical phantom'};
ecmoco2_dof1_exp.values  = {[1 1 1 1 1 1 0 0 0 0 0 0] [1 1 1 1 1 1 0 1 0 1 1 0] [1 1 1 0 0 0 1 0 1 1 0]};
ecmoco2_dof1_exp.val     = {[1 1 1 1 1 1 0 0 0 0 0 0]};

ecmoco2_dof1_manual_exp         = cfg_entry;
ecmoco2_dof1_manual_exp.tag     = 'ecmoco2_dof1_manual';
ecmoco2_dof1_manual_exp.name    = 'Manual expression';
ecmoco2_dof1_manual_exp.help    = {'Specify the degrees of freedom (DOFs) of the registration. You can choose between 12 affine parameters.;'
                                   'Note that the input vector for this parameter must have 12 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco2_dof1_manual_exp.strtype = 'e';
ecmoco2_dof1_manual_exp.num     = [1 12];
ecmoco2_dof1_manual_exp.val     = {[0 0 0 0 0 0 0 0 0 0 0 0]};

ecmoco2_dof1_type        = cfg_choice;
ecmoco2_dof1_type.tag    = 'ecmoco2_dof1_type';
ecmoco2_dof1_type.name   = 'Degrees of freedom for/and volume/slice-wise registration';
ecmoco2_dof1_type.help   = {''};
ecmoco2_dof1_type.values = {ecmoco2_dof1_file, ecmoco2_dof1_exp, ecmoco2_dof1_manual_exp};


% dof2
ecmoco2_dof2_file         = cfg_files;
ecmoco2_dof2_file.tag     = 'ecmoco2_dof2_file';
ecmoco2_dof2_file.name    = 'File (*.mat|txt)';
ecmoco2_dof2_file.help    = {'Select a file containing the degtrees of freedom (slice-wise).'
                             'Note: The file should contain a 1 x 7 vector.'};
ecmoco2_dof2_file.filter  = 'any';
ecmoco2_dof2_file.ufilter = 'mat|txt';
ecmoco2_dof2_file.num     = [0 1];
ecmoco2_dof2_file.val     = {{''}};

ecmoco2_dof2_exp         = cfg_menu;
ecmoco2_dof2_exp.tag     = 'ecmoco2_dof2_exp';
ecmoco2_dof2_exp.name    = 'Choose predefined option';
ecmoco2_dof2_exp.help    = {'Recommended parameters:' 
                           'a) Correcting only for subject motion: [1 1 1 1 1 1 0 0 0 0 0 0];'
                           'b) Correcting only for subject motion and whole-brain eddy currents: [1 1 1 1 1 1 0 1 0 1 1 0];'
                           'c) Correcting distortions in a spherical phantom: [1 1 1 0 0 0 1 0 1 1 0].'};
ecmoco2_dof2_exp.labels = {'Recommended for spinal-cord dMRI'};
ecmoco2_dof2_exp.values  = {[0 1 0 0 1 0 0]};
ecmoco2_dof2_exp.val     = {[0 1 0 0 1 0 0]};

ecmoco2_dof2_manual_exp         = cfg_entry;
ecmoco2_dof2_manual_exp.tag     = 'ecmoco2_dof2_manual';
ecmoco2_dof2_manual_exp.name    = 'Manual expression';
ecmoco2_dof2_manual_exp.help    = {'Specify the degrees of freedom (DOFs) of the registration. You can choose between 7 affine parameters.;'
                                   'Note that the input vector for this parameter must have 7 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco2_dof2_manual_exp.strtype = 'e';
ecmoco2_dof2_manual_exp.num     = [1 7];
ecmoco2_dof2_manual_exp.val     = {[0 0 0 0 0 0 0]};

ecmoco2_dof2_type        = cfg_choice;
ecmoco2_dof2_type.tag    = 'ecmoco2_dof2_type';
ecmoco2_dof2_type.name   = 'Degrees of freedom for slice-wise registration';
ecmoco2_dof2_type.help   = {'Specify the degrees of freedom (DOFs) of the registration. You can choose between 12 affine parameters. The 4 eddy current parameters are displayed in Figure 1 (see ECMOCO_Documentation). We propose three sets of parameters for different purposes (see below), but you can select the parameters freely.' 
                            'Note that the input vector for this parameter must have 12 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'};
ecmoco2_dof2_type.values = {ecmoco2_dof2_file, ecmoco2_dof2_exp, ecmoco2_dof2_manual_exp};
%ecmoco2_dof2_type.val    = {''};


% b values
ecmoco2_bvals_file         = cfg_files;
ecmoco2_bvals_file.tag     = 'ecmoco2_bvals_file';
ecmoco2_bvals_file.name    = 'File (*.mat|txt|bval)';
ecmoco2_bvals_file.help    = {'Select a file specifying the b-values (bval) of the N dMRI volumes.'
                       'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
ecmoco2_bvals_file.filter  = 'any';
ecmoco2_bvals_file.ufilter = 'mat|txt|bval';
ecmoco2_bvals_file.num     = [0 1];
ecmoco2_bvals_file.val     = {{''}};

ecmoco2_bvals_exp         = cfg_entry;
ecmoco2_bvals_exp.tag     = 'ecmoco2_bvals_exp';
ecmoco2_bvals_exp.name    = 'Expression';
ecmoco2_bvals_exp.help    = {'Specify the b-values (bval) of the N dMRI volumes.'
                             'Note: The expression should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
ecmoco2_bvals_exp.strtype = 'e';
ecmoco2_bvals_exp.num     = [Inf Inf];
ecmoco2_bvals_exp.val     = {''};

ecmoco2_bvals_type        = cfg_choice;
ecmoco2_bvals_type.tag    = 'ecmoco2_bvals_type';
ecmoco2_bvals_type.name   = 'b-values (recommended)';
ecmoco2_bvals_type.help   = {''};
ecmoco2_bvals_type.values = {ecmoco2_bvals_file, ecmoco2_bvals_exp};
%ecmoco2_bvals_type.val    = {ecmoco2_bvals_file};



% initialization mode
ecmoco_dummy_single_or_multi_target      = cfg_menu;
ecmoco_dummy_single_or_multi_target.tag  = 'ecmoco_dummy_single_or_multi_target';
ecmoco_dummy_single_or_multi_target.name = 'Target mode (single or multi target)';
ecmoco_dummy_single_or_multi_target.help = {'Single target:'
                                            '"EC_MO_correction" is a modification of the "spm_coreg" algorithm written by John Ashburner (http://www.fil.ion.ucl.ac.uk/spm) for dealing with EC distortions and motion correction of diffusion weighted images based on a linear eddy current model described in Mohammadi at al. (2010).'
                                            'Please cite Mohammadi et al., MRM 2010 when using this code.'
                                            'Because this toolbox uses the optimization function of spm_coreg, it is modality independent, i.e. in principal any low-b-value image or high-b-value image can be used as target, provided the SNR of the DTI dataset is high enough. It is recommend to use either a low-b-value image or the mean of the high-b-value images as target.'  
                                            'Multi target:'
                                            'This method uses for each diffusion shell a specific target, which resamples the average contrast and SNR of the images of each shell.'
                                            'It aims at compensating for a potential reduction of the performance of the standard eddy current and motion correction method if the SNR is very low.'
                                              };
ecmoco_dummy_single_or_multi_target.labels = {
                                              'Single target'
                                              'Multi target'
                                            }';
ecmoco_dummy_single_or_multi_target.values    = {true false};
ecmoco_dummy_single_or_multi_target.val       = {false};

% target image
ecmoco2_target         = cfg_files;
ecmoco2_target.tag     = 'ecmoco2_target';
ecmoco2_target.name    = 'Target Image (ONLY for single target!)';
ecmoco2_target.help    = {'Select one reference image for single target mode. All images will be registered to this image. Recommended is using either the low-b-value image or the mean of the high-b-value images as target.'};
ecmoco2_target.filter  = 'image';
ecmoco2_target.ufilter = '.*';
ecmoco2_target.num     = [0 1];
ecmoco2_target.val     = {{''}};

% initialization mode
ecmoco2_dummy_init      = cfg_menu;
ecmoco2_dummy_init.tag  = 'ecmoco2_dummy_init';
ecmoco2_dummy_init.name = 'Initialization mode (ON only for data with interspersed b0!)';
ecmoco2_dummy_init.help = {'If ON, the iteration for the transformation parameters (TP) is initialized by the TPs of the b0 volumes.'
                               'It is the recommended option if the b0 and higher b-shells are acquired interspersed.'
                         };
ecmoco2_dummy_init.labels = {
               'ON'
               'OFF'
}';
ecmoco2_dummy_init.values    = {true false};
ecmoco2_dummy_init.val       = {true};

% excluded
ecmoco2_excluded         = cfg_files;
ecmoco2_excluded.tag     = 'ecmoco2_excluded';
ecmoco2_excluded.name    = 'Excluded volumes (optional)';
ecmoco2_excluded.help    = {'Indices of volumes which will be ignored in the registration.'};
ecmoco2_excluded.filter  = 'mat|txt';
ecmoco2_excluded.ufilter = '.*';
ecmoco2_excluded.num     = [0 1];
ecmoco2_excluded.val     = {{''}};

% mask
ecmoco2_mask         = cfg_files;
ecmoco2_mask.tag     = 'ecmoco2_mask';
ecmoco2_mask.name    = 'Image mask (optional)';
ecmoco2_mask.help    = {'Select Image mask. Regions outside the mask will be ignored when obtaining the registration parameters.'};
ecmoco2_mask.filter  = 'image';
ecmoco2_mask.ufilter = '.*';
ecmoco2_mask.num     = [0 1];
ecmoco2_mask.val     = {{''}};

% biasfield
ecmoco2_biasfield         = cfg_files;
ecmoco2_biasfield.tag     = 'ecmoco2_biasfield';
ecmoco2_biasfield.name    = 'Bias field Image (optional)';
ecmoco2_biasfield.help    = {'Select Biasfield. This might improve the registration between shells. If Biasfield is not available select done. You can get the Biasfield, e.g., when segmenting the b=0 image.'};
ecmoco2_biasfield.filter  = 'image';
ecmoco2_biasfield.ufilter = '.*';
ecmoco2_biasfield.num     = [0 Inf];
ecmoco2_biasfield.val     = {{''}};


% zsmooth
ecmoco2_zsmooth      = cfg_menu;
ecmoco2_zsmooth.tag  = 'ecmoco2_zsmooth';
ecmoco2_zsmooth.name = 'z-averaging';
ecmoco2_zsmooth.help = {'Activate averaging in z-direction (only for slice-wise registration).',};
ecmoco2_zsmooth.labels = {
               'ON'
               'OFF'
}';
ecmoco2_zsmooth.values = {1 0};
ecmoco2_zsmooth.val    = {0};



% ---------------------------------------------------------------------
% parallel_prog
% ---------------------------------------------------------------------
ecmoco_parallel_prog        = cfg_entry;
ecmoco_parallel_prog.tag    = 'ecmoco_parallel_prog';
ecmoco_parallel_prog.name   = 'Parallel programming';
ecmoco_parallel_prog.help   = {'Note this option requires the parallel programming toolbox. Specify number of cores for parallel programming. No indicates that no parallel programming is used.'};
ecmoco_parallel_prog.strtype = 'e';
ecmoco_parallel_prog.num    = [1 1];
ecmoco_parallel_prog.val    = {1};

% write resliced images
ecmoco2_dummy_write        = cfg_menu;
ecmoco2_dummy_write.tag    = 'ecmoco2_dummy_write';
ecmoco2_dummy_write.name   = 'Choose write option';
ecmoco2_dummy_write.help   = {'Choose whether you want to reslice and output the registered images. (default: ON)'};
ecmoco2_dummy_write.labels = {
               'ON'
               'OFF'
}';
ecmoco2_dummy_write.values = {1 0};
ecmoco2_dummy_write.val    = {1};

% prefix
ecmoco2_prefix         = cfg_entry;
ecmoco2_prefix.tag     = 'ecmoco2_prefix';
ecmoco2_prefix.name    = 'Prefix for output';
ecmoco2_prefix.help    = {'Prefix for the output filename.'};
ecmoco2_prefix.strtype = 's';
ecmoco2_prefix.val     = {'r'};
%{
% phase-encoding direction
ecmoco2_phdir      = cfg_menu;
ecmoco2_phdir.tag  = 'ecmoco2_phdir';
ecmoco2_phdir.name = 'Direction of phase-encoding';
ecmoco2_phdir.help = {''
'Specify the phase-encoding or distortion direction of your data.'
''};
ecmoco2_phdir.labels = {'x','y','z'};
ecmoco2_phdir.values = {1 2 3};
ecmoco2_phdir.val    = {2};
%}
% exbranch
ecmoco2      = cfg_exbranch;
ecmoco2.tag  = 'ecmoco2';
ecmoco2.name = 'Estimate & write ECMOCO';
ecmoco2.val  = {ecmoco2_sources, ecmoco2_dummy_type, ecmoco2_dof1_type, ecmoco2_dof2_type, ecmoco2_bvals_type, ecmoco_dummy_single_or_multi_target, ecmoco2_target, ecmoco2_dummy_init, ecmoco2_excluded, ecmoco2_mask, ecmoco2_zsmooth, ecmoco2_biasfield, ecmoco2_prefix, ecmoco_parallel_prog};
ecmoco2.help = {
                    'Multi Targe Eddy Current and Motion Correction.'
                    'This method uses for each diffusion shell a specific target, which resamples the average contrast and SNR of the images of each shell.'
                    'It aims at compensating for a potential reduction of the performance of the standard eddy current and motion correction method if the SNR is very low.'
                  };
ecmoco2.prog = @local_ECMO_VARTAR_correction;
ecmoco2.vout = @vout_ECMO_VARTAR_correction;

end

function out = local_ECMO_VARTAR_correction(job)
    
ecmoco2_interpol    = acid_get_defaults('ecmo.interpol_reslice');
ecmoco2_plot        = acid_get_defaults('ecmo.plot');
ecmoco2_dummy_write = acid_get_defaults('ecmo.write');
ecmoco2_sep         = acid_get_defaults('ecmo.separation');
ecmoco2_phdir       = acid_get_defaults('ecmo.phase');




% read in dof1
if isfield(job.ecmoco2_dof1_type,'ecmoco2_dof1_file')
    job.ecmoco2_dof1 = job.ecmoco2_dof1_type.ecmoco2_dof1_file;
    job.ecmoco2_dof1 = cell2mat(job.ecmoco2_dof1);
    [~,~,e] = fileparts(job.ecmoco2_dof1);
    if ~isempty(job.ecmoco2_dof1_type.ecmoco2_dof1_file{1})
        switch lower(e)
        case '.mat'
            job.ecmoco2_dof1 = cell2mat(struct2cell(load(job.ecmoco2_dof1)));
        case '.nii'
            error('Unexpected file extension!')
        otherwise
            job.ecmoco2_dof1 = dlmread(job.ecmoco2_dof1);
        end
    else
        job.ecmoco2_dof1 = '';
    end
elseif isfield(job.ecmoco2_dof1_type,'ecmoco2_dof1_exp')
    if ~isempty(job.ecmoco2_dof1_type.ecmoco2_dof1_exp)
        job.ecmoco2_dof1 = job.ecmoco2_dof1_type.ecmoco2_dof1_exp;
    else
        job.ecmoco2_dof1 = '';
    end
elseif isfield(job.ecmoco2_dof1_type,'ecmoco2_dof1_manual')
    if ~isempty(job.ecmoco2_dof1_type.ecmoco2_dof1_manual)
        job.ecmoco2_dof1 = job.ecmoco2_dof1_type.ecmoco2_dof1_manual;
    else
        job.ecmoco2_dof1 = '';
    end
end
    
    
    
 % read in dof2
if isfield(job.ecmoco2_dof2_type,'ecmoco2_dof2_file')
    job.ecmoco2_dof2 = job.ecmoco2_dof2_type.ecmoco2_dof2_file;
    job.ecmoco2_dof2 = cell2mat(job.ecmoco2_dof2);
    [~,~,e] = fileparts(job.ecmoco2_dof2);
    if ~isempty(job.ecmoco2_dof2_type.ecmoco2_dof2_file{1})
        switch lower(e)
        case '.mat'
            job.ecmoco2_dof2 = cell2mat(struct2cell(load(job.ecmoco2_dof2)));
        case '.nii'
            error('Unexpected file extension!')
        otherwise
            job.ecmoco2_dof2 = dlmread(job.ecmoco2_dof2);
        end
    else
        job.ecmoco2_dof2 = '';
    end
elseif isfield(job.ecmoco2_dof2_type,'ecmoco2_dof2_exp')
    if ~isempty(job.ecmoco2_dof2_type.ecmoco2_dof2_exp)
        job.ecmoco2_dof2 = job.ecmoco2_dof2_type.ecmoco2_dof2_exp;
    else
        job.ecmoco2_dof2 = '';
    end
elseif isfield(job.ecmoco2_dof2_type,'ecmoco2_dof2_manual')
    if ~isempty(job.ecmoco2_dof2_type.ecmoco2_dof2_manual)
        job.ecmoco2_dof2 = job.ecmoco2_dof2_type.ecmoco2_dof2_manual;
    else
        job.ecmoco2_dof2 = '';
    end
end
    
    
    

%    dof1 = cell2mat(struct2cell(job.ecmoco2_dof1_type));







%dof2 = cell2mat(struct2cell(job.ecmoco2_dof2_type));

if job.ecmoco2_zsmooth == 1
    ecmoco2_zsmooth_slc_number = acid_get_defaults('ecmo.zsmooth');
else
    ecmoco2_zsmooth_slc_number = 1;
end


    ecmoco2_thr_hist = 0; % no thresholding for now
    
    % read in bvals
    if isfield(job.ecmoco2_bvals_type,'ecmoco2_bvals_file')
        job.ecmoco2_bvals = job.ecmoco2_bvals_type.ecmoco2_bvals_file;
        job.ecmoco2_bvals = cell2mat(job.ecmoco2_bvals);
        [~,~,e] = fileparts(job.ecmoco2_bvals);
        if ~isempty(job.ecmoco2_bvals_type.ecmoco2_bvals_file{1})
            switch lower(e)
            case '.mat'
                job.ecmoco2_bvals = cell2mat(struct2cell(load(job.ecmoco2_bvals)));
            case '.nii'
                error('Unexpected file extension!')
            otherwise
                job.ecmoco2_bvals = dlmread(job.ecmoco2_bvals);
            end
        else
            job.ecmoco2_bvals = '';
        end
    elseif isfield(job.ecmoco2_bvals_type,'ecmoco2_bvals_exp')
        if ~isempty(job.ecmoco2_bvals_type.ecmoco2_bvals_exp)
            job.ecmoco2_bvals = job.ecmoco2_bvals_type.ecmoco2_bvals_exp;
        else
            job.ecmoco2_bvals = '';
        end
    end
    
    VG = acid_ecmoco_main(char(job.ecmoco2_sources), job.ecmoco2_dummy_type, job.ecmoco2_dof1, job.ecmoco2_dof2, job.ecmoco2_bvals, job.ecmoco2_dummy_init, job.ecmoco2_excluded, ...
        char(job.ecmoco2_mask), char(job.ecmoco2_biasfield), ecmoco2_dummy_write, ecmoco2_plot, job.ecmoco2_prefix, ...
        ecmoco2_thr_hist, ecmoco2_sep, ecmoco2_interpol, ecmoco2_phdir,ecmoco2_zsmooth_slc_number, job.ecmoco_parallel_prog, job.ecmoco_dummy_single_or_multi_target, job.ecmoco2_target);
    
    for i = 1:size(job.ecmoco2_sources,1)
        [a,b,c] = fileparts(job.ecmoco2_sources{i,:});
        out.rfiles{i,:} = [a filesep job.ecmoco2_prefix b c];
    end
    
    VG = VG(:);
    for i = 1:size(VG,1)
        out.VGfiles(i,1) = {VG(i).fname};
    end
    out.f1      = out.rfiles(1);
    out.b0      = acid_spm_file(out.rfiles{1},'prefix','mean_b0_','format','.nii');
    out.matfile = acid_spm_file(char(job.ecmoco2_sources(1)),'prefix','ecmoco_params_','format','.mat');
    if isfield(job.ecmoco2_bvals_type,'ecmoco2_bvals_file')
        out.bval = cellstr(job.ecmoco2_bvals_type.ecmoco2_bvals_file);
    elseif isfield(job.ecmoco2_bvals_type,'ecmoco2_bvals_exp')
        out.bval = job.ecmoco2_bvals_type.ecmoco2_bvals_exp;
    end
end

function dep = vout_ECMO_VARTAR_correction(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resliced images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'First resliced image';
    dep(2).src_output = substruct('.','f1');
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'Target images';
    dep(3).src_output = substruct('.','VGfiles');
    dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(4)            = cfg_dep;
    dep(4).sname      = 'Average b0 image (resliced)';
    dep(4).src_output = substruct('.','b0');
    dep(4).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(5)            = cfg_dep;
    dep(5).sname      = 'b-values';
    dep(5).src_output = substruct('.','bval');
    dep(5).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
    dep(6)            = cfg_dep;
    dep(6).sname      = 'Transformation parameters';
    dep(6).src_output = substruct('.','matfile');
    dep(6).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
end