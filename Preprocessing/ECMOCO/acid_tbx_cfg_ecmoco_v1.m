function ecmoco1 = acid_tbx_cfg_ecmoco_v1

% target image
ecmoco1_target         = cfg_files;
ecmoco1_target.tag     = 'ecmoco1_target';
ecmoco1_target.name    = 'Target Image';
ecmoco1_target.help    = {'Select one reference image. All images will be registered to this image. Recommended is using either the low-b-value image or the mean of the high-b-value images as target.'};
ecmoco1_target.filter  = 'image';
ecmoco1_target.ufilter = '.*';
ecmoco1_target.num     = [1 1];

% source images
ecmoco1_sources         = cfg_files;
ecmoco1_sources.tag     = 'ecmoco1_sources';
ecmoco1_sources.name    = 'Source Images';
ecmoco1_sources.help    = {'Select source images. These images will be registered to the sources image.'};
ecmoco1_sources.filter  = 'image';
ecmoco1_sources.ufilter = '.*';
ecmoco1_sources.num     = [0 Inf];

% degrees of freedom 
ecmoco1_dof         = cfg_entry;
ecmoco1_dof.tag     = 'ecmoco1_dof';
ecmoco1_dof.name    = 'Enter 12 binaries';
ecmoco1_dof.help    = {'Choose the parameters, which you want to correct. You can choose between 12 affine parameters. The 4 eddy current parameters are displayed in Figure 1 (see ECMOCO_Documentation). We propose three sets of parameters for different purposes (see below), but you can select the parameters freely.' 
                  'Proposed parameters:' 
                  'a) Correcting only for subject motion: [1 1 1 1 1 1 0 0 0 0 0 0];'
                  'b) Correcting only for subject motion and whole-brain eddy currents: [1 1 1 1 1 1 0 1 0 1 1 0];'
                  'c) Correcting distortions in a spherical phantom: [1 1 1 0 0 0 1 0 1 1 0].' 
                  'Note that the input vector for this parameter must have 12 components: For each component you can choose only between 0 and 1 (0: the parameter is not estimated; 1: the parameter is estimated).'                  
                  };
ecmoco1_dof.strtype = 'e';
ecmoco1_dof.num     = [1 12];
ecmoco1_dof.val     = {[ones(1,6) 0 1 0 1 1 0]};

% option for writing registered images
ecmoco1_write        = cfg_menu;
ecmoco1_write.tag    = 'ecmoco1_write';
ecmoco1_write.name   = 'Choose write option';
ecmoco1_write.help   = {'Choose whether you want to write the registered images. By default the write option is ON.  For each image a matfile is written, which contains the registration parameters (the mut-file is always written).'};
ecmoco1_write.labels = {
               'ON'
               'OFF'
}';
ecmoco1_write.values = {1 0};
ecmoco1_write.val    = {1};

% option for plotting registration paramters
ecmoco1_plot        = cfg_menu;
ecmoco1_plot.tag    = 'ecmoco1_plot';
ecmoco1_plot.name   = 'Choose display option';
ecmoco1_plot.help   = {'Choose whether you want to see the estimated EC and motion parameter for each image.  This option might be helpful to provide you with a feeling about the artefact level in your dataset. You might want to turn it off if more than one subject is registered, because two figures will be displayed for each subject. Note that those figures will also be written in eps-format.'
                         };
ecmoco1_plot.labels = {
               'ON'
               'OFF'
}';
ecmoco1_plot.values = {1 0};
ecmoco1_plot.val    = {1};


% exbranch
ecmoco1         = cfg_exbranch;
ecmoco1.tag     = 'ecmoco1';
ecmoco1.name    = 'Single target';
ecmoco1.val     = {ecmoco1_target ecmoco1_sources ecmoco1_dof ecmoco1_write ecmoco1_plot};
ecmoco1.help    = {
                    'Eddy Current and Motion Correction.'
                    '"EC_MO_correction" is a modification of the "spm_coreg" algorithm written by John Ashburner (http://www.fil.ion.ucl.ac.uk/spm) for dealing with EC distortions and motion correction of diffusion weighted images based on a linear eddy current model described in Mohammadi at al. (2010).'
                    'Please cite Mohammadi et al., MRM 2010 when using this code.'
                    'Because this toolbox uses the optimization function of spm_coreg, it is modality independent, i.e. in principal any low-b-value image or high-b-value image can be used as target, provided the SNR of the DTI dataset is high enough. It is recommend to use either a low-b-value image or the mean of the high-b-value images as target.'
}';
ecmoco1.prog = @local_EC_MO_correction;
ecmoco1.vout = @vout_EC_MO_correction;

end

function out = local_EC_MO_correction(job)
    EC_MO_correction(char(job.ecmoco1_target), char(job.ecmoco1_sources), job.ecmoco1_dof, job.ecmoco1_write,job.ecmoco1_plot);
    out.rfiles = acid_spm_file(char(job.ecmoco1_sources),'prefix','r','format','.nii');
    out.rfiles = acid_select(out.rfiles(:));
    out.rfiles = out.rfiles(:);
end

function dep = vout_EC_MO_correction(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resliced Images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end