function vol = acid_ecmoco_reslice(mireg,params,j,dm,res_hold,dummy_slicewise)

%========================================================================
% The script reslices a single image using the given transformation
% parameters. Note that the all source images have to be given in the
% argument (mireg), and the image to be resliced is specified by the
% argument j.
%
% Inputs:
%   mireg    - structure containing the headers of source images and template
%   pamars   - transformation parameters
%   j        - index of source image to be resliced
%   dm       - dimension of resliced image
%   res_hold - type of interpolation
%   dummy_slicewise - 0: volume-wise registration
%                     1: slice-wise registration
%========================================================================

global phase_encoding;

VFtmp = mireg.Vwrite(j);

switch phase_encoding
    case 1 
        dm = dm([2 1 3]);
        vol = zeros(dm);
        mireg.VG.mat = mireg.VG.mat([2 1 3 4],:);
        VFtmp.mat = VFtmp.mat([2 1 3 4],:); % (:,[2 1 3 4])
    case 2        
        vol = zeros(dm);
    case 3
        dm = dm([1 3 2]);
        vol = zeros(dm);
        mireg.VG.mat = mireg.VG.mat([1 3 2 4],:);
        VFtmp.mat = VFtmp.mat([1 3 2 4],:); % (:,[2 1 3 4]
end

if ~dummy_slicewise
    iM = inv(acid_spm_matrix(params));
    for p = 1:dm(3) % looping over slices
        M = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(mireg.VG.mat)*iM*VFtmp.mat);
        tmp = spm_slice_vol(VFtmp,M,dm(1:2),res_hold);
        vol(:,:,p) = tmp;
    end
else  
    for p = 1:dm(3) % looping over slices
        iM = inv(acid_spm_matrix(params(:,p)));
        M = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(mireg.VG.mat)*iM*VFtmp.mat);
        tmp = spm_slice_vol(VFtmp,M,dm(1:2),res_hold);
        vol(:,:,p) = tmp;
    end    
end
end