function I = acid_ecmoco_write(params,V,VG,prefix,interpol,phase, dummy_bids)

% params        - transformation parameters
% V             - headers of the source images
% VG            - header of the target image
% prefix        - prefix for output
% interpol      - type of interpolation
% phase         - phase-encoding direction
%
% based on EC_MO_write_rFiles_varRef_xyzphase
% 21/09/2014

global phase_encoding
phase_encoding = phase;

% determine whether VW or SW registration
if numel(size(params)) == 3
    dummy_slicewise = 1;
else
    dummy_slicewise = 0;
end

% determine whether 3D or 4D
if size(V,1) == 1
    dummy_4d = 0;
elseif strcmp(V(1).fname(1:end-3),V(2).fname(1:end-3))
    dummy_4d = 1;
else
    dummy_4d = 0;
end

% transformation parameters
if size(params,2)~=size(V,1)
    error('The size of the specified registration parameters is not compatible with the number of images!');
elseif size(params,1) ~= 12
    error('Incorrect registration parameters: The first dimension of the specified registration matrix has to be 12.');
end
if dummy_slicewise
    params = permute(params,[2 1 3]);
else
    params = params';
end

%% WRITING RESLICED IMAGES

% create a structure containing headers of source and target images
miregtmp        = struct('VG',[],'Vwrite',[]);
miregtmp.Vwrite = V;
miregtmp.VG     = VG;
dm              = miregtmp.VG.dim;

% looping through volumes
for k = 1:size(params,1)
    % reslice source images
    %   miregtmp - structure containing the filenames of source images and template
    %   pamars   - transformation parameters
    %   k        - index of source image to be resliced
    %   dm       - dimension of resliced image
    %   default_Vol - type of interpolation
    %   I        - resliced image matrix
    if ~dummy_slicewise
        I(:,:,:,k) = acid_ecmoco_reslice(miregtmp,params(k,:),k,dm,interpol,dummy_slicewise);
    else
        I(:,:,:,k) = acid_ecmoco_reslice(miregtmp,squeeze(params(k,:,:)),k,dm,interpol,dummy_slicewise); 
    end
end

% save resliced image
Vtmp = miregtmp.VG;
if dummy_4d
    Vtmp.fname = miregtmp.Vwrite(1).fname;
    %Vtmp.dt = [spm_type('int16'),spm_platform('bigend')];
    acid_write_vol(I,Vtmp,prefix);
else
    for k = 1:size(params,1)
        Vtmp.fname = miregtmp.Vwrite(k).fname;
        %Vtmp.dt = [spm_type('int16'),spm_platform('bigend')];
        acid_write_vol(I(:,:,:,k),Vtmp,prefix);
        disp(['Resampling Volume ' num2str(k)]);
    end
end

clear Ai;
disp('All diffusion weighted images resliced')

end
