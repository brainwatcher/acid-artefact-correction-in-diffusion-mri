function [xk, xk0] = acid_coreg_slicew(IS1,IT1,flags,thr_hist,mask,Vref, zsmooth)

% ========================================================================
% This script obtains slice-wise (SW) linear transformation parameters (xkk) between volumes VF
% and VG using parameters specified in flags.
%
% The script turns SW registration into VW registration by filling up a
% volume using a single slice.
%
%
% Input:
%   IS1    - source image
%   IT1    - target image
%   flags  - parameters for registration
%   thr_hist - lower threshold for histogram: values below this threshold
%           are ignored in the optimization process
%   mask   - mask to apply on the image before optimization. Voxels outside
%           are ignored in the optimization process
%   Vref   - reference header
%
% Output:
%   xk    - final transformation parameters (VW + SW)
%   xk0   - VW transformation parameters
% ========================================================================

dm = Vref.dim;
xk = zeros([12 dm(3)]);

%% ---------- VW registration -----------

fprintf('\nInitial volume-wise registration:\n\n')

flags1 = flags;
flags1.freeze = flags.freeze(1:12);
IS = IS1; AFtmp = IS(:); AFtmp(isnan(AFtmp)) = [];
IT = IT1; ATtmp = IT(:); ATtmp(isnan(ATtmp)) = [];

if ~sum(AFtmp) || ~sum(ATtmp)
    warning('Skipping this slice because the template image contains zeros only!')
else
    
    % determine VW registration parameters to match VF with VG
    xk = repmat(acid_spm_coreg(IT,IS,flags1,thr_hist,mask,Vref)',[1 dm(3)]); % estimated ecmoco parameters for VW
    
    % initialize the registration with the determined VW parameters
    flags.params = xk(:,1);
    
    % return VW parameters in the output
    xk0 = xk(:,1);
    
end

%% ---------- SW registration ----------
fprintf('\nSlice-wise registration:\n')
flags2 = flags;
flags2.freeze = zeros(1,12);
flags2.freeze([1,2,6,7,8,10,11]) = flags.freeze([13,14,15,16,17,18,19]);


for i = 1:dm(3)
    fprintf('\nslice: ');
    disp(int2str(i))
    % fprintf('\n');
    tmp  = IT1(:,:,i); tmpp = tmp(:); tmpp(isnan(tmpp)) = [];
    tmp1 = IS1(:,:,i); tmpp1 = tmp1(:); tmpp1(isnan(tmpp1)) = [];
    
    if ~sum(tmpp(:)) || ~sum(tmpp1(:))
        warning('Skipping this slice because the template image contains zeros only!')
    else
        % Idea: spm cannot register single slices, so we fill up an entire
        % volume with a single slice
        
        % boundary slices
        if i==1 || i==dm(3)
            for j = 1:dm(3)
                IS(:,:,j) = IS1(:,:,i);
                IT(:,:,j) = IT1(:,:,i);
            end
            % inner slices
        else
            if zsmooth < 2
                % Idea: spm cannot register single slices, so we fill up an entire
                % volume with a single slice;
                for j = 1:dm(3)
                    IS(:,:,j) = IS1(:,:,i);
                    IT(:,:,j) = IT1(:,:,i);
                end
            else
                zsmooth_indx = (zsmooth - 1)/2;
                
                if round(zsmooth_indx) == zsmooth_indx
                    
                    zsmooth_indx_minus = zsmooth_indx;
                    zsmooth_indx_plus  = zsmooth_indx;
                else
                    zsmooth_indx_minus = round(zsmooth_indx) - 1;
                    zsmooth_indx_plus  = round(zsmooth_indx);
                    
                end
                
                if zsmooth_indx_minus < 1, zsmooth_indx_minus = 1; end
                if zsmooth_indx_plus  > dm(3), zsmooth_indx_plus = dm(3); end
                
                
                for j = 1:dm(3)
                    IS(:,:,j) = median(IS1(:,:,i-zsmooth_indx_minus:i+zsmooth_indx_plus),zsmooth,'omitnan');
                    IT(:,:,j) = IT1(:,:,i);
                end
            end
        end
        
        % determine linear registration parameters to match VF with VG
        xk(:,i) = acid_spm_coreg(IT,IS,flags2,thr_hist,mask,Vref);
    end
end
%{
if(isempty(xk))
    xk = zeros([12 dm(3)]);
    xk0 = zeros([12 dm(3)]);
end
%}
%delete(VGtmp.fname)
%delete(VFtmp.fname)

end