function [VG,bC] = acid_ecmoco_create_targets(P,bval,freeze,Pmask,Pbias,dummy_init,prefix)
% =========================================================
% Generating registration templates (target images) by calculating
% the mean of each shell.
%
% Input:
%   P      - filename of dMRI dataset
%   bval   - b values
%   freeze - degrees of freedom of registration
%   Pmask  - filename of the mask 
%   Pbias  - filename of bias field image
%   dummy_init - dummy for initialization mode
%
% Output:
%   VG  - header of the target images
%   bC  - 
%
% S. Mohammadi 13.05.2015
% =========================================================
      
% get header of dMRI dataset
V = spm_vol(P);

% get dimension
dm = V(1).dim;

% get unique b values
bval = round(bval/100)*100;
[bC,~,~] = unique(bval);
    
% default flags for registration
def_flags = struct('sep',[4 2],'params',[0 0 0  0 0 0  1 1 1  0 0 0], ...
 'cost_fun','nmi','fwhm',[7 7],...
 'tol',[0.01 0.01 0.01  0.005 0.005 0.005  0.005 0.005 0.005  0.005 0.005 0.005], ...
 'graphics',1, 'freeze', freeze,'perc', 0.8,'AMSK',ones(dm),'reshold',-4,'smk',[1 1 1]);

% remove non-rigid body dofs
flags        = def_flags;
flags.freeze = cat(2,freeze(1:6),zeros(1,6));
% flags.freeze = ones(1,12);

% load in mask
Vmask = spm_vol(Pmask);

for i = 1:size(bC,2) % loop through each b value
    
    % get volume indices of the given shell
    bMSK = find(bval==bC(i));

    % load in each volume within the shell
    lApdw = zeros([dm(1:3) numel(bMSK)]);
    for j = 1:numel(bMSK)
        for zpos = 1:V(1).dim(3)        
            lApdw(:,:,zpos,j) = acid_read_vols(V(bMSK(j)),V(1),1,zpos);
        end
    end

    if(i==1) % if b0 shell 
        
        % median b0 image
        lApdw = reshape(lApdw,[],numel(bMSK));
        %Atmp  = mean(lApdw,2);
        Atmp  = median(lApdw,2);
        Atmp  = reshape(Atmp,dm);
        
        % save median b0 image
        %V(1).dt = [spm_type('float32'),spm_platform('bigend')];
        VG(i)   = acid_write_vol(Atmp,V(1),['target_meanDWI_b' num2str(bC(i)) '_' prefix]);
        VGtmp   = V(bval==bC(1));
        
        % apply bias field
        if(~isempty(Pbias))
            Abias = acid_read_vols(spm_vol(Pbias),VGtmp(1),1);
        end
         
        % register mean image to the first b0 image
        disp(['Registering target number ' num2str(i) ' to the first b0 image'])
        %VGtmp(1).dt = [spm_type('uint16'),spm_platform('bigend')];
        if(~isempty(Pbias))
            flags.Abias = Abias;
            params = acid_spm_coreg_freeze_bias(VGtmp(1),VG(i),flags,0,Vmask);
        else
            params = acid_spm_coreg_freeze(VGtmp(1),VG(i),flags,0,Vmask);
        end

        % apply transformation to header of template
        iMb0 = inv(spm_matrix(params));
        MM   = spm_get_space(deblank(VG(i).fname));
        spm_get_space(deblank(VG(i).fname), iMb0*MM);
        VG(i)= spm_vol(VG(i).fname);
        
    else % if not the b0 shell
              
        % mean image per shell
        lApdw = reshape(lApdw,[],numel(bMSK));
        Atmp  = mean(lApdw,2);
        Atmp  = reshape(Atmp,dm);
        
        % save mean image
        %V(1).dt = [spm_type('float32'),spm_platform('bigend')];
        VG(i)   = acid_write_vol(Atmp,V(1),['target_meanDWI_b' num2str(bC(i)) '_' prefix]);
       
        % register mean image to the first image
        if(dummy_init == false)
            disp(['Registering target number ' num2str(i) ' to the b0 template'])
            if(~isempty(Pbias))
                flags.Abias = Abias;
                params = acid_spm_coreg_freeze_bias(VG(1),VG(i),flags,0,Vmask);
            else            
                params = acid_spm_coreg_freeze(VG(1),VG(i),flags,0,Vmask);
            end
                        
            % apply transformation to header of template
            iM          = inv(spm_matrix(params));
            MM          = spm_get_space(deblank(VG(i).fname));
            spm_get_space(deblank(VG(i).fname), iM*MM);
            VG(i)       = spm_vol(VG(i).fname);
        else
                
            MM          = spm_get_space(deblank(VG(i).fname));
            spm_get_space(deblank(VG(i).fname), iMb0*MM);
            VG(i)       = spm_vol(VG(i).fname);
        end
    end
        
    % reslice on the first image of the dataset
    if strcmp(V(1).fname,V(2).fname)
        P = char([V(1).fname ',1'],VG(i).fname);
    else
        P = char(V(1).fname,VG(i).fname);
    end
    flags.mask   = spm_get_defaults('coreg.write.mask');
    flags.mean   = 0;
    flags.interp = spm_get_defaults('coreg.write.interp');
    flags.which  = 1;
    flags.wrap   = spm_get_defaults('coreg.write.wrap');
    flags.prefix = '';

    spm_reslice(P, flags); % saves resliced image
    [p,f,e]     = spm_fileparts(VG(i).fname);
    [Ptmp,dirs] = spm_select('FPList',p,['^' flags.prefix f e]);
    VG(i)       = spm_vol(Ptmp);
end
    
    fprintf('\ncTarget generation finished.\n');
    
 end