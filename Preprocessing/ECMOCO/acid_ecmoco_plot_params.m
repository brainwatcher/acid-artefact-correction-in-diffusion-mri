function acid_ecmoco_plot_params(params,PM,IN_freeze,dummy_slicewise)
    
% =============================================
% This script creates diagnostic plots for ecmoco by plotting the estimated
% transformation parameters.
%
% Inputs:
%  xx   - transformation paramaters
%  PM   - filename to create a basename for figures
%  IN_freeze - degrees of freedom
%  dummy_slicewise - SW or VW registration
%
% =============================================

if(~exist('dummy_slicewise','var'))
    dummy_slicewise = 0;
end

if(~exist('params','var'))
    % get matfile
    PM = char(cfg_getfile(Inf,'MAT','Source matfiles','','','^ecmoco_params.*'));
    for j=1:size(PM,1)
        load(deblank(PM(j,:)));
        params(j,:)=xk;
    end
end

[p,n,~] = spm_fileparts(PM(1,:));

if(~exist('IN_freeze','var'))
    IN_freeze = spm_input('Type number of averages','','i',[ones(1,6) 0 1 0 1 1 0]);
end
    
if(~dummy_slicewise)
% for VW registration, display two figures, one for MO parameters and one for
% EC parameters.

    if(isempty(find(IN_freeze(1:6)>0,1)))
        warning('Motion parameters were not estimated.')
    else
        plot_motion_parameters(params,IN_freeze,n,p);
    end
        
    if(IN_freeze(2)==1 || IN_freeze(8)==1 || IN_freeze(10)==1 || IN_freeze(11)==1)
        plot_ec_parameters(params,IN_freeze,n,p);
    else
        warning('EC parameters were not estimated.')
    end
    
else
% for SW registration, display one figure for both MO and EC parameters. Display
% only estimated parameters.
    disp_all(params,IN_freeze,n,p);
end

end

%% FUNCTION FOR PLOTTING MOTION PARAMETERS
function plot_motion_parameters(xx,freeze,n,p)
      
    % -------- Translation ---------
        
    figure
    subplot(2,1,1)
    ylabel('translation [mm]','FontSize',12)
    hold on
    
    if freeze(1)==1
        subplot(2,1,1); plot((xx(:,1)),'r.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlex = 'x (red)';
        hold on;
    else
        titlex = 'x (not est.)';
    end
    
    if freeze(2)==1
        subplot(2,1,1); plot((xx(:,2)),'g.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titley = 'y (green)';
        hold on;
    else
        titley = 'y (not est.)';
    end
    
    if freeze(3)==1
        subplot(2,1,1); plot((xx(:,3)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlez = 'z (blue)';
    else
        titlez = 'z (not est.)';
    end
    
    Title = ['Translation in ' titlex ', ' titley ', ' titlez];
    h = title(Title); set(h,'FontSize',12); set(gca,'FontSize',12);
    
%     Title1{1,1} = 'Translation in x (red), y (green), z (blue)';
%     if ~any(MSK==1) || ~any(MSK==2) || ~any(MSK==3)
%         Title1{2,1} = sprintf('Not estimated:');
%         for jj=1:3
%             if ~any(MSK==jj)
%                 Title1{2,1}=strcat(Title1{2,1},sprintf(' %s, ',titles{jj}));
%             end
%         end
%     end
    
    
    % -------- Rotation ---------
    
    subplot(2,1,2)
    ylabel('rotation [rad]','FontSize',12)
    hold on
    
    if freeze(4)==1
        subplot(2,1,2); plot((xx(:,4)),'r.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titlex = 'x (red)';
        hold on;
    else
        titlex = 'x (not est.)';
    end
    
    if freeze(5)==1
        subplot(2,1,2); plot((xx(:,5)),'g.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titley = 'y (green)';
        hold on;
    else
        titley = 'y (not est.)';
    end
    
    if freeze(6)==1
        subplot(2,1,2); plot((xx(:,6)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1])
        titley = 'z (blue)';
    else
        titlez = 'z (not est.)';
    end
    
    Title = ['Rotation along ' titlex ', ' titley ', ' titlez];
    h = title(Title); set(h,'FontSize',12); set(gca,'FontSize',12);
    
%     Title2{1,1} = 'Rotation along x (red), y (green), z (blue)';
%     if ~any(MSK==4) || ~any(MSK==5) || ~any(MSK==6)
%         Title2{2,1} = sprintf('Not estimated:');
%         for jj=4:6
%             if ~any(MSK==jj)
%                 Title2{2,1}=strcat(Title2{2,1},sprintf(' %s, ',titles{jj}));
%             end
%         end
%     end
      
    saveas(gcf,[p filesep 'MO_params_' n '.fig'], 'fig');
    
end

%% FUNCTION FOR PLOTTING EC PARAMETERS
function plot_ec_parameters(xx,freeze,n,p)
   
    figure
    % x-y shear: Gx
    subplot(2,2,1);
    hold on
    if freeze(10)==1
        plot((xx(:,10)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title1 = 'x-y shear: G_x';
    else
        Title1 = sprintf('x-y shear: G_x (not est.)');
    end
    h=title(Title1); set(h,'FontSize',12); set(gca,'FontSize',12);
    
    % y scaling: Gy
    subplot(2,2,2);
    hold on
    if freeze(8)==1
        plot((xx(:,8)),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title2 = 'y scaling: G_y';
    else
        Title2 = sprintf('y scaling: G_y (not est.)');
    end
    h=title(Title2); set(h,'FontSize',12); set(gca,'FontSize',12);
    
    % y-z shear: Gz
    subplot(2,2,3);
    hold on
    if freeze(11)==1
        plot(xx(:,11),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);set(gca,'FontSize',20);
        Title3 = 'y-z shear: G_z';
    else
        Title3 = sprintf('y-z shear: G_z (not est.)');
    end
    h=title(Title3); set(h,'FontSize',12); set(gca,'FontSize',12);    
    
    % y translation
    subplot(2,2,4);
    hold on
    if freeze(2)==1
        subplot(2,2,4); plot(xx(:,2),'b.-','LineWidth',2); xlim([0 size(xx,1)+1]);
        Title4 = 'y translation: B_0';
    else
        Title4 = sprintf('y translation: B_0 (not est.)');
    end
    h=title(Title4); set(h,'FontSize',12); set(gca,'FontSize',12);    
    
    saveas(gcf,[p filesep 'EC_params_' n '.fig'], 'fig');
    
end

%% FUNCTION FOR PLOTTING ALL PARAMETERS [for SW registration]
function disp_all(xx,freeze,n,p)

    figure
    MSK = find(freeze(1:12)>0);
    nm = numel(MSK);
    if(nm>1)
        
    % specify colors for plots
    colors(1,:) = [1 1 0]; % yellow
    colors(2,:) = [1 0 1]; % magenta
    colors(3,:) = [0 1 1]; % cyan
    colors(4,:) = [1 0 0]; % red
    colors(5,:) = [0 1 0]; % green
    colors(6,:) = [0 0 1]; % blue
    colors(7,:) = [0 0 0]; % black
    if size(xx,3) > 7 % if more slices, generate random colors for them
        colors(8:size(xx,3),:) = rand(size(xx,3)-7,3);
    end
    
    % specify titles for plots
    titles = cell(12,1);
    titles{1,1}  = 'Translation in x (mm)';
    titles{2,1}  = 'Translation in y (mm)';
    titles{3,1}  = 'Translation in z (mm)';
    titles{4,1}  = 'Rotation along x';
    titles{5,1}  = 'Rotation along y';
    titles{6,1}  = 'Rotation along z';
    titles{7,1}  = 'Scaling in x';
    titles{8,1}  = 'Scaling in y';
    titles{9,1}  = 'Scaling in z';
    titles{10,1} = 'Shearing in x-y';
    titles{11,1} = 'Shearing in y-z';
    titles{12,1} = 'Shearing in x-z';
        
        % looping through DOFs
        for i=1:nm 

            subplot(2,ceil(nm/2),i); % creates a subplot for each DOF
            
            if i == nm % plot with legend         
                for kk = 1:size(xx,3)
                    plot(xx(:,MSK(i),kk),'color',colors(kk,:),'LineWidth',0.05,'DisplayName',sprintf('Slice %d',kk)); xlim([0 size(xx,1)+1]);
                    h_l = legend('-DynamicLegend');
                    set(h_l,'FontSize',10);
                    set(h_l,'Location','east');
                    hold all
                end
                title(titles{MSK(i),1}, 'FontSize', 20);
                set(gca,'FontSize',20);
            else % plot without legend
                for kk = 1:size(xx,3)
                    plot(xx(:,MSK(i),kk),'color',colors(kk,:),'LineWidth',0.05); xlim([0 size(xx,1)+1]);
                    hold all
                end
                title(titles{MSK(i),1}, 'FontSize', 20);
                set(gca,'FontSize',20);
            end
            
        end
    end
    saveas(gcf,[p filesep 'ecmoco_params_' n '.fig'], 'fig');
    
end