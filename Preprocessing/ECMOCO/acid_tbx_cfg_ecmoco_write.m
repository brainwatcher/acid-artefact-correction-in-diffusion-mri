function ecmoco_write = acid_tbx_cfg_ecmoco_write

% registration parameters
ecmoco_write_Mfile         = cfg_files;
ecmoco_write_Mfile.tag     = 'ecmoco_write_Mfile';
ecmoco_write_Mfile.name    = 'Registration parameters';
ecmoco_write_Mfile.help    = {'Select the matfile containing the registration parameters'};
ecmoco_write_Mfile.filter  = 'mat';
ecmoco_write_Mfile.ufilter = '.*';
ecmoco_write_Mfile.num     = [0 Inf];

% target
ecmoco_write_target         = cfg_files;
ecmoco_write_target.tag     = 'ecmoco_write_target';
ecmoco_write_target.name    = 'Target Image';
ecmoco_write_target.help    = {'Select one reference image. All images will be registered to this image. Recommended is using either the low-b-value image or the mean of the high-b-value images as target.'};
ecmoco_write_target.filter = 'image';
ecmoco_write_target.ufilter = '.*';
ecmoco_write_target.num     = [1 1];

% source images
ecmoco_write_sources         = cfg_files;
ecmoco_write_sources.tag     = 'ecmoco_write_sources';
ecmoco_write_sources.name    = 'Source Images';
ecmoco_write_sources.help    = {'Select source images. These images will be registered to the sources image.'};
ecmoco_write_sources.filter = 'image';
ecmoco_write_sources.ufilter = '.*';
ecmoco_write_sources.num     = [0 Inf];

% prefix
ecmoco_write_prefix         = cfg_entry;
ecmoco_write_prefix.tag     = 'ecmoco_write_prefix';
ecmoco_write_prefix.name    = 'Prefix for output';
ecmoco_write_prefix.help    = {'Prefix for the output filename.'};
ecmoco_write_prefix.strtype = 's';
ecmoco_write_prefix.val     = {'r'};

% interpolation
ecmoco_write_interpol        = cfg_entry;
ecmoco_write_interpol.tag     = 'ecmoco_write_interpol';
ecmoco_write_interpol.name    = 'Interpolation order';
ecmoco_write_interpol.help    = {'Interpolation order as defined in spm_reslice. A splin-interpolation is used by default.'};
ecmoco_write_interpol.strtype = 'e';
ecmoco_write_interpol.num     = [1 1];
ecmoco_write_interpol.val     = {-7};

% phase-encoding direction
ecmoco_write_phdir         = cfg_menu;
ecmoco_write_phdir.tag     = 'ecmoco_write_phdir';
ecmoco_write_phdir.name    = 'Direction of phase-encoding';
ecmoco_write_phdir.help    = {''
'Specify the phase-encoding or distortion direction of your data.'
''};
ecmoco_write_phdir.labels = {'x','y','z'};
ecmoco_write_phdir.values = {1 2 3};
ecmoco_write_phdir.val    = {2};

% exbranch
ecmoco_write         = cfg_exbranch;
ecmoco_write.tag     = 'ecmoco_write';
ecmoco_write.name    = 'Write ECMOCO';
ecmoco_write.val     = {ecmoco_write_Mfile, ecmoco_write_target, ecmoco_write_sources, ecmoco_write_prefix, ecmoco_write_interpol, ecmoco_write_phdir};
ecmoco_write.help    = {
                    'Reslices images using the transformation parameters stored in the matfiles produced by EC and motion correction (with or without variable targets). It allows to specify a target image to which the images will be resliced and a dimension, along which the EC distortions were be corrected.'
                    };
ecmoco_write.prog    = @local_ECMO_write;
ecmoco_write.vout    = @vout_ECMO_write;

end

function out = local_ECMO_write(job)
    params = load(char(job.ecmoco_write_Mfile));
    
    acid_ecmoco_write(params.params, char(job.ecmoco_write_sources), char(job.ecmoco_write_target),...
        job.ecmoco_write_prefix, job.ecmoco_write_interpol, job.ecmoco_write_phdir);

    out.rfiles = acid_spm_file(char(job.ecmoco_write_sources),'prefix','r','format','.nii');
    out.rfiles = acid_select(out.rfiles(:));
    out.rfiles = out.rfiles(:);
end

function dep = vout_ECMO_write(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resliced Images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end