function [VG] = acid_ecmoco_main(P,dummy_slicewise,dof1,dof2,bvals,dummy_init,excluded,Vmask,Pbias,dummy_write,dummy_plot,prefix,thr_hist,sep,interpol,phase, zsmooth_indx, npool, dummy_single_or_multi_target, ST_target)

% ======================================
% The script performs eddy current and motion correction on dMRI images by
% means of registration.
%
% Inputs:
%   P               - filenames of source images
%   dummy_slicewise - 1: slice-wise registration
%                     0: volume-wise registration
%   dof1            - degrees of freedom for VW registration
%   dof2            - degrees of freedom for SW registration
%   bvals           - b-values of the source images
%   dummy_init      - 1: initialization mode
%                     0: no initialization mode
%   excluded        - list of excluded volumes
%   Pmask           - filename of the binary mask
%   Pbias           - filename of bias field image
%   dummy_write     - 1: write corrected images
%                     0: do not write corrected images
%   dummy_plot      - 1: display eddy current and motion parameters
%                     0: do not display eddy current and motion parameters
%   prefix          - prefix for output filename
%   thr_hist        - threshold for histogram, below which voxels are ignored for registration
%   sep             - resampling parameter for optimization
%   interpol        - type of interpolation
%   phase           - phase-encoding direction
%
% Outputs:
%   VG              - headers of the target volumes
%
% written: S. Mohammadi (02/02/2013)
% adapted: G. David
% ======================================

global IN_freeze VFmat InterpLength VGmat VFdim phase_encoding

%% CHECK INPUTS
% defaults:
InterpLength = 1;
phase_encoding = phase;

[p,~,~] = fileparts(P(1,:));
diary([p filesep 'logfile_ecmoco.txt'])
if ~exist('P','var')
    P = char(cfg_getfile(Inf,'IMAGE','Source Images (all images)','','','.*'));
end
dummy_4D = 0;
% Prepare if 4D-Volumes (one image == 4D)
if size(P,1) == 1
    struct4D   = nifti(P);
    dim4D = struct4D.dat.dim;
    n   = dim4D(4);
    if n == 1
        error('A single 3D source image was selected. Choose a single 4D volume or select all 3D volumes manually!');
    end
    dummy_4D = 1;
    P = strcat(repmat(P(1:end-2), n, 1), ',', num2str([1:n]'));
end

% Read volumes
V = spm_vol(P);

% BIDS
[~,fname_bidsi,~] = spm_fileparts(V(1).fname);

dummy_bids = 0;

if strcmp(fname_bidsi(1:4),'sub-') == 1  && strcmp(fname_bidsi(end-3:end),'_dwi') == 1 && dummy_4D == 1
    dummy_bids = 1;
end

% disable experimanetal BIDS support
dummy_bids = 0;


if ~exist('dummy_slicewise','var')
    dummy_slicewise = 0;
end

if ~exist('bvals','var')
    bvals = spm_input('Select b values for each image','','r','',size(P,1));
end

if ~exist('Vmask','var')
    Vmask = [];
end

if ~exist('Pbias','var')
    Pbias = [];
end

if ~exist('dummy_init','var')
    dummy_init = false;
end

if ~exist('thr_hist','var')
    thr_hist = 0;
end

if isempty(bvals)
    bvals = 1E5*ones(1,size(P,1));
end

if size(bvals,2)~=size(P,1)
    error('The number of source images and entries of the corresponding b-value vector have to be the same!');
end

%% Single target

if dummy_single_or_multi_target
    %
    % Inputs:
    % PG  ST_target          - reference image
    % rP  P          - source images
    % IN_freeze     - transformation parameters
    % dummy_write   - 0: don't write registered images
    %                 1: write registred images
    % dummy_disp    - display eddy current and motion parameters
    %
    % S. Mohammadi (05/08/2012)
    
    % defaults:
    InterpLength = 1;
    
    % write
    IN_freeze = dof1;
    % select the target image
    % PG      = char(cfg_getfile(1,'img',['Target Images: b0 image']));
    VG      = spm_vol(P(1,:));
    %VG      = cell2mat(VG);
    VGmat   = VG(1).mat; % slicewise
    
    % check whether P is a 4d dataset
    [p,f,n] = spm_fileparts(P(1,:));
    [PGtest,~]  = spm_select('FPList',p, ['^' f n]);
    VGtest  = spm_vol(PGtest);
    [p,f,n] = spm_fileparts(P(1,:));
    [rPtest,~]  = spm_select('FPList',p, ['^' f n]);
    rVtest  = spm_vol(rPtest);
    
    if(size(VGtest,1)~=size(PGtest,1) || size(rPtest,1)~=size(rVtest,1))
        error('You are using 4D image(s), data must be converted into 3D or use EC and Motion Correction multi targets.');
    else
        rV      = spm_vol(P(1:size(P,1),:));
    end
    
    mireg    = struct('VG',[],'VF',[],'PO','');
    % rename target
    mireg.VG = spm_vol(VG(1));
    
    [p,n,e] = spm_fileparts(mireg.VG.fname);
    
    % rename source(s)
    mireg.VF    = rV;
    VFmat       = rV(1).mat; % slicewise
    VFdim       = rV(1).dim; % slicewise
    
    %%%%%%%%%%%%%% Coregister %%%%%%%%%%%%%%%%%%%%%%%%%
    for j=1:size(mireg.VF,1),
        fprintf('\nSource image: ');
        disp(mireg.VF(j).fname);
        fprintf('\n\nTarget image: ');
        disp(mireg.VG.fname);
        fprintf('\n\n\n');
        [p n e] = spm_fileparts(P(j,:));
        tmp = [p filesep 'ecmoco_params_' n '.mat'];
        Mfiles(j,1:numel(tmp)) = [p filesep 'ecmoco_params_' n '.mat'];
        params(:,j) = acid_spm_coreg_freeze(mireg.VG, mireg.VF(j));
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    if(dummy_write)
        EC_MO_write_rFiles(Mfiles,mireg,deblank(e));
    end
    
    if(dummy_plot)
        disp_ECMO(Mfiles,IN_freeze)
    end
    disp('All diffusion weighted images coregistered to B0')
    
    
    
    %% Multi target
else
    % set up DOFs
    if dummy_slicewise
        % do not allow only SW for a given DOF
        if (dof2(1)==1 && dof1(1)==0), dof2(1)=0; end
        if (dof2(2)==1 && dof1(2)==0), dof2(2)=0; end
        if (dof2(3)==1 && dof1(6)==0), dof2(3)=0; end
        if (dof2(4)==1 && dof1(7)==0), dof2(4)=0; end
        if (dof2(5)==1 && dof1(8)==0), dof2(5)=0; end
        if (dof2(6)==1 && dof1(10)==0),dof2(6)=0; end
        if (dof2(7)==1 && dof1(11)==0),dof2(7)=0; end
        freeze = cat(2,dof1,dof2);
    else
        freeze = dof1;
    end
    IN_freeze = freeze;
    
    % indices of included (non-excluded) volumes
    i = 1;
    
    if ~isempty(excluded{1})
        
        [~, ~, e]=fileparts(excluded{1});
        if strcmp(e,'.mat')
            excluded = load(excluded{1});
        elseif strcmp(e,'.txt')
            excluded = dlmread(excluded{1});
        end
        if size(excluded,2)==1
            excluded=excluded';
        end
        if size(excluded,1)>1
            excluded = excluded.idx(1,:)';
        end
        included = zeros(size(P,1)-length(excluded),1);
        for k = 1:size(P,1)
            if ~ismember(k,excluded)
                included(i,1) = k;
                i = i+1;
            end
        end
    else
        excluded = [];
        included = (1:size(P,1))';
    end
    
    
    
    
    
    %Vmask = spm_vol(Vmask);
    %if (~exist('Vmask','var') && ~isequal(Vmask.dim,V(1).dim))
    %    error('The dimensions of the mask and the source images have to be identical!');
    %end
    
    if(~isempty(Pbias))
        Abias = acid_read_vols(spm_vol(Pbias),VG(1),1);
    end
    
    %% GENERATE TARGETS
    
    if bvals(1) < 1E5
        % calculate targets for each shell
        %   P      - filename of dMRI dataset
        %   bvals  - b values
        %   freeze - degrees of freedom of registration
        %   Pmask  - filename of the mask
        %   Pbias  - filename of bias field image
        %   dummy_init - dummy for initialization mode
        [VG,bvalues] = acid_ecmoco_create_targets(P,bvals,freeze,Vmask,Pbias,dummy_init,prefix);
        [bC,~,~] = unique(bvals);
        %[VG,bvalues] = acid_ecmoco_create_targets_v2(P,bvals);
    else
        % if no b values are specified, take the first volume as target
        VG = V(1);
        bvalues = 9999;
    end
    
    if iscell(VG)
        VG = cell2mat(VG);
    end
    
    vg_size = VG.dim;
    
    if ~isempty(Vmask) % mask
        Vmask = acid_read_vols(spm_vol(Vmask),VG(1),1);
    else
        Vmask = ones(vg_size(1),vg_size(2),vg_size(3));
    end
    
    
    %% CREATE REGISTRATION STRUCTURE
    % define flags for registration
    % flags - a structure containing the following elements:
    %          sep      - optimisation sampling steps (mm)
    %                     default: [4 2]
    %          params   - starting estimates (6 elements)
    %                     default: [0 0 0  0 0 0]
    %          cost_fun - cost function string:
    %                      'mi'  - Mutual Information
    %                      'nmi' - Normalised Mutual Information
    %                      'ecc' - Entropy Correlation Coefficient
    %                      'ncc' - Normalised Cross Correlation
    %                      default: 'nmi'
    %          tol      - tolerences for accuracy of each param
    %                     default: [0.02 0.02 0.02 0.001 0.001 0.001]
    %          fwhm     - smoothing to apply to 256x256 joint histogram
    %                     default: [7 7]
    %          freeze   - degrees of freedom
    
    % default flags
    %dm = VG(1).dim;
    dm = VG(1).dim;
    %{
if ~isempty(Vmask) % mask
    Vmask = acid_read_vols(Vmask,VG,1);
else
    Vmask = ones(size(VG));
end
    %}
    
    def_flags = struct('sep',sep,'params',[0 0 0  0 0 0  1 1 1  0 0 0], ...
        'cost_fun','nmi','fwhm',[7 7],...
        'tol',[0.01 0.01 0.01  0.005 0.005 0.005  0.005 0.005 0.005  0.005 0.005 0.005], ...
        'graphics',1, 'freeze', freeze, 'perc', 0.8,'AMSK',ones(dm),'reshold',interpol,'smk',[1 1 1]);
    
    % flags for dwi volumes (all DOFs)
    flags = def_flags;
    
    % flags for b0 volumes (only rigid-body DOFs)
    flags0 = flags;
    if dummy_slicewise
        flags0.freeze([7:12,16:19]) = 0;
    else
        flags0.freeze(7:12) = 0;
    end
    
    % create structure containing the source and target images
    %  VG   - header of the target image
    %  VF   - header of the source image
    %  PO   -
    mireg = struct('VG',[],'VF',[],'PO','');
    mireg.VG = VG;
    mireg.VF = V;
    
    VFmat = V(1).mat; % slicewise
    VFdim = V(1).dim; % slicewise
    
    %% OBTAIN TRANSFORMATION PARAMETERS
    
    % initializing transformation and initialization parameters
    if(~dummy_slicewise)
        params = zeros(12,size(P,1));
        params(7:9,:) = 1;
        initparams = params;
    else
        params = zeros(12,size(P,1),dm(3));
        params(7:9,:,:) = 1;
        initparams = squeeze(params(:,:,1));
    end
    
    % round b values to 100's
    % return b values without repetition
    bvals = bvals';
    refNtmp = round(bvals/100)*100;
    [aa,~,~] = unique(refNtmp,'rows');
    idx = [];
    idx_dw = [];
    
    % idx: indices of b0 volumes followed by indices of dw volumes (shells in increasing order)
    % idx_b0: indices of b0 volumes
    % idx_dw: indices of dw volumes
    for m = 1:numel(aa)
        idx_tmp = find(refNtmp==aa(m));
        idx = cat(1,idx,idx_tmp);
        if m==1
            idx_b0 = idx_tmp;
        else
            idx_dw = cat(1,idx_dw,idx_tmp);
        end
    end
    
    % idx2: same as idx but excluding the specified volumes
    % idx2_b0: same as idx_b0 but excluding the specified volumes
    % idx2_dw: same as idx_dw but excluding the specified volumes
    i = 1;
    for k = 1:numel(idx)
        if ~ismember(idx(k),excluded)
            idx2(i,1) = idx(k);
            i = i+1;
        end
    end
    i = 1;
    for k = 1:numel(idx_b0)
        if ~ismember(idx_b0(k),excluded)
            idx2_b0(i,1) = idx_b0(k);
            i = i+1;
        end
    end
    i = 1;
    for k = 1:numel(idx_dw)
        if ~ismember(idx_dw(k),excluded)
            idx2_dw(i,1) = idx_dw(k);
            i = i+1;
        end
    end
    
    
    
    if(npool>1)
        if(exist('parpool')>0)
            poolobj = parpool('local',npool);
            dummy_matlabpool = false;
        elseif(exist('matlab')>0)
            parpool('open',npool);
            dummy_matlabpool = true;
        else
            npool = 1;
            warning('No parallel processing possible on this matlab version! Proceed without parallel processing.')
        end
    end
    
    if(npool>1)
        par_dummy = npool;
    else
        par_dummy = 0;
    end
    
    %% VOLUME-WISE REGISTRATION
    if(~dummy_slicewise)
        
        
        for g = 1:numel(aa)
            %[~,i] = min(abs(refNtmp(g) - aa));
            VG_array(:,:,:,g) = acid_read_vols(mireg.VG(g),mireg.VG(g),1); % target image
            
        end
        %XXXXX
        for u = 1:numel(idx2_b0)
            
            l = idx2_b0(u);
            [~,b] = min(abs(refNtmp(l) - aa));
            VF_array(:,:,:,l) = acid_read_vols(mireg.VF(l),mireg.VG(b),1); % target image
            
        end
        
        
        % MOTION CORRECTION
        % looping through b0 volumes (first b0, then dwi)
        for jj = 1:numel(idx2_b0)
            j = idx2_b0(jj);
            [~,b] = min(abs(refNtmp(j) - aa));
            fprintf('\nSource image: ');
            disp([mireg.VF(j).fname ', ' int2str(mireg.VF(j).n(1,1))]);
            fprintf('Target image: ');
            disp([mireg.VG(b).fname ', ' int2str(mireg.VG(b).n(1,1))]);
            
            % create registration flags for b0 and dwi volumes
            % NOTE: for b0 rigid-body (6 DOF) registration is performed
            if(bvalues(b)<100)
                flagstmp = flags0;
            else
                flagstmp = flags;
            end
            
            % initialize transformation parameters with the interpolated values
            % create new registration flags
            if (dummy_init)
                if size(idx_b0,1) == 1
                    error('Only one b0 image was identified. Please deactivate the initilization mode or check the entered b values.')
                end
                if j > idx2(1)
                    if ismember(j,idx2_b0)
                        flagstmp.params = initparams(:,idx2_b0(jj-1))'; % use the value from the previous b0 volume
                    else
                        flagstmp.params = initparams(:,j)';
                    end
                end
            else
                disp('No interspersed b=0 images! Registration is uncorrelated.')
            end
            
            % obtain transformation parameters for motion
            if(~isempty(Pbias))
                flagstmp.Abias = Abias;
                % xk = acid_spm_coreg_freeze_bias(mireg.VG(b),mireg.VF(j),flagstmp,Vmask);
                xk = acid_spm_coreg(VG_array(:,:,:,b),VF_array(:,:,:,j),flagstmp,thr_hist,Vmask,mireg.VG(b));
            else
                %xk = acid_spm_coreg_freeze(mireg.VG(b),mireg.VF(j),flagstmp,thr_hist,Vmask);
                xk = acid_spm_coreg(VG_array(:,:,:,b),VF_array(:,:,:,j),flagstmp,thr_hist,Vmask,mireg.VG(b));
            end
            params(:,j) = xk;
            
            % create interpolated values for the b0 and dwi volumes
            if (dummy_init)
                % if b0 volume, keep the obtained parameters for initialization
                if ismember(j,idx_b0)
                    initparams(:,j) = xk;
                end
                
                % registration parameters are obtained for b0 volumes first,
                % then for dwi volumes
                % after the last b0 volume, registration parameters of b0 volumes are
                % interpolated for the dwi volumes
                % NOTE: only rigid-body parameters are interpolated
                if(j==idx2_b0(end))
                    % parameters for b0 volumes
                    paramb0 = params(1:6,idx2_b0);
                    % interpolation is performed for each dof separately
                    for inxpb0 = 1:size(paramb0,1)
                        initparams(inxpb0,:) = interp1(idx2_b0',paramb0(inxpb0,:),1:numel(bvals),'linear',paramb0(inxpb0,end));
                    end
                end
            end
        end
        
        
        if ~exist('idx2_dw','var')
            idx2_dw = 1;
        end
        
        
        tmp_matrix = zeros(12, length(idx2_dw));
        
        f = (numel(idx2_dw) / npool);
        
        if round(f) ~= f
            f = ceil(f);
            f = f + 1;
            
        end
        
        
        for k = 1:f
            
            start_counter = (1 + npool * (k-1));
            
            end_counter = (npool * k);
            
            while end_counter > numel(idx2_dw)
                end_counter = end_counter - 1;
            end
            
            for o = start_counter : end_counter
                
                l = idx2_dw(o);
                [~,b] = min(abs(refNtmp(l) - aa));
                VF_array(:,:,:,l) = acid_read_vols(mireg.VF(l),mireg.VG(b),1); % target image
                
            end
            
            
            
            % MOTION CORRECTION
            % looping through dw volumes (first b0, then dwi)
            parfor (jj = start_counter : end_counter, par_dummy)
                j = idx2_dw(jj);
                [~,b] = min(abs(refNtmp(j) - aa));
                fprintf('\nSource image: ');
                disp([mireg.VF(j).fname ', ' int2str(mireg.VF(j).n(1,1))]);
                fprintf('Target image: ');
                disp([mireg.VG(b).fname ', ' int2str(mireg.VG(b).n(1,1))]);
                
                % create registration flags for b0 and dwi volumes
                % NOTE: for b0 rigid-body (6 DOF) registration is performed
                if(bvalues(b)<100)
                    flagstmp = flags0;
                else
                    flagstmp = flags;
                end
                
                % initialize transformation parameters with the interpolated values
                % create new registration flags
                if (dummy_init)
                    if size(idx_b0,1) == 1
                        error('Only one b0 image was identified. Please deactivate the initilization mode or check the entered b values.')
                    end
                    if j > idx2(1)
                        if ismember(j,idx2_b0)
                            prev_b0 = idx2_b0(jj-1);
                            flagstmp.params = initparams(:,prev_b0)'; % use the value from the previous b0 volume
                        else
                            flagstmp.params = initparams(:,j)';
                        end
                    end
                else
                    disp('No interspersed b=0 images! Registration is uncorrelated.')
                end
                
                % obtain transformation parameters for motion
                if(~isempty(Pbias))
                    %flagstmp.Abias = Abias;
                    %tmp_matrix(:,jj) = acid_spm_coreg_freeze_bias(mireg.VG(b),mireg.VF(j),flagstmp,Vmask);
                    tmp_matrix(:,jj) = acid_spm_coreg(VG_array(:,:,:,b),VF_array(:,:,:,j),flagstmp,thr_hist,Vmask,mireg.VG(b));
                else
                    %tmp_matrix(:,jj) = acid_spm_coreg_freeze(mireg.VG(b),mireg.VF(j),flagstmp,thr_hist,Vmask);
                    tmp_matrix(:,jj) = acid_spm_coreg(VG_array(:,:,:,b),VF_array(:,:,:,j),flagstmp,thr_hist,Vmask,mireg.VG(b));
                end
                %params(:,j) = xk;
                
            end
            
            
            VF_array(:,:,:,:) = 0;
        end
        params(:,idx2_dw) = tmp_matrix;
    else
        %% SLICE-WISE REGISTRATION
        
        for g = 1:numel(aa)
            
            %[~,i] = min(abs(refNtmp(g) - aa));
            VG_array(:,:,:,g) = acid_read_vols(mireg.VG(g),mireg.VG(g),1); % target image
        end
        %XXXXX
        for u = 1:numel(idx2_b0)
            
            l = idx2_b0(u);
            [~,b] = min(abs(refNtmp(l) - aa));
            VF_array(:,:,:,l) = acid_read_vols(mireg.VF(l),mireg.VG(b),1); % target image
            
        end
        
        
        % looping through b0 volumes (first b0, then dwi)
        for jj=1:numel(idx2_b0)
            j = idx2_b0(jj);
            [~,b] = min(abs(refNtmp(j) - aa));
            fprintf('\n\nSource image: ');
            disp([mireg.VF(j).fname ', ' int2str(mireg.VF(j).n(1,1))]);
            fprintf('Target image: ');
            disp([mireg.VG(b).fname ', ' int2str(mireg.VG(b).n(1,1))]);
            
            % create registration flags for b0 and dwi volumes
            % NOTE: for b0 rigid-body (6 DOF) registration is performed
            if(bvalues(b)<100)
                flagstmp = flags0;
            else
                flagstmp = flags;
            end
            
            % initialize transformation parameters with the interpolated values
            % create new registration flags
            if (dummy_init)
                if size(idx_b0,1) == 1
                    error('Only one b0 image was identified. Please deactivate the initilization mode or check the entered b values.')
                end
                if j > idx2(1)
                    if ismember(j,idx2_b0)
                        flagstmp.params = initparams(:,idx2_b0(jj-1))';
                    else
                        flagstmp.params = initparams(:,jj)';
                    end
                end
            else
                disp('No interspersed b=0 images! Registration are uncorrelated.')
            end
            
            % obtain transformation parameters
            [xk,xk0] = acid_coreg_slicew(VF_array(:,:,:,j),VG_array(:,:,:,b),flagstmp,thr_hist,Vmask,mireg.VG(b), zsmooth_indx);
            params(:,j,:) = xk;
            
            % create interpolated values for the b0 and dwi volumes
            if (dummy_init)
                % if b0 volume, keep the obtained par ameters for initialization
                if ismember(j,idx2_b0)
                    initparams(:,j) = xk0;
                end
                
                % registration parameters are obtained for b0 volumes first,
                % then for dwi volumes
                % after the last b0 volume, registration parameters of b0 volumes are
                % interpolated for the dwi volumes
                % NOTE: only rigid-body parameters are interpolated
                if(j==idx2_b0(end))
                    paramb0 = params(1:6,idx2_b0);
                    for inxpb0 = 1:size(paramb0,1)
                        initparams(inxpb0,:) = interp1(idx2_b0',paramb0(inxpb0,:),1:numel(bvals),'linear',paramb0(inxpb0,end));
                    end
                end
            end
        end
        
        tmp_matrix = zeros(12, length(idx2_dw),dm(3));
        
        f = (numel(idx2_dw) / npool);
        
        if round(f) ~= f
            f = ceil(f);
            f = f + 1;
            
        end
        
        
        for k = 1:f
            
            start_counter = (1 + npool * (k-1));
            
            end_counter = (npool * k);
            
            while end_counter > numel(idx2_dw)
                end_counter = end_counter - 1;
            end
            
            for o = start_counter : end_counter
                
                l = idx2_dw(o);
                [~,b] = min(abs(refNtmp(l) - aa));
                VF_array(:,:,:,l) = acid_read_vols(mireg.VF(l),mireg.VG(b),1); % target image
                
            end
            
            % looping through dw volumes (first b0, then dwi)
            
            parfor (jj = start_counter : end_counter, par_dummy)
                j = idx2_dw(jj);
                [~,b] = min(abs(refNtmp(j) - aa));
                fprintf('\n\nSource image: ');
                disp([mireg.VF(j).fname ', ' int2str(mireg.VF(j).n(1,1))]);
                fprintf('Target image: ');
                disp([mireg.VG(b).fname ', ' int2str(mireg.VG(b).n(1,1))]);
                
                % create registration flags for b0 and dwi volumes
                % NOTE: for b0 rigid-body (6 DOF) registration is performed
                if(bvalues(b)<100)
                    flagstmp = flags0;
                else
                    flagstmp = flags;
                end
                
                % initialize transformation parameters with the interpolated values
                % create new registration flags
                if (dummy_init)
                    if size(idx_b0,1) == 1
                        error('Only one b0 image was identified. Please deactivate the initilization mode or check the entered b values.')
                    end
                    if j > idx2(1)
                        if ismember(j,idx2_b0)
                            flagstmp.params = initparams(:,idx2_b0(jj-1))';
                        else
                            flagstmp.params = initparams(:,j)';
                        end
                    end
                else
                    disp('No interspersed b=0 images! Registration are uncorrelated.')
                end
                
                % obtain transformation parameters
                [xk,~] = acid_coreg_slicew(VF_array(:,:,:,j),VG_array(:,:,:,b),flagstmp,thr_hist,Vmask,mireg.VG(b), zsmooth_indx);
                tmp_matrix(:,jj,:) = xk;
            end
            VF_array(:,:,:,:) = 0;
        end
        
        params(:,idx2_dw,:) = tmp_matrix;
    end
    
    
    if(npool>1)
        if(dummy_matlabpool)
            parpool close;
        else
            delete(poolobj);
        end
    end
    
    disp('Replace registration parameters for the excluded volumes')
    % replace registration parameters for the excluded volumes
    for k = 1:numel(excluded)
        
        % i: index of volume whose reg. parameters will be copied
        % if the first volume is excluded
        if excluded(k)<included(1)
            i = included(1);
            while ismember(i,excluded)
                i = i+1;
            end
        else
            i = excluded(k)-1;
            while ismember(i,excluded)
                i = i-1;
            end
        end
        if ~dummy_slicewise
            params(:,excluded(k)) = params(:,i);
        else
            params(:,excluded(k),:) = params(:,i,:);
        end
    end
    disp('Done!')
    
    disp('Save transformation parameters')
    % save transformation parameters
    if (dummy_slicewise)
        [p, n, ~] = spm_fileparts(V(1).fname);
        Mtmp1 = [p filesep 'ecmoco_params_' n '.mat'];
        save(Mtmp1,'params')
    else
        [p, n, ~] = spm_fileparts(V(1).fname);
        Mtmp1 = [p filesep 'ecmoco_params_' n '.mat'];
        save(Mtmp1,'params')
    end
    disp('Done!')
    % plot parameters
    if(dummy_plot)
        if dummy_slicewise
            acid_ecmoco_plot_params(permute(params,[2 1 3]),V(1).fname,IN_freeze,dummy_slicewise)
        else
            acid_ecmoco_plot_params(params',V(1).fname,IN_freeze,dummy_slicewise)
        end
    end
    
    %% RESLICE IMAGES (based on the obtained transformation parameters)
    disp('Reslice images')
    % params         - transformation parameters
    % V              - headers of the source images
    % VG(1).fname    - header of the target image
    % flags.reshold  - type of interpolation
    % phase          - phase-encoding direction
    if dummy_write
        
        if any(bvals)
            I = acid_ecmoco_write(params,V,V(1),prefix,flags.reshold,phase,dummy_bids);
        else
            I = acid_ecmoco_write(params,V,V(1),prefix,flags.reshold,phase,dummy_bids);
        end
        
        % generate mean images within each shell (after ecmoco)
        if bvals(1) < 1E5
            Vtmp = V(1);
            for m = 1:numel(aa)
                idx_tmp = find(refNtmp==aa(m));
                MeanShell = mean(I(:,:,:,idx_tmp),4);
                [~,f,~] = fileparts(P(1,:));
                [pstart,~,~] = fileparts(P(1,:));
                Vtmp.fname = [pstart filesep 'mean_b' num2str(aa(m)) '_' prefix f '.nii'];
                Vtmp.dim = size(MeanShell);
                Vtmp.n = [1,1];
                Vtmp.descrip = '3D image';
                spm_write_vol(Vtmp,MeanShell);
            end
        end
    end
    disp('Done!')
    disp('All dMRI volumes registered!')
    diary off
end
end