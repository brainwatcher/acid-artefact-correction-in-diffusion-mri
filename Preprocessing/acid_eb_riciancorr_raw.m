function [V4D,dummy_4D]= acid_eb_riciancorr_raw(prefix,encha, noise_est, filen, dummy_4D)

%-------------------------------------------------------------------------%
%   Script for denoising images non-central Chi distributed
%   1- estimation of noise underlying gaussian standard deviation from 
%       noise data (noRF acquisition)
%   2- denoising assuming gaussian distribution (BM4D algorithm)
%   3- correction taking into account effective non-central Chi distribution
%   4- mapping to gaussian distribution (optional)
%
%   Step 3 is important for low SNR data and will therefore impact the
%   results of DKI fitting with high b-values.
%
%-------------------------------------------------------------------------%
%   USAGE
%   eb_denoise(ncha, is_siemens_recon, is_nCC2gauss_required)
%   - ncha = number of channels
%   - is_siemens_recon = 0/1, whether image reconstructed by Siemens, in
%       which case data points have integer values and the distribution 
%       will be central-Chi with N = ncha. Otherwise, values are non 
%       integer and central-Chi distribution with N = ncha/2 (homodyne 
%       recon)
%   - is_nCC2gauss_required = 0/1, whether calculating an image gaussian
%       distributed is required (quite time consuming and more for
%       investigational purpose than really useful for noise correction!) 
%
%-------------------------------------------------------------------------%
%   Evelyne Balteau - Cyclotron Research Centre - Liege/Belgium
%   May 2012
%  The Rician bias correction on raw data using the M2 method described in 
%  Andre et al., 2014 (https://doi.org/10.1371/journal.pone.0094531) was 
%  implemented to ACID toolbox by S. Mohammadi 10/10/2019
%-------------------------------------------------------------------------%

% defaults
V4D                     = [];
is_siemens_recon        = 1;
is_nCC2gauss_required   = 0;
sig                     = noise_est;
N                       = encha;

% check whether is a 4D dataset
V = spm_vol(filen);
szV = size(V,1);
if szV>1
    dummy_4D = true;
end
VG = V(1);

if dummy_4D == true
    Vout = VG;
    Vout.descrip =['4d array of Rician corrected images'];
   [V4D, Ni] = ACID_write_4D(szV,Vout,VG,prefix);
   
   spm_progress_bar('Init',szV,Ni.descrip,'volumeses completed');

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% remove rice bias in all images - sigma is assumed to be the same in all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for cim = 1:szV;

    [pth,nam] = fileparts(filen(cim,:));
    disp(['Processing ' nam ' (image #' num2str(cim) '/' num2str(size(filen,1)) ')']);

    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    % load image
    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    Y = acid_read_vols(V(cim),VG,1);

%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
% calculate lookup table for noise correction
%++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%       [An, Mn] = eb_An_Mn_LUT(sig, max(Y(:))*1.01, N);
%
%         %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%         % calculate noise-free signal based on <m> estimate and LUT
%         %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%         etaY = Y;
%         minMn = sig*sqrt(pi/2)*fact(2*N-1,2)/(2^(N-1)*fact(N-1,1));
%         etaY(Y<minMn) = minMn;
%         etaY = interp1(Mn,An,etaY(:),'linear');
%         etaY = reshape(etaY, V(cim).dim);    
%         powY    =etaY;
%         powY(powY<=0)       = 1;
%        powY = powY+20;

%     %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
%     % calculate noise-free signal based on power image subtraction
%     %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    powY = sqrt(Y.^2-2*N*sig^2);
    powY(imag(powY)>0)  = 0;
    powY(isnan(powY))   = 0;
    powY(powY<=0)       = 1;

    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    % save images as nii
    %++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    if(~dummy_4D)
        kk = cim;
        if(kk<10)
            inx = ['-00' num2str(kk)];
        elseif(kk>=10 && kk<100)
            inx = ['-0' num2str(kk)];
        elseif(kk>=100 && kk<1000)
            inx = ['-' num2str(kk)];
        else
            error('Sorry cannot cound that far...')
        end
        my_write_vol_nii(powY,VG,'pow_',inx,['Rician corrected image']);

    else
        Ni.dat(:,:,:,cim) = single(powY);
        spm_progress_bar('Set',cim);
    end
    disp(['Rician bias corrected 4D images (# ' num2str(cim) ') written'])
end
spm_progress_bar('Clear');

