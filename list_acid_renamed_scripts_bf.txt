old script name		new script name

my_write_data			acid_write_data							Ausführlicherer Header sonst identisch
				acid_write_vol									nutzt vol anstatt dat/a 					
acid_my_select			acid_select								identisch
acid_read_vols			acid_read_vols								Ausführlicherer Header sonst identisch

ECMOCO:
EC_MO_VARTAR_correction.m	acid_ecmoco_main.m
ShellwisemeanDWI.m		acid_ecmoco_create_targets.m
spm_coreg_freeze_v04.m		acid_spm_coreg_freeze_bias.m
spm_coreg_freeze_v03.m		acid_spm_coreg_freeze.m
ACID_coreg_slicew.m		acid_ecmoco_coreg_slicew.m
disp_ECMO_v00.m 		acid_ecmoco_plot_params.m

The four scripts below have been combined into a single one (acid_ecmoco_write.m):
EC_MO_write_function_sw_v03.m	acid_ecmoco_write.m
EC_MO_write_sw_function_v04.m	acid_ecmoco_write.m
EC_MO_write_function_v03.m	acid_ecmoco_write.m
EC_MO_write_function_v04.m	acid_ecmoco_write.m

write_image_ECMT_v3.m		acid_ecmoco_reslice.m
my_write_vol_nii.m		acid_write_vol.m
acid_read_vols.m		acid_read_vols.m
spm_coreg_freeze_v03.m		acid_spm_coreg_freeze.m
spm_powell_VARTARG.m		acid_spm_powell_VARTARG.m
spm_matrix_mod.m		acid_spm_matrix.m

ACID_weightbasedcombination.m	acid_combine_images.m
ACID_make_Bmask.m		acid_make_brainmask.m
COVIPER_PROD.m			acid_coviper_prod.m


MODEL FITTING:
DTI_olsq_robust		acid_dti_main								Von mir kommentiert
geometrical_matrix_basser	acid_dti_get_bmatrix							Identisch (andere variablennamen, header vorhanden)
geometrical_matrix_varTE_varb	acid_dti_get_bmatrix_varTE_varb					Absolut identisch
callogDWincludingS0_vstruct_2D acid_dti_get_logS_2D							Unterschieldich (in ursprünglicher version eine for 																	schleife zuviel?)
callogDW_vstruct		acid_dti_get_logS							Identisch (varibale umbennant, ausführlicherer Header)
robust_estimate_Zwiers2010_slicew_weights	acid_dti_robust_compute_tensor_IRLS			Laut header ursprüngliche Dateie 3 Jahre 														neuer?	, write weights hinzugefügt	, zeile 59? 														neue version nutzt ols Ergebnis
robustestimate_DT_prod		acid_dti_robust_estimate_Zwiers2010					Zeile 74 alt / Zeile 99 neu?, prefactor?
estimate_tensornew		acid_dti_compute_eigenvalues						Identisch (größerer Header, etwas anderer Stil)
write_DTIdataset_prod		acid_dti_compute_DTImaps						Neue Version deutlich kürzer, sehr wahrscheinlich auf 																mehrere Dateien aufgeteilt

				acid_dti_compute_error							Neue Datei (keine vorherige implementierung?)


ACID_make_Bmask											Warum ersatzlos weggefallen?



*********************my list**************************************


estimate_tensornew + estimate_tensor		acid_dti_manage_eigenvalues + acid_dti_compute_eigenvalues.m
DTI_olsq_robust				acid_dti_main.m
DKI_quadprog					acid_dki_main.m
geometrical_matrix_basser			acid_dti_get_bmatrix.m
geometrical_matrix_varTE_varb			acid_dti_get_bmatrix_varTE_varb.m
callogDW_vstruct				acid_dti_get_logS.m
robust_estimate_Zwiers2010_slicew_weights	acid_dti_robust_compute_tensor_IRLS.m
robustestimate_DT_prod				acid_dti_robust_estimate_Zwiers2010.m
write_DTIdataset_prod				acid_dti_compute_DTImaps.m
callogDWincludingS0_vstruct_2D 		acid_dti_get_logS_2D.m
EC_MO_VARTAR_correction.m			acid_ecmoco_main.m
ShellwisemeanDWI.m				acid_ecmoco_create_targets.m
spm_coreg_freeze_v04.m				acid_spm_coreg_freeze_bias.m
spm_coreg_freeze_v03.m				acid_spm_coreg_freeze.m
ACID_coreg_slicew.m				acid_ecmoco_coreg_slicew.m

The four scripts below have been combined into a single one (acid_ecmoco_write.m):
EC_MO_write_function_sw_v03.m			acid_ecmoco_write.m
EC_MO_write_sw_function_v04.m			acid_ecmoco_write.m
EC_MO_write_function_v03.m			acid_ecmoco_write.m
EC_MO_write_function_v04.m			acid_ecmoco_write.m

write_image_ECMT_v3.m				acid_ecmoco_reslice.m
spm_coreg_freeze_v03.m				acid_spm_coreg_freeze_noflag.m
spm_powell_VARTARG				acid_spm_powell_VARTARG
spm_matrix_mod.m				acid_spm_matrix.m
