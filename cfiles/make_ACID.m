P = mfilename('fullpath');
[p,f,e]=spm_fileparts(P);
oldpwd = pwd;
cd(p);
mex spm_hist2_z_exp_polyval2.c
mex spm_hist2_masking.c
mex acid_c_dti_to_fa_HBM2010.c
%mex DTtoFA_2d.c
mex acid_c_dti_to_ev_ew.c


% POAS
cd ..
pPOAS = ['Preprocessing' filesep 'POAS'];
cd(pPOAS)
mex adsmse3ms.c
mex ghfse3i.c
mex ipolsp.c
mex linterpol.c
mex lkfse3i.c
mex lkfulls0.c
cd(oldpwd)