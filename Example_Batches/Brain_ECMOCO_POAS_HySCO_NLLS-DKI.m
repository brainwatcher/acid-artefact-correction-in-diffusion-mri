%-----------------------------------------------------------------------
% Job saved on 10-Jun-2021 13:58:46 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------

[r,z,i]=fileparts(which('acid_local_defaults.m'));

spm_path = r(1:end-25);

local_defaults_path = [r filesep z i];



matlabbatch{1}.spm.tools.dti.acid_config.acid_setdef.customised = {local_defaults_path};
%%
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_sources = '<UNDEFINED>';
%%
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_type = 0;
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof1_type.ecmoco2_dof1_exp = [1 1 1 1 1 1 0 1 0 1 1 0];
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof2_type.ecmoco2_dof2_exp = [0 1 0 0 1 0 0];
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_bvals_type.ecmoco2_bvals_exp = '<UNDEFINED>';
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_dummy_single_or_multi_target = false;
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_target = {''};
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_init = true;
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_excluded = {''};
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_mask = {''};
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_zsmooth = 0;
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_biasfield = {''};
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_prefix = 'rup';
matlabbatch{2}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_parallel_prog = 1;
%%
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_sources = '<UNDEFINED>';
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_type = 0;
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof1_type.ecmoco2_dof1_exp = [1 1 1 1 1 1 0 1 0 1 1 0];
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof2_type.ecmoco2_dof2_exp = [0 1 0 0 1 0 0];
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_bvals_type.ecmoco2_bvals_exp = '<UNDEFINED>';
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_dummy_single_or_multi_target = false;
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_target = {''};
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_init = true;
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_excluded = {''};
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_mask = {''};
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_zsmooth = 0;
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_biasfield = {''};
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_prefix = 'rdw';
matlabbatch{3}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_parallel_prog = 1;
matlabbatch{4}.cfg_basicio.file_dir.file_ops.cfg_file_split.name = 'u2b0up';
matlabbatch{4}.cfg_basicio.file_dir.file_ops.cfg_file_split.files(1) = cfg_dep('Estimate & write ECMOCO: Target images', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','VGfiles'));
matlabbatch{4}.cfg_basicio.file_dir.file_ops.cfg_file_split.index = {1};


%[p,f,e]=fileparts(which('TPM.nii'));

tpm_path = [spm_path 'tpm/TPM.nii'];


matlabbatch{5}.spm.spatial.preproc.channel.vols(1) = cfg_dep('File Set Split: u2b0up (1)', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{5}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{5}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{5}.spm.spatial.preproc.channel.write = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(1).tpm = {[tpm_path ',1']};
matlabbatch{5}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{5}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{5}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(2).tpm = {[tpm_path ',2']};
matlabbatch{5}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{5}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{5}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(3).tpm = {[tpm_path ',3']};
matlabbatch{5}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{5}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{5}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(4).tpm = {[tpm_path ',4']};
matlabbatch{5}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{5}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{5}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(5).tpm = {[tpm_path ',5']};
matlabbatch{5}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{5}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{5}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(6).tpm = {[tpm_path ',6']};
matlabbatch{5}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{5}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{5}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{5}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{5}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{5}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{5}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{5}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{5}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{5}.spm.spatial.preproc.warp.write = [0 0];
matlabbatch{5}.spm.spatial.preproc.warp.vox = NaN;
matlabbatch{5}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
                                              NaN NaN NaN];
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(1) = cfg_dep('Segment: c1 Images', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{1}, '.','c', '()',{':'}));
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(2) = cfg_dep('Segment: c2 Images', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{2}, '.','c', '()',{':'}));
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(3) = cfg_dep('Segment: c3 Images', substruct('.','val', '{}',{5}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{3}, '.','c', '()',{':'}));
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PDTI = {''};
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_perc = 0.76;
matlabbatch{6}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_smk = [3 3 3];
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.in_vols(1) = cfg_dep('Estimate & write ECMOCO: Resliced images', substruct('.','val', '{}',{2}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rfiles'));
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.brain_msk(1) = cfg_dep('Make Brain Mask: Brain Mask', substruct('.','val', '{}',{6}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','BMSKfiles'));
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_bvectors_type.poas_bvectors_exp = '<UNDEFINED>';
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_bvals_type.poas_bvals_exp = '<UNDEFINED>';
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.kstar = 12;
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.sigma = 11.1;
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.ncoils = 2;
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.dummy_3dor4d = 1;
matlabbatch{7}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_prefix = 'poas';
matlabbatch{8}.cfg_basicio.file_dir.file_ops.cfg_file_split.name = 'u2b0dw';
matlabbatch{8}.cfg_basicio.file_dir.file_ops.cfg_file_split.files(1) = cfg_dep('Estimate & write ECMOCO: Target images', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','VGfiles'));
matlabbatch{8}.cfg_basicio.file_dir.file_ops.cfg_file_split.index = {1};
matlabbatch{9}.spm.spatial.preproc.channel.vols(1) = cfg_dep('File Set Split: u2b0dw (1)', substruct('.','val', '{}',{8}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{9}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{9}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{9}.spm.spatial.preproc.channel.write = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(1).tpm = {[tpm_path ',1']};
matlabbatch{9}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{9}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{9}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(2).tpm = {[tpm_path ',2']};
matlabbatch{9}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{9}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{9}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(3).tpm = {[tpm_path ',3']};
matlabbatch{9}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{9}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{9}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(4).tpm = {[tpm_path ',4']};
matlabbatch{9}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{9}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{9}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(5).tpm = {[tpm_path ',5']};
matlabbatch{9}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{9}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{9}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(6).tpm = {[tpm_path ',6']};
matlabbatch{9}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{9}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{9}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{9}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{9}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{9}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{9}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{9}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{9}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{9}.spm.spatial.preproc.warp.write = [0 0];
matlabbatch{9}.spm.spatial.preproc.warp.vox = NaN;
matlabbatch{9}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
                                              NaN NaN NaN];
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(1) = cfg_dep('Segment: c1 Images', substruct('.','val', '{}',{9}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{1}, '.','c', '()',{':'}));
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(2) = cfg_dep('Segment: c2 Images', substruct('.','val', '{}',{9}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{2}, '.','c', '()',{':'}));
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(3) = cfg_dep('Segment: c3 Images', substruct('.','val', '{}',{9}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{3}, '.','c', '()',{':'}));
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PDTI = {''};
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_perc = 0.76;
matlabbatch{10}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_smk = [3 3 3];
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.in_vols(1) = cfg_dep('Estimate & write ECMOCO: Resliced images', substruct('.','val', '{}',{3}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rfiles'));
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.brain_msk(1) = cfg_dep('Make Brain Mask: Brain Mask', substruct('.','val', '{}',{6}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','BMSKfiles'));
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_bvectors_type.poas_bvectors_exp = '<UNDEFINED>';
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_bvals_type.poas_bvals_exp = '<UNDEFINED>';
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.kstar = 12;
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.sigma = 11.1;
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.ncoils = 2;
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.dummy_3dor4d = 1;
matlabbatch{11}.spm.tools.dti.prepro_choice.poas_choice.poas.poas_prefix = 'poas';
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.source_up(1) = cfg_dep('File Set Split: u2b0up (1)', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.source_dw(1) = cfg_dep('File Set Split: u2b0dw (1)', substruct('.','val', '{}',{8}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.others_up(1) = cfg_dep('POAS: POAS smoothed data', substruct('.','val', '{}',{7}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rsource'));
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.others_dw(1) = cfg_dep('POAS: POAS smoothed data', substruct('.','val', '{}',{11}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rsource'));
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.perm_dim = 2;
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_fast = 1;
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_ecc = 0;
matlabbatch{12}.spm.tools.dti.prepro_choice.hysco_choice.hysco2.dummy_3dor4d = 1;
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.source_up(1) = cfg_dep('File Set Split: u2b0up (1)', substruct('.','val', '{}',{4}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.others_u2_up(1) = cfg_dep('HySCO 2.0 (preferred): Unwarped Blip up images', substruct('.','val', '{}',{12}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rothers_up'));
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.others_u2_dw(1) = cfg_dep('HySCO 2.0 (preferred): Unwarped Blip down images', substruct('.','val', '{}',{12}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rothers_dw'));
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.weights_up = {''};
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.weights_dw = {''};
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.bfield(1) = cfg_dep('HySCO 2.0 (preferred): Estimated fieldmap', substruct('.','val', '{}',{12}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','fieldmap'));
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.perm_dim = 2;
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.dummy_3dor4d = 1;
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.multihysco_wcomb_write = 3;
matlabbatch{13}.spm.tools.dti.prepro_choice.hysco_choice.hysco_wcomb_multi.dummy_ACID = 1;
matlabbatch{14}.cfg_basicio.file_dir.file_ops.cfg_file_split.name = 'wab0';
matlabbatch{14}.cfg_basicio.file_dir.file_ops.cfg_file_split.files(1) = cfg_dep('Write combined images using multiple HySCO fieldmaps: Weighted mean of blip-up and down images', substruct('.','val', '{}',{13}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rothers_wa'));
matlabbatch{14}.cfg_basicio.file_dir.file_ops.cfg_file_split.index = {1};
matlabbatch{15}.spm.spatial.preproc.channel.vols(1) = cfg_dep('File Set Split: wab0 (1)', substruct('.','val', '{}',{14}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('{}',{1}));
matlabbatch{15}.spm.spatial.preproc.channel.biasreg = 0.001;
matlabbatch{15}.spm.spatial.preproc.channel.biasfwhm = 60;
matlabbatch{15}.spm.spatial.preproc.channel.write = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(1).tpm = {[tpm_path ',1']};
matlabbatch{15}.spm.spatial.preproc.tissue(1).ngaus = 1;
matlabbatch{15}.spm.spatial.preproc.tissue(1).native = [1 0];
matlabbatch{15}.spm.spatial.preproc.tissue(1).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(2).tpm = {[tpm_path ',2']};
matlabbatch{15}.spm.spatial.preproc.tissue(2).ngaus = 1;
matlabbatch{15}.spm.spatial.preproc.tissue(2).native = [1 0];
matlabbatch{15}.spm.spatial.preproc.tissue(2).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(3).tpm = {[tpm_path ',3']};
matlabbatch{15}.spm.spatial.preproc.tissue(3).ngaus = 2;
matlabbatch{15}.spm.spatial.preproc.tissue(3).native = [1 0];
matlabbatch{15}.spm.spatial.preproc.tissue(3).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(4).tpm = {[tpm_path ',4']};
matlabbatch{15}.spm.spatial.preproc.tissue(4).ngaus = 3;
matlabbatch{15}.spm.spatial.preproc.tissue(4).native = [1 0];
matlabbatch{15}.spm.spatial.preproc.tissue(4).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(5).tpm = {[tpm_path ',5']};
matlabbatch{15}.spm.spatial.preproc.tissue(5).ngaus = 4;
matlabbatch{15}.spm.spatial.preproc.tissue(5).native = [1 0];
matlabbatch{15}.spm.spatial.preproc.tissue(5).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(6).tpm = {[tpm_path ',6']};
matlabbatch{15}.spm.spatial.preproc.tissue(6).ngaus = 2;
matlabbatch{15}.spm.spatial.preproc.tissue(6).native = [0 0];
matlabbatch{15}.spm.spatial.preproc.tissue(6).warped = [0 0];
matlabbatch{15}.spm.spatial.preproc.warp.mrf = 1;
matlabbatch{15}.spm.spatial.preproc.warp.cleanup = 1;
matlabbatch{15}.spm.spatial.preproc.warp.reg = [0 0.001 0.5 0.05 0.2];
matlabbatch{15}.spm.spatial.preproc.warp.affreg = 'mni';
matlabbatch{15}.spm.spatial.preproc.warp.fwhm = 0;
matlabbatch{15}.spm.spatial.preproc.warp.samp = 3;
matlabbatch{15}.spm.spatial.preproc.warp.write = [0 0];
matlabbatch{15}.spm.spatial.preproc.warp.vox = NaN;
matlabbatch{15}.spm.spatial.preproc.warp.bb = [NaN NaN NaN
                                               NaN NaN NaN];
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(1) = cfg_dep('Segment: c1 Images', substruct('.','val', '{}',{15}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{1}, '.','c', '()',{':'}));
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(2) = cfg_dep('Segment: c2 Images', substruct('.','val', '{}',{15}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{2}, '.','c', '()',{':'}));
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PSEG(3) = cfg_dep('Segment: c3 Images', substruct('.','val', '{}',{15}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','tiss', '()',{3}, '.','c', '()',{':'}));
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_PDTI = {''};
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_perc = 0.76;
matlabbatch{16}.spm.tools.dti.misc_choice.make_brainMSK.make_brainMSK_smk = [3 3 3];
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_vols_GN(1) = cfg_dep('Write combined images using multiple HySCO fieldmaps: Weighted mean of blip-up and down images', substruct('.','val', '{}',{13}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','rothers_wa'));
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.diff_dirs_GN(1) = cfg_dep('POAS: b-vectors', substruct('.','val', '{}',{7}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','bvec'));
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.b_vals_GN(1) = cfg_dep('POAS: b-values', substruct('.','val', '{}',{7}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','bval'));
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.dummy_algorithm_GN = 2;
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.dummy_RBC_GN = 1;
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_sigma_RBC = 11.1;
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_msk_GN(1) = cfg_dep('Make Brain Mask: Brain Mask', substruct('.','val', '{}',{16}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','BMSKfiles'));
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.RMatrix_GN = [1 0 0
                                                                          0 1 0
                                                                          0 0 1];
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_L_RBC = 2;
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_noise_map_GN = {''};
matlabbatch{17}.spm.tools.dti.fit_choice.dki_choice.diff_GN.in_npool = 4;
