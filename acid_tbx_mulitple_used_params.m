function [dummy_Tfreiburg, brain_msk, b_vals, diff_dirs, in_vols, RMatrix] = acid_tbx_mulitple_used_params


% ---------------------------------------------------------------------
% dummy_robust
% ---------------------------------------------------------------------
dummy_Tfreiburg   = cfg_menu;
dummy_Tfreiburg.tag     = 'dummy_Tfreiburg';
dummy_Tfreiburg.name    = 'Write Freiburg Tractography format';
dummy_Tfreiburg.help    = {'Write Freiburg Tractography format. The path of the Freiburg Tractography tools have to be included - otherwise it will not work.'};
dummy_Tfreiburg.labels = {
               'NO'
               'YES'
}';
dummy_Tfreiburg.values = {0 1};
dummy_Tfreiburg.val    = {0};

% ---------------------------------------------------------------------
% Binary Map defining Region of Interest
% ---------------------------------------------------------------------
brain_msk         = cfg_files;
brain_msk.tag     = 'brain_msk';
brain_msk.name    = 'Region of interest image (or done for none)';
brain_msk.val     = {{''}};
brain_msk.help    = {'This entry is optional. Note that if your had acquired HARDI data (and/or high-resolution data), it is recommened to provide a mask unless you believe that you have got enough memory!'};
brain_msk.filter = 'image';
brain_msk.ufilter = '.*';
brain_msk.num     = [0 Inf];

% ---------------------------------------------------------------------
% b-value directions
% ---------------------------------------------------------------------
b_vals         = cfg_entry;
b_vals.tag     = 'b_vals';
b_vals.name    = 'b-values (bval)';
b_vals.help    = {'Provide an 1 x N  - array with b-values, b-values should appear in the same order as the low- and high-diffusion weighted images were entered. b-values is expected in units of s/mm^2.' 
                  'Note that the provided entry is only for illustration.'};
b_vals.strtype = 'e';
b_vals.num     = [Inf Inf];
b_vals.val     = {[5 1000 1000 2000]};

% ---------------------------------------------------------------------
% diffusion directions
% ---------------------------------------------------------------------
diff_dirs         = cfg_entry;
diff_dirs.tag     = 'diff_dirs';
diff_dirs.name    = 'Diffusion directions (bvec)';
diff_dirs.help    = {'Provide a 3 x N  - matrix with b-vectors, b-vectors should appear in the same order as the low- and high-diffusion weighted images were entered. The b-vectors are dimensionless.' 
                     'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'};
diff_dirs.strtype = 'e';
diff_dirs.num     = [3 Inf];
diff_dirs.val     = {[1 0 0; 0 1 0; 0 0 1; 0 1/sqrt(2) 1/sqrt(2)]'};

% ---------------------------------------------------------------------
% in_vols
% ---------------------------------------------------------------------
in_vols         = cfg_files;
in_vols.tag     = 'in_vols';
in_vols.name    = 'Diffusion weighted images';
in_vols.help    = {'Select high- and low-b-value images.'};
in_vols.filter = 'image';
in_vols.ufilter = '.*';
in_vols.num     = [0 Inf];

% ---------------------------------------------------------------------
% Reorientation Matrix for b-vectors
% ---------------------------------------------------------------------
RMatrix         = cfg_entry;
RMatrix.tag     = 'RMatrix';
RMatrix.name    = 'Reorientation Matrix';
RMatrix.help    = {
                      'If the vendor uses another coordinate system than the coordinate system, in which your b-vectors were defined, you need to reorient them.'
                      'Provide a 3 x 3  - matrix to reorient b-vectors.'
};
RMatrix.strtype = 'e';
RMatrix.num     = [3 3];
RMatrix.val     = {[1 0 0; 0 1 0; 0 0 1]};