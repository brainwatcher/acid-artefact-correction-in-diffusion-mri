function make_brainMSK = acid_tbx_cfg_make_brainmask

% GM and WM probability maps
make_brainMSK_PSEG         = cfg_files;
make_brainMSK_PSEG.tag     = 'make_brainMSK_PSEG';
make_brainMSK_PSEG.name    = 'GM and WM probability maps';
make_brainMSK_PSEG.help    = {'...'};
make_brainMSK_PSEG.filter  = 'image';
make_brainMSK_PSEG.ufilter = '.*';
make_brainMSK_PSEG.num     = [0 Inf];

% DTI maps to be masked
make_brainMSK_PDTI         = cfg_files;
make_brainMSK_PDTI.tag     = 'make_brainMSK_PDTI';
make_brainMSK_PDTI.name    = 'DTI maps to be masked';
make_brainMSK_PDTI.help    = {'...'};
make_brainMSK_PDTI.filter  = 'image';
make_brainMSK_PDTI.ufilter = '.*';
make_brainMSK_PDTI.num     = [0 Inf];

% Inerpolation order
make_brainMSK_perc         = cfg_entry;
make_brainMSK_perc.tag     = 'make_brainMSK_perc';
make_brainMSK_perc.name    = 'Brain coverage';
make_brainMSK_perc.help    = {'Brain coverage (0: brain covers full field-of-view).'};
make_brainMSK_perc.strtype = 'e';
make_brainMSK_perc.num     = [1 1];
make_brainMSK_perc.val     = {0.8};

% smoothing kernel used in both, DTI fit and brain mask
make_brainMSK_smk         = cfg_entry;
make_brainMSK_smk.tag     = 'make_brainMSK_smk';
make_brainMSK_smk.name    = 'Smoothing kernel';
make_brainMSK_smk.help    = {'This option reduces holes in the brain mask by smoothing the mask or the tissue probability maps. If it is set to zero no smoothing is used.'};
make_brainMSK_smk.strtype = 'e';
make_brainMSK_smk.num     = [1 3];
make_brainMSK_smk.val     = {[3 3 3]};

% exbranch
make_brainMSK      = cfg_exbranch;
make_brainMSK.tag  = 'make_brainMSK';
make_brainMSK.name = 'Make Brain Mask';
make_brainMSK.val  = {make_brainMSK_PSEG make_brainMSK_PDTI make_brainMSK_perc make_brainMSK_smk};
make_brainMSK.help = {
                    'This function constructs a mask from input images and applies the mask on other images. Recommendation about image(s) that are used for mask creation: apply ???SPM New Segment??? on low-b-value image and use take c1-c3 for mask creation.'
}';
make_brainMSK.prog = @local_make_brainMSK;
make_brainMSK.vout = @vout_make_brainMSK;

function out = local_make_brainMSK(job)
    dummy_options.smk   = job.make_brainMSK_smk;
    dummy_options.perc  = job.make_brainMSK_perc;
    acid_make_brainmask(char(job.make_brainMSK_PSEG), char(job.make_brainMSK_PDTI),[],dummy_options);
    out.BMSKfiles = acid_spm_file(char(job.make_brainMSK_PSEG(1)),'prefix','MSK_');
    out.MDTIfiles = acid_spm_file(char(job.make_brainMSK_PDTI(:)),'prefix','M_');
    out.MDTIfiles = acid_select(out.MDTIfiles(:));
    out.MDTIfiles = out.MDTIfiles(:);
end

function dep = vout_make_brainMSK(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Brain Mask';
    dep(1).src_output = substruct('.','BMSKfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'Brain Masked Images';
    dep(2).src_output = substruct('.','MDTIfiles');
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end

end