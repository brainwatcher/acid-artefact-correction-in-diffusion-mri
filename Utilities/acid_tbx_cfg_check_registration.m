function checkreg = acid_tbx_cfg_check_registration

% reference image
checkreg_ref         = cfg_files;
checkreg_ref.tag     = 'checkreg_ref';
checkreg_ref.name    = 'Reference image';
checkreg_ref.help    = {'Select reference image.'};
checkreg_ref.filter  = 'image';
checkreg_ref.ufilter = '.*';
checkreg_ref.num     = [0 1];

% 1. dataset
checkreg_set1         = cfg_files;
checkreg_set1.tag     = 'checkreg_set1';
checkreg_set1.name    = '1. dataset';
checkreg_set1.help    = {'Select the first dataset.'};
checkreg_set1.filter  = 'image';
checkreg_set1.ufilter = '.*';
checkreg_set1.num     = [1 Inf];

% 2. dataset
checkreg_set2         = cfg_files;
checkreg_set2.tag     = 'checkreg_set2';
checkreg_set2.name    = '2. dataset (optional)';
checkreg_set2.help    = {'Select the second dataset (or click Done for none).'};
checkreg_set2.filter  = 'image';
checkreg_set2.ufilter = '.*';
checkreg_set2.num     = [0 Inf];
checkreg_set2.val     = {{''}};

% 3. dataset
checkreg_set3         = cfg_files;
checkreg_set3.tag     = 'checkreg_set3';
checkreg_set3.name    = '3. dataset (optional)';
checkreg_set3.help    = {'Select the third dataset (or click Done for none)'};
checkreg_set3.filter  = 'image';
checkreg_set3.ufilter = '.*';
checkreg_set3.num     = [0 Inf];
checkreg_set3.val     = {{''}};

% dummy whether all slices
checkreg_dummy_allslices  = cfg_menu;
checkreg_dummy_allslices.tag  = 'checkreg_dummy_allslices';
checkreg_dummy_allslices.name = 'Display all slices?';
checkreg_dummy_allslices.help = {''
                    'Here you can specify whether you want to display all slices or only a single slice.'
                                ''};
checkreg_dummy_allslices.labels = {'All slices','Single slice'};
checkreg_dummy_allslices.values = {1 0};
checkreg_dummy_allslices.val    = {1};

% dummy which order to display
checkreg_dummy_order  = cfg_menu;
checkreg_dummy_order.tag  = 'checkreg_dummy_order';
checkreg_dummy_order.name = 'If all slices, in which order?';
checkreg_dummy_order.help = {''
'When displaying all slices, in which order?.'
'0: inner loop across volumes'
'1: inner loop across slices'
''};
checkreg_dummy_order.labels = {'First volumes','First slices'};
checkreg_dummy_order.values = {0 1};
checkreg_dummy_order.val    = {0};

% slice position
checkreg_slice         = cfg_entry;
checkreg_slice.tag     = 'checkreg_slice';
checkreg_slice.name    = 'Slice position';
checkreg_slice.help    = {'Provide an integer number that defines the slice at which the movie is shown. Note that it must be within the dimensions of the image.'};
checkreg_slice.strtype = 'e';
checkreg_slice.num     = [0 Inf];
checkreg_slice.val     = {[]};

% dummy whether save movie or not
checkreg_dummy_movie      = cfg_menu;
checkreg_dummy_movie.tag  = 'checkreg_dummy_movie';
checkreg_dummy_movie.name = 'Save Movie?';
checkreg_dummy_movie.help = {''
'Here you can choose to save the movie that is generated.'
''};
checkreg_dummy_movie.labels = {'Yes','No'};
checkreg_dummy_movie.values = {1 0};
checkreg_dummy_movie.val    = {1};

% pause
checkreg_pause         = cfg_entry;
checkreg_pause.tag     = 'checkreg_pause';
checkreg_pause.name    = 'Time interval';
checkreg_pause.help    = {'Video is paused for this period after each image'};
checkreg_pause.strtype = 'e';
checkreg_pause.num     = [1 1];
checkreg_pause.val     = {0.2};

% interpolation kernel
checkreg_int         = cfg_entry;
checkreg_int.tag     = 'checkreg_int';
checkreg_int.name    = 'Interpolation kernel';
checkreg_int.help    = {'This is the interpolation kernel (negative is sinc interpolation, 1 is trilinear).'};
checkreg_int.strtype = 'e';
checkreg_int.num     = [1 1];
checkreg_int.val     = {1};

% % Interval reference image
% intv_ref         = cfg_entry;
% intv_ref.tag     = 'intv_ref';
% intv_ref.name    = 'Interval Reference image';
% intv_ref.help    = {'Provide the limits for the reference image (default is for b=0 image: [0 1000]).'};
% intv_ref.strtype = 'e';
% intv_ref.num     = [1 2];
% intv_ref.val     = {[1 1000]};
% 
% % Interval source 1 image
% intv_src1         = cfg_entry;
% intv_src1.tag     = 'intv_src1';
% intv_src1.name    = 'Interval Soruce 1 image';
% intv_src1.help    = {'Provide the limits for the source 1 image (default is for b=1000 image: [0 300]).'};
% intv_src1.strtype = 'e';
% intv_src1.num     = [1 2];
% intv_src1.val     = {[1 300]};

checkreg         = cfg_exbranch;
checkreg.tag     = 'checkreg';
checkreg.name    = 'Check registration';
checkreg.val     = {checkreg_ref, checkreg_set1, checkreg_set2, checkreg_set3, checkreg_dummy_allslices, checkreg_dummy_order, checkreg_slice, ...
    checkreg_dummy_movie, checkreg_pause, checkreg_int};
checkreg.help    = {
                    ''
};
checkreg.prog = @local_check_registration;
checkreg.vout = @vout_check_registration;

end

function out = local_check_registration(job)
    acid_check_registration(char(job.checkreg_ref), char(job.checkreg_set1), char(job.checkreg_set2), char(job.checkreg_set3),...
        job.checkreg_dummy_allslices, job.checkreg_dummy_order, job.checkreg_slice, job.checkreg_dummy_movie, job.checkreg_pause, job.checkreg_int);
    
    out.invol1 = job.checkreg_set1(:);
    out.invol2 = job.checkreg_set2(:);
    out.invol3 = job.checkreg_set3(:);
end

function dep = vout_check_registration(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = '1st input dataset';
    dep(1).src_output = substruct('.','invol1'); 
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = '2nd input dataset';
    dep(2).src_output = substruct('.','invol2'); 
    dep(2).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = '3rd input dataset';
    dep(3).src_output = substruct('.','invol3'); 
    dep(3).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end