function wfname = acid_crop_images(P,matrix,midz,midxy,prefix)

% ---------------------------------------------------------
% The function cuts the input image to a reduced field of view (rFOV). 
% It will draw a rFOV around the selected point as origin. If edge-length 
% is one- or two-dimensional, it will be cutted to a qubic rFOV. 
% To account for the shift of the origin, the bounding box is calculated
% using a snip of code named world_bb
%
% Input:
%   P           - input image
%   matrix      - matrix size of the reduced-FOV
%   midz        - middle slice
%   midxy       - middle points in-plane
%   prefix      - prefix of the output
%
% Based on John Ashburner's reorient.m
% http://www.sph.umich.edu/~nichols/JohnsGems.html#Gem7
% http://www.sph.umich.edu/~nichols/JohnsGems5.html#Gem2
% and Ged Ridgway interpolation tool
% 
% Adapted by S.Mohammadi 08/10/2014
% Modified by G David 07/10/2019
% ---------------------------------------------------------

%% Check arguments
if ~exist('interp','var')
    interp = -4;
end

% getting headers
V  = spm_vol(P);            % headers of all images
I  = spm_read_vols(V);      % all images

% flip if necessary
Sign = sign([V(1).mat(1,1),V(1).mat(2,2),V(1).mat(3,3)]);
if Sign(1) == -1, I = flip(I,1); end
if Sign(2) == -1, I = flip(I,2); end
if Sign(3) == -1, I = flip(I,3); end

I1 = I(:,:,:,1);   % first image

% check feasibility of cropping
if (midz < 1 || midz > size(I1,3))
    error('Incorrect middle slice: the entered slice does not lie in the image.');
end
if (matrix(1)<3 && matrix(1)~=-1) || (matrix(2)<3 && matrix(2)~=-1)
    error('Incorrect matrix dimensions: the matrix size in the x and y dimensions has to be at least 3.');
elseif (matrix(3)<1 && matrix(3)~=-1)
    error('Incorrect matrix dimensions: the matrix size in the z dimension has to be at least 1.');    
end

%% Determine the FOV of the cropped images

% extracting the center in the given slice
if numel(midxy)==0
    % generating figure
    Fig_1 = figure('Name','Cropping'); colormap gray;

    % extracting the given slice of the first image
    axis square, title('Select center position!');
    imshow(rot90(squeeze(I1(:,:,midz))),[0 max(I1(:))]);
    [Cent_x, Cent_y] = ginput(1);
    midxy(1) = round(Cent_x);
    midxy(2) = round(size(I1,2)-Cent_y+1); % flip because indexing starts from the bottom of the image, while ginput starts from the top
else
    Cent_x = round(midxy(1));
    Cent_y = round(midxy(2));
    if (Cent_x<1 || Cent_x>size(I1,1))
        error('Incorrect midpoint specification: the midpoint in x does not lie in the image.');
    elseif (Cent_y<1 || Cent_y>size(I1,2))
        error('Incorrect midpoint specification: the midpoint in y does not lie in the image.');
    end
    Fig_1 = figure('Name','Cropping'); colormap gray;
end

% x-FOV
if matrix(1) == -1
    xmin = 1; xmax = size(I1,1);
else
    xmin = midxy(1) - ceil(matrix(1)/2) + 1; 
    if xmin < 1, xmin = 1; end
    if xmin > size(I1,1), xmin = size(I1,1); end
    xmax = midxy(1) + floor(matrix(1)/2);
    if xmax < 1, xmax = 1; end
    if xmax > size(I1,1), xmax = size(I1,1); end
end

% y-FOV
if matrix(2) == -1
    ymin = 1; ymax = size(I1,2);
else
    ymin = midxy(2) - ceil(matrix(2)/2) + 1;
    if ymin < 1, ymin = 1; end
    if ymin > size(I1,2), ymin = size(I1,2); end
    ymax = midxy(2) + floor(matrix(2)/2);
    if ymax < 1, ymax = 1; end
    if ymax > size(I1,2), ymax = size(I1,2); end
end

% z-FOV
if matrix(3) == -1
    zmin = 1; zmax = size(I1,3);
else
    zmin = midz - ceil(matrix(3)/2) + 1;
    if zmin < 1, zmin = 1; end
    if zmin > size(I1,3), zmin = size(I1,3); end
    zmax = midz + floor(matrix(3)/2);
    if zmax < 1, zmax = 1; end
    if zmax > size(I1,3), zmax = size(I1,3); end
end

% save matrix size and midpoint
[p,f,~] = fileparts(P(1,:));
save([p filesep 'cropping_params_' f '.mat'],'matrix','midxy','midz')

%% Write out cropped images

if Sign(1) == -1, a = size(I1,1)-xmax+1; else, a = xmin; end
if Sign(2) == -1, b = size(I1,2)-ymax+1; else, b = ymin; end
if Sign(3) == -1, c = size(I1,3)-zmax+1; else, c = zmin; end

% save resliced image
for i = 1:size(P,1)
    
    [p,f,e] = fileparts(V(i).fname);
    
    if isequal(V(i).n,[1,1])
        VO = V(i);
        VO.fname    = fullfile(p,[prefix f e]);
        VO.dim(1:3) = [numel(xmin:xmax) numel(ymin:ymax) numel(zmin:zmax)];
        VO.mat      = V(i).mat * spm_matrix([-1 -1 -1]*diag([1 1 1]))*spm_matrix([a,b,c]);       
        VO.n = [1 1];
        if V(i).mat(1,1) < 0
            spm_write_vol(VO,flip(I(xmin:xmax,ymin:ymax,zmin:zmax,i)));
        else
            spm_write_vol(VO,I(xmin:xmax,ymin:ymax,zmin:zmax,i)); 
        end
        wfname(i) = {VO.fname};
    else
        VO = V;
        VO(i).fname    = fullfile(p,[prefix f e]);
        VO(i).dim(1:3) = [numel(xmin:xmax) numel(ymin:ymax) numel(zmin:zmax)];
        VO(i).mat      = V(i).mat * spm_matrix([-1 -1 -1]*diag([1 1 1]))*spm_matrix([a,b,c]);
        VO(i).n = [i 1];
        if V(i).mat(1,1) < 0
            spm_write_vol(VO(i),flip(I(xmin:xmax,ymin:ymax,zmin:zmax,i)));
        else
            spm_write_vol(VO(i),I(xmin:xmax,ymin:ymax,zmin:zmax,i));
        end
        wfname(i) = {VO(i).fname};
    end
end

close(Fig_1)
disp('Done!');

end