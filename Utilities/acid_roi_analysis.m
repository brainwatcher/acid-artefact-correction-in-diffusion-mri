function avgROI = acid_roi_analysis(maps,maskGLO,maskSUB,maskREL,dummy_slicewise)

% ===========================================================================
% The following script extracts average values within masks
%
% Input:
%   - masks: subject-specific masks
%               -> requires a character array with filenames of the masks;
%                  if not specified, file selector pops up
%
%   - maskGLO: global mask
%               -> requires the filename of the global mask
%                  if not specified, file selector pops up
%
%   - maskSUB: subject specific masks
%               -> requires a character array with filenames of the masks;
%                  if not specified, file selector pops up
%
%   - maskREL: subject specific reliability masks
%               -> requires a character array with filenames of the reliability masks;
%                  if not specified, file selector pops up
%
%   - dummy_slicewise: specifies whether the script extract values from the
%                   whole volume or in each slice separately
%                   0: volume-wise extraction
%                   1: slice-wise extraction
%
% Output:
%   - avgROI: average value within the intersection of masks specified above
%
% ===========================================================================

% check and select input parameters
if ~exist('maps','var')
    maps = char(cfg_getfile([1 Inf],'any','Select DTI maps','','','.*'));
end

if ~exist('maskGLO','var')
    maskGLO = char(cfg_getfile([1 Inf],'any','Select ROIs','','','.*'));
end
if isempty(maskGLO)
    dummy_maskGLO = 0;
else
    dummy_maskGLO = 1;
end

if ~exist('maskSUB','var')
    maskSUB = char(cfg_getfile([0 Inf],'any','Select subject-specific masks','','','.*'));
end
if isempty(maskSUB)
    dummy_maskSUB = 0;
else
    dummy_maskSUB = 1;
    if size(maskSUB,1)~=size(maps,1)
        error('The number of specified subject-specific ROIs has to match the number of DTI maps.');
    end
end

if ~exist('maskREL','var')
    maskREL = char(cfg_getfile([0 Inf],'any','Select reliability masks','','','.*'));
end
if isempty(maskREL)
    dummy_maskREL = 0;
else
    dummy_maskREL = 1;
    if size(maskREL,1)~=size(maps,1)
        error('The number of specified reliability masks has to match the number of DTI maps.');
    end
end

if ~exist('dummy_slicewise','var')
    dummy_slicewise = spm_input('Slice-wise average? 0 (no), 1 (yes)',1);
end

% initialization
if ~dummy_slicewise
    if ~dummy_maskGLO
        avgROI = nan(size(maps,1),1);
    else
        avgROI = nan(size(maps,1),size(maskGLO,1));
    end
else
    sl = zeros(size(maps,1),1);
    for  i = 1:size(maps,1)
        hdr_map = spm_vol(maps(i,:));
        sl(i) = hdr_map.dim(3);
    end
    
    if ~dummy_maskGLO
        avgROI = nan(size(maps,1),max(sl));
    else
        avgROI = nan(size(maps,1),size(maskGLO,1),max(sl));
    end
end

for k = 1:size(maps,1)
    
    disp(['Processing: ' maps(k,:)]);
    % load in maps
    hdr_map = spm_vol(maps(k,:));
    map = acid_read_vols(hdr_map,hdr_map,1);
    dm = hdr_map.dim;
           
    % masking with subject-specific ROI
    if dummy_maskSUB
        hdr_maskSUB = spm_vol(maskSUB(k,:));
        MaskSUB = acid_read_vols(hdr_maskSUB,hdr_map,0);
        MaskSUB(MaskSUB>10^-3) = 1;
        map = map.*MaskSUB;
    end
    
    % masking with subject-specific reliability mask
    if dummy_maskREL
        h_maskREL = spm_vol(maskREL(k,:));
        MaskREL = acid_read_vols(h_maskREL,hdr_map,0);
        MaskREL(MaskREL>10^-3) = 1;
        map = map.*MaskREL;
    end
    
    % global ROIs
    if dummy_maskGLO
        for m = 1:size(maskGLO,1)
            hdr_maskGLO = spm_vol(maskGLO(m,:));
            ROI = acid_read_vols(hdr_maskGLO,hdr_map,0);
            ROI(ROI>10^-3)=1;
            map2 = map.*ROI;
            if ~dummy_slicewise
                temp = map2; temp = temp(:); temp(temp==0) = []; temp(isnan(temp)) = [];
                avgROI(k,m) = mean(temp);
            else
                for i = 1:dm(3)
                    temp = squeeze(map2(:,:,i));  temp = temp(:); temp(temp==0) = []; temp(isnan(temp)) = [];
                    avgROI(k,m,i) = mean(temp);
                end
            end
        end
    else
        if ~dummy_slicewise
            temp = map; temp = temp(:); temp(temp==0) = []; temp(isnan(temp)) = [];
            avgROI(k,1) = mean(temp);
        else
            for i = 1:dm(3)
                temp = squeeze(map(:,:,i));  temp = temp(:); temp(temp==0) = []; temp(isnan(temp)) = [];
                avgROI(k,i) = mean(temp);
            end
        end
    end
       
end

% save extracted metrics
save('extracted_metrics.mat','avgROI');

end