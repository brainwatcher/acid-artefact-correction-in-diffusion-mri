function roianalysis = acid_tbx_cfg_roi_analysis

% source image
roianalysis_maps         = cfg_files;
roianalysis_maps.tag     = 'roianalysis_maps';
roianalysis_maps.name    = 'DTI maps';
roianalysis_maps.help    = {'Select the maps for ROI analysis'};
roianalysis_maps.filter  = 'image';
roianalysis_maps.ufilter = '.*';
roianalysis_maps.num     = [1 Inf];

% ROIs
roianalysis_roi         = cfg_files;
roianalysis_roi.tag     = 'roianalysis_roi';
roianalysis_roi.name    = 'Global ROIs';
roianalysis_roi.help    = {'Select binary masks in which metrics will be extracted.'};
roianalysis_roi.filter  = 'image';
roianalysis_roi.ufilter = '.*';
roianalysis_roi.num     = [0 Inf];

% subject-specific masks
roianalysis_masksSUB         = cfg_files;
roianalysis_masksSUB.tag     = 'roianalysis_masksSUB';
roianalysis_masksSUB.name    = 'Subject-specific ROIs';
roianalysis_masksSUB.help    = {'Select subject-specific binary masks in which metrics will be extracted.'};
roianalysis_masksSUB.filter  = 'image';
roianalysis_masksSUB.ufilter = '.*';
roianalysis_masksSUB.num     = [0 Inf];

% reliability masks
roianalysis_masksREL         = cfg_files;
roianalysis_masksREL.tag     = 'roianalysis_masksREL';
roianalysis_masksREL.name    = 'Reliability masks';
roianalysis_masksREL.help    = {'Select reliability masks.'};
roianalysis_masksREL.filter  = 'image';
roianalysis_masksREL.ufilter = '.*';
roianalysis_masksREL.num     = [0 Inf];

% dummy for slice-wise
roianalysis_dummy_slicewise      = cfg_menu;
roianalysis_dummy_slicewise.tag  = 'roianalysis_dummy_slicewise';
roianalysis_dummy_slicewise.name = 'Slice-wise extraction';
roianalysis_dummy_slicewise.help = {'Choose whether the metric extraction from the ROI'};
roianalysis_dummy_slicewise.labels = {'Yes','No'};
roianalysis_dummy_slicewise.values = {1 0};
roianalysis_dummy_slicewise.val    = {0};

% exbranch:
roianalysis = cfg_exbranch;
roianalysis.tag    = 'roianalysis';
roianalysis.name   = 'ROI analysis';
roianalysis.help   = {'Extraction of average metrics from specified ROIs.'};
roianalysis.val  = {roianalysis_maps, roianalysis_roi, roianalysis_masksSUB, roianalysis_masksREL, roianalysis_dummy_slicewise};
roianalysis.prog = @local_roi_analysis;

end

function out = local_roi_analysis(job)
    acid_roi_analysis(char(job.roianalysis_maps), char(job.roianalysis_roi), char(job.roianalysis_masksSUB), char(job.roianalysis_masksREL),...
        job.roianalysis_dummy_slicewise);
end