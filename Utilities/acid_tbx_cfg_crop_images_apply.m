function crop_apply = acid_tbx_cfg_crop_images_apply

% images to crop
crop_apply_images         = cfg_files;
crop_apply_images.tag     = 'crop_apply_images';
crop_apply_images.name    = 'Dataset to be cropped';
crop_apply_images.help    = {'Select the dMRI dataset you want to crop. You can load in either a set of 3d nifti or a single 4d nifti file.'};
crop_apply_images.filter  = 'image';
crop_apply_images.ufilter = '.*';
crop_apply_images.num     = [0 Inf];

% cropping parameters
crop_apply_Mfile         = cfg_files;
crop_apply_Mfile.tag     = 'crop_apply_Mfile';
crop_apply_Mfile.name    = 'Cropping parameters';
crop_apply_Mfile.help    = {'Select the matfile containing the cropping parameters'};
crop_apply_Mfile.filter  = 'mat';
crop_apply_Mfile.ufilter = '.*';
crop_apply_Mfile.num     = [0 Inf];

% prefix for output
crop_apply_prefix         = cfg_entry;
crop_apply_prefix.tag     = 'crop_apply_prefix';
crop_apply_prefix.name    = 'Prefix for output';
crop_apply_prefix.help    = {'The threshold value in model-fit error above which voxels are rejected.'};
crop_apply_prefix.strtype = 's';
crop_apply_prefix.val     = {'c'};

% exbranch
crop_apply       = cfg_exbranch;
crop_apply.tag   = 'crop_apply';
crop_apply.name  = 'Apply existing cropping';
crop_apply.help  = {'Apply existing cropping'};
crop_apply.val   = {crop_apply_images, crop_apply_Mfile, crop_apply_prefix};
crop_apply.prog  = @local_crop_images;
crop_apply.vout  = @vout_crop_images;

end

function out = local_crop_images(job)
    params = load(char(job.crop_apply_Mfile));
    
    acid_crop_images(char(job.crop_apply_images), params.matrix, params.midz, params.midxy, char(job.crop_apply_prefix));
    
    out.cfiles = acid_spm_file(char(job.crop_apply_images(:)),'prefix',char(job.crop_apply_prefix),'format','.nii');
    out.cfiles = acid_select(out.cfiles(:));
    out.cfiles = out.cfiles(:);
    out.matrix = params.matrix;
    out.midz   = params.midz;
    out.midxy  = params.midxy;
end

function dep = vout_crop_images(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Cropped images';
    dep(1).src_output = substruct('.','cfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'New image matrix';
    dep(2).src_output = substruct('.','matrix');
    dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'Middle slice';
    dep(3).src_output = substruct('.','midz');
    dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});  
    dep(4)            = cfg_dep;
    dep(4).sname      = 'Midpoint in the plain';
    dep(4).src_output = substruct('.','midxy');
    dep(4).tgt_spec   = cfg_findspec({{'strtype','e'}});
end