function split4d = acid_tbx_cfg_split4d

% input 
convert_split_in_vol         = cfg_files;
convert_split_in_vol.tag     = 'convert_split_in_vol';
convert_split_in_vol.name    = 'images';
convert_split_in_vol.help    = {'Select dataset that should be splitted into 3d niftis.'};
convert_split_in_vol.filter  = 'nifti';
convert_split_in_vol.ufilter = '.*';
convert_split_in_vol.num     = [0 Inf];

% interpolation
convert_split_interpol         = cfg_entry;
convert_split_interpol.tag     = 'convert_split_interpol';
convert_split_interpol.name    = 'resampling method';
convert_split_interpol.help    = {'Enter the desired resampling method.'
                      'This function uses spm_sample_vol.m, which allows to choose between the following resampling schemes:'
                      '0          Zero-order hold (nearest neighbour).'
                      '1          First-order hold (trilinear interpolation).'
                      '2->127     Higher order Lagrange (polynomial) interpolation using different holds (second-order upwards).' 
                      '-127 <- -1   Different orders of sinc interpolation.'  
    };
convert_split_interpol.strtype = 'r';
convert_split_interpol.num     = [1 1];
convert_split_interpol.val     = {-7};

% exbranch
split4d      = cfg_exbranch;
split4d.tag  = 'split4d';
split4d.name = '4d to 3d';
split4d.val  = {convert_split_in_vol convert_split_interpol};
split4d.help = {
                    ''
};
split4d.prog = @local_split4d;
split4d.vout = @vout_split4d;

end

function out = local_split4d(job)
    acidsci_split4d(char(job.in_vols4),job.defaults);
    % I am not sure whether this is right
    out.spite_4d = acid_spm_file(char(job.in_vols4),'ending','_4d');
end

% I am not sure whether this is right
function dep = vout_split4d(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Splitted 4d';
    dep(1).src_output = substruct('.','split_4d');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end