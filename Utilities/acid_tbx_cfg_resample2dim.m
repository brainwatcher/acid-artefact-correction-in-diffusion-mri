function resample2dim = acid_tbx_cfg_resample2dim

% source images
resample2dim_sources         = cfg_files;
resample2dim_sources.tag     = 'resample2dim_sources';
resample2dim_sources.name    = 'Source Images';
resample2dim_sources.help    = {'Select source images. These images will be registered to the sources image.'};
resample2dim_sources.filter  = 'image';
resample2dim_sources.ufilter = '.*';
resample2dim_sources.num     = [0 Inf];
    
% voxel dimensions
resample2dim_voxdim         = cfg_entry;
resample2dim_voxdim.tag     = 'resample2dim_voxdim';
resample2dim_voxdim.name    = 'Voxel size in Millimeters';
resample2dim_voxdim.help    = {'Provide a 1 x 3  - vector with the desired voxel size.'};
resample2dim_voxdim.strtype = 'e';
resample2dim_voxdim.num     = [1 3];
resample2dim_voxdim.val     = {[1 1 1]};
    
% interpolation
resample2dim_write_interpol         = cfg_entry;
resample2dim_write_interpol.tag     = 'resample2dim_write_interpol';
resample2dim_write_interpol.name    = 'Interpolation order';
resample2dim_write_interpol.help    = {'Interpolation order as defined in spm_reslice. A splin-interpolation is used by default.'};
resample2dim_write_interpol.strtype = 'e';
resample2dim_write_interpol.num     = [1 1];
resample2dim_write_interpol.val     = {-7};

% branch
resample2dim       = cfg_exbranch;
resample2dim.tag   = 'resample2dim';
resample2dim.name  = 'Resample to dimension';
resample2dim.val   = {resample2dim_sources resample2dim_voxdim resample2dim_write_interpol};
resample2dim.help  = {
                    'This function resamples the DTI dataset to a resolution of choice.'
                    'Interpolation to a higher spatial resolution might be advantageous for improved delination of small structures in the brain.' 
                    'For spinal cord DTI (Mohammadi et al., Neuroimage, 2013), we showed that interpolation to higher in-plane spatial resolution increased the effective resolution of the tensor estimates and thus improved deliniation of the butterfly-shaped gray matter structure in the spinal cord.'}';
resample2dim.prog  = @local_resampletool;
resample2dim.vout  = @vout_resampletool;

end

function out = local_resampletool(job)
    resize_img_rotate(char(job.sources), job.Voxdim, nan(2,3), false, job.interpol_reslice);
    out.rfiles = acid_spm_file(job.sources(:),'prefix','i');
end

function dep = vout_resampletool(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resized images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end