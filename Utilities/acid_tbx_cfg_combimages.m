function combima = acid_tbx_cfg_combimages

% add files in resamplingtools folder
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(d);
end

%define variable: volumes
combima_input         = cfg_files;
combima_input.tag     = 'combima_input';
combima_input.name    = 'images';
combima_input.help    = {'Select DWI images'};
combima_input.filter  = 'image';
combima_input.ufilter = '.*';
combima_input.num     = [0 Inf];

% define variable: weights
combima_weights         = cfg_files;
combima_weights.tag     = 'combima_weights';
combima_weights.name    = 'weight images';
combima_weights.help    = {'Select weight images. Note the weights must have the same length as the DWIs.'};
combima_weights.filter  = 'image';
combima_weights.ufilter = '.*';
combima_weights.num     = [0 Inf];

% b-value directions
combima_bvals         = cfg_entry;
combima_bvals.tag     = 'combima_bvals';
combima_bvals.name    = 'b-values (bval)';
combima_bvals.help    = {'Provide an 1 x N  - array with b-values, b-values should appear in the same order as the low- and high-diffusion weighted images were entered. b-values is expected in units of s/mm^2.' 
                  'Note that the provided entry is only for illustration.'};
combima_bvals.strtype = 'e';
combima_bvals.num     = [1 Inf];
combima_bvals.val     = {[5 1000 1000 2000]};

% diffusion directions
combima_bvecs         = cfg_entry;
combima_bvecs.tag     = 'combima_bvecs';
combima_bvecs.name    = 'Diffusion directions (bvec)';
combima_bvecs.help    = {'Provide a 3 x N  - matrix with b-vectors, b-vectors should appear in the same order as the low- and high-diffusion weighted images were entered. The b-vectors are dimensionless.' 
                     'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'};
combima_bvecs.strtype = 'e';
combima_bvecs.num     = [3 Inf];
combima_bvecs.val     = {[1 0 0; 0 1 0; 0 0 1; 0 1/sqrt(2) 1/sqrt(2)]'};

% b-value directions
combima_rep         = cfg_entry;
combima_rep.tag     = 'combima_rep';
combima_rep.name    = 'Integer repetion vector';
combima_rep.help    = {'Provide an 1 x N  - array with integer values that should appear in the same order as the images that belong to each repetion.' 
    'For example if you have two repetions acquired after each other: rep  = [1 ... 1 2 ... 2] .'};
combima_rep.strtype = 'e';
combima_rep.num     = [1 Inf];
combima_rep.val     = {[1 1 2 2]};

% prefix Filename Prefix
combima_prefix         = cfg_entry;
combima_prefix.tag     = 'combima_prefix';
combima_prefix.name    = 'Filename Prefix';
combima_prefix.help    = {'Specify the string to be prepended to the filenames of weighted combined file(s). Default prefix is ''co''.'};
combima_prefix.strtype = 's';
combima_prefix.num     = [1 Inf];
combima_prefix.val     = {'co'};

% call local resample function
combima      = cfg_exbranch;
combima.tag  = 'combima';
combima.name = 'Combine repetitions';
combima.val  = {combima_input combima_weights combima_bvals combima_bvecs combima_rep combima_prefix};
combima.help = {
                    ''
};
combima.prog = @local_weightbasedcomb;
combima.vout = @vout_weightbasedcomb;
% %% cfg_choice resample function(s)
% % ---------------------------------------------------------------------
% rw3or4dnifti         = cfg_choice;
% rw3or4dnifti.tag     = 'rw3or4dnifti';
% rw3or4dnifti.name    = 'read-write nifti';
% rw3or4dnifti.help    = {
%                  'Choose read or write option.'
%                  };
% rw3or4dnifti.values  = {read4d write4d};
end

%% dependencies
function out = local_weightbasedcomb(job)
    [Vout,bval,bvec] = ACID_weightbasedcombination(char(job.in_vols),char(job.in_weights),job.b_vals,job.diff_dirs,job.rep,job.prefix);
    for i = 1:size(Vout(:),1)
        Pcomb(i) = {Vout(i).fname};
    end
    out.comb = Pcomb(:);
    out.bval = bval;
    out.bvec = bvec;
end

function dep = vout_weightbasedcomb(job)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Combined images';
    dep(1).src_output = substruct('.','comb');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2).sname      = 'b-values';
    dep(2).src_output = substruct('.','bval');
    dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'b-vectors';
    dep(3).src_output = substruct('.','bvec');
    dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});
end
