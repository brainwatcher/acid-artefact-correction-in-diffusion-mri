function inspect = acid_tbx_cfg_inspect_images

% source image
inspect_source         = cfg_files;
inspect_source.tag     = 'inspect_source';
inspect_source.name    = 'dMRI dataset';
inspect_source.help    = {'Select the dMRI dataset.'};
inspect_source.filter  = 'image';
inspect_source.ufilter = '.*';
inspect_source.num     = [1 Inf];

% b values
inspect_bvals_file         = cfg_files;
inspect_bvals_file.tag     = 'inspect_bvals_file';
inspect_bvals_file.name    = 'File (*.bval)';
inspect_bvals_file.help    = {'Select a file specifying the b-values (bval) of the selected dMRI dataset.'
    'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
inspect_bvals_file.filter  = 'any';
inspect_bvals_file.ufilter = '.*bval';
inspect_bvals_file.num     = [1 1];
inspect_bvals_file.val     = {{''}};

inspect_bvals_exp         = cfg_entry;
inspect_bvals_exp.tag     = 'inspect_bvals_exp';
inspect_bvals_exp.name    = 'Expression';
inspect_bvals_exp.help    = {'Select a file specifying the b-values (bval) of the N dMRI volumes.'
    'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
inspect_bvals_exp.strtype = 'e';
inspect_bvals_exp.num     = [Inf Inf];
inspect_bvals_exp.val     = {''};

inspect_bvals_type        = cfg_choice;
inspect_bvals_type.tag    = 'inspect_bvals_type';
inspect_bvals_type.name   = 'b values';
inspect_bvals_type.help   = {'Select a file specifying the b-values (bval) of the N dMRI volumes.'
    'Note: The file should contain a 1 x N vector, where b-values should appear in the same order as the source images were entered. b-values is expected in units of s/mm^2.'};
inspect_bvals_type.values = {inspect_bvals_file, inspect_bvals_exp};
inspect_bvals_type.val    = {inspect_bvals_file};

% b vectors
inspect_bvecs_file         = cfg_files;
inspect_bvecs_file.tag     = 'inspect_bvecs_file';
inspect_bvecs_file.name    = 'File (*.bvec)';
inspect_bvecs_file.help    = {'Select a file containing the diffusion gradient directions (bvec) of the selected dMRI dataset.'
    'Note: The file should contain a 3 x N vector, with the length of each column vector normalised to 1. b-vectors should appear in the same order as the source images were entered. The vectors are dimensionless.'};
inspect_bvecs_file.filter  = 'any';
inspect_bvecs_file.ufilter = '.*bvec';
inspect_bvecs_file.num     = [1 1];
inspect_bvecs_file.val     = {{''}};

inspect_bvecs_exp         = cfg_entry;
inspect_bvecs_exp.tag     = 'inspect_bvecs_exp';
inspect_bvecs_exp.name    = 'Expression';
inspect_bvecs_exp.help    = {'Select a file containing the diffusion gradient directions (bvec) of the selected dMRI dataset.'
    'Note: The file should contain a 3 x N vector, with the length of each column vector normalised to 1. b-vectors should appear in the same order as the source images were entered. The vectors are dimensionless.'};
inspect_bvecs_exp.strtype = 'e';
inspect_bvecs_exp.num     = [Inf Inf];
inspect_bvecs_exp.val     = {''};

inspect_bvecs_type        = cfg_choice;
inspect_bvecs_type.tag    = 'inspect_bvecs_type';
inspect_bvecs_type.name   = 'Diffusion vectors (bvec)';
inspect_bvecs_type.help   = {'Select a file containing the diffusion gradient directions (bvec) of the selected dMRI dataset.'
    'Note: The file should contain a 3 x N vector, with the length of each column vector normalised to 1. b-vectors should appear in the same order as the source images were entered. The vectors are dimensionless.'};
inspect_bvecs_type.values = {inspect_bvecs_file, inspect_bvecs_exp};
inspect_bvecs_type.val    = {inspect_bvecs_file};

% exbranch:
inspect         = cfg_exbranch;
inspect.tag     = 'inspect';
inspect.name    = 'Inspect images';
inspect.help    = {'Visual inspection of the dMRI dataset.'};
inspect.val     = {inspect_source, inspect_bvals_type, inspect_bvecs_type};
inspect.prog    = @local_VisualInspection;
inspect.vout    = @out_VisualInspection;

end


function out = local_VisualInspection(job)

    % read in bvals
    if isfield(job.inspect_bvals_type,'inspect_bvals_file')
        job.inspect_bvals = job.inspect_bvals_type.inspect_bvals_file;
        job.inspect_bvals = cell2mat(job.inspect_bvals);
        [~,~,e] = fileparts(job.inspect_bvals);
        if ~isempty(job.inspect_bvals_type.inspect_bvals_file{1})
            switch lower(e)
                case '.mat'
                    job.inspect_bvals = cell2mat(struct2cell(load(job.inspect_bvals)));
                case '.nii'
                    error('Unexpected file extension!')
                otherwise
                    job.inspect_bvals = dlmread(job.inspect_bvals);
            end
        else
            job.inspect_bvals = '';
        end
    elseif isfield(job.inspect_bvals_type,'inspect_bvals_exp')
        if ~isempty(job.inspect_bvals_type.inspect_bvals_exp)
            job.inspect_bvals = job.inspect_bvals_type.inspect_bvals_exp;
        else
            job.inspect_bvals = '';
        end
    end

    % read in bvecs
    if isfield(job.inspect_bvecs_type,'inspect_bvecs_file')
        job.inspect_bvecs = job.inspect_bvecs_type.inspect_bvecs_file;
        job.inspect_bvecs = cell2mat(job.inspect_bvecs);
        [~,~,e] = fileparts(job.inspect_bvecs);
        if ~isempty(job.inspect_bvecs_type.inspect_bvecs_file{1})
            switch lower(e)
                case '.mat'
                    job.inspect_bvecs = cell2mat(struct2cell(load(job.inspect_bvecs)));
                case '.nii'
                    error('Unexpected file extension!')
                otherwise
                    job.inspect_bvecs = dlmread(job.inspect_bvecs);
            end
        else
            job.inspect_bvecs = '';
        end
    elseif isfield(job.inspect_bvecs_type,'inspect_bvecs_exp')
        if ~isempty(job.inspect_bvecs_type.inspect_bvecs_exp)
            job.inspect_bvecs = job.inspect_bvecs_type.inspect_bvecs_exp;
        else
            job.inspect_bvecs = '';
        end
    end

    acid_inspect_images(char(job.inspect_source),job.inspect_bvals,job.inspect_bvecs);

    out.files   = job.inspect_source;
    out.matfile = acid_spm_file(char(job.inspect_source(1)),'prefix','labeled_slices_','format','.mat');
    if isfield(job.inspect_bvals_type,'inspect_bvals_file')
        out.bval = cellstr(job.inspect_bvals_type.inspect_bvals_file);
    elseif isfield(job.inspect_bvals_type,'inspect_bvals_exp')
        out.bval = job.inspect_bvals_type.inspect_bvals_exp;
    end
    if isfield(job.inspect_bvecs_type,'inspect_bvecs_file')
        out.bvec = cellstr(job.inspect_bvecs_type.inspect_bvecs_file);
    elseif isfield(job.inspect_bvecs_type,'inspect_bvecs_exp')
        out.bvec = job.inspect_bvecs_type.inspect_bvecs_exp;
    end

end

function dep = out_VisualInspection(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Images';
    dep(1).src_output = substruct('.','files');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'b-values';
    dep(2).src_output = substruct('.','bval');
    dep(2).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'b-vectors';
    dep(3).src_output = substruct('.','bvec');
    dep(3).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
    dep(4)            = cfg_dep;
    dep(4).sname      = 'Labeled volumes';
    dep(4).src_output = substruct('.','matfile');
    dep(4).tgt_spec   = cfg_findspec({{'filter','mat','strtype','e'}});
end