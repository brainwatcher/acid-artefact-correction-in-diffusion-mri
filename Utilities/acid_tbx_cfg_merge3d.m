function merge3d = acid_tbx_cfg_merge3d

% input 
convert_merge_in_vols         = cfg_files;
convert_merge_in_vols.tag     = 'convert_merge_in_vols';
convert_merge_in_vols.name    = 'images';
convert_merge_in_vols.help    = {'Select dataset that should be merged into 4d nifti.'};
convert_merge_in_vols.filter  = 'image';
convert_merge_in_vols.ufilter = '.*';
convert_merge_in_vols.num     = [0 Inf];

% interpolation
convert_merge_interpol       = cfg_entry;
convert_merge_interpol.tag   = 'convert_merge_interpol';
convert_merge_interpol.name  = 'resampling method';
convert_merge_interpol.help  = {'Enter the desired resampling method.'
                      'This function uses spm_sample_vol.m, which allows to choose between the following resampling schemes:'
                      '0          Zero-order hold (nearest neighbour).'
                      '1          First-order hold (trilinear interpolation).'
                      '2->127     Higher order Lagrange (polynomial) interpolation using different holds (second-order upwards).' 
                      '-127 <- -1   Different orders of sinc interpolation.'  
    };
convert_merge_interpol.strtype = 'r';
convert_merge_interpol.num     = [1 1];
convert_merge_interpol.val     = {-7};

% exbranch
merge3d      = cfg_exbranch;
merge3d.tag  = 'merge3d';
merge3d.name = '3d to 4d';
merge3d.val  = {convert_merge_in_vols convert_merge_interpol};
merge3d.help = {
                    ''
};
merge3d.prog = @local_merge3ds;
merge3d.vout = @vout_merge3ds;

end

function out = local_merge3ds(job)
    acidsci_merge3ds(char(job.in_vols),job.defaults);
    % I am not sure whether this is right
    out.merged_4d = acid_spm_file(char(job.in_vols),'ending','');
    
    % I am not sure whether this is right
    function varout = acid_spm_file(filename,options,prename)
    for i=1:size(filename,1)
        if(strcmp('prefix',options))
            [p,n,e] = spm_fileparts(filename(i,:));
            varout  = [p filesep [prename n e]];
        elseif(strcmp('ending',options))
            [p,n,e]=spm_fileparts(filename(i,:));
            varout  = [p filesep [n prename e]];
        end
    end
    end
end

% I am not sure whether this is right
function dep = vout_merge3ds(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Merged 4d';
    dep(1).src_output = substruct('.','merged_4d');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end