function acid_check_registration(Pref,P1,P2,P3,dummy_allslices,dummy_order,slice,dummy_movie,Pause,int)

% %% Check input parameters
% 
% % load in reference image and image series
% if(~exist('Pref','var'))
%     Pref = char(cfg_getfile(Inf,'IMAGE','Select reference image','','','.*'));
% end
% if(~exist('P1','var'))
%     P1 = char(cfg_getfile(Inf,'IMAGE','Select first set of images','','','.*'));
% end
% if(~exist('P2','var'))
%     P2 = char(cfg_getfile(Inf,'IMAGE','Select second set of images','','','.*'));
% end
% if(~exist('P3','var'))
%     P3 = char(cfg_getfile(Inf,'IMAGE','Select third set of images','','','.*'));
% end
% if(~exist('dummy_allslices','var'))
%     dummy_allslices = spm_input('Display all slices? 0 (no), 1 (yes)',1);
% end
% if dummy_allslices == 0
%     slice = spm_input('Which slice to display?',1);
% end
% if dummy_allslices == 1
%     dummy_order = spm_input('In which order to display slices and volumes? 0 (volumes,slices), 1 (slices,volumes)',1);
% end
% if(~exist('pause','var'))
%     localtime = spm_input('Duration of a frame?',1);
% end
% if(~exist('dummy_movie','var'))
%     dummy_movie = spm_input('Save movie? 1 (yes), 0 (no)',1);
% end
% if(~exist('int','var'))
%     int = -7;
% end

% determine number of datasets
if isempty(P1)
    error('Select at least one dataset')
elseif isempty(P2)
    N = 1;
elseif isempty(P3)
    N = 2;
else
    N = 3;
end

%% load in images

% reference image
VG = spm_vol(Pref);
dm = VG.dim;
ARef = spm_read_vols(VG);

if (~dummy_allslices && (slice<1 || slice>dm(3)))
    error('The specified slice number does not lie in the image');
end

% 1. dataset
Asource1 = zeros([dm size(P1,1)]);
V1 = spm_vol(P1);
for i = 1:size(V1,1)
    Asource1(:,:,:,i) = acid_read_vols(V1(i),VG,int);
end

% 2. dataset
if N > 1
    if (size(P2,1)~=size(P1,1))
        error('The size of the first and second dataset is not the same.')
    end
    Asource2 = zeros([dm size(P2,1)]);
    V2 = spm_vol(P2);
    for i = 1:size(V2,1)
        Asource2(:,:,:,i) = acid_read_vols(V2(i),VG,int);
    end
    if V2(1).dim(3) ~= V1(1).dim(3)
        error('The number of slices in the first and second dataset is not the same.');
    end
end

% 3. dataset
if N == 3
    if (size(P3,1)~=size(P1,1))
        error('The size of the first and third dataset is not the same.');
    elseif (size(P3,1)~=size(P2,1))
        error('The size of the second and third dataset is not the same.');
    end
    Asource3 = zeros([dm size(P3,1)]);
    V3 = spm_vol(P3);
    for i=1:size(V3,1)
        Asource3(:,:,:,i) = acid_read_vols(V3(i),VG,int);
    end
    if V1(1).dim(3) ~= V3(1).dim(3)
        error('The number of slices in the first and third dataset is not the same.');
    elseif V2(1).dim(3) ~= V3(1).dim(3)
        error('The number of slices in the second and third dataset is not the same.');
    end
end


%%
    
fig1=figure;
figure('units','normalized','outerposition', [0 0 1 1]); %'outerposition',[0 0 1 1],
colormap gray;
set(fig1, 'Name', 'QC of motion correction');
    
if dummy_movie
    cwd = pwd;
    % define subjects path here
    [pth,fname,~] = spm_fileparts(Pref);
    subjectpath = pth;

    cd(subjectpath)
    % Starting a movie object
    aviobj = VideoWriter(['checkreg_' fname '.avi']);
    open(aviobj);
end
    
    
switch N
    
    % 1 DATASET
    case 1
        % all slices
        if dummy_allslices
            
            if ~dummy_order
                for k=1:dm(3)
                    for i=1:size(Asource1,4)
                        subplot(1,2,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}, 'FontSize', 18, 'FontWeight', 'bold'); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,2,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}, 'FontSize', 18, 'FontWeight', 'bold'); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end
            else
                for i=1:size(Asource1,4)
                    for k=1:dm(3)                    
                        subplot(1,2,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,2,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end 
            end
            
        % single slice    
        else
            
            for i=1:size(Asource1,4)
                subplot(1,2,1);imagesc(rot90(ARef(:,:,slice))); axis off; title({'Reference image',sprintf('Slice %d',slice)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,2,2);imagesc(rot90(Asource1(:,:,slice,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                pause(Pause)                    
                if dummy_movie                        
                    frame = getframe(gcf);
                    writeVideo(aviobj,frame);
                end
            end
            
        end
        
    % 2 DATASETS 
    case 2
        % all slices
        if dummy_allslices
            
            if ~dummy_order
                for k=1:dm(3)
                    for i=1:size(Asource1,4)
                        subplot(1,3,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,3,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,3,3);imagesc(rot90(Asource2(:,:,k,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end
            else
                for i=1:size(Asource1,4)
                    for k=1:dm(3)                    
                        subplot(1,3,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,3,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,3,3);imagesc(rot90(Asource2(:,:,k,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end 
            end
            
        % single slice    
        else
            
            for i=1:size(Asource1,4)
                subplot(1,3,1);imagesc(rot90(ARef(:,:,slice))); axis off; title({'Reference image',sprintf('Slice %d',slice)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,3,2);imagesc(rot90(Asource1(:,:,slice,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,3,3);imagesc(rot90(Asource2(:,:,slice,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                pause(Pause)                    
                if dummy_movie                        
                    frame = getframe(gcf);
                    writeVideo(aviobj,frame);
                end
            end
            
        end
    
    % 3 DATASETS   
    case 3
        % all slices
        if dummy_allslices
            
            if ~dummy_order
                for k=1:dm(3)
                    for i=1:size(Asource1,4)
                        subplot(1,4,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,3);imagesc(rot90(Asource2(:,:,k,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,4);imagesc(rot90(Asource3(:,:,k,i))); axis off; title({'Images 3', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end
            else
                for i=1:size(Asource1,4)
                    for k=1:dm(3)                    
                        subplot(1,4,1);imagesc(rot90(ARef(:,:,k))); axis off; title({'Reference image',sprintf('Slice %d',k)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,2);imagesc(rot90(Asource1(:,:,k,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,3);imagesc(rot90(Asource2(:,:,k,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        subplot(1,4,4);imagesc(rot90(Asource3(:,:,k,i))); axis off; title({'Images 3', sprintf('Slice %d, Volume %d',k,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                        pause(Pause)
                        if dummy_movie                        
                            frame = getframe(gcf);
                            writeVideo(aviobj,frame);
                        end
                    end
                end 
            end
            
        % single slice    
        else
            
            for i=1:size(Asource1,4)
                subplot(1,4,1);imagesc(rot90(ARef(:,:,slice))); axis off; title({'Reference image',sprintf('Slice %d',slice)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,4,2);imagesc(rot90(Asource1(:,:,slice,i))); axis off; title({'Images 1', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,4,3);imagesc(rot90(Asource2(:,:,slice,i))); axis off; title({'Images 2', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                subplot(1,4,4);imagesc(rot90(Asource3(:,:,slice,i))); axis off; title({'Images 3', sprintf('Slice %d, Volume %d',slice,i)}); pbaspect([dm(1) dm(2) dm(3)]);
                pause(Pause)                    
                if dummy_movie                        
                    frame = getframe(gcf);
                    writeVideo(aviobj,frame);
                end
            end
            
        end
end

close(fig1)
        
if dummy_movie 
    close(aviobj);
    cd(cwd);
end
    
end