function crop_new = acid_tbx_cfg_crop_images

% images to crop
crop_new_images         = cfg_files;
crop_new_images.tag     = 'crop_new_images';
crop_new_images.name    = 'Dataset to be cropped';
crop_new_images.help    = {'Select the dMRI dataset you want to crop. You can load in either a set of 3d nifti or a single 4d nifti file.'};
crop_new_images.filter  = 'image';
crop_new_images.ufilter = '.*';
crop_new_images.num     = [0 Inf];

% new matrix size
crop_new_matrix         = cfg_entry;
crop_new_matrix.tag     = 'crop_new_matrix';
crop_new_matrix.name    = 'New image matrix';
crop_new_matrix.help    = {'Enter the matrix size of the cropped image.'
    'The three values represent matrix size in the x, y, and z dimensions, respectively'};
crop_new_matrix.strtype = 'e';
crop_new_matrix.num     = [1 3];
crop_new_matrix.val     = {};

% middle slice (around which image will be cropped in the z direction)
crop_new_midz         = cfg_entry;
crop_new_midz.tag     = 'crop_new_midz';
crop_new_midz.name    = 'Middle slice';
crop_new_midz.help    = {'Enter the middle slice.'};
crop_new_midz.strtype = 'e';
crop_new_midz.num     = [1 1];
crop_new_midz.val     = {};

% midpoint in the plane (around which the image will be cropped in the x and y directions)
crop_new_midxy         = cfg_entry;
crop_new_midxy.tag     = 'crop_new_midxy';
crop_new_midxy.name    = 'Midpoint in the plain';
crop_new_midxy.help    = {'Enter the midpoint around which the slices will be cropped.'
    'If left unspecified, the midpoint has to be selected manually in a pop-up window.'};
crop_new_midxy.strtype = 'e';
crop_new_midxy.num     = [0 Inf];
crop_new_midxy.val     = {[]};

% prefix for output
crop_new_prefix         = cfg_entry;
crop_new_prefix.tag     = 'crop_new_prefix';
crop_new_prefix.name    = 'Prefix for output';
crop_new_prefix.help    = {'Specify the prefix for the new files. If this is left empty, original files will be overwritten.'};
crop_new_prefix.strtype = 's';
crop_new_prefix.val     = {'c'};

% exbranch
crop_new       = cfg_exbranch;
crop_new.tag   = 'crop_new';
crop_new.name  = 'Create new cropping';
crop_new.help  = {'Create new cropping'};
crop_new.val   = {crop_new_images, crop_new_matrix, crop_new_midz, crop_new_midxy, crop_new_prefix};
crop_new.prog  = @local_crop_images;
crop_new.vout  = @vout_crop_images;

end

function out = local_crop_images(job)
    acid_crop_images(char(job.crop_new_images), job.crop_new_matrix, job.crop_new_midz, job.crop_new_midxy, char(job.crop_new_prefix));
    out.cfiles = acid_spm_file(char(job.crop_new_images(:)),'prefix',char(job.crop_new_prefix),'format','.nii');
    out.cfiles = acid_select(out.cfiles(:));
    out.cfiles = out.cfiles(:);
    params = load(char(acid_spm_file(char(job.crop_new_images(1)),'prefix','cropping_params_','format','.mat')));   
    out.matrix = params.matrix;
    out.midz   = params.midz;
    out.midxy  = params.midxy;
end

function dep = vout_crop_images(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Cropped images';
    dep(1).src_output = substruct('.','cfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'New image matrix';
    dep(2).src_output = substruct('.','matrix');
    dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
    dep(3)            = cfg_dep;
    dep(3).sname      = 'Middle slice';
    dep(3).src_output = substruct('.','midz');
    dep(3).tgt_spec   = cfg_findspec({{'strtype','e'}});  
    dep(4)            = cfg_dep;
    dep(4).sname      = 'Midpoint in the plain';
    dep(4).src_output = substruct('.','midxy');
    dep(4).tgt_spec   = cfg_findspec({{'strtype','e'}});
end