function Test_ACID_Biophysical_Parameters(fit_data,check_output)




profile_code = 0;


% fit_data = 1;
% 
% check_output = 1;

if profile_code == 1 
    profile on
end

if (fit_data == 1)
    
    for branch = 1:2
    
       [filepath,~,~] = fileparts(mfilename('fullpath'));
       

        pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Biophysical_Parameters' filesep] ;
        
     
        jobfile = {[filepath filesep 'Batch_Files' filesep 'BP_Test_Batch_job.m']};
   



       
         jobs = repmat(jobfile, 1, 1);
       

     
        per_D=spm_select('FPList', [pth], ['^DKI_RBC_OFF_Radial_GN_simulation_001.nii']); 

        para_D=spm_select('FPList', [pth], ['^DKI_RBC_OFF_Axial_GN_simulation_001.nii']); 

        per_K=spm_select('FPList', [pth], ['^DKI_RBC_OFF_Kperp_GN_simulation_001.nii']); 

        para_K=spm_select('FPList', [pth], ['^DKI_RBC_OFF_Kparallel_GN_simulation_001.nii']); 

        mean_W=spm_select('FPList', [pth], ['^DKI_RBC_OFF_Wmean_GN_simulation_001.nii']); 

 


        inputs = cell(6,1);

        
        
        inputs{1, 1} = {per_D}; % Estimation Of Biophysical Parameters: Perpendicular Diffusivity image - cfg_files
        inputs{2, 1} = {para_D}; % Estimation Of Biophysical Parameters: Parallel Diffusivitiy image - cfg_files
        inputs{3, 1} = {per_K}; % Estimation Of Biophysical Parameters: Perpedicular Kurtosis image - cfg_files
        inputs{4, 1} = {para_K}; % Estimation Of Biophysical Parameters: Parallel Kurtosis image - cfg_files
        inputs{5, 1} = {mean_W}; % Estimation Of Biophysical Parameters: Mean Kurtosis image - cfg_files
        inputs{6, 1} = branch; % Estimation Of Biophysical Parameters: dummy_branch - cfg_entry


    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});



    end

end




if (check_output == 1)
    
            MSK =[26:150]';
            
            [filepath,~,~] = fileparts(mfilename('fullpath'));

            cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
            load Ground_Truth_BP.mat


       
       [filepath,~,~] = fileparts(mfilename('fullpath'));
        pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Biophysical_Parameters' filesep] ;
           
            
       
    BP_Variables ={'Da','De_par','De_perp','f','Kappa',};
   for branch = 1:2 
    for inx_var = 1:numel(BP_Variables)
        Vol=(spm_vol([pth 'BP_' BP_Variables{inx_var} '_Sample_Objective_Branch_' num2str(branch) '_DKI_RBC_OFF_Radial_GN_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,branch,:) = parameter(MSK);
        
        if (isequaln(estimated_parameter_values(inx_var,branch,:), parameter_values_ground_truth_BP(inx_var,branch,:)))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[BP_Variables{inx_var} ' Branch ' num2str(branch) ' Value is the same as the one estimated with the Sample Objective BP fit of 17/12/2020 \n']);
        else
            cprintf('*red',[BP_Variables{inx_var} ' Branch ' num2str(branch) ' Value is NOT the same as the one estimated with the Sample Objective BP fit of 17/12/2020 \n']);
            display([BP_Variables{inx_var} ' Mean should be (excluding NaNs):  ' num2str(nanmean(parameter_values_ground_truth_BP(inx_var,branch,:)),100)]);
            display([BP_Variables{inx_var} ' Mean is (excluding NaNs):         ' num2str(nanmean(estimated_parameter_values(inx_var,branch,:)),100)]);
            display([BP_Variables{inx_var} ' Mean Difference (excluding NaNs): ' num2str(nanmean(abs(estimated_parameter_values(inx_var,branch,:)-parameter_values_ground_truth_BP(inx_var,branch,:))),10)]);
        end
   

    end   
    
   end
             
    
end

if profile_code == 1 
    profile viewer
end