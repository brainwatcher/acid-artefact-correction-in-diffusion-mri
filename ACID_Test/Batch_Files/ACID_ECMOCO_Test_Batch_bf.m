% List of open inputs
nrun = X; % enter the number of runs here
jobfile = {'/home/fricke/matlab/spm12/toolbox/acid-artefact-correction-in-diffusion-mri/ACID_Test/Batch_Files/ACID_ECMOCO_Test_Batch_bf_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(0, nrun);
for crun = 1:nrun
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
