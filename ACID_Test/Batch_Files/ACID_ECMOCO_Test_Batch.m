% List of open inputs
% Estimate & write ECMOCO: Source Images - cfg_files
nrun = X; % enter the number of runs here
jobfile = {'/home/fricke/matlab/spm12/toolbox/acid-artefact-correction-in-diffusion-mri/ACID_Test/Batch_Files/ACID_ECMOCO_Test_Batch_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(1, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimate & write ECMOCO: Source Images - cfg_files
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
