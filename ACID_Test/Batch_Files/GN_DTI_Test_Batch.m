% List of open inputs
% NLLS: DTI: Measured diffusion weighted magnitude images - cfg_files
nrun = X; % enter the number of runs here
jobfile = {'C:\Users\oeschger\Documents\MATLAB\toolbox\spm12\toolbox\ACID\ACID_Test\Batch_Files\GN_DTI_Test_Batch_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(1, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % NLLS: DTI: Measured diffusion weighted magnitude images - cfg_files
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
