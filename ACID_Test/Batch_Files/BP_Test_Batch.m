% List of open inputs
% Estimation Of Biophysical Parameters: Perpendicular Diffusivity image - cfg_files
% Estimation Of Biophysical Parameters: Parallel Diffusivitiy image - cfg_files
% Estimation Of Biophysical Parameters: Perpedicular Kurtosis image - cfg_files
% Estimation Of Biophysical Parameters: Parallel Kurtosis image - cfg_files
% Estimation Of Biophysical Parameters: Mean Kurtosis image - cfg_files
% Estimation Of Biophysical Parameters: dummy_branch - cfg_entry
nrun = X; % enter the number of runs here
jobfile = {'C:\Users\oeschger\Documents\MATLAB\toolbox\spm12\toolbox\ACID\ACID_Test\BP_Test_Batch_job.m'};
jobs = repmat(jobfile, 1, nrun);
inputs = cell(6, nrun);
for crun = 1:nrun
    inputs{1, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: Perpendicular Diffusivity image - cfg_files
    inputs{2, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: Parallel Diffusivitiy image - cfg_files
    inputs{3, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: Perpedicular Kurtosis image - cfg_files
    inputs{4, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: Parallel Kurtosis image - cfg_files
    inputs{5, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: Mean Kurtosis image - cfg_files
    inputs{6, crun} = MATLAB_CODE_TO_FILL_INPUT; % Estimation Of Biophysical Parameters: dummy_branch - cfg_entry
end
spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});
