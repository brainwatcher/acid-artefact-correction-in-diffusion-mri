%-----------------------------------------------------------------------
% Job saved on 03-May-2021 11:33:44 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_sources = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_type = 1;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof1_type.ecmoco2_dof1_exp = [1 1 1 1 1 1 0 1 0 1 1 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof2_type.ecmoco2_dof2_exp = [0 1 0 0 1 0 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_bvals_type.ecmoco2_bvals_exp = [0 0 500 1000 2000 500 1000 2000 500 1000 2000 0 500 1000 2000 500 1000 2000 500 1000 2000 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_dummy_single_or_multi_target = false;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_target = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_init = true;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_excluded = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_mask = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_zsmooth = 0;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_biasfield = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_prefix = 'r';
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_parallel_prog = 4;
