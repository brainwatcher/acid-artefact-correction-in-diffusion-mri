%-----------------------------------------------------------------------
% Job saved on 17-Dec-2020 13:51:31 by cfg_util (rev $Rev: 7345 $)
% spm SPM - SPM12 (7771)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_Radial = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_Axial = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_Wperp = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_Wpara = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_Wmean = '<UNDEFINED>';
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_dummy_um = 1;
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_npool = 1;
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_dummy_WK = 1;
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_msk = {''};
matlabbatch{1}.spm.tools.dti.biophys_choice.acid_biophysical_parameters.in_dummy_branch = '<UNDEFINED>';
