%-----------------------------------------------------------------------
% Job saved on 15-Mar-2021 09:33:06 by cfg_util (rev $Rev: 6942 $)
% spm SPM - SPM12 (7219)
% cfg_basicio BasicIO - Unknown
%-----------------------------------------------------------------------
%%
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_sources = {
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00001.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00002.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00003.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00004.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00005.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00006.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00007.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00008.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00009.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00010.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00011.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00012.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00013.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00014.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00015.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00016.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00017.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00018.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00019.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00020.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00021.nii,1'
                                                                                  '/projects/crunchie/fricke/ECMOCO_UNIT_TEST_DATEN/ECMOCOfit/c_dwi_spine_00022.nii,1'
                                                                                  };
%%
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_type = 1;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof1_type.ecmoco2_dof1_predef = [1 1 1 1 1 1 0 1 0 1 1 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dof2_type.ecmoco2_dof2_predef = [0 1 0 0 1 0 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_bvals_type.ecmoco2_bvals_exp = [0 0 500 1000 2000 500 1000 2000 500 1000 2000 0 500 1000 2000 500 1000 2000 500 1000 2000 0];
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_dummy_init = true;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_excluded = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_mask = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_zsmooth = 0;
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_biasfield = {''};
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco2_prefix = 'r';
matlabbatch{1}.spm.tools.dti.prepro_choice.ecmo_choice.ecmoco2.ecmoco_parallel_prog = 4;
