function Test_ACID_Gauss_Newton_Fit(fit_data,check_output)



% This script checks whether the ACID Gauss Newton output is
% the same as the one computed with the ACID version of the beta_branch
% branch on 06/11/2020 with Matlab version 2019b




 load bvalues.mat % these are the corresponding b values
 bvalues0 = bvalues;



% fit_data = 1;
% 
% check_output = 1;


if (fit_data == 1)
    
    for which_fit = 1:4
    
       [filepath,~,~] = fileparts(mfilename('fullpath'));
       

        pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Gauss_Newton' filesep] ;
        
         if (which_fit == 1)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_DTI_Test_Batch_job.m']};
        elseif(which_fit == 2)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_DKI_Test_Batch_job.m']};
        elseif(which_fit == 3)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_Ax_Sym_DKI_Test_Batch_job.m']};
        elseif(which_fit == 4)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_Ax_Sym_DKI_RBC_ON_Test_Batch_job.m']};
        end



       
       jobs = repmat(jobfile, 1, 1);
       
           PP=[];

          for inx = 1:numel(bvalues0)

            if(inx<10)
                num = ['_00' num2str(inx)];
            elseif(inx<100)
                num = ['_0' num2str(inx)];
            elseif(inx<1000)
                num = ['_' num2str(inx)];
            end

            Ptmp=(spm_select('FPList', [pth], ['^simulation' num '.nii']));
            PP = cat(1,PP,Ptmp);
            clear Ptmp
            
          end

            P=cell(size(PP,1),1); 
            for inx=1:size(PP,1)
                P(inx) = {deblank(PP(inx,:))};
            end


        inputs = cell(1,1);

        inputs{1, 1} = P; % Fit Diffusion Tensor: DTI images - cfg_files

    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});


    end

    
    
end


       [filepath,~,~] = fileparts(mfilename('fullpath'));
       MSK =[26:150]';
       MSK_check = [1:50,52:125]'; % Voxel number 51 showed a minute numerical difference that seems to be due to the operating system ( with identical ACID versions a difference occurs between the results obtained on a Windows 10 laptop and a linux system on the server, however, for the axial symmetric fit it occurs for all voxels).
       MSK_RBC_ON_AXIAL_FIT = [1:24,27:38,41:92,94:124]';% these voxels had a big difference (1e-4) due to different operating systems.
       
       tolerance = 1e-5;
       
       pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'Gauss_Newton' filesep] ;


if (check_output == 1)
    
    
    
            cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
            load Ground_Truth_Gauss_Newton.mat
       
           
            
       

    DKI_Variables ={'Dparallel','FA','Wparallel','Wperp','MD','MK','Dperp','Wmean','RES','meanDWI','Kperp','Kparallel','b0'};

    for inx_var = 1:numel(DKI_Variables) 
        Vol=(spm_vol([pth 'NLLS_DKI_RBC_OFF_' DKI_Variables{inx_var} '_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,MSK_check) == parameter_values_ground_truth_GN_DKI(inx_var,MSK_check) )
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DKI_Variables{inx_var} ' GN DKI Value is the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DKI_Variables{inx_var} ' GN DKI Value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display([DKI_Variables{inx_var} ' Mean  should be:  ' num2str(nanmean(parameter_values_ground_truth_GN_DKI(inx_var,:)) ,100)]);
            display([DKI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)) ,100)]);
            display([DKI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:) -parameter_values_ground_truth_GN_DKI(inx_var,:) )),10)]);
        end
   

    end   
    
    dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol=(spm_vol([pth 'NLLS_DKI_RBC_OFF_EVEC_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvector=acid_read_vols(Vol,VG,1);
        
        estimated_eigenvector(inx_eigen_var,:) = eigenvector(MSK);
        
        if (estimated_eigenvector(inx_eigen_var,MSK_check) == parameter_values_ground_truth_eigenvectors_GN_DKI(inx_eigen_var,MSK_check))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' GN DKI value is the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' GN DKI value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(parameter_values_ground_truth_eigenvectors_GN_DKI(inx_eigen_var,:)),100)]);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvector(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                                      ' num2str(nanmean(abs(estimated_eigenvector(inx_eigen_var,:)-parameter_values_ground_truth_eigenvectors_GN_DKI(inx_eigen_var,:))),10)]);
        end
        
    end   
    
    dimension ={'1','2','3'};

   for inx_eigen_var = 1:numel(dimension)
        Vol = (spm_vol([pth 'NLLS_DKI_RBC_OFF_EVAL_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvalue =acid_read_vols(Vol,VG,1);
        
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
        

        if (estimated_eigenvalue(inx_eigen_var,MSK_check) == parameter_values_ground_truth_eigenvalue_GN_DKI(inx_eigen_var,MSK_check))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' GN DKI value is the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' GN DKI value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(parameter_values_ground_truth_eigenvalue_GN_DKI(inx_eigen_var,:)),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvalue(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                       ' num2str(nanmean(abs(estimated_eigenvalue(inx_eigen_var,:)-parameter_values_ground_truth_eigenvalue_GN_DKI(inx_eigen_var,:))),10)]);
        end

   end 
    

       

    
    DTI_Variables ={'Dparallel','FA','MD','Dperp','RES','meanDWI','b0'};
    
    for inx_var = 1:numel(DTI_Variables)
        Vol=(spm_vol([pth 'NLLS_DTI_RBC_OFF_' DTI_Variables{inx_var} '_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,MSK_check) == parameter_values_ground_truth_GN_DTI(inx_var,MSK_check) )
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DTI_Variables{inx_var} ' GN DTI Value is the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DTI_Variables{inx_var} ' GN DTI Value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display([DTI_Variables{inx_var} ' Mean should be:  ' num2str(nanmean(parameter_values_ground_truth_GN_DTI(inx_var,:)) ,100)]);
            display([DTI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)) ,100)]);
            display([DTI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:) -parameter_values_ground_truth_GN_DTI(inx_var,:) )),10)]);
        end
   

    end   
    
    dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol=(spm_vol([pth 'NLLS_DTI_RBC_OFF_EVEC_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvector=acid_read_vols(Vol,VG,1);
        
        estimated_eigenvector(inx_eigen_var,:) = eigenvector(MSK);
        
        if (estimated_eigenvector(inx_eigen_var,MSK_check) == parameter_values_ground_truth_eigenvectors_GN_DTI(inx_eigen_var,MSK_check))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' GN DTI value is the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' GN DTI value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(parameter_values_ground_truth_eigenvectors_GN_DTI(inx_eigen_var,:)),100)]);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvector(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                                      ' num2str(nanmean(abs(estimated_eigenvector(inx_eigen_var,:)-parameter_values_ground_truth_eigenvectors_GN_DTI(inx_eigen_var,:))),10)]);
        end
        
    end   
    

    dimension ={'1','2','3'};

   for inx_eigen_var = 1:numel(dimension)
        Vol = (spm_vol([pth 'NLLS_DTI_RBC_OFF_EVAL_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvalue =acid_read_vols(Vol,VG,1);
        
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
        

        if (estimated_eigenvalue(inx_eigen_var,MSK_check) == parameter_values_ground_truth_eigenvalue_GN_DTI(inx_eigen_var,MSK_check))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' GN DTI value is the same as the one estimated with the ACID beta_branch branch version of 11/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' GN DTI value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(parameter_values_ground_truth_eigenvalue_GN_DTI(inx_eigen_var,:)),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvalue(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                       ' num2str(nanmean(abs(estimated_eigenvalue(inx_eigen_var,:)-parameter_values_ground_truth_eigenvalue_GN_DTI(inx_eigen_var,:))),10)]);
        end

   end 

               
        
        
        

        
        
     Ax_sym_DKI_Variables ={'Axis_Of_Symmetry_x','Axis_Of_Symmetry_y','Axis_Of_Symmetry_z','FA','Kparallel','Kperp','MD','Wmean','meanDWI','Dparallel','Dperp','Wparallel','Wperp','b0'};
    
    for inx_var = 1:numel(Ax_sym_DKI_Variables)
        Vol=(spm_vol([pth 'NLLS_AxDKI_RBC_OFF_' Ax_sym_DKI_Variables{inx_var} '_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (abs(estimated_parameter_values(inx_var,MSK_check) - parameter_values_ground_truth_GN_Axial_Symmetric(inx_var,MSK_check))< tolerance )
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[Ax_sym_DKI_Variables{inx_var} ' GN Axial Symmetric DKI Value is the same as the one estimated with the ACID beta_branch branch version of 11/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[Ax_sym_DKI_Variables{inx_var} ' GN Axial Symmetric DKI Value is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean should be:  ' num2str(nanmean(parameter_values_ground_truth_GN_Axial_Symmetric(inx_var,:)) ,100)]);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)) ,100)]);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:) -parameter_values_ground_truth_GN_Axial_Symmetric(inx_var,:) )),10)]);
        end
   

    end   
        
        
        
        
        
        
        
        
        
        
       Ax_sym_DKI_Variables ={'Axis_Of_Symmetry_x','Axis_Of_Symmetry_y','Axis_Of_Symmetry_z','FA','Kparallel','Kperp','MD','Wmean','meanDWI','Dparallel','Dperp','Wparallel','Wperp','b0'};
    
    for inx_var = 1:numel(Ax_sym_DKI_Variables)
        Vol=(spm_vol([pth 'NLLS_AxDKI_RBC_ON_' Ax_sym_DKI_Variables{inx_var} '_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (abs( estimated_parameter_values(inx_var,MSK_check(MSK_RBC_ON_AXIAL_FIT)) - parameter_values_ground_truth_GN_Axial_Symmetric_RBC_ON(inx_var,MSK_check(MSK_RBC_ON_AXIAL_FIT)))< tolerance )
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[Ax_sym_DKI_Variables{inx_var} ' GN Axial Symmetric DKI Value with RBC is the same as the one estimated with the ACID beta_branch branch version of 11/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[Ax_sym_DKI_Variables{inx_var} ' GN Axial Symmetric DKI Value with RBC is NOT the same as the one estimated with the ACID beta_branch branch version of 11/06/2020 with Matlab version 2019b. \n']);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean should be:  ' num2str(nanmean(parameter_values_ground_truth_GN_Axial_Symmetric_RBC_ON(inx_var,:)) ,100)]);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)) ,100)]);
            display([Ax_sym_DKI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:) -parameter_values_ground_truth_GN_Axial_Symmetric_RBC_ON(inx_var,:) )),10)]);
        end
   

    end   
        
        
        
        
        
        
    
end


