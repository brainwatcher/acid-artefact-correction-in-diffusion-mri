function Test_ACID_Branch(fit_data,check_output)


% This script checks whether the ACID ordinary least squares DKI output is
% the same as the one computed with the ACID version of the master branch
% on 09/09/2020 with Matlab version 2019b


load bvalues.mat % these are the corresponding b values






fit_data_DKI = fit_data;
check_output_DKI = check_output; % for DKI estimation

fit_data_DTI = fit_data;
check_output_DTI = check_output;

b = bvalues/1000;% to convert them from mm�/s to um�/ms


if( fit_data_DKI == 1)
    
    
       [filepath,~,~] = fileparts(mfilename('fullpath'));
       

        pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DKI' filesep] ;
        jobfile = {[filepath filesep 'Batch_Files' filesep 'ACID_Test_SPM_Batch_job.m']};

       
       jobs = repmat(jobfile, 1, 1);
       
           PP=[];

          for inx = 1:numel(bvalues)

            if(inx<10)
                num = ['_00' num2str(inx)];
            elseif(inx<100)
                num = ['_0' num2str(inx)];
            elseif(inx<1000)
                num = ['_' num2str(inx)];
            end

            Ptmp=(spm_select('FPList', [pth], ['^simulation' num '.nii']));
            PP = cat(1,PP,Ptmp);
            clear Ptmp
            
          end

            P=cell(size(PP,1),1); 
            for inx=1:size(PP,1)
                P(inx) = {deblank(PP(inx,:))};
            end


        inputs = cell(1,1);

        inputs{1, 1} = P; % Fit Diffusion Tensor: DTI images - cfg_files


    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});




end

if( fit_data_DTI == 1)
    
    
       [filepath,~,~] = fileparts(mfilename('fullpath'));
       

        pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DTI' filesep] ;
        jobfile = {[filepath filesep 'Batch_Files' filesep 'ACID_DTI_Test_SPM_Batch_job.m']};

       
       jobs = repmat(jobfile, 1, 1);
       
           PP=[];

          for inx = 1:numel(bvalues)

            if(inx<10)
                num = ['_00' num2str(inx)];
            elseif(inx<100)
                num = ['_0' num2str(inx)];
            elseif(inx<1000)
                num = ['_' num2str(inx)];
            end

            Ptmp=(spm_select('FPList', [pth], ['^simulation' num '.nii']));
            PP = cat(1,PP,Ptmp);
            clear Ptmp
            
          end

            P=cell(size(PP,1),1); 
            for inx=1:size(PP,1)
                P(inx) = {deblank(PP(inx,:))};
            end


        inputs = cell(1,1);

        inputs{1, 1} = P; % Fit Diffusion Tensor: DTI images - cfg_files


    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});




end



MSK =[26:150]';
[filepath,~,~] = fileparts(mfilename('fullpath'));



if (check_output_DKI == 1)
    
            cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
            load Ground_Truth_ACID.mat

       
      pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DKI' filesep] ;
    
    DKI_Variables ={'Dparallel','FA','Kparallel','Kperp','MD','MK','Dperp','Wmean','Wperp','Wparallel','b0'};
  
    for inx_var = 1:numel(DKI_Variables)
        Vol=(spm_vol([pth DKI_Variables{inx_var} '_ols_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,:) == ground_truth_acid_DKI(inx_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DKI_Variables{inx_var} ' DKI Value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DKI_Variables{inx_var} ' DKI Value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            display([DKI_Variables{inx_var} ' Mean should be:  ' num2str(nanmean(ground_truth_acid_DKI(inx_var,:)),100)]);
            display([DKI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)),100)]);
            display([DKI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:)-ground_truth_acid_DKI(inx_var,:))),10)]);
        end
   

    end   
    
    dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol=(spm_vol([pth 'EVEC_ols_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvector=acid_read_vols(Vol,VG,1);
        
        estimated_eigenvector(inx_eigen_var,:) = eigenvector(MSK);
        
        if (estimated_eigenvector(inx_eigen_var,:) == eigenvector_ground_truth_acid_DKI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' DKI value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' DKI value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(eigenvector_ground_truth_acid_DKI(inx_eigen_var,:)),100)]);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvector(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                                      ' num2str(nanmean(abs(estimated_eigenvector(inx_eigen_var,:)-eigenvector_ground_truth_acid_DKI(inx_eigen_var,:))),10)]);
        end
        
    end   
    
    dimension ={'1','2','3'};

   for inx_eigen_var = 1:numel(dimension)
        Vol = (spm_vol([pth 'EVAL_ols_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvalue =acid_read_vols(Vol,VG,1);
        
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
        

        if (estimated_eigenvalue(inx_eigen_var,:) == eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DKI value is the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DKI value is NOT the same as the one estimated with the ACID master branch version of 09/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:)),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvalue(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                       ' num2str(nanmean(abs(estimated_eigenvalue(inx_eigen_var,:)-eigenvalue_ground_truth_acid_DKI(inx_eigen_var,:))),10)]);
        end

    end 
    
    
    
end







if (check_output_DTI == 1)
    
   
      cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
      load Ground_Truth_ACID.mat

       
      pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DTI' filesep] ;
       
    
    DTI_Variables ={'Dparallel','FA','MD','Dperp','b0'};
    
    for inx_var = 1:numel(DTI_Variables)
        Vol=(spm_vol([pth DTI_Variables{inx_var} '_ols_simulation_001.nii'])); 
        VG = Vol(1);
        
        parameter=acid_read_vols(Vol,VG,1);
        
        estimated_parameter_values(inx_var,:) = parameter(MSK);
        
        if (estimated_parameter_values(inx_var,:) == ground_truth_acid_DTI(inx_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',[DTI_Variables{inx_var} ' DTI Value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',[DTI_Variables{inx_var} ' DTI Value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display([DTI_Variables{inx_var} ' Mean should be:  ' num2str(nanmean(ground_truth_acid_DTI(inx_var,:)),100)]);
            display([DTI_Variables{inx_var} ' Mean is:         ' num2str(nanmean(estimated_parameter_values(inx_var,:)),100)]);
            display([DTI_Variables{inx_var} ' Mean Difference: ' num2str(nanmean(abs(estimated_parameter_values(inx_var,:)-ground_truth_acid_DTI(inx_var,:))),10)]);
        end
   

    end   
    
    dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};
    
    for inx_eigen_var = 1:numel(dimension)
        Vol=(spm_vol([pth 'EVEC_ols_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvector=acid_read_vols(Vol,VG,1);
        
        estimated_eigenvector(inx_eigen_var,:) = eigenvector(MSK);
        
        if (estimated_eigenvector(inx_eigen_var,:) == eigenvector_ground_truth_acid_DTI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvector dimension ' dimension{inx_eigen_var} ' DTI value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvector dimension ' dimension{inx_eigen_var} ' DTI value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(eigenvector_ground_truth_acid_DTI(inx_eigen_var,:)),100)]);
            display(['Eigenvector dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvector(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                                      ' num2str(nanmean(abs(estimated_eigenvector(inx_eigen_var,:)-eigenvector_ground_truth_acid_DTI(inx_eigen_var,:))),10)]);
        end
        
    end   
    
    dimension ={'1','2','3'};

   for inx_eigen_var = 1:numel(dimension)
        Vol = (spm_vol([pth 'EVAL_ols_simulation_001-' dimension{inx_eigen_var} '.nii']));
        VG = Vol(1);
        
        eigenvalue =acid_read_vols(Vol,VG,1);
        
        estimated_eigenvalue(inx_eigen_var,:) = eigenvalue(MSK);
        

        if (estimated_eigenvalue(inx_eigen_var,:) == eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:))
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DTI value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['Eigenvalue dimension ' dimension{inx_eigen_var} ' DTI value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean should be: ' num2str(nanmean(eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:)),100)]);
            display(['Eigenvalue dimension ' dimension{inx_eigen_var} ' Mean is:        ' num2str(nanmean(estimated_eigenvalue(inx_eigen_var,:)),100)]);
            display(['Mean Difference:                       ' num2str(nanmean(abs(estimated_eigenvalue(inx_eigen_var,:)-eigenvalue_ground_truth_acid_DTI(inx_eigen_var,:))),10)]);
        end

   end 
    
        Vol=(spm_vol([pth 'meanDWI_simulation_001.nii']));
        VG = Vol(1);
        
        meanDWI_param =  acid_read_vols(Vol,VG,1);

        
        meanDWI = meanDWI_param(1:150);
        
        if (meanDWI == meanDWI_DTI_ground_truth_acid_DTI)
            cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]',['meanDWI DTI value is the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
        else
            cprintf('*red',['meanDWI value is NOT the same as the one estimated with the ACID master branch version of 17/09/2020 with Matlab version 2019b. \n']);
            display(['meanDWI Mean should be: ' num2str(nanmean(meanDWI_DTI_ground_truth_acid_DTI),100)]);
            display(['meanDWI Mean is:        ' num2str(nanmean(meanDWI),100)]);
            display(['Mean Difference:           ' num2str(nanmean(abs(meanDWI-meanDWI_DTI_ground_truth_acid_DTI)),10)]);
        end
        
 
    
end
