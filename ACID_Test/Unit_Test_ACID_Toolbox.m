test_standard_ACID_DTI_DKI = 1; 

test_Gauss_Newton = 1;

test_Biophysical_Parameters = 1;

fit_data = 1;

check_output = 1;

test_ECMOCO = 0;

test_POAS = 0;

test_HySCO = 0;

if test_standard_ACID_DTI_DKI == 1
    Test_ACID_Branch(fit_data,check_output);
end


if test_Gauss_Newton == 1
    Test_ACID_Gauss_Newton_Fit(fit_data,check_output)
end

if test_Biophysical_Parameters == 1
    Test_ACID_Biophysical_Parameters(fit_data,check_output)
end

if test_ECMOCO == 1
    Test_ACID_ECMOCO;
end

if test_POAS == 1
    Test_ACID_POAS;
end

if test_HySCO == 1
    Test_ACID_HySCO;
end

disp('Unit test done.')
