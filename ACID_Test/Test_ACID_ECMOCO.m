function Test_ACID_ECMOCO

[filepath,~,~] = fileparts(mfilename('fullpath'));


pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ECMOCO' filesep] ;
jobfile = {[filepath filesep 'Batch_Files' filesep 'ACID_ECMOCO_Test_Batch_job.m']};


jobs = repmat(jobfile, 1, 1);

PP=[];

for inx = 1:22
    
    if(inx<10)
        num = ['_0000' num2str(inx)];
    elseif(inx<100)
        num = ['_000' num2str(inx)];
    elseif(inx<1000)
        num = ['_00' num2str(inx)];
    end
    
    Ptmp=(spm_select('FPList', [pth], ['^c_dwi_spine' num '.nii']));
    PP = cat(1,PP,Ptmp);
    clear Ptmp
    
end

P=cell(size(PP,1),1);
for inx=1:size(PP,1)
    P(inx) = {deblank(PP(inx,:))};
end


inputs = cell(1,1);

inputs{1, 1} = P;


spm('defaults', 'FMRI');
spm_jobman('run', jobs, inputs{:});



cd([filepath filesep 'Ground_Truth_Data' filesep 'ECMOCO']);
load Ground_Truth_ECMOCO.mat


params_GT = params;
%pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DTI' filesep];


cd([filepath filesep 'Fit_Results_Of_Test' filesep 'ECMOCO']);
load ecmoco_params_c_dwi_spine_00001.mat

tolerance = 1e-5;

if((params_GT - params) < tolerance)
%if(xk == xk_old38)
    cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'ECMOCO parameters are correct. \n');
    %disp('Identical')
else
    cprintf('red', 'ECMOCO parameters are different. \n');
    %disp('Different')
end
    
disp('ECMOCO test finished!')

end

