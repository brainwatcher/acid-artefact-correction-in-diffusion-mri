
clear;



    




load bvalues.mat % these are the corresponding b values


bvalues0 = bvalues;



are_you_sure_you_want_to_do_this = 'No';
are_you_really_really_sure_you_want_to_do_this = 'Definitely Not'; 



fit_data = 0;

check_output = 0;




    

if (fit_data == 1)
    
    for which_fit = 1:4
    
       [filepath,~,~] = fileparts(mfilename('fullpath'));
       

        pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Gauss_Newton' filesep] ;
        
        if (which_fit == 1)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_DTI_Test_Batch_job.m']};
        elseif(which_fit == 2)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_DKI_Test_Batch_job.m']};
        elseif(which_fit == 3)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_Ax_Sym_DKI_Test_Batch_job.m']};
        elseif(which_fit == 4)
                jobfile = {[filepath filesep 'Batch_Files' filesep 'GN_Ax_Sym_DKI_RBC_ON_Test_Batch_job.m']};
        end



       
       jobs = repmat(jobfile, 1, 1);
       
           PP=[];

          for inx = 1:numel(bvalues0)

            if(inx<10)
                num = ['_00' num2str(inx)];
            elseif(inx<100)
                num = ['_0' num2str(inx)];
            elseif(inx<1000)
                num = ['_' num2str(inx)];
            end

            Ptmp=(spm_select('FPList', [pth], ['^simulation' num '.nii']));
            PP = cat(1,PP,Ptmp);
            clear Ptmp
            
          end

            P=cell(size(PP,1),1); 
            for inx=1:size(PP,1)
                P(inx) = {deblank(PP(inx,:))};
            end


        inputs = cell(1,1);

        inputs{1, 1} = P; % Fit Diffusion Tensor: DTI images - cfg_files


    spm('defaults', 'FMRI');
    spm_jobman('run', jobs, inputs{:});



    end

end






if (are_you_sure_you_want_to_do_this == 12)
   if (are_you_really_really_sure_you_want_to_do_this == 14)


            if (check_output == 1)

                MSK =[26:150]';



                   [filepath,~,~] = fileparts(mfilename('fullpath'));
                   pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Gauss_Newton' filesep] ;





                DKI_Variables ={'Dparallel','FA','Wparallel','Wperp','MD','MK','Dperp','Wmean','RES','meanDWI','Kperp','Kparallel','b0'};

                for inx_var = 1:numel(DKI_Variables)
                    Vol=(spm_vol([pth 'NLLS_DKI_RBC_OFF_' DKI_Variables{inx_var} '_simulation_001.nii'])); 
                    VG = Vol(1);

                    parameter=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_GN_DKI(inx_var,:) = parameter(MSK);


                end   

                dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};

                for inx_eigen_var = 1:numel(dimension)
                    Vol=(spm_vol([pth 'NLLS_DKI_RBC_OFF_EVEC_simulation_001-' dimension{inx_eigen_var} '.nii']));
                    VG = Vol(1);

                    eigenvector=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_eigenvectors_GN_DKI(inx_eigen_var,:) = eigenvector(MSK);



                end   

                dimension ={'1','2','3'};

               for inx_eigen_var = 1:numel(dimension)
                    Vol = (spm_vol([pth 'NLLS_DKI_RBC_OFF_EVAL_simulation_001-' dimension{inx_eigen_var} '.nii']));
                    VG = Vol(1);

                    eigenvalue =acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_eigenvalue_GN_DKI(inx_eigen_var,:) = eigenvalue(MSK);



               end 






                   [filepath,~,~] = fileparts(mfilename('fullpath'));
                   pth =  [filepath filesep 'Ground_Truth_Data' filesep 'Gauss_Newton' filesep] ;

                DTI_Variables ={'Dparallel','FA','MD','Dperp','RES','meanDWI','b0'};

                for inx_var = 1:numel(DTI_Variables)
                    Vol=(spm_vol([pth 'NLLS_DTI_RBC_OFF_' DTI_Variables{inx_var} '_simulation_001.nii'])); 
                    VG = Vol(1);

                    parameter=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_GN_DTI(inx_var,:) = parameter(MSK);




                end   

                dimension = {'x1','y1','z1','x2','y2','z2','x3','y3','z3'};

                for inx_eigen_var = 1:numel(dimension)
                    Vol=(spm_vol([pth 'NLLS_DTI_RBC_OFF_EVEC_simulation_001-' dimension{inx_eigen_var} '.nii']));
                    VG = Vol(1);

                    eigenvector=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_eigenvectors_GN_DTI(inx_eigen_var,:) = eigenvector(MSK);



                end   


                dimension ={'1','2','3'};

               for inx_eigen_var = 1:numel(dimension)
                    Vol = (spm_vol([pth 'NLLS_DTI_RBC_OFF_EVAL_simulation_001-' dimension{inx_eigen_var} '.nii']));
                    VG = Vol(1);

                    eigenvalue =acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_eigenvalue_GN_DTI(inx_eigen_var,:) = eigenvalue(MSK);




               end 







                 Ax_sym_DKI_Variables ={'Axis_Of_Symmetry_x','Axis_Of_Symmetry_y','Axis_Of_Symmetry_z','FA','Kparallel','Kperp','MD','Wmean','meanDWI','Dparallel','Dperp','Wparallel','Wperp','b0'};

                for inx_var = 1:numel(Ax_sym_DKI_Variables)
                    Vol=(spm_vol([pth 'NLLS_AxDKI_RBC_OFF_' Ax_sym_DKI_Variables{inx_var} '_simulation_001.nii'])); 
                    VG = Vol(1);

                    parameter=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_GN_Axial_Symmetric(inx_var,:) = parameter(MSK);



                end   










                 Ax_sym_DKI_Variables ={'Axis_Of_Symmetry_x','Axis_Of_Symmetry_y','Axis_Of_Symmetry_z','FA','Kparallel','Kperp','MD','Wmean','meanDWI','Dparallel','Dperp','Wparallel','Wperp','b0'};

                for inx_var = 1:numel(Ax_sym_DKI_Variables)
                    Vol=(spm_vol([pth 'NLLS_AxDKI_RBC_ON_' Ax_sym_DKI_Variables{inx_var} '_simulation_001.nii'])); 
                    VG = Vol(1);

                    parameter=acid_read_vols(Vol,VG,1);

                    parameter_values_ground_truth_GN_Axial_Symmetric_RBC_ON(inx_var,:) = parameter(MSK);



                end   







            end
   end
   
    cd([filepath filesep 'Ground_Truth_Data' filesep 'Ground_Truth_Datasets']);
    save('Ground_Truth_Gauss_Newton.mat','parameter_values_ground_truth_GN_Axial_Symmetric_RBC_ON','parameter_values_ground_truth_GN_Axial_Symmetric','parameter_values_ground_truth_eigenvalue_GN_DTI','parameter_values_ground_truth_eigenvectors_GN_DTI','parameter_values_ground_truth_GN_DTI','parameter_values_ground_truth_eigenvalue_GN_DKI','parameter_values_ground_truth_eigenvectors_GN_DKI','parameter_values_ground_truth_GN_DKI')

   
end