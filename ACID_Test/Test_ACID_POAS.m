function Test_ACID_POAS

[filepath,~,~] = fileparts(mfilename('fullpath'));


pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'POAS' filesep] ;
jobfile = {[filepath filesep 'Batch_Files' filesep 'ACID_POAS_Test_Batch_job.m']};


jobs = repmat(jobfile, 1, 1);
%{
PP=[];

for inx = 1:22
    
    if(inx<10)
        num = ['_0000' num2str(inx)];
    elseif(inx<100)
        num = ['_000' num2str(inx)];
    elseif(inx<1000)
        num = ['_00' num2str(inx)];
    end
    
    Ptmp=(spm_select('FPList', [pth], ['^c_dwi_spine' num '.nii']));
    PP = cat(1,PP,Ptmp);
    clear Ptmp
    
end

P=cell(size(PP,1),1);
for inx=1:size(PP,1)
    P(inx) = {deblank(PP(inx,:))};
end


inputs = cell(1,1);

inputs{1, 1} = P;
%}

spm('defaults', 'FMRI');
spm_jobman('run', jobs); %, inputs{:});



PInd_Result = [filepath filesep 'Ground_Truth_Data' filesep 'POAS' filesep 'POAS_GT_rc_dwi_spine.nii,2'];
%load 

Result = spm_vol(PInd_Result);
Result = acid_read_vols(Result, Result(1),1);
%pth =  [filepath filesep 'Fit_Results_Of_Test' filesep 'ACID_DTI' filesep];


PInd_GT = [filepath filesep 'Fit_Results_Of_Test' filesep 'POAS' filesep 'new_rc_dwi_spine.nii,2'];
%load 

GT = spm_vol(PInd_GT);
GT = acid_read_vols(GT, GT(1),1);

tolerance = 1e-5;

if(sum(Result - GT) < tolerance)
%if(xk == xk_old38)
    cprintf('*[0.0627450980392157 0.67843137254902 0.0627450980392157]', 'POAS results are correct. \n');
    %disp('Identical')
else
    cprintf('red', 'POAS results are different. \n');
    %disp('Different')
end
    
%disp('POAS test finished!')

end

