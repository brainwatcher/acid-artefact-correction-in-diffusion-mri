function dti = DTI_cfg_artefact


%% Add paths
d = fileparts(mfilename('fullpath'));
if ~isdeployed
    addpath(fullfile(d));
    
    % read image
    
    addpath(fullfile(d,'read_data'));
    
    % pre-processing
    
    addpath(fullfile(d,'Preprocessing'));
    addpath(fullfile(d,'Postprocessing'));
    addpath(fullfile(d,'Utilities'));
    pd = [d filesep 'Preprocessing'];
    addpath(fullfile(pd,'ECMOCO'));
    
    % resample tool
    
    if(exist(fullfile(pd,'resamplingtools'),'dir'))
        addpath(fullfile(pd,'resamplingtools'));
        dummy_resampletool =1;
    else
        dummy_resampletool =0;
    end
    addpath(fullfile(pd,'write3or4d_nifti'));
    if(exist(fullfile(pd,'HySCO')))
        addpath(fullfile(pd,'HySCO'));
        EPIpath = [pd filesep 'HySCO'];
        addpath(fullfile(EPIpath,'FAIRkernel'));
        dummy_EPI = 1;
    else
        dummy_EPI = 0;
    end
    
    % poas
    
    if(exist(fullfile(pd,'POAS')))
        addpath(fullfile(pd,'POAS'));
        dummy_POAS = 1;
    else
        dummy_POAS = 0;
    end
    
    % tensor fit
    
    if(exist(fullfile(d,'LPF_correction_PROD')))
        addpath(fullfile(d,'LPF_correction_PROD'));
        dummy_lpf = 1;
    else
        dummy_lpf = 0;
    end
    addpath(fullfile(d,'COVIPER_PROD'));
    addpath(fullfile(d,'cfiles'));
    addpath(fullfile(d,'TensorFit'));
    addpath(fullfile(d,'config'));
    ld = [d filesep 'config'];
    addpath(fullfile(ld,'local'));
    addpath(fullfile(d,'TensorFit','Gauss_Newton_Fit'));
    
    Tensorfitpath = [d filesep 'TensorFit'];
    addpath(fullfile(Tensorfitpath,'robustfitting'));
    
    % nonlinear fitting
    
    if(exist(fullfile(Tensorfitpath,'Nonlinear')))
        addpath(fullfile(Tensorfitpath,['Nonlinear' filesep 'PROD']));
        dummy_nlfit = 1;
    else
        dummy_nlfit = 0;
    end
    % Kurtosis estimation
    
    if(exist(fullfile(Tensorfitpath,'Kurtosis'),'dir'))
        addpath(fullfile(Tensorfitpath,'Kurtosis'));
        addpath(fullfile(Tensorfitpath,['Kurtosis' filesep 'Elliptic_Integrals']));
        addpath(fullfile(Tensorfitpath,['Kurtosis' filesep 'quadraticprogramming']));
        dummy_DKIfiles = 1;
    else
        dummy_DKIfiles = 0;
    end
    addpath(fullfile(Tensorfitpath,'writeData'));
    
    % biophys
     if(exist(fullfile(d,'biophys'),'dir'))
        addpath(fullfile(d,'biophys'));
     end
    
    % NODDI-DTI
    
    if(exist(fullfile(d,'biophys','noddidti'),'dir'))
        addpath(fullfile(d,'biophys','noddidti'));
        dummy_noddidti =1;
    else
        dummy_noddidti =0;
    end
    
    % Biophysical Parameters
    
    if(exist(fullfile(d,'biophys','biophysical_parameters'),'dir'))
        addpath(fullfile(d,'biophys','biophysical_parameters'));
        dummy_biophysical_params =1;
    else
        dummy_biophysical_params =0;
    end
    
    
    %     % WMTI Parameters
    %     if(exist(fullfile(d,'biophys','WMTI'),'dir'))
    %         addpath(fullfile(d,'biophys','WMTI'));
    %         dummy_WMTI =1;
    %     else
    %         dummy_WMTI =0;
    %     end
    %

    % FA-VBS
    
    addpath(fullfile(d,'FAVBS_normalisation_toolbox'));
    FAVBSpath = [d filesep 'FAVBS_normalisation_toolbox'];
    addpath(fullfile(FAVBSpath,'Images2MNI'));
    
    % Tensor Fiber Density - REiser et al., NI, 2013
    
    if(exist(fullfile(d,'diffusion.forSia'),'dir'))
        addpath(genpath(fullfile(d,'diffusion.forSia')));
    end
    
    % check whether multi-hysco-field option exist
    
    path_hysco = [pd filesep 'HySCO'];
    if(exist(fullfile(path_hysco,'HySCO_write_multiB.m'),'file'))
        dummy_hysco_apply_multi = true;
    else
        dummy_hysco_apply_multi = false;
    end
else
    
    % Example_Batches
    
    addpath(fullfile(d,'Example_Batches'));
    
    % configuration
    
    addpath(fullfile(d,'config'));
    ld = [d filesep 'config'];
    addpath(fullfile(ld,'local'));
    
    % pre-processing
    
    pd = [d filesep 'Preprocessing'];
    if(exist(fullfile(pd,'resamplingtools'),'dir'))
        dummy_resampletool =1;
    else
        dummy_resampletool =0;
    end
    if(exist(fullfile(pd,'HySCO'),'dir'))
        EPIpath = [pd filesep 'HySCO'];
        dummy_EPI = 1;
    else
        dummy_EPI = 0;
    end
    
    % poas
    
    if(exist(fullfile(pd,'POAS'),'dir'))
        dummy_POAS = 1;
    else
        dummy_POAS = 0;
    end
    
    % tensor fit
    
    if(exist(fullfile(d,'LPF_correction_PROD'),'dir'))
        dummy_lpf = 1;
    else
        dummy_lpf = 0;
    end
    Tensorfitpath = [d filesep 'TensorFit'];
    
    % nonlinear fitting
    
    if(exist(fullfile(Tensorfitpath,'Nonlinear'),'dir'))
        addpath(fullfile(Tensorfitpath,['Nonlinear' filesep 'PROD']));
        dummy_nlfit = 1;
    else
        dummy_nlfit = 0;
    end
    
    % Kurtosis estimation
    
    if(exist(fullfile(Tensorfitpath,'Kurtosis'),'var'))
        dummy_DKIfiles = 1;
    else
        dummy_DKIfiles = 0;
    end
    
    % NODDI-DTI
    
    if(exist(fullfile(pd,'biophys','noddidti'),'dir'))
        dummy_noddidti =1;
    else
        dummy_noddidti =0;
    end
    
    % Biophysical_parameters
    
    if(exist(fullfile(pd,'biophys','biophysical_parameters'),'dir'))
        dummy_biophysical_params =1;
    else
        dummy_biophysical_params =0;
    end
    
    
    %     % WMTI
    %     if(exist(fullfile(pd,'biophys','WMTI'),'dir'))
    %         dummy_WMTI =1;
    %     else
    %         dummy_WMTI =0;
    %     end
    
    % Gauss Newton Fit
    
    if(exist(fullfile(pd,'TensorFit','Gauss_Newton_Fit'),'dir'))
        dummy_GN =1;
    else
        dummy_GN =0;
    end

    
    % Tensor Fiber Density - REiser et al., NI, 2013
    
    if(exist(fullfile(d,'diffusion.forSia'),'dir'))
        addpath(genpath(fullfile(d,'diffusion.forSia')));
    end
    
    % check whether multi-hysco-field option exist
    
    path_hysco = [d filesep 'HySCO'];
    if(exist(fullfile(path_hysco,'HySCO_write_multiB.m'),'file'))
        dummy_hysco_apply_multi = true;
    else
        dummy_hysco_apply_multi = false;
    end
end

% configuration

addpath(fullfile(d,'config'));
ld = [d filesep 'config'];
addpath(fullfile(ld,'local'));
config = tbx_main_acid_config;


% ACID_Test
addpath(fullfile(d,'ACID_Test'));
td = [d filesep 'ACID_Test'];
addpath(fullfile(td,'Batch_Files'));




% %% Begin FAVBS
% 
% % Begin favbs_norm
% favbs_acid = tbxdti_acid2_cfg_favbs_norm;
% 
% % Begin of pre-processing for FA-VBS
% Ima2MNI1 = tbxdti_acid_cfg_Ima2MNI;
% 
% 
% %% FAVBS choice
% favbs_choice         = cfg_choice;
% favbs_choice.tag     = 'favbs_choice';
% favbs_choice.name    = 'FA-FVBS options';
% favbs_choice.help    = {
%     'Pre-processing and normalisation options for FA-VBS'
%     }';
% favbs_choice.values  = {Ima2MNI1 favbs_acid};

%% LPF correction

if(dummy_lpf)
    
    % ---------------------------------------------------------------------
    % order of spherical harmonics
    % ---------------------------------------------------------------------
    Nl_lpf   = cfg_menu;
    Nl_lpf.tag     = 'Nl_lpf';
    Nl_lpf.name    = 'Order of spherical harmonics';
    Nl_lpf.help    = {'Choose the order of spherical harmonics. Note that currently only spherical harmonics up to the order of three are available.'};
    Nl_lpf.labels = {
        '1st order spherical harmonics'
        '2nd order spherical harmonics'
        '3rd order spherical harmonics'
        }';
    Nl_lpf.values = {0 1 2};
    Nl_lpf.val    = {2};
    
    % ---------------------------------------------------------------------
    % regularisation factor epsilon
    % ---------------------------------------------------------------------
    epsilon_lpf      = cfg_entry;
    epsilon_lpf.tag     = 'epsilon_lpf';
    epsilon_lpf.name    = 'regularisation factor';
    epsilon_lpf.help    = {'Regularisation factor. See Mohammadi et al., Neuroimage 2012 for details. Do not touch default value if you are not sure.'};
    epsilon_lpf.strtype = 'e';
    epsilon_lpf.num     = [1 1];
    epsilon_lpf.val     = {1/10};
    
    % ---------------------------------------------------------------------
    % percentage coverage of brain mask
    % ---------------------------------------------------------------------
    perc_lpf         = cfg_entry;
    perc_lpf.tag     = 'perc_lpf';
    perc_lpf.name    = 'brain mask parameter';
    perc_lpf.help    = {'Factor that depends on ratio between brain coverage and field-of-view. Less brain coverage of field-of-view means lower perc-value.'};
    perc_lpf.strtype = 'e';
    perc_lpf.num     = [1 1];
    perc_lpf.val     = {0.8};
    
    % ---------------------------------------------------------------------
    % b values
    % ---------------------------------------------------------------------
    b_vals_lpf         = cfg_entry;
    b_vals_lpf.tag     = 'b_vals_lpf';
    b_vals_lpf.name    = 'b-values';
    b_vals_lpf.help    = {'Provide a 1 x N  - matrix with b-values, b-values should appear in the same order as the low- and high-diffusion weighted images were entered. b-values is expected in units of s/mm^2.'
        'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'};
    b_vals_lpf.strtype = 'e';
    b_vals_lpf.num     = [1 Inf];
    b_vals_lpf.val     = {[5 1000 1000 2000]};
    
    % ---------------------------------------------------------------------
    % diffusion directions
    % ---------------------------------------------------------------------
    diff_dirs_lpf         = cfg_entry;
    diff_dirs_lpf.tag     = 'diff_dirs_lpf';
    diff_dirs_lpf.name    = 'Diffusion directions';
    diff_dirs_lpf.help    = {'Provide a 3 x N  - matrix with b-vectors, b-vectors should appear in the same order as the low- and high-diffusion weighted images were entered. The b-vectors are dimensionless.'
        'Entry should be a 3 x N vector. Each vector should be normalised. If directions are unknown for the low-bvalue images, you should provide an arbitrary direction. Note that the provided entry is only for illustration.'};
    diff_dirs_lpf.strtype = 'e';
    diff_dirs_lpf.num     = [3 Inf];
    diff_dirs_lpf.val     = {[1 0 0; 0 1 0; 0 0 1; 0 1/sqrt(2) 1/sqrt(2)]'};
    
    % ---------------------------------------------------------------------
    % in_vols phantom data
    % ---------------------------------------------------------------------
    in_vols_lpf_ph         = cfg_files;
    in_vols_lpf_ph.tag     = 'in_vols_lpf_ph';
    in_vols_lpf_ph.name    = 'Phantom DTI images';
    in_vols_lpf_ph.help    = {'Select high- and low-b-value images of the water phantom.'};
    in_vols_lpf_ph.filter = 'image';
    in_vols_lpf_ph.ufilter = '.*';
    in_vols_lpf_ph.num     = [0 Inf];
    
    
    % ---------------------------------------------------------------------
    % in_vols diffusion tensor
    % ---------------------------------------------------------------------
    in_vols_lpf         = cfg_files;
    in_vols_lpf.tag     = 'in_vols_lpf';
    in_vols_lpf.name    = 'DTI images';
    in_vols_lpf.help    = {'Select high- and low-b-value images of the diffusion data set that has to be correction for LPFs.'};
    in_vols_lpf.filter = 'image';
    in_vols_lpf.ufilter = '.*';
    in_vols_lpf.num     = [0 Inf];
    
end
%% NODDI-DTI

if(dummy_noddidti)
    acid_noddidti = tbx_cfg_acid_noddidti;
end

%% Biophysical Parameters

if(dummy_biophysical_params)
    acid_biophysical_params = tbx_cfg_acid_biophysical_params;
end

% if(dummy_WMTI)
%     acid_WMTI = tbx_cfg_acid_WMTI;
% end

%% Dthr

Dthr.val = acid_get_defaults('diffusion.dthr');

%% Multiple used params

[dummy_Tfreiburg, brain_msk, b_vals, diff_dirs, in_vols, RMatrix] = acid_tbx_mulitple_used_params;

%% begin defining tensor estimation

% -----------coviper correction--------------------------------------------

diff_coviper = tbxdti_acid_cfg_coviper(dummy_Tfreiburg, RMatrix, Dthr);

if(dummy_lpf)
    % -----------lpf correction------------------------------------------------
    diff_lpf         = cfg_exbranch;
    diff_lpf.tag     = 'diff_lpf';
    diff_lpf.name    = 'LPF correction of Diffusion Tensor';
    diff_lpf.val     = {in_vols_lpf in_vols_lpf_ph diff_dirs_lpf b_vals_lpf Nl_lpf perc_lpf epsilon_lpf};
    diff_lpf.help    = {'Least square tensor estimation using LPF Ellipsoid to correct for perturbation due to gradient inhomogeneities, miscalibration, etc.'
        'The LPF correction has been first suggested by Bammer et al., MRM 2003.'
        'The LPF estimation from a water phantom is described in Mohammadi et al., Neuroimage 2012.'
        'Note that this code is not optimised for RAM space. If the DTI data set is too big, you might get RAM problems.'
        'Please cite Mohammadi et al., Neuroimage 2012 (doi: 10.1016/j.neuroimage.2011.12.009) when using this code.'};
    diff_lpf.prog = @local_diff_lpf;
    diff_lpf.vout = @vout_diff_lpf;
end

%% DTI fitting

dti_choice         = cfg_choice;
dti_choice.tag     = 'dti_choice';
dti_choice.name    = 'Diffusion Tensor Imaging   (DTI)';
dti_choice.help    = {
    'Choose DTI as fitting method.'
    }';
if(dummy_lpf)
    dti_choice.values = {diff_robust diff_lpf diff_coviper diff_GN};
else
    
    dummy_dki_option  = 0;
    diff_robust = tbxdti_acid_dti_dki(dummy_DKIfiles, in_vols, diff_dirs, b_vals, RMatrix, brain_msk, dummy_Tfreiburg, Dthr, dummy_nlfit, dummy_dki_option);
    diff_robust.name  = 'OLS: DTI';
    
    fit_option      = 1;
    diff_GN         = tbxdti_acid_gauss_newton_DTI(fit_option);
    diff_GN.name    = 'Experimental stage: NLLS, DTI';
    
    
    dti_choice.values = {diff_robust diff_GN diff_coviper};
end

%% DKI fitting

dki_choice         = cfg_choice;
dki_choice.tag     = 'dki_choice';
dki_choice.name    = 'Diffusion Kurtosis Imaging (DKI)';
dki_choice.help    = {
    'Choose DKI as fitting method.'
    }';
if(dummy_lpf)
    dki_choice.values = {diff_robust diff_lpf diff_coviper diff_GN};
else
    dummy_dki_option  = 1;
    diff_robust = tbxdti_acid_dti_dki(dummy_DKIfiles, in_vols, diff_dirs, b_vals, RMatrix, brain_msk, dummy_Tfreiburg, Dthr, dummy_nlfit, dummy_dki_option);
    diff_robust.name  = 'OLS: DKI';
    
    fit_option      = 2;
    diff_GN         = tbxdti_acid_gauss_newton_DKI(fit_option);
    diff_GN.name    = 'Experimental stage: NLLS, DKI or Axial Symmetric DKI';
    
    
    dki_choice.values = {diff_robust diff_GN};
end

%% Diffusion signal fitting models

fit_choice         = cfg_choice;
fit_choice.tag     = 'fit_choice';
fit_choice.name    = 'Diffusion signal fitting models';
fit_choice.help    = {
    'Choose the tensor fitting method.'
    }';
if(dummy_lpf)
    fit_choice.values  = {diff_robust diff_lpf diff_coviper diff_GN};
else
    fit_choice.values  = {dti_choice dki_choice};
end

%% dummy whether 3d or 4d output

dummy_3dor4d          = cfg_menu;
dummy_3dor4d.tag     = 'dummy_3dor4d';
dummy_3dor4d.name    = 'Output format';
dummy_3dor4d.help    = {''
    'Specify whether data should be in 3d or 4d.'
    ''};
dummy_3dor4d.labels = {'3d output','4d output'};
dummy_3dor4d.values = {0 1};
dummy_3dor4d.val    = {0};

if(dummy_POAS)
    
    poas_choice = tbxdti_acid_poas(in_vols, brain_msk, diff_dirs, dummy_3dor4d);
    
end


if(dummy_EPI)
    fair_epi_choice = tbxdti_acid_hysco(dummy_3dor4d, dummy_hysco_apply_multi);
end

%% source

sources         = cfg_files;
sources.tag     = 'sources';
sources.name    = 'Source Images';
sources.help    = {'Select source images. These images will be registered to the sources image.'};
sources.filter = 'image';
sources.ufilter = '.*';
sources.num     = [0 Inf];

%% PRE-PROCESSING: ECMOCO

% Exbranch 1: Eddy-current and motion correction
%ecmoco_v1 = acid_tbx_cfg_ecmoco_v1;

% Exbranch 2: Eddy-current and motion correction
ecmoco_v2 = acid_tbx_cfg_ecmoco_v2;

% Exbranch 3: Write eddy current and motion correction
ecmoco_write = acid_tbx_cfg_ecmoco_write;

% Branch: ECMOCO
ecmo_choice      = cfg_choice;
ecmo_choice.tag  = 'ecmo_choice';
ecmo_choice.name = 'ECMOCO';

ecmo_choice.help = {
    'Estimate and write ECMOCO'
    'Write ECMOCO'
    }';
ecmo_choice.values  = {ecmoco_v2 ecmoco_write};

%% Resample tool

if(dummy_resampletool)
    resample2dim = tbxdti_acid_resample(sources);
end


% %% write nifti choice
% 
% rw3or4dnifti = tbx_cfg_readwrite_3_4dnitfi;
% 

%% Rician bias correction

powrice = tbx_cfg_powrice;

%% Preprocessing

prepro_choice         = cfg_choice;
prepro_choice.tag     = 'prepro_choice';
prepro_choice.name    = 'Pre-processing';
if(dummy_EPI && dummy_POAS && dummy_resampletool)
    prepro_choice.help    = {
        'Pre-processing of DTI images. Includes:'
        '- Displays and creates movies'
        '- Write and read 4d nifti'
        '- Interpolate whole DTI dataset to voxel size of choice.'
        '- Combine repetitions.'
        '- Rician bias correction on raw data'
        '- Eddy current (EC) and Motion correction'
        '- Hyperelastic Susceptibility Artifact Correction (HySCo)'
        '- Position orientation adaptive smoothing (POAS)'
        }';
    prepro_choice.values  = {powrice ecmo_choice fair_epi_choice poas_choice};
elseif(dummy_EPI && dummy_POAS)
    prepro_choice.help    = {
        'Pre-processing of DTI images. Includes:'
        '- Write and read 4d nifti'
        '- Eddy current (EC) and Motion correction'
        '- Hyperelastic Susceptibility Artifact Correction (HySCo)'
        '- Position orientation adaptive smoothing (POAS)'
        }';
    prepro_choice.values  = {ecmo_choice fair_epi_choice poas_choice};
elseif(dummy_EPI && ~dummy_POAS)
    prepro_choice.help    = {
        'Pre-processing of DTI images. Includes:'
        '- Write and read 4d nifti'
        '- Eddy current (EC) and Motion correction'
        '- Hyperelastic Susceptibility Artifact Correction (HySCo)'
        }';
    prepro_choice.values  = {ecmo_choice fair_epi_choice};
elseif(~dummy_EPI && dummy_POAS)
    prepro_choice.help    = {
        'Pre-processing of DTI images. Includes:'
        '- Write and read 4d nifti'
        '- Creation of brain mask'
        '- Eddy current (EC) and Motion correction'
        '- Position orientation adaptive smoothing (POAS)'
        }';
    prepro_choice.values  = {ecmo_choice poas_choice};
else
    prepro_choice.help    = {
        'Pre-processing of DTI images. Includes:'
        '- Write and read 4d nifti'
        '- Creation of brain mask'
        '- Eddy current (EC) and Motion correction'
        }';
    prepro_choice.values  = {ecmo_choice};
end

%% POST-PROCSESSING: Slice-wise realignment (manual)

% Exbranch 1: manual registration
realign_est = acid_tbx_cfg_realign_est;

% Exbranch 2: apply manual registration
realign_apply = acid_tbx_cfg_realign_apply;

% Branch: Slice-wise realignment
realign_choice        = cfg_choice;
realign_choice.tag    = 'realign_choice';
realign_choice.name   = 'Slice-wise realignment';
realign_choice.help   = {
    'Slice-wise realignment'
    'Write realigned files'
    }';
realign_choice.values = {realign_est realign_apply};

%% POST-PROCESSING: Reliability masking

% Exbranch 1: determine threshold for reliability masking
relmask_thr = acid_tbx_cfg_relmask_thr;

% Exbranch 2: create reliability masks
relmask_mask = acid_tbx_cfg_relmask_mask;

% Branch: Reliability masking
relmask_choice      = cfg_choice;
relmask_choice.tag  = 'relmask_choice';
relmask_choice.name = 'Reliability masking';
relmask_choice.help = {
    'Determine threshold for masking'
    'Create reliability masks'
    }';
relmask_choice.values = {relmask_thr relmask_mask};

%% Post-processing branch

postpro_choice        = cfg_choice;
postpro_choice.tag    = 'postpro_choice';
postpro_choice.name   = 'Post-processing';
postpro_choice.help   = {'Post-processing'};
postpro_choice.values = {realign_choice, relmask_choice};

% postpro_choice.values = {realign_choice, relmask_choice, favbs_choice};




%% UTILITIES: Inspect images

inspect = acid_tbx_cfg_inspect_images;

%% UTILITIES: Crop images

% Exbranch 1: Create new cropping
crop_new = acid_tbx_cfg_crop_images;

% Exbranch 2: Apply cropping
crop_apply = acid_tbx_cfg_crop_images_apply;

% Branch: image cropping
crop      = cfg_choice;
crop.tag  = 'crop';
crop.name = 'Crop images';
crop.help = {
    'New cropping'
    'Apply previous cropping'
    }';
crop.values  = {crop_new crop_apply};

%% UTILITIES: Checking registration

checkreg = acid_tbx_cfg_check_registration;

%% UTILITIES: Combine repetitions

combima = acid_tbx_cfg_combimages;

%% UTILITIES: ROI analysis

roianalysis = acid_tbx_cfg_roi_analysis;

%% UTILITIES: Create brain mask 

make_brainMSK = acid_tbx_cfg_make_brainmask;

%% ACID Utilities

misc_choice        = cfg_choice;
misc_choice.tag    = 'misc_choice';
misc_choice.name   = 'Utilities';
misc_choice.help   = {'ACID Utilities tools. It includes:'
    '- Displays and creates movies'
    '- Write and read 4d nifti''Miscellaneous'};
misc_choice.values = {inspect, crop, checkreg, combima, roianalysis, make_brainMSK, resample2dim};

%% DTI artefact correction

dti         = cfg_choice;
dti.tag     = 'dti';
dti.name    = 'ACID Toolbox';
dti.help    = {
    'Artefact Correction in Diffusion MRI - Version beta 01.'
    }';

%% biophysical models

if dummy_noddidti
    biophys_choice     = cfg_choice;
    biophys_choice.tag     = 'biophys_choice';
    biophys_choice.name    = 'Biophysical models';
    biophys_choice.help    = {
        'Choose the biophysical model.'
        }';
    
    fit_option      = 4;
    diff_BP         = tbxdti_acid_gauss_newton_biophysical(fit_option);
    diff_BP.name    = 'Experimental stage: NLLS, fit biophysical model directly';
    
%     biophys_choice.values  = { acid_noddidti diff_BP acid_biophysical_params };% activate to include the "fit biophysical model directly" option
     biophys_choice.values  = { acid_noddidti  acid_biophysical_params };
end

%% ACID batch
if  dummy_noddidti
    dti.values  = {prepro_choice fit_choice biophys_choice postpro_choice misc_choice config};
else
    dti.values  = {prepro_choice fit_choice postpro_choice misc_choice config};
end

%% Biophysical batch
% if  dummy_biophysical_params
%     dti.values  = {prepro_choice fit_choice biophysical_params_choice favbs_choice};
% else
%     dti.values  = {prepro_choice fit_choice favbs_choice};
% end


%% ----LPF correction-------------------
function out = local_diff_lpf(job)
LPFestim_sphharm_PROD(char(job.in_vols_lpf),char(job.in_vols_lpf_ph),job.diff_dirs_lpf,job.b_vals_lpf,job.Nl_lpf, job.perc_lpf, job.epsilon_lpf);
if(exist('spm_file','var'))
    out.FAfiles = spm_file(job.in_vols_lpf{1},'prefix','lpf_'); % to be adjusted
else
    out.FAfiles = my_spm_file(job.in_vols_lpf{1},'prefix','lpf_'); % to be adjusted
end
%------------------------
function dep = vout_diff_lpf(job) %to be adjusted
dep(1)            = cfg_dep;
dep(1).sname      = 'Diffusion Tensor measures';
dep(1).src_output = substruct('.','FAfiles'); % to be adjusted
dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}}); % to be adjusted


%% ---extra function
function varout = my_spm_file(varargin)
if(nargin>=1)
    filename=char(varargin{1});
    for i=1:size(filename,1)
        if(nargin>=3)
            options=varargin{2};
            prename=varargin{3};
            if(strcmp('prefix',options))
                [p,n,e] = spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[prename, n, e]);
                if(nargin>=4 && nargin<=5)
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[prename,n, endname]);
                    elseif(strcmp('ending',options))
                        varout{i}  = fullfile(p,[prename,n, endname, e]);
                    end
                elseif(nargin>=7)
                    options=varargin{4};
                    endname=varargin{5};
                    options2=varargin{6};
                    endname2=varargin{7};
                    if(strcmp('ending',options))
                        if(strcmp('format',options2))
                            varout{i}  = fullfile(p,[prename,n, endname, endname2]);
                        else
                            error('Error in assigning dependencies');
                        end
                    else
                        error('Error in assigning dependencies');
                    end
                end
            elseif(strcmp('ending',options))
                [p,n,e]=spm_fileparts(filename(i,:));
                varout{i}  = fullfile(p,[n prename e]);
                if(nargin>=5)
                    options=varargin{4};
                    endname=varargin{5};
                    if(strcmp('format',options))
                        [p,n,e]=spm_fileparts(filename(i,:));
                        varout{i}  = fullfile(p,[n prename endname]);
                    end
                end
            end
        else
            varout  = varargin{1};
        end
    end
end