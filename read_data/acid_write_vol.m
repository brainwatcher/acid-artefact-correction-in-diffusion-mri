function Vout = acid_write_vol(vol,V,prefix,ending,describe,dummy_bids)
%Vout = acid_write_vol(vol,V,prefix,ending,describe,dummy_bidsi)

% Vout = acid_write_vol(vol,V,prefix,dummy_bids)
% ==========================================================================================
% This script creates a nifti file from the 3d or 4d matrix received in the
% input (vol) and using the header of V.
%
% Inputs:
%   vol      - 3d or 4d matrix, to be written as a nifti file
%   V        - reference header used to create the header of the output nifti file 
%   prefix   - prefix of the output nifti file
%   ending   - postfix of the output nifti file
%   describe - description entry of the header
%
% Output:
%   Vout    - header of the output nifti file
%   Plus, a nifti file is saved with the specified name.
%
% S. Mohammadi 11/02/2014
% ==========================================================================================
if ~exist('dummy_bids','var')
    dummy_bids = 0;
end
[pth,fname,~] = spm_fileparts(V.fname);
wV      = V;
%dt      = [spm_type('float32'),spm_platform('bigend')];
dt      = wV.dt;
dm      = V(1).dim;
Ni      = nifti;
Ni.mat  = V(1).mat;
Ni.mat0 = V(1).mat;

if exist('describe','var')
    Ni.descrip = describe;
end

% for 3D data
if numel(size(vol))==3
    
    if ~exist('ending','var')
        wV.fname = fullfile(pth,[prefix fname '.nii']);
        Ni.dat   = file_array(wV.fname,dm,dt, 0,1,0);
    else
        wV.fname = fullfile(pth,[prefix fname ending '.nii']);
        Ni.dat   = file_array(wV.fname,dm,dt, 0,1,0);
    end
    create(Ni);
    spm_progress_bar('Init',dm(3),Ni.descrip,'planes completed');
    for p=1:size(vol,3)
        Ni.dat(:,:,p) = vol(:,:,p);
        spm_progress_bar('Set',p);
    end
    
% for 4D data
elseif numel(size(vol))==4
    
    if dummy_bids == 1
        if contains("_desc", fname)
            fname = [fname(1:end-8) '-' keyword '_dwi'];
        else
            fname = [fname(1:end-8) '_desc-' keyword '_dwi'];
        end
        if ~exist('ending','var')
            wV.fname = fullfile(pth, filesep, 'derivates', filesep ,[fname '.nii']);
            Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
        else
            wV.fname = fullfile(pth, filesep, 'derivates', filesep ,[fname ending '.nii']);
            Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
        end
    else
        if ~exist('ending','var')
            wV.fname = fullfile(pth,[prefix fname '.nii']);
            Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
        else
            wV.fname = fullfile(pth,[prefix fname ending '.nii']);
            Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
        end
    end
    
    create(Ni);
    spm_progress_bar('Init',size(vol,4),Ni.descrip,'volumes completed');
    for p = 1:size(vol,4)
        Ni.dat(:,:,:,p) = vol(:,:,:,p);
        spm_progress_bar('Set',p);
    end
end

spm_progress_bar('Clear');
Vout = spm_vol(wV.fname);

end