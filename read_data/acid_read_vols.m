% ========================================================================
% function Aout = acid_read_vols(V,VG,res,p)
%
% read image volume
% 
% Input:
%   V      - structure containing image volume information of ith image
%   VG     - structure containing image volume information of target image
%   res    - resampling function / order of sinc (-7 recommended)
%   p      - z position
%
% Output:
% Aout     - 2D or 3D image in the space of the target image VG 
% ========================================================================
% S.Mohammadi 06.05.2020
function Aout = acid_read_vols(V,VG,res,p)
    dm      = VG.dim;
    if(exist('p','var'))
        M = VG.mat*spm_matrix([0 0 p]);
        Aout = spm_slice_vol(V,V.mat\M,dm(1:2),res);
    else
        Aout    = zeros(dm);
        for p=1:dm(3)
            M = VG.mat*spm_matrix([0 0 p]);
            Aout(:,:,p) = spm_slice_vol(V,V.mat\M,dm(1:2),res);
        end
    end
end

