function Vout = acid_write_data(data,V,prefix,dm,MSKidx,ending)

% ==========================================================================================
% This script creates a nifti file from the 3d or 4d matrix received in the
% input (dat/a) and using the header of V.
%
% Inputs:
%   data     - 1D file
%   V        - reference header used to create the header of the output nifti file 
%   prefix   - prefix of the output nifti file
%   dm       - dimensions of the matrix, 1x3 or 1x4 array
%   MSKidx   - list of indices for masking
%   ending   - postfix of the output nifti file
%
% Output:
%   Vout    - header of the output nifti file
%   Plus, a nifti file is saved with the specified name.
%
% S. Mohammadi 12/07/2014
% ==========================================================================================

vol = zeros(dm);
vol(MSKidx) = data;

[pth,fname,~] = spm_fileparts(V.fname);
wV = V;

dt      = [spm_type('float32'),spm_platform('bigend')];
dm      = V(1).dim;
Ni      = nifti;
Ni.mat  = V(1).mat;
Ni.mat0 = V(1).mat;

% for 3D data
if numel(size(vol))==3
    if ~exist('ending','var')
        wV.fname = fullfile(pth,[prefix fname '.nii']);
        Ni.dat   = file_array(wV.fname,dm,dt, 0,1,0);
    else
        wV.fname = fullfile(pth,[prefix fname ending '.nii']);
        Ni.dat   = file_array(wV.fname,dm,dt, 0,1,0);
    end
    create(Ni);
    spm_progress_bar('Init',dm(3),Ni.descrip,'planes completed');
    for p=1:size(vol,3)
        Ni.dat(:,:,p) = vol(:,:,p);
        spm_progress_bar('Set',p);
    end
    
% for 4D data
elseif numel(size(vol))==4
    if ~exist('ending','var')
        wV.fname = fullfile(pth,[prefix fname '.nii']);
        Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
    else
        wV.fname = fullfile(pth,[prefix fname ending '.nii']);
        Ni.dat   = file_array(wV.fname,size(vol),dt, 0,1,0);
    end
    create(Ni);
    spm_progress_bar('Init',size(vol,4),Ni.descrip,'volumes completed');
    for p = 1:size(vol,4)
        Ni.dat(:,:,:,p) = vol(:,:,:,p);
        spm_progress_bar('Set',p);
    end
end

spm_progress_bar('Clear');
Vout = spm_vol(wV.fname);

end