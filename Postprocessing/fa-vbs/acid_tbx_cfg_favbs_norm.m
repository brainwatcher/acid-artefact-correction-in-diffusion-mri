function favbs_acid = acid_tbx_cfg_favbs_norm
% 'FAVBS Normalisation' - MATLABBATCH configuration
% This MATLABBATCH configuration file has been generated automatically
% by MATLABBATCH using ConfGUI. It describes menu structure, validity
% constraints and links to run time code.
% Changes to this file will be overwritten if the ConfGUI batch is executed again.
% Created at 2009-12-31 00:30:17.

% PFA FA Images
favbs_PFA         = cfg_files;
favbs_PFA.tag     = 'favbs_PFA';
favbs_PFA.name    = 'FA Images';
favbs_PFA.help    = {'Enter the FA image for all subjects.'};
favbs_PFA.filter  = 'image';
favbs_PFA.ufilter = '.*';
favbs_PFA.num     = [1 Inf];

% Pb0 b0 Images
favbs_Pb0         = cfg_files;
favbs_Pb0.tag     = 'favbs_Pb0';
favbs_Pb0.name    = 'b0 Images';
favbs_Pb0.help    = {'Enter the b0 image for all subjects.'};
favbs_Pb0.filter = 'image';
favbs_Pb0.ufilter = '.*';
favbs_Pb0.num     = [0 Inf];

% Pmean Mean DW Images
favbs_Pmean         = cfg_files;
favbs_Pmean.tag     = 'favbs_Pmean';
favbs_Pmean.name    = 'Mean DW Images';
favbs_Pmean.help    = {'Enter the mean DW image for all subjects.'};
favbs_Pmean.filter  = 'image';
favbs_Pmean.ufilter = '.*';
favbs_Pmean.num     = [0 Inf];

% NCT CT: No LFA Registration
favbs_NCT         = cfg_const;
favbs_NCT.tag     = 'favbs_NCT';
favbs_NCT.name    = 'CT: No LFA Registration';
favbs_NCT.val = {0};

% SCT SCT: Final LFA-Registration?
favbs_SCT         = cfg_menu;
favbs_SCT.tag     = 'favbs_SCT';
favbs_SCT.name    = 'SCT: Finale LFA-Registration?';
favbs_SCT.labels = {
              'off'
              'on'
              }';
favbs_SCT.values = {
              0
              1
              }';

% CT CT: Normal or Symmetrisized (NCT or SCT)?
favbs_CT         = cfg_choice;
favbs_CT.tag     = 'favbs_CT';
favbs_CT.name    = 'CT: Normal or Symmetrisized (NCT or SCT)?';
favbs_CT.values  = {favbs_NCT favbs_SCT };

% PGb0 External b0 Template
favbs_PGb0         = cfg_files;
favbs_PGb0.tag     = 'favbs_PGb0';
favbs_PGb0.name    = 'External b0 Template';
favbs_PGb0.help    = {'Enter external template (or done for none)'};
favbs_PGb0.filter  = 'image';
favbs_PGb0.ufilter = '.*';
favbs_PGb0.num     = [0 1];

% PGFA External FA Template
favbs_PGFA         = cfg_files;
favbs_PGFA.tag     = 'favbs_PGFA';
favbs_PGFA.name    = 'External FA Template';
favbs_PGFA.help    = {'Enter external template (or done for none)'};
favbs_PGFA.filter  = 'image';
favbs_PGFA.ufilter = '.*';
favbs_PGFA.num     = [0 1];

% PGLFA External LFA Template
favbs_PGLFA         = cfg_files;
favbs_PGLFA.tag     = 'favbs_PGLFA';
favbs_PGLFA.name    = 'External LFA Template';
favbs_PGLFA.help    = {'Enter external template (or done for none)'};
favbs_PGLFA.filter  = 'image';
favbs_PGLFA.ufilter = '.*';
favbs_PGLFA.num     = [0 1];

% SC Single-Contrast: b0, FA or LFA
favbs_SC         = cfg_choice;
favbs_SC.tag     = 'favbs_SC';
favbs_SC.name    = 'Single-Contrast: b0, FA or LFA';
favbs_SC.help    = {'First Entry: b0, second: FA, third: LFA'};

favbs_SC.values  = {favbs_PGb0 favbs_PGFA favbs_PGLFA };

% PGb0 External b0 Template
favbs_PGb0         = cfg_files;
favbs_PGb0.tag     = 'favbs_PGb0';
favbs_PGb0.name    = 'External b0 Template';
favbs_PGb0.help    = {'Enter external template (or done for none)'};
favbs_PGb0.filter  = 'image';
favbs_PGb0.ufilter = '.*';
favbs_PGb0.num     = [0 1];

% PGFA External FA Template
favbs_PGFA         = cfg_files;
favbs_PGFA.tag     = 'favbs_PGFA';
favbs_PGFA.name    = 'External FA Template';
favbs_PGFA.help    = {'Enter external template (or done for none)'};
favbs_PGFA.filter  = 'image';
favbs_PGFA.ufilter = '.*';
favbs_PGFA.num     = [0 1];

% PGLFA External LFA Template
favbs_PGLFA         = cfg_files;
favbs_PGLFA.tag     = 'favbs_PGLFA';
favbs_PGLFA.name    = 'External LFA Template';
favbs_PGLFA.help    = {'Enter external template (or done for none)'};
favbs_PGLFA.filter  = 'image';
favbs_PGLFA.ufilter = '.*';
favbs_PGLFA.num     = [0 1];

% MC Multi-Contrast: b0, FA and LFA
favbs_MC      = cfg_branch;
favbs_MC.tag  = 'favbs_MC';
favbs_MC.name = 'Multi-Contrast: b0, FA and LFA';
favbs_MC.val  = {favbs_PGb0 favbs_PGFA favbs_PGLFA };
favbs_MC.help = {'First Entry: b0, second: FA, third: LFA (selection of each template is optional: "done for none")'};

% ET ET: Single-Contrast or Multi-Contrast (SCT or MCT)?
favbs_ET        = cfg_choice;
favbs_ET.tag    = 'favbs_ET';
favbs_ET.name   = 'ET: Single-Contrast or Multi-Contrast (SCT or MCT)?';
favbs_ET.values = {favbs_SC favbs_MC };

% templ Customized or External Template (CT or ET)?
favbs_templ        = cfg_choice;
favbs_templ.tag    = 'favbs_templ';
favbs_templ.name   = 'Customized or External Template (CT or ET)?';
favbs_templ.values = {favbs_CT favbs_ET };

% b0 b0
favbs_b0      = cfg_const;
favbs_b0.tag  = 'favbs_b0';
favbs_b0.name = 'b0';
favbs_b0.val  = {1};

% FA FA
favbs_FA         = cfg_const;
favbs_FA.tag     = 'favbs_FA';
favbs_FA.name    = 'FA';
favbs_FA.val = {0};

% steps Normalisation Step(s)
favbs_steps         = cfg_repeat;
favbs_steps.tag     = 'favbs_steps';
favbs_steps.name    = 'Normalisation Step(s)';
favbs_steps.values  = {favbs_b0 favbs_FA };
favbs_steps.num     = [1 Inf];
favbs_steps.forcestruct = true;

% niter Number of Iterations
favbs_niter         = cfg_entry;
favbs_niter.tag     = 'favbs_niter';
favbs_niter.name    = 'Number of Iterations';
favbs_niter.strtype = 'w';
favbs_niter.num     = [1  1];
favbs_niter.val     = {2};

% branch
favbs_norm      = cfg_branch;
favbs_norm.tag  = 'favbs_norm';
favbs_norm.name = 'Normalisation Procedure';
favbs_norm.val  = {favbs_templ favbs_steps favbs_niter };
favbs_norm.help = {'In the first entry the choice between customized and external template (CT or ET) is performed.'};

% percentage coverage of brain mask
favbs_MSKperc         = cfg_entry;
favbs_MSKperc.tag     = 'favbs_MSKperc';
favbs_MSKperc.name    = 'brain mask parameter';
favbs_MSKperc.help    = {'Factor that depends on ratio between brain coverage and field-of-view. Less brain coverage of field-of-view means lower perc-value.'};
favbs_MSKperc.strtype = 'e';
favbs_MSKperc.num     = [1 1];
favbs_MSKperc.val     = {0.8};

% percentage coverage of brain mask
favbs_cutoff         = cfg_entry;
favbs_cutoff.tag     = 'favbs_cutoff';
favbs_cutoff.name    = 'Cutoff in mm';
favbs_cutoff.help    = {'This measure is inverse proportional to the number of cosines used for normalization. '};
favbs_cutoff.strtype = 'e';
favbs_cutoff.num     = [1 1];
favbs_cutoff.val     = {25};

% Normalization for spinal cord DTI - yes - no
favbs_SCDTI      = cfg_menu;
favbs_SCDTI.tag  = 'favbs_SCDTI';
favbs_SCDTI.name = 'Use the spinal cord modus';
favbs_SCDTI.help = {'This option is still under construction - it does not work at the moment!'}; 
% Choose whether you want to use the spinal cord modus. This modus works best if the MD contrast is used under b0 normalization.'
%                         };
favbs_SCDTI.labels    = {
               'YES'
               'NO'
}';
favbs_SCDTI.values    = {1 0};
favbs_SCDTI.val       = {0};
% ---------------------------------------------------------------------
% favbs_norm FAVBS Normalisation
% ---------------------------------------------------------------------
favbs_acid      = cfg_exbranch;
favbs_acid.tag  = 'favbs_acid';
favbs_acid.name = 'FA-VBS Normalisation toolbox';
favbs_acid.val  = {favbs_PFA favbs_Pb0 favbs_Pmean favbs_norm favbs_MSKperc favbs_SCDTI favbs_cutoff};
% favbs_acid.check   = @(job)tbxdti_run_favbs_norm('check','files',job);
favbs_acid.prog = @(job)tbxdti_acid2_run_favbs_norm('run',job);
favbs_acid.vout = @(job)tbxdti_acid2_run_favbs_norm('vout',job);
