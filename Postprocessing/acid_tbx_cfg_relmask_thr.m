function relmask_thr = acid_tbx_cfg_relmask_thr

% model-fit error maps
relmask_thr_errmaps         = cfg_files;
relmask_thr_errmaps.tag     = 'relmask_thr_errmaps';
relmask_thr_errmaps.name    = 'Model-fit error maps';
relmask_thr_errmaps.help    = {'Select the model-fit error maps.'};
relmask_thr_errmaps.filter  = 'image';
relmask_thr_errmaps.ufilter = '.*';
relmask_thr_errmaps.num     = [0 Inf];

% FA maps
relmask_thr_famaps         = cfg_files;
relmask_thr_famaps.tag     = 'relmask_thr_famaps';
relmask_thr_famaps.name    = 'FA maps';
relmask_thr_famaps.help    = {'Select the FA maps.'};
relmask_thr_famaps.filter  = 'image';
relmask_thr_famaps.ufilter = '.*';
relmask_thr_famaps.num     = [0 Inf];

% subject-specific masks
relmask_thr_masks         = cfg_files;
relmask_thr_masks.tag     = 'relmask_thr_masks';
relmask_thr_masks.name    = 'Subject-specific masks';
relmask_thr_masks.help    = {'Select subject-specific masks (or Done for none).'};
relmask_thr_masks.filter  = 'image';
relmask_thr_masks.ufilter = '.*';
relmask_thr_masks.num     = [0 Inf];

% global mask
relmask_thr_maskGLO         = cfg_files;
relmask_thr_maskGLO.tag     = 'relmask_thr_maskGLO';
relmask_thr_maskGLO.name    = 'Global mask';
relmask_thr_maskGLO.help    = {'Select a global mask (or Done for none).'};
relmask_thr_maskGLO.filter  = 'image';
relmask_thr_maskGLO.ufilter = '.*';
relmask_thr_maskGLO.num     = [0 Inf];
%{
% start
relmask_thr_errmin         = cfg_entry;
relmask_thr_errmin.tag     = 'relmask_thr_errmin';
relmask_thr_errmin.name    = 'Lowest tested model-fit error.';
relmask_thr_errmin.help    = {'Lowest tested model-fit error.'};
relmask_thr_errmin.strtype = 'r';
relmask_thr_errmin.num     = [1 1];
relmask_thr_errmin.val     = {1};

% end
relmask_thr_errmax         = cfg_entry;
relmask_thr_errmax.tag     = 'relmask_thr_errmax';
relmask_thr_errmax.name    = 'Highest tested model-fit error.';
relmask_thr_errmax.help    = {'Highest tested model-fit error.'};
relmask_thr_errmax.strtype = 'r';
relmask_thr_errmax.num     = [1 1];
relmask_thr_errmax.val     = {3};

% number of steps
relmask_thr_nsteps          = cfg_entry;
relmask_thr_nsteps.tag      = 'relmask_thr_nsteps';
relmask_thr_nsteps.name    = 'Number of steps.';
relmask_thr_nsteps.help    = {'Number of steps.'};
relmask_thr_nsteps.strtype = 'r';
relmask_thr_nsteps.num     = [1 1];
relmask_thr_nsteps.val     = {100};
%}
% display figure
relmask_thr_plot           = cfg_menu;
relmask_thr_plot.tag       = 'relmask_thr_plot';
relmask_thr_plot.name      = 'Choose display option';
relmask_thr_plot.help      = {'Choose whether you want to see the plot.'
                       'default: ON'
                         };
relmask_thr_plot.labels    = {
                                'ON'
                                'OFF'
                                    }';
relmask_thr_plot.values    = {1 0};
relmask_thr_plot.val       = {1};

% output folder
relmask_thr_folder         = cfg_files;
relmask_thr_folder.tag     = 'relmask_thr_folder';
relmask_thr_folder.name    = 'Output folder';
relmask_thr_folder.help    = {'The threshold value in model-fit error above which voxels are rejected.'
                   'This can be either set to an arbitrary value or to a value determined automatically.'};
relmask_thr_folder.filter  = 'dir';
relmask_thr_folder.ufilter = '.*';
relmask_thr_folder.num     = [0 Inf];

%{
% relmask_thr defaults
relmask_thr_defaults          = cfg_branch;
relmask_thr_defaults.tag     = 'relmask_thr_defaults';
relmask_thr_defaults.name    = 'Defaults';
relmask_thr_defaults.val     = {relmask_thr_errmin, relmask_thr_errmax, relmask_thr_nsteps, relmask_thr_plot, relmask_thr_folder};
relmask_thr_defaults.help    = {'These parameters are for more advanced users. For details please contact the autor'};
%}

% exbranch
relmask_thr      = cfg_exbranch;
relmask_thr.tag  = 'relmask_thr';
relmask_thr.name = 'Determine threshold';
relmask_thr.help = {'Post-processing'};
relmask_thr.val  = {relmask_thr_errmaps, relmask_thr_famaps, relmask_thr_masks, relmask_thr_maskGLO, relmask_thr_plot, relmask_thr_folder};
relmask_thr.prog = @local_REL_determine_thr;
relmask_thr.vout = @vout_REL_determine_thr;

end

function out = local_REL_determine_thr(job)

    relmask_thr_errmin = acid_get_defaults('relmask.relmask_thr_errmin');
    relmask_thr_errmax = acid_get_defaults('relmask.relmask_thr_errmax');
    relmask_thr_nsteps = acid_get_defaults('relmask.relmask_thr_nsteps');

    acid_relmask_determine_thr(char(job.relmask_thr_errmaps), char(job.relmask_thr_famaps), char(job.relmask_thr_masks), ...
        char(job.relmask_thr_maskGLO), relmask_thr_errmin, relmask_thr_errmax,...
        relmask_thr_nsteps, job.relmask_thr_plot, char(job.relmask_thr_folder));
    out.errmaps = job.relmask_thr_errmaps(:);
    if numel(char(job.relmask_thr_folder))==0
        [p,~,~] = fileparts(job.relmask_thr_errmaps{1});
    else
        p = char(job.relmask_thr_folder);
    end
    out.thr = load([p filesep 'threshold_for_relmasking.mat']);
    out.thr = out.thr.thr;
end

function dep = vout_REL_determine_thr(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Model-fit error maps';
    dep(1).src_output = substruct('.','errmaps'); 
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'Threshold for reliability masking';
    dep(2).src_output = substruct('.','thr');
    dep(2).tgt_spec   = cfg_findspec({{'strtype','e'}});
end