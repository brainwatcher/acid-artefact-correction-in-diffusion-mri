function relmask_mask = acid_tbx_cfg_relmask_mask

% model-fit error maps
relmask_mask_errmaps         = cfg_files;
relmask_mask_errmaps.tag     = 'relmask_mask_errmaps';
relmask_mask_errmaps.name    = 'Model-fit error maps';
relmask_mask_errmaps.help    = {'Select the model-fit error maps.'};
relmask_mask_errmaps.filter  = 'image';
relmask_mask_errmaps.ufilter = '.*';
relmask_mask_errmaps.num     = [0 Inf];

% threshold for reliability masking
relmask_mask_errthr         = cfg_entry;
relmask_mask_errthr.tag     = 'relmask_mask_errthr';
relmask_mask_errthr.name    = 'Threshold for reliability masking';
relmask_mask_errthr.help    = {'The threshold value in model-fit error above which voxels are rejected.'
                   'This can be either set to an arbitrary value or to a value determined automatically.'};
relmask_mask_errthr.strtype = 'e';
relmask_mask_errthr.num     = [1 1];
relmask_mask_errthr.val     = {};

% output folder for reliability masking
relmask_mask_folder         = cfg_files;
relmask_mask_folder.tag     = 'relmask_mask_folder';
relmask_mask_folder.name    = 'Output folder';
relmask_mask_folder.help    = {'The threshold value in model-fit error above which voxels are rejected.'
                   'This can be either set to an arbitrary value or to a value determined automatically'};
relmask_mask_folder.filter  = 'dir';
relmask_mask_folder.ufilter = '.*';
relmask_mask_folder.num     = [0 Inf];

% prefix of reliability masks
relmask_mask_prefix         = cfg_entry;
relmask_mask_prefix.tag     = 'relmask_mask_prefix';
relmask_mask_prefix.name    = 'Prefix';
relmask_mask_prefix.help    = {'Specify a prefix for the binary reliability masks.'};
relmask_mask_prefix.strtype = 's';
relmask_mask_prefix.val     = {'MRel_'};

% exbranch
relmask_mask      = cfg_exbranch;
relmask_mask.tag  = 'relmask_mask';
relmask_mask.name = 'Create reliability masks';
relmask_mask.help = {'Post-processing'};
relmask_mask.val  = {relmask_mask_errmaps, relmask_mask_errthr, relmask_mask_folder, relmask_mask_prefix};
relmask_mask.prog = @local_REL_create_mask;
relmask_mask.vout = @vout_REL_create_mask;

end

function out = local_REL_create_mask(job)
    acid_relmask_create_mask(char(job.relmask_mask_errmaps), job.relmask_mask_errthr, char(job.relmask_mask_folder), char(job.relmask_mask_prefix));
    out.masks = acid_spm_file(char(job.relmask_mask_errmaps(:)),'prefix',char(job.relmask_mask_prefix),'format','.nii');
    out.masks = acid_select(out.masks(:));
    out.masks = out.masks(:);
end

function dep = vout_REL_create_mask(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Reliability masks';
    dep(1).src_output = substruct('.','masks');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end