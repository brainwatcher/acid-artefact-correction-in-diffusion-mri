function realign_est = acid_tbx_cfg_realign_est

% source image
realign_est_source         = cfg_files;
realign_est_source.tag     = 'realign_est_source';
realign_est_source.name    = 'Source image';
realign_est_source.help    = {'Select the image to be realigned.'};
realign_est_source.filter  = 'image';
realign_est_source.ufilter = '.*';
realign_est_source.num     = [1 1];

% target image
realign_est_target         = cfg_files;
realign_est_target.tag     = 'realign_est_target';
realign_est_target.name    = 'Target image';
realign_est_target.help    = {'Select the reference image. This image will remain stationary during the realignment process.'};
realign_est_target.filter  = 'image';
realign_est_target.ufilter = '.*';
realign_est_target.num     = [1 1];

% other images
realign_est_others         = cfg_files;
realign_est_others.tag     = 'realign_est_others';
realign_est_others.name    = 'Other images';
realign_est_others.help    = {'Select images to apply slicwise transformation (should be alligned with the source image).'};
realign_est_others.filter  = 'image';
realign_est_others.ufilter = '.*';
realign_est_others.num     = [0 Inf];

% prefix
realign_est_prefix         = cfg_entry;
realign_est_prefix.tag     = 'realign_est_prefix';
realign_est_prefix.name    = 'Prefix for output';
realign_est_prefix.help    = {'The threshold value in model-fit error above which voxels are rejected.'};
realign_est_prefix.strtype = 's';
realign_est_prefix.val     = {'r'};

% exbranch
realign_est = cfg_exbranch;
realign_est.tag    = 'realign_est';
realign_est.name   = 'Slice-wise realignment';
realign_est.help   = {'Slice-wise realignment'};
realign_est.val = {realign_est_source realign_est_target realign_est_others realign_est_prefix};
realign_est.prog = @local_realign_slices;
realign_est.vout = @vout_realign_slices;

end

function out = local_realign_slices(job)
    acid_realign_slices(char(job.realign_est_source),char(job.realign_est_target),char(job.realign_est_others),job.realign_est_prefix);    
    out.rfiles = [acid_spm_file(char(job.realign_est_source),'prefix',char(job.realign_est_prefix),'format','.nii'),...
                  acid_spm_file(char(job.realign_est_others),'prefix',char(job.realign_est_prefix),'format','.nii')];
    out.rfiles = acid_select(out.rfiles(:));
    out.rfiles = out.rfiles(:);
    out.matfile = acid_spm_file(char(job.realign_est_source),'prefix','Mat_','format','.mat');
end

function dep = vout_realign_slices(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resliced images';
    dep(1).src_output = substruct('.','rfiles');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
    dep(2)            = cfg_dep;
    dep(2).sname      = 'Transformation parameters';
    dep(2).src_output = substruct('.','matfile');
    dep(2).tgt_spec   = cfg_findspec({{'filter','r','strtype','e'}});    
end