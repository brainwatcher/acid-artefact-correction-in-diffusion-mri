function acid_realign_slices(PS,PT,PO,prefix)

% -------------------------------------------------------------------------
% The script allows the user to realign images in a slice-wise manner.
%
% Input:
%       PS - source image
%       PT - target image
%       PO - other images
%       prefix - prefix for outpu
%
% Output:
%       realigned images with the specified prefix
%
% -------------------------------------------------------------------------

% load in source image
if(~exist('PS','var'))
    PS = char(cfg_getfile(1,'any','Select Source','','','.*'));
end

% load in target image
if(~exist('PT','var'))
    PT = char(cfg_getfile(1,'any','Select Target','','','.*'));
end

% load in other images (same transformation will be applied as on the source image)
if(~exist('PO','var'))
    PO = char(cfg_getfile(Inf,'any','Select other images','','','.*'));
end

% Defaults
if(~exist('prefix','var'))
    prefix = 'r';
end

interp_def = -4;
if(~exist('slice','var'))
    slice = 1;
end
subj = 1;
cax  = [0 1];

% load in source image
VS = spm_vol(PS);

% load in target image
VT    = spm_vol(PT);
dm_SM = VT.dim;
vol   = readvol(dm_SM,VT,VT,interp_def,[],slice);

% load in other images
VO = spm_vol(PO);

% params contains the degree of freedom (max 12) for each slice.
% Scaling in each dimension is allowed for all slices.
params = zeros(dm_SM(3),12);
params(:,7:9) = ones(dm_SM(3),3);

figure(2); clf;
subplot(1,2,1)
imagesc(vol);
caxis(cax);
colormap gray
axis off
title('Reference')

alpha = [1e2 1e-2];
alpha2 = [1 1e-2];
a1Range = [-100 100];
alphaSTART1 = a1Range(1);
a2Range = [-0.5 0.5];
alphaSTART2 = a2Range(1);

%% GUI OBJECTS
% sliders for Y (Ya1Slider - translation, Ya2Slider - scaling)
Ya1Slider = uicontrol('Style','slider','Min',a1Range(1),'Max',a1Range(2),...
    'SliderStep',[.25 .25]./(a1Range(2)-a1Range(1)),'Value',log10(alpha(1)),...
    'Position',[180 45 300 25],'Callback',@Callback_Ya1Slider);

Ya2Slider = uicontrol('Style','slider','Min',a2Range(1),'Max',a2Range(2),...
    'SliderStep',1e-1*[.25 .25]./(a2Range(2)-a2Range(1)),'Value',log10(1+alpha2(1)),...
    'Position',[180 15 300 25],'Callback',@Callback_Ya2Slider);

% sliders for X (Xa1Slider - translation, Xa2Slider - scaling)
Xa1Slider = uicontrol('Style','slider','Min',a1Range(1),'Max',a1Range(2),...
    'SliderStep',[.25 .25]./(a1Range(2)-a1Range(1)),'Value',log10(alpha(1)),...
    'Position',[720 45 300 25],'Callback',@Callback_Xa1Slider);

Xa2Slider = uicontrol('Style','slider','Min',a2Range(1),'Max',a2Range(2),...
    'SliderStep',1e-1*[.25 .25]./(a2Range(2)-a2Range(1)),'Value',log10(1+alpha2(1)),...
    'Position',[720 15 300 25],'Callback',@Callback_Xa2Slider);

% slider for slice selection
sliceSlider = uicontrol('Style','slider','Min',1,'Max',dm_SM(3),...
    'SliderStep',[1 1]/(dm_SM(3)-1),'Value',slice,...
    'Position',[80 270 40 400]);
set(sliceSlider,'Callback',@Callback_sliceSlider);

% button for applying transformation
cax1Button = uicontrol('Style','push',...
    'Position', [1100 20 130 60], 'FontWeight', 'bold','string','Apply Transformation','Callback',@Callback_applytransf);

% texts on the interface
sliceText = uicontrol('Style','text',...
    'String',['slice=' num2str(slice)],...
    'Position',[50 680 100 20], 'FontSize', 15, 'FontWeight', 'bold');
Ya1Text = uicontrol('Style','text',...
    'String',['y-position=' num2str(alpha(1)+alphaSTART1)],...
    'Position',[40 47 120 20], 'FontSize', 15, 'FontWeight', 'bold');
Ya2Text = uicontrol('Style','text',...
    'String',['y-scaling=' num2str(1+alpha2(1))],...
    'Position',[40 17 120 20], 'FontSize', 15, 'FontWeight', 'bold');
Xa1Text = uicontrol('Style','text',...
    'String',['x-position=' num2str(alpha(1)+alphaSTART1)],...
    'Position',[580 47 120 20], 'FontSize', 15, 'FontWeight', 'bold');
Xa2Text = uicontrol('Style','text',...
    'String',['x-scaling=' num2str(1+alpha2(1))],...
    'Position',[580 17 120 20], 'FontSize', 15, 'FontWeight', 'bold');

%%
% plot source and target images
showReg();


%% FUNCTION DEFINITIONS

% function for plotting images
    function showReg()
        subplot(1,2,1);
        title(sprintf('Target'), 'FontSize', 20, 'FontWeight', 'bold');
        hold on;
        vol1 = readvol(dm_SM,VT,VT,interp_def,[],slice);
        imagesc(vol1);
        caxis(cax);
        
        subplot(1,2,2)
        imagesc(vol);
        title(sprintf('Source'), 'FontSize', 20, 'FontWeight', 'bold');
        caxis(cax);
        axis off
        subplot(1,2,1);
        hold on;
        contour(vol,'r');
    end

% callback function for updating slice ()
    function Callback_sliceSlider(hObject,eventdata)
        slicetmp = slice;
        slice = round(get(hObject,'Value'));
        deActivateSliders();
        paramstmp = params(slicetmp,:);
        params(slice,:) = paramstmp;
        vol2D = readvol(dm_SM,VT,VT,interp_def,paramstmp,slice);
        
        subplot(1,2,1)
        imagesc(vol2D)
        colormap gray
        axis off
        title('Reference')
        
        vol = readvol(dm_SM,VT,VS(subj),interp_def,paramstmp,slice);
        showReg()
        activateSliders();
        clear slicetmp paramstmp;
    end

% callback function for translation in Y
    function Callback_Ya1Slider(hObject,eventdata)
        alpha(1) = (get(hObject,'Value'));
        params(slice,2) = alpha(1);
        xpar = params(slice,:);
        deActivateSliders();
        vol = readvol(dm_SM,VT,VS(subj),interp_def,xpar,slice);
        clc
        showReg()
        activateSliders();
    end

% callback function for scaling in Y
    function Callback_Ya2Slider(hObject,eventdata)
        alpha2(1) = (get(hObject,'Value'));
        params(slice,8) = 1+alpha2(1);
        xpar = params(slice,:);
        deActivateSliders();
        vol = readvol(dm_SM,VT,VS(subj),interp_def,xpar,slice);
        showReg()
        activateSliders();
    end

% callback function for translation in X
    function Callback_Xa1Slider(hObject,eventdata)
        alpha(1) = (get(hObject,'Value'));
        params(slice,1) = alpha(1);
        xpar = params(slice,:);
        deActivateSliders();
        vol = readvol(dm_SM,VT,VS(subj),interp_def,xpar,slice);
        showReg()
        activateSliders();
    end

% callback function for scaling in X
    function Callback_Xa2Slider(hObject,eventdata)
        alpha2(1) = (get(hObject,'Value'));
        params(slice,7) = 1+alpha2(1);
        xpar = params(slice,:);
        deActivateSliders();
        vol = readvol(dm_SM,VT,VS(subj),interp_def,xpar,slice);
        showReg()
        activateSliders();
    end

    function activateSliders()
        set(Ya1Slider,'Enable','on')
        set(Ya2Slider,'Enable','on')
        set(Xa1Slider,'Enable','on')
        set(Xa2Slider,'Enable','on')
        set(sliceSlider,'Enable','on')
    end

    function deActivateSliders()
        set(Ya1Text,'String',['y-pos=' num2str(alpha(1))])
        set(Ya2Text,'String',['y-scale=' num2str(alpha2(1))])
        set(Xa1Text,'String',['x-pos=' num2str(alpha(1))])
        set(Xa2Text,'String',['x-scale=' num2str(alpha2(1))])
        set(sliceText,'String',['slice=' num2str(slice)])
        set(Ya1Slider,'Enable','off')
        set(Ya2Slider,'Enable','off')
        set(Xa1Slider,'Enable','off')
        set(Xa2Slider,'Enable','off')
        set(sliceSlider,'Enable','off')
        pause(.001);
    end

    function vol = readvol(dm_SM,VT,VF,interp_def,params,zpos)
        if(exist('params','var') && ~isempty(params))
            iM = inv(acid_spm_matrix(params));
            if(exist('zpos','var'))
                M = spm_matrix([0 0 -zpos 0 0 0 1 1 1]);
                M1 = inv(M*inv(VT.mat)*iM*VF.mat);
                vol = spm_slice_vol(VF,M1,dm_SM(1:2),interp_def);
            else
                vol = zeros(dm_SM);
                for zpos=1:dm_SM(3)
                    M = spm_matrix([0 0 -zpos 0 0 0 1 1 1]);
                    M1 = inv(M*inv(VT.mat)*iM*VF.mat);
                    vol(:,:,zpos) = spm_slice_vol(VF,M1,dm_SM(1:2),interp_def);
                end
            end
        else
            if(exist('zpos','var'))
                M   = spm_matrix([0 0 -zpos 0 0 0 1 1 1]);
                M1  = inv(M*inv(VT.mat)*VF.mat);
                vol = spm_slice_vol(VF,M1,dm_SM(1:2),interp_def);
            else
                vol = zeros(dm_SM);
                for zpos=1:dm_SM(3)
                    M = spm_matrix([0 0 -zpos 0 0 0 1 1 1]);
                    M1 = inv(M*inv(VT.mat)*VF.mat);
                    vol(:,:,zpos) = spm_slice_vol(VF,M1,dm_SM(1:2),interp_def);
                end
            end
        end
        if(any(vol(:)>0))
            [y,x] = hist(vol(find(vol>0)),size(vol,1));
            cy    = cumsum(y);
            sz    = size(vol(find(vol>0)),1);
            
            tmp = find(cy<=sz*0.9);
            if(~isempty(tmp))
                THR = x(max(tmp));
            else
                THR = max(vol(:));
            end
        else
            THR = 1;
        end
        vol = vol/THR;
    end

% callback function for applying transformation
    function Callback_applytransf(hObject,eventdata)
        % apply transformation to source
        vol1 = zeros(dm_SM);
        for p=1:dm_SM(3)
            xpar = params(p,:);
            iM = inv(acid_spm_matrix(xpar));
            M = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(VT.mat)*iM*VS(subj).mat);
            tmp = spm_slice_vol(VS(subj),M,dm_SM(1:2),interp_def);
            vol1(:,:,p) = tmp;
        end
        
        Vout = VT;
        Vout.fname = VS(subj).fname;
        acidsc_write_vol(vol1,Vout,prefix); % change prefix
        
        % apply transformation to other images
        sz = size(VO,1);
        for inx = 1:sz
            vol1 = zeros(dm_SM);
            Vout = VT;
            [p,f,e] = fileparts(VO(inx).fname);
            Vout.fname = [p filesep prefix f e];
            Vout.n = VO(inx).n;
            Vout.descrip = VO(inx).descrip;
            for p = 1:dm_SM(3)
                xpar = params(p,:);
                iM   = inv(acid_spm_matrix(xpar));
                M    = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(VT.mat)*iM*VO(inx).mat);
                tmp  = spm_slice_vol(VO(inx),M,dm_SM(1:2),interp_def);
                vol1(:,:,p) = tmp;
            end
            %acidsc_write_vol(vol1,Vout,prefix,Vout.descrip);
            spm_write_vol(Vout,vol1);
        end
        
        % write mat-file
        [pth,fname,~] = spm_fileparts(VS(subj).fname);
        mfile = [pth filesep 'Mat_' fname '.mat'];
        save(mfile,'params')
    end

end