function acid_relmask_create_mask (errmaps,thr,p,prefix)

% ================================================================================
% The function creates binary reliability mask (M) by comparing the model-fit error (e) 
% with the threshold value (thr) in each voxel. Voxels with model-fit error above the
% threshold will be masked out:
%
%   M(x) = 1, if e(x)<=thr
%   M(x) = 0, if e(x)>thr
%
% Input:
%   - errmaps: model-fit error maps of a single or multiple subjects;
%                the script expects a char array with the filenames of
%                model-fit error maps in the rows;
%                use spm_select to generate this input
%
%   - thr: model-fit error threshold value for reliability masking;
%          this value can be either set to an arbitrary value or an optimal
%          value can be obtained using the script '';
%          no default value
%
%   - p: output folder
%             default: folder of input images
%
%   - prefix: prefix for the filenames of reliability masks;
%             default: 'MRel_'
%
% Output:
%   - binary mask for each subject specified in the input; these masks will
%     be saved in the folder of the input files
%
% Created: G David, May 2017
% ================================================================================

% check input
if numel(errmaps)==0
    error('No model-fit error maps specified.');
elseif numel(thr)==0
    error('No threshold value specified.');
end
if thr<=0
    error('The specified threshold has to be at least 0.');
end

for k = 1:size(errmaps,1)
    
    % output folder
    if numel(p)==0
        [folder,f,~] = fileparts(errmaps(k,:));    
    else
        [~,f,~] = fileparts(errmaps(k,:)); 
        folder = p;
    end
    fprintf('Processing subject: %s\n', folder);
    
    % load in model-fit error map
    I_err = spm_vol(errmaps(k,:));
    err = acid_read_vols(I_err,I_err,1);
        
    % create binary reliability mask
    REL_mask = double(err <= thr);
    
    % save reliability mask
    I_err.fname = [folder filesep prefix f '.nii'];
    spm_write_vol(I_err,REL_mask);
    
end

disp('Job done!')

end