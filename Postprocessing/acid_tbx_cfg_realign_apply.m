function realign_apply = acid_tbx_cfg_realign_apply

% target
realign_apply_target         = cfg_files;
realign_apply_target.tag     = 'realign_apply_target';
realign_apply_target.name    = 'Target images';
realign_apply_target.help    = {'The target image is the image to which the source image is registered. The number of slicewise transformations will be based on the dimension of this image.'};
realign_apply_target.filter  = 'image';
realign_apply_target.ufilter = '.*';
realign_apply_target.num     = [1 1];

% other images
realign_apply_sources         = cfg_files;
realign_apply_sources.tag     = 'realign_apply_sources';
realign_apply_sources.name    = 'Source images';
realign_apply_sources.help    = {'Select images to apply slicwise transformation (should be alligned with the source image).'};
realign_apply_sources.filter  = 'image';
realign_apply_sources.ufilter = '.*';
realign_apply_sources.num     = [1 Inf];

% matfile
realign_apply_msw = cfg_files;
realign_apply_msw.tag     = 'realign_apply_msw';
realign_apply_msw.name    = 'Matfile of slicewise transformations';
realign_apply_msw.help    = {'Select matfile(s) that was/were produced when pressing "apply" on manuel registration. Note that you can have more than one matfile.'};
realign_apply_msw.filter  = 'mat';
realign_apply_msw.ufilter = '.*';
realign_apply_msw.num     = [1 Inf];

% prefix
realign_apply_prefix         = cfg_entry;
realign_apply_prefix.tag     = 'realign_apply_prefix';
realign_apply_prefix.name    = 'Prefix for output';
realign_apply_prefix.help    = {'The threshold value in model-fit error above which voxels are rejected.'};
realign_apply_prefix.strtype = 's';
realign_apply_prefix.val     = {'r'};

% exbranch
realign_apply         = cfg_exbranch;
realign_apply.tag     = 'realign_apply';
realign_apply.name    = 'Apply slicewise registration';
realign_apply.val     = {realign_apply_target realign_apply_sources realign_apply_msw realign_apply_prefix};
realign_apply.help    = {
                    ''
};
realign_apply.prog    = @local_apply_realign_slices;
realign_apply.vout    = @vout_apply_realign_slices;

end

function out = local_apply_realign_slices(job)
    acid_apply_realign_slices(char(job.realign_apply_target),char(job.realign_apply_sources),char(job.realign_apply_msw),char(job.realign_apply_prefix)) 
    out.sources = acid_spm_file(char(job.realign_apply_sources),'prefix',char(job.realign_apply_prefix),'format','.nii');
    out.sources = acid_select(out.sources(:));
    out.sources = out.sources(:);
end

function dep = vout_apply_realign_slices(~)
    dep(1)            = cfg_dep;
    dep(1).sname      = 'Resliced images';
    dep(1).src_output = substruct('.','sources');
    dep(1).tgt_spec   = cfg_findspec({{'filter','image','strtype','e'}});
end