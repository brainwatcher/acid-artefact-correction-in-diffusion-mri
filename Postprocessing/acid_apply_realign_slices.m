function rVO = acid_apply_realign_slices(PT,PS,PMAT,prefix)

% ========================================================================
% function ACID_applytransf_manual_multitimes(PG, PF,params)
%
% Input:
%   PT     - string containing path and name of target image
%   PS     - string containing path and name of other images to apply slicewise
%            affine transformation
%   PMAT   - array containing transformation parameters for each slide
%   prefix - prefix for output
%
% Output:
%   rVO    - string contianing path and name of resliced images
% 
% written by
% S.Mohammadi 12/02/2016
% ========================================================================

% interpolation kernel
interp_def = -4;

for i = 1:size(PMAT,1)
    tmp = deblank(PMAT(i,:));
    load(tmp)
    paramsslice{i} = params;
end

% load in target and source images
VT = spm_vol(PT);
VS = spm_vol(PS);
dm = VT.dim;

% transform parameter file
paramout = zeros(size(params));
for z = 1:dm(3)
    mat = eye(4);
    for i = 1:numel(paramsslice)
        paramtmp = paramsslice{i};
        mat = spm_matrix(paramtmp(z,:))*mat;
    end
    paramout(z,:) = spm_imatrix(mat);
end

% apply transformation to source images
sz = size(VS,1);        
for inx = 1:sz
    vol1 = zeros(dm);            
    Vout = VT;
    Vout.fname = VS(inx).fname;
    for p=1:dm(3)
        xpar    = paramout(p,:);
        iM      = inv(spm_matrix_mod(xpar));
        M       = inv(spm_matrix([0 0 -p 0 0 0 1 1 1])*inv(VT.mat)*iM*VS(inx).mat);
        tmp     = spm_slice_vol(VS(inx),M,dm(1:2),interp_def);
        vol1(:,:,p) = tmp;
    end
        rVO(inx) = my_write_vol_nii(vol1,Vout,prefix);
end
end
